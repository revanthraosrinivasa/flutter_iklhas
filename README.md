# IKHLAS Customer App
![IKHLAS logo](https://images.contentstack.io/v3/assets/blt2a130c768c36b9df/blt3efcb3afcad0de6f/5f7a94fad4fbb50ef3075d03/ikhlas-greenish.svg)

This is a IKHLAS customer app was made with Flutter.

## Running the project
### Pre-requisites
| Technology | Minimum Version | Installation Guide
| ------ | ------ | ------ |
| Flutter | v1.22.5 | [Flutter Official Docs](https://flutter.dev/docs/get-started/install) |
| Dart | v2.9.2| Installed automatically with Flutter |

> Flutter relies on a full installation of Android Studio to supply its Android platform dependencies. However, you can write your Flutter apps in a number of editors.


[Flutter Official Docs](https://flutter.dev/docs/get-started/install) will guide you through configuring your preferred IDE or Code Editor. For this project, we recommends [Android Studio](https://developer.android.com/studio).

### Step-by-step
Once you have installed all the pre-requisites, please follow the steps below to run the frontend mobile application. You will need to issue a number of commands via your system's Terminal window.

 1. **Clone the current repository to your machine**
 2. **Open Android Studio/your IDE**
 3. **Open the Project using your IDE**
 4. **Installing all project dependencies**
			- *Installing dependencies declared in `pubspec.yaml`*<br>
     ```flutter packages get```

 5. **Run the app**

## Upload to Playstore and Appstore
### 1. Playstore (Android)
1. Set the version code and version name at **pubspec.yaml** file. Update the following line: `version: 1.0.0+1`. The version number is three numbers separated by dots, such as `1.0.0` in the example, followed by an optional build number such as `1` in the example above, separated by a `+`.
2. Create the app bundle run `flutter build appbundle` from the terminal. The release build will be placed at **[project]/build/app/outputs/bundle/release/app.aab.**
3. Upload the build to Playstore.


### 2. Appstore (ios)

 1. On android studio, open the **ios** folder in xCode. ![ios module on xcode](https://i.stack.imgur.com/UCEEN.png)
 2. Select the **Runner** project in the Xcode project navigator. Then, in the main view sidebar, select the **Runner** target. Select the  **General**  tab. Set the version code and build number on **General** tab. ![General tab](https://flutter.dev/images/releaseguide/xcode_settings.png)
 3. If you have set the certificate, go to next step. If you havent, please go to **Signing & Capabilities** and select **Automatically manage signing** and Select the team associated with your registered Apple Developer account. If required, select **Add Account…**, then update this setting.
 4. Open terminal in Android Studio, create the build archive by running this command `flutter build ipa`
 5. Open `build/ios/archive/MyApp.xcarchive`
 6. Click the  **Validate App**  button. If any issues are reported, address them and produce another build. You can reuse the same build ID until you upload an archive.
 7. After the archive has been successfully validated, click **Distribute App**. You can follow the status of your build in the Activities tab of your app’s details page on [App Store Connect](https://appstoreconnect.apple.com/).

You should receive an email within 30 minutes notifying you that your build has been validated and is available to release to testers on TestFlight. At this point you can choose whether to release on TestFlight, or go ahead and release your app to the App Store.

## Coding guide lines
* This project uses [ Provider](https://pub.dev/packages/provider) as state management, every single feature must follow this state managemnt.
* [SOLID principles](https://medium.com/flutter-community/s-o-l-i-d-the-first-5-principles-of-object-oriented-design-with-dart-f31d62135b7e) must not be broken.
* For project and coding structure, styles first go through this repo's codes first, your coding styles and structure has to be matched with this project's coding styles and structures.
* Any kind of anti-patterns must not be implemented or design pattern must not be broken.