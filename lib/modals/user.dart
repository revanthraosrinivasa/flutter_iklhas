// To parse this JSON data, do
//
//     final userResult = userResultFromJson(jsonString);

import 'dart:convert';

import 'package:muslim/modals/login.dart';

UserResult userResultFromJson(String str) =>
    UserResult.fromJson(json.decode(str));

String userResultToJson(UserResult data) => json.encode(data.toJson());

class UserResult {
  UserResult({
    this.errorCode,
    this.msg,
    this.developerMsg,
    this.data,
  });

  String errorCode;
  String msg;
  String developerMsg;
  User data;

  factory UserResult.fromJson(Map<String, dynamic> json) => UserResult(
        errorCode: json["errorCode"],
        msg: json["msg"],
        developerMsg: json["developerMsg"],
        data: User.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "errorCode": errorCode,
        "msg": msg,
        "developerMsg": developerMsg,
        "data": data.toJson(),
      };
}
