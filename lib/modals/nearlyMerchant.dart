// To parse this JSON data, do
//
//     final nearlyMerchants = nearlyMerchantsFromJson(jsonString);

import 'dart:convert';

NearlyMerchants nearlyMerchantsFromJson(String str) =>
    NearlyMerchants.fromJson(json.decode(str));

String nearlyMerchantsToJson(NearlyMerchants data) =>
    json.encode(data.toJson());

class NearlyMerchants {
  NearlyMerchants({
    this.errorCode,
    this.msg,
    this.developerMsg,
    this.data,
  });

  String errorCode;
  String msg;
  String developerMsg;
  List<NearlyMerchantsDatum> data;

  factory NearlyMerchants.fromJson(Map<String, dynamic> json) =>
      NearlyMerchants(
        errorCode: json["errorCode"],
        msg: json["msg"],
        developerMsg: json["developerMsg"],
        data: List<NearlyMerchantsDatum>.from(
            json["data"].map((x) => NearlyMerchantsDatum.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "errorCode": errorCode,
        "msg": msg,
        "developerMsg": developerMsg,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class NearlyMerchantsDatum {
  NearlyMerchantsDatum(
      {this.id,
      this.name,
      this.previewPhoto,
      this.distance,
      this.row,
      this.followed,
      this.type});

  String id;
  String name;
  NearlyMerchantsPreviewPhoto previewPhoto;
  double distance;
  String row;
  bool followed;
  int type;

  factory NearlyMerchantsDatum.fromJson(Map<String, dynamic> json) =>
      NearlyMerchantsDatum(
          id: json["id"],
          name: json["name"],
          previewPhoto:
              NearlyMerchantsPreviewPhoto.fromJson(json["previewPhoto"]),
          distance: json["distance"].toDouble(),
          row: json["row"],
          followed: json["followed"],
          type: json["type"]);

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "previewPhoto": previewPhoto.toJson(),
        "distance": distance,
        "row": row,
        "type": type,
        "followed": followed
      };
}

class NearlyMerchantsPreviewPhoto {
  NearlyMerchantsPreviewPhoto({
    this.id,
    this.url,
    this.active,
    this.sizes,
  });

  String id;
  String url;
  bool active;
  Sizes sizes;

  factory NearlyMerchantsPreviewPhoto.fromJson(Map<String, dynamic> json) =>
      NearlyMerchantsPreviewPhoto(
        id: json["id"],
        url: json["url"],
        active: json["active"],
        sizes: Sizes.fromJson(json["sizes"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "url": url,
        "active": active,
        "sizes": sizes.toJson(),
      };
}

class Sizes {
  Sizes({
    this.thumb,
  });

  Thumb thumb;

  factory Sizes.fromJson(Map<String, dynamic> json) => Sizes(
        thumb: Thumb.fromJson(json["thumb"]),
      );

  Map<String, dynamic> toJson() => {
        "thumb": thumb.toJson(),
      };
}

class Thumb {
  Thumb({
    this.resize,
    this.w,
    this.h,
  });

  String resize;
  String w;
  String h;

  factory Thumb.fromJson(Map<String, dynamic> json) => Thumb(
        resize: json["resize"],
        w: json["w"],
        h: json["h"],
      );

  Map<String, dynamic> toJson() => {
        "resize": resize,
        "w": w,
        "h": h,
      };
}
