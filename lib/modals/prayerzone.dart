// To parse this JSON data, do
//
//     final malaysiaPrayerZone = malaysiaPrayerZoneFromJson(jsonString);

import 'dart:convert';

MalaysiaPrayerZone malaysiaPrayerZoneFromJson(String str) =>
    MalaysiaPrayerZone.fromJson(json.decode(str));

String malaysiaPrayerZoneToJson(MalaysiaPrayerZone data) =>
    json.encode(data.toJson());

class MalaysiaPrayerZone {
  MalaysiaPrayerZone({
    this.errorCode,
    this.msg,
    this.developerMsg,
    this.data,
  });

  String errorCode;
  String msg;
  String developerMsg;
  List<MalaysiaPrayerZoneDatum> data;

  factory MalaysiaPrayerZone.fromJson(Map<String, dynamic> json) =>
      MalaysiaPrayerZone(
        errorCode: json["errorCode"],
        msg: json["msg"],
        developerMsg: json["developerMsg"],
        data: List<MalaysiaPrayerZoneDatum>.from(
            json["data"].map((x) => MalaysiaPrayerZoneDatum.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "errorCode": errorCode,
        "msg": msg,
        "developerMsg": developerMsg,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class MalaysiaPrayerZoneDatum {
  MalaysiaPrayerZoneDatum({
    this.zone,
    this.description,
  });

  String zone;
  String description;

  factory MalaysiaPrayerZoneDatum.fromJson(Map<String, dynamic> json) =>
      MalaysiaPrayerZoneDatum(
        zone: json["zone"],
        description: json["description"],
      );

  Map<String, dynamic> toJson() => {
        "zone": zone,
        "description": description,
      };
}
