// To parse this JSON data, do
//
//     final googleTimeZoneData = googleTimeZoneDataFromJson(jsonString);

import 'dart:convert';

GoogleTimeZoneData googleTimeZoneDataFromJson(String str) =>
    GoogleTimeZoneData.fromJson(json.decode(str));

String googleTimeZoneDataToJson(GoogleTimeZoneData data) =>
    json.encode(data.toJson());

class GoogleTimeZoneData {
  GoogleTimeZoneData({
    this.dstOffset,
    this.rawOffset,
    this.status,
    this.timeZoneId,
    this.timeZoneName,
  });

  int dstOffset;
  int rawOffset;
  String status;
  String timeZoneId;
  String timeZoneName;

  factory GoogleTimeZoneData.fromJson(Map<String, dynamic> json) =>
      GoogleTimeZoneData(
        dstOffset: json["dstOffset"],
        rawOffset: json["rawOffset"],
        status: json["status"],
        timeZoneId: json["timeZoneId"],
        timeZoneName: json["timeZoneName"],
      );

  Map<String, dynamic> toJson() => {
        "dstOffset": dstOffset,
        "rawOffset": rawOffset,
        "status": status,
        "timeZoneId": timeZoneId,
        "timeZoneName": timeZoneName,
      };
}
