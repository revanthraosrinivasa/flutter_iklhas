import 'dart:convert';

NormalResult normalResultFromJson(String str) =>
    NormalResult.fromJson(json.decode(str));

String normalResultToJson(NormalResult data) => json.encode(data.toJson());

class NormalResult {
  NormalResult({
    this.errorCode,
    this.msg,
    this.developerMsg,
    this.data,
  });

  String errorCode;
  String msg;
  String developerMsg;
  dynamic data;

  factory NormalResult.fromJson(Map<String, dynamic> json) => NormalResult(
        errorCode: json["errorCode"],
        msg: json["msg"],
        developerMsg: json["developerMsg"],
        data: json["data"],
      );

  Map<String, dynamic> toJson() => {
        "errorCode": errorCode,
        "msg": msg,
        "developerMsg": developerMsg,
        "data": data,
      };
}

// class Data {
//   Data();

//   factory Data.fromJson(Map<String, dynamic> json) => Data();

//   Map<String, dynamic> toJson() => {};
// }
