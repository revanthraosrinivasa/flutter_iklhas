// To parse this JSON data, do
//
//     final prayerTimesResult = prayerTimesResultFromJson(jsonString);

import 'dart:convert';

PrayerTimesResult prayerTimesResultFromJson(String str) =>
    PrayerTimesResult.fromJson(json.decode(str));

String prayerTimesResultToJson(PrayerTimesResult data) =>
    json.encode(data.toJson());

class PrayerTimesResult {
  PrayerTimesResult({
    this.prayerTime,
    this.errorCode,
    this.msg,
    this.developerMsg,
  });

  List<PrayerTimeResult> prayerTime;
  String errorCode;
  String msg;
  String developerMsg;

  factory PrayerTimesResult.fromJson(Map<String, dynamic> json) =>
      PrayerTimesResult(
        prayerTime: List<PrayerTimeResult>.from(
            json["data"].map((x) => PrayerTimeResult.fromJson(x))),
        errorCode: json["errorCode"],
        msg: json["msg"],
        developerMsg: json["developerMsg"],
      );

  Map<String, dynamic> toJson() => {
        "data": List<dynamic>.from(prayerTime.map((x) => x.toJson())),
        "errorCode": errorCode,
        "msg": msg,
        "developerMsg": developerMsg,
      };
}

class PrayerTimeResult {
  PrayerTimeResult({
    this.hijri,
    this.date,
    this.day,
    this.imsak,
    this.fajr,
    this.syuruk,
    this.dhuhr,
    this.asr,
    this.maghrib,
    this.isha,
  });

  DateTime hijri;
  String date;
  String day;
  String imsak;
  String fajr;
  String syuruk;
  String dhuhr;
  String asr;
  String maghrib;
  String isha;

  factory PrayerTimeResult.fromJson(Map<String, dynamic> json) =>
      PrayerTimeResult(
        hijri: DateTime.parse(json["hijri"]),
        date: json["date"],
        day: json["day"],
        imsak: json["imsak"],
        fajr: json["fajr"],
        syuruk: json["syuruk"],
        dhuhr: json["dhuhr"],
        asr: json["asr"],
        maghrib: json["maghrib"],
        isha: json["isha"],
      );

  Map<String, dynamic> toJson() => {
        "hijri":
            "${hijri.year.toString().padLeft(4, '0')}-${hijri.month.toString().padLeft(2, '0')}-${hijri.day.toString().padLeft(2, '0')}",
        "date": date,
        "day": day,
        "imsak": imsak,
        "fajr": fajr,
        "syuruk": syuruk,
        "dhuhr": dhuhr,
        "asr": asr,
        "maghrib": maghrib,
        "isha": isha,
      };
}
