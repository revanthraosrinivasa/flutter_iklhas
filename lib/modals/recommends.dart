// To parse this JSON data, do
//
//     final recommends = recommendsFromJson(jsonString);

import 'dart:convert';

Recommends recommendsFromJson(String str) =>
    Recommends.fromJson(json.decode(str));

String recommendsToJson(Recommends data) => json.encode(data.toJson());

class Recommends {
  Recommends({
    this.errorCode,
    this.msg,
    this.developerMsg,
    this.data,
  });

  String errorCode;
  String msg;
  String developerMsg;
  List<Datum> data;

  factory Recommends.fromJson(Map<String, dynamic> json) => Recommends(
        errorCode: json["errorCode"],
        msg: json["msg"],
        developerMsg: json["developerMsg"],
        data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "errorCode": errorCode,
        "msg": msg,
        "developerMsg": developerMsg,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class Datum {
  Datum({
    this.id,
    this.merchantId,
    this.merchantName,
    this.content,
    this.postTime,
    this.lastModified,
    this.urls,
    this.photos,
    this.preViewPhoto,
  });

  String id;
  String merchantId;
  String merchantName;
  String content;
  DateTime postTime;
  DateTime lastModified;
  List<Url> urls;
  List<Photo> photos;
  Photo preViewPhoto;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"],
        merchantId: json["merchantId"],
        merchantName: json["merchantName"],
        content: json["content"],
        postTime: DateTime.parse(json["postTime"]),
        lastModified: DateTime.parse(json["lastModified"]),
        urls: List<Url>.from(json["urls"].map((x) => Url.fromJson(x))),
        photos: List<Photo>.from(json["photos"].map((x) => Photo.fromJson(x))),
        preViewPhoto: Photo.fromJson(json["preViewPhoto"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "merchantId": merchantId,
        "merchantName": merchantName,
        "content": content,
        "postTime": postTime.toIso8601String(),
        "lastModified": lastModified.toIso8601String(),
        "urls": List<dynamic>.from(urls.map((x) => x.toJson())),
        "photos": List<dynamic>.from(photos.map((x) => x.toJson())),
        "preViewPhoto": preViewPhoto.toJson(),
      };
}

class Photo {
  Photo({
    this.id,
    this.url,
    this.active,
    this.sizes,
  });

  String id;
  String url;
  bool active;
  Sizes sizes;

  factory Photo.fromJson(Map<String, dynamic> json) => Photo(
        id: json["id"],
        url: json["url"],
        active: json["active"] == null ? null : json["active"],
        sizes: Sizes.fromJson(json["sizes"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "url": url,
        "active": active == null ? null : active,
        "sizes": sizes.toJson(),
      };
}

class Sizes {
  Sizes({
    this.thumb,
  });

  Thumb thumb;

  factory Sizes.fromJson(Map<String, dynamic> json) => Sizes(
        thumb: Thumb.fromJson(json["thumb"]),
      );

  Map<String, dynamic> toJson() => {
        "thumb": thumb.toJson(),
      };
}

class Thumb {
  Thumb({
    this.resize,
    this.w,
    this.h,
  });

  String resize;
  String w;
  String h;

  factory Thumb.fromJson(Map<String, dynamic> json) => Thumb(
        resize: json["resize"],
        w: json["w"],
        h: json["h"],
      );

  Map<String, dynamic> toJson() => {
        "resize": resize,
        "w": w,
        "h": h,
      };
}

class Url {
  Url({
    this.url,
    this.indices,
    this.unwound,
    this.displayUrl,
    this.expandedUrl,
  });

  String url;
  List<int> indices;
  Unwound unwound;
  String displayUrl;
  String expandedUrl;

  factory Url.fromJson(Map<String, dynamic> json) => Url(
        url: json["url"],
        indices: json["indices"] == null
            ? []
            : List<int>.from(json["indices"].map((x) => x)),
        unwound: Unwound.fromJson(json["unwound"]),
        displayUrl: json["display_url"],
        expandedUrl: json["expanded_url"],
      );

  Map<String, dynamic> toJson() => {
        "url": url,
        "indices": List<dynamic>.from(indices.map((x) => x)),
        "unwound": unwound.toJson(),
        "display_url": displayUrl,
        "expanded_url": expandedUrl,
      };
}

class Unwound {
  Unwound({
    this.url,
    this.title,
    this.status,
    this.description,
  });

  String url;
  String title;
  int status;
  String description;

  factory Unwound.fromJson(Map<String, dynamic> json) => Unwound(
        url: json["url"],
        title: json["title"],
        status: json["status"],
        description: json["description"],
      );

  Map<String, dynamic> toJson() => {
        "url": url,
        "title": title,
        "status": status,
        "description": description,
      };
}
