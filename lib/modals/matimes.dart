// To parse this JSON data, do
//
//     final malaysiaPrayerTime = malaysiaPrayerTimeFromJson(jsonString);

import 'dart:convert';

MalaysiaPrayerTime malaysiaPrayerTimeFromJson(String str) =>
    MalaysiaPrayerTime.fromJson(json.decode(str));

String malaysiaPrayerTimeToJson(MalaysiaPrayerTime data) =>
    json.encode(data.toJson());

class MalaysiaPrayerTime {
  MalaysiaPrayerTime({
    this.data,
  });

  MalaysiaPrayerTimeData data;

  factory MalaysiaPrayerTime.fromJson(Map<String, dynamic> json) =>
      MalaysiaPrayerTime(
        data: MalaysiaPrayerTimeData.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "data": data.toJson(),
      };
}

class MalaysiaPrayerTimeData {
  MalaysiaPrayerTimeData({
    this.provider,
    this.code,
    this.year,
    this.month,
    this.place,
    this.attributes,
  });

  String provider;
  String code;
  int year;
  int month;
  String place;
  Attributes attributes;

  factory MalaysiaPrayerTimeData.fromJson(Map<String, dynamic> json) =>
      MalaysiaPrayerTimeData(
        provider: json["provider"],
        code: json["code"],
        year: json["year"],
        month: json["month"],
        place: json["place"],
        attributes: Attributes.fromJson(json["attributes"]),
      );

  Map<String, dynamic> toJson() => {
        "provider": provider,
        "code": code,
        "year": year,
        "month": month,
        "place": place,
        "attributes": attributes.toJson(),
      };
}

class Attributes {
  Attributes({
    this.jakimCode,
    this.jakimSource,
  });

  String jakimCode;
  String jakimSource;

  factory Attributes.fromJson(Map<String, dynamic> json) => Attributes(
        jakimCode: json["jakim_code"],
        jakimSource: json["jakim_source"],
      );

  Map<String, dynamic> toJson() => {
        "jakim_code": jakimCode,
        "jakim_source": jakimSource,
      };
}
