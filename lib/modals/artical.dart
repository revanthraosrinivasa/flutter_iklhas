// To parse this JSON data, do
//
//     final artical = articalFromJson(jsonString);

import 'dart:convert';

Artical articalFromJson(String str) => Artical.fromJson(json.decode(str));

String articalToJson(Artical data) => json.encode(data.toJson());

class Artical {
  Artical({
    this.data,
  });

  Data data;

  factory Artical.fromJson(Map<String, dynamic> json) => Artical(
        data: Data.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "data": data.toJson(),
      };
}

class Data {
  Data({
    this.allBlogPosts,
  });

  AllBlogPosts allBlogPosts;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        allBlogPosts: AllBlogPosts.fromJson(json["all_blog_posts"]),
      );

  Map<String, dynamic> toJson() => {
        "all_blog_posts": allBlogPosts.toJson(),
      };
}

class AllBlogPosts {
  AllBlogPosts({
    this.items,
  });

  List<Item> items;

  factory AllBlogPosts.fromJson(Map<String, dynamic> json) => AllBlogPosts(
        items: List<Item>.from(json["items"].map((x) => Item.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "items": List<dynamic>.from(items.map((x) => x.toJson())),
      };
}

class Item {
  Item({
    this.title,
    this.url,
    this.system,
    this.artworkConnection,
  });

  String title;
  String url;
  System system;
  ArtworkConnection artworkConnection;

  factory Item.fromJson(Map<String, dynamic> json) => Item(
        title: json["title"],
        url: json["url"],
        system: System.fromJson(json["system"]),
        artworkConnection:
            ArtworkConnection.fromJson(json["artworkConnection"]),
      );

  Map<String, dynamic> toJson() => {
        "title": title,
        "url": url,
        "system": system.toJson(),
        "artworkConnection": artworkConnection.toJson(),
      };
}

class ArtworkConnection {
  ArtworkConnection({
    this.edges,
  });

  List<Edge> edges;

  factory ArtworkConnection.fromJson(Map<String, dynamic> json) =>
      ArtworkConnection(
        edges: List<Edge>.from(json["edges"].map((x) => Edge.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "edges": List<dynamic>.from(edges.map((x) => x.toJson())),
      };
}

class Edge {
  Edge({
    this.node,
  });

  Node node;

  factory Edge.fromJson(Map<String, dynamic> json) => Edge(
        node: Node.fromJson(json["node"]),
      );

  Map<String, dynamic> toJson() => {
        "node": node.toJson(),
      };
}

class Node {
  Node({
    this.url,
  });

  String url;

  factory Node.fromJson(Map<String, dynamic> json) => Node(
        url: json["url"],
      );

  Map<String, dynamic> toJson() => {
        "url": url,
      };
}

class System {
  System({
    this.tags,
    this.updatedAt,
  });

  List<String> tags;
  DateTime updatedAt;

  factory System.fromJson(Map<String, dynamic> json) => System(
        tags: List<String>.from(json["tags"].map((x) => x)),
        updatedAt: DateTime.parse(json["updated_at"]),
      );

  Map<String, dynamic> toJson() => {
        "tags": List<dynamic>.from(tags.map((x) => x)),
        "updated_at": updatedAt.toIso8601String(),
      };
}
