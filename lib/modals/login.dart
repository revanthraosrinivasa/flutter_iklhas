// To parse this JSON data, do
//
//     final loginResult = loginResultFromJson(jsonString);

import 'dart:convert';

LoginResult loginResultFromJson(String str) =>
    LoginResult.fromJson(json.decode(str));

String loginResultToJson(LoginResult data) => json.encode(data.toJson());

class LoginResult {
  LoginResult({
    this.errorCode,
    this.msg,
    this.developerMsg,
    this.data,
  });

  String errorCode;
  String msg;
  String developerMsg;
  Data data;

  factory LoginResult.fromJson(Map<String, dynamic> json) => LoginResult(
        errorCode: json["errorCode"],
        msg: json["msg"],
        developerMsg: json["developerMsg"],
        data: Data.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "errorCode": errorCode,
        "msg": msg,
        "developerMsg": developerMsg,
        "data": data.toJson(),
      };
}

class Data {
  Data({
    this.firstLogin,
    this.user,
    this.token,
  });

  bool firstLogin;
  User user;
  Token token;

  factory Data.fromJson(Map<String, dynamic> json) {
    if (json.isEmpty) {
      return null;
    }
    return Data(
      firstLogin: json["firstLogin"],
      user: User.fromJson(json["user"]),
      token: Token.fromJson(json["token"]),
    );
  }

  Map<String, dynamic> toJson() => {
        "firstLogin": firstLogin,
        "user": user.toJson(),
        "token": token.toJson(),
      };
}

class Token {
  Token({
    this.accessToken,
    this.tokenType,
    this.expiry,
    this.refreshToken,
  });

  String accessToken;
  String tokenType;
  DateTime expiry;
  String refreshToken;

  factory Token.fromJson(Map<String, dynamic> json) =>
      json == null || json.isEmpty
          ? null
          : Token(
              accessToken: json["accessToken"],
              tokenType: json["tokenType"],
              expiry: DateTime.parse(json["expiry"]),
              refreshToken: json["refreshToken"],
            );

  Map<String, dynamic> toJson() => {
        "accessToken": accessToken,
        "tokenType": tokenType,
        "expiry": expiry.toIso8601String(),
        "refreshToken": refreshToken,
      };
}

// class User {
//   User({
//     this.id,
//     this.phoneNumber,
//     this.phonePrefix,
//     this.fullName,
//     this.email,
//     this.birthday,
//     this.gender,
//     this.disable,
//     this.uuid,
//     this.createdAt,
//     this.updatedAt,
//   });

//   String id;
//   String phoneNumber;
//   String phonePrefix;
//   dynamic fullName;
//   dynamic email;
//   dynamic birthday;
//   dynamic gender;
//   bool disable;
//   String uuid;
//   DateTime createdAt;
//   DateTime updatedAt;

//   factory User.fromJson(Map<String, dynamic> json) => User(
//         id: json["id"],
//         phoneNumber: json["phoneNumber"],
//         phonePrefix: json["phonePrefix"],
//         fullName: json["fullName"],
//         email: json["email"],
//         birthday: json["birthday"],
//         gender: json["gender"],
//         disable: json["disable"],
//         uuid: json["uuid"],
//         createdAt: DateTime.parse(json["createdAt"]),
//         updatedAt: DateTime.parse(json["updatedAt"]),
//       );

//   Map<String, dynamic> toJson() => {
//         "id": id,
//         "phoneNumber": phoneNumber,
//         "phonePrefix": phonePrefix,
//         "fullName": fullName,
//         "email": email,
//         "birthday": birthday,
//         "gender": gender,
//         "disable": disable,
//         "uuid": uuid,
//         "createdAt": createdAt.toIso8601String(),
//         "updatedAt": updatedAt.toIso8601String(),
//       };
// }

class User {
  User(
      {this.id,
      this.phoneNumber,
      this.phonePrefix,
      this.fullName,
      this.nickName,
      this.avatarId,
      this.email,
      this.birthday,
      this.gender,
      this.disable,
      this.uuid,
      this.lastestLoginAt,
      this.createdAt,
      this.updatedAt,
      this.hasPwd});

  String id;
  String phoneNumber;
  String phonePrefix;
  String fullName;
  String nickName;
  String avatarId;
  String email;
  String birthday;
  String gender;
  bool disable;
  String uuid;
  DateTime lastestLoginAt;
  DateTime createdAt;
  DateTime updatedAt;
  bool hasPwd;

  factory User.fromJson(Map<String, dynamic> json) => User(
      id: json["id"],
      phoneNumber: json["phoneNumber"],
      phonePrefix: json["phonePrefix"],
      fullName: json["fullName"],
      nickName: json["nickName"],
      avatarId: json["avatarId"],
      email: json["email"],
      birthday: json["birthday"],
      gender: json["gender"],
      disable: json["disable"],
      uuid: json["uuid"],
      lastestLoginAt: DateTime.parse(json["lastestLoginAt"]),
      createdAt: DateTime.parse(json["createdAt"]),
      updatedAt: DateTime.parse(json["updatedAt"]),
      hasPwd: json["hasPwd"]);

  Map<String, dynamic> toJson() => {
        "id": id,
        "phoneNumber": phoneNumber,
        "phonePrefix": phonePrefix,
        "fullName": fullName,
        "nickName": nickName,
        "avatarId": avatarId,
        "email": email,
        "birthday": birthday,
        "gender": gender,
        "disable": disable,
        "uuid": uuid,
        "lastestLoginAt": lastestLoginAt.toIso8601String(),
        "createdAt": createdAt.toIso8601String(),
        "updatedAt": updatedAt.toIso8601String(),
        "hasPwd": hasPwd
      };
}
