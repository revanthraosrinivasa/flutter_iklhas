// To parse this JSON data, do
//
//     final versionResult = versionResultFromJson(jsonString);

import 'dart:convert';

VersionResult versionResultFromJson(String str) =>
    VersionResult.fromJson(json.decode(str));

String versionResultToJson(VersionResult data) => json.encode(data.toJson());

class VersionResult {
  VersionResult({
    this.errorCode,
    this.developMsg,
    this.msg,
    this.data,
  });

  String errorCode;
  String developMsg;
  String msg;
  VersionResultData data;

  factory VersionResult.fromJson(Map<String, dynamic> json) => VersionResult(
        errorCode: json["code"],
        developMsg: json["developMsg"],
        msg: json["msg"],
        data: VersionResultData.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "errorCode": errorCode,
        "developMsg": developMsg,
        "msg": msg,
        "data": data.toJson(),
      };
}

class VersionResultData {
  VersionResultData({
    this.appId,
    this.version,
    this.minVersion,
    this.apkUrl,
    this.updateDesc,
    this.apkSize,
    this.md5,
    this.isUpdate,
    this.isForceUpdate,
  });

  String appId;
  String version;
  String minVersion;
  String apkUrl;
  String updateDesc;
  String apkSize;
  String md5;
  bool isUpdate;
  bool isForceUpdate;

  factory VersionResultData.fromJson(Map<String, dynamic> json) =>
      VersionResultData(
        appId: json["app_id"],
        version: json["version"],
        minVersion: json["min_version"],
        apkUrl: json["apk_url"],
        updateDesc: json["update_desc"],
        apkSize: json["apk_size"],
        md5: json["md5"],
        isUpdate: json["is_update"],
        isForceUpdate: json["is_force_update"],
      );

  Map<String, dynamic> toJson() => {
        "app_id": appId,
        "version": version,
        "min_version": minVersion,
        "apk_url": apkUrl,
        "update_desc": updateDesc,
        "apk_size": apkSize,
        "md5": md5,
        "is_update": isUpdate,
        "is_force_update": isForceUpdate,
      };
}
