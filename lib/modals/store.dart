import 'dart:convert';

Store storeFromJson(String str) => Store.fromJson(json.decode(str));

String storeToJson(Store data) => json.encode(data.toJson());

class Store {
  Store({
    this.name,
    this.address,
    this.time,
    this.details,
    this.images,
    this.phone,
    this.email,
    this.facebook,
    this.instagram,
  });

  String name;
  String address;
  String time;
  String details;
  List<dynamic> images;
  String phone;
  String email;
  String facebook;
  String instagram;

  factory Store.fromJson(Map<String, dynamic> json) => Store(
        name: json["name"],
        address: json["address"],
        time: json["time"],
        details: json["details"],
        images: List<dynamic>.from(json["images"].map((x) => x)),
        phone: json["phone"],
        email: json["email"],
        facebook: json["facebook"],
        instagram: json["instagram"],
      );

  Map<String, dynamic> toJson() => {
        "name": name,
        "address": address,
        "time": time,
        "details": details,
        "images": List<dynamic>.from(images.map((x) => x)),
        "phone": phone,
        "email": email,
        "facebook": facebook,
        "instagram": instagram,
      };
}
