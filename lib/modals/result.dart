import 'dart:convert';

CommonResult userResultFromJson(String str) =>
    CommonResult.fromJson(json.decode(str));

String userResultToJson(CommonResult data) => json.encode(data.toJson());

class CommonResult {
  CommonResult({
    this.errorCode,
    this.msg,
    this.developerMsg,
  });

  String errorCode;
  String msg;
  String developerMsg;

  factory CommonResult.fromJson(Map<String, dynamic> json) => CommonResult(
        errorCode: json["errorCode"],
        msg: json["msg"],
        developerMsg: json["developerMsg"],
      );

  Map<String, dynamic> toJson() => {
        "errorCode": errorCode,
        "msg": msg,
        "developerMsg": developerMsg,
      };
}
