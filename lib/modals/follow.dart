// To parse this JSON data, do
//
//     final followMerchants = followMerchantsFromJson(jsonString);

import 'dart:convert';

FollowMerchants followMerchantsFromJson(String str) =>
    FollowMerchants.fromJson(json.decode(str));

String followMerchantsToJson(FollowMerchants data) =>
    json.encode(data.toJson());

class FollowMerchants {
  FollowMerchants({
    this.errorCode,
    this.msg,
    this.developerMsg,
    this.data,
  });

  String errorCode;
  String msg;
  String developerMsg;
  List<FollowMerchantsDatum> data;

  factory FollowMerchants.fromJson(Map<String, dynamic> json) =>
      FollowMerchants(
        errorCode: json["errorCode"],
        msg: json["msg"],
        developerMsg: json["developerMsg"],
        data: List<FollowMerchantsDatum>.from(
            json["data"].map((x) => FollowMerchantsDatum.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "errorCode": errorCode,
        "msg": msg,
        "developerMsg": developerMsg,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class FollowMerchantsDatum {
  FollowMerchantsDatum(
      {this.id, this.name, this.previewPhoto, this.type, this.state});

  String id;
  String name;
  PreviewPhoto previewPhoto;
  int type;
  int state;

  factory FollowMerchantsDatum.fromJson(Map<String, dynamic> json) =>
      FollowMerchantsDatum(
        id: json["id"],
        name: json["name"],
        previewPhoto: json["previewPhoto"] == null
            ? null
            : PreviewPhoto.fromJson(json["previewPhoto"]),
        type: json["type"],
        state: json["state"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "previewPhoto": previewPhoto.toJson(),
        "type": type,
        "state": state
      };
}

class PreviewPhoto {
  PreviewPhoto({
    this.id,
    this.url,
    this.sizes,
  });

  String id;
  String url;
  Sizes sizes;

  factory PreviewPhoto.fromJson(Map<String, dynamic> json) => PreviewPhoto(
        id: json["id"],
        url: json["url"],
        sizes: Sizes.fromJson(json["sizes"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "url": url,
        "sizes": sizes.toJson(),
      };
}

class Sizes {
  Sizes({
    this.thumb,
  });

  Thumb thumb;

  factory Sizes.fromJson(Map<String, dynamic> json) => Sizes(
        thumb: Thumb.fromJson(json["thumb"]),
      );

  Map<String, dynamic> toJson() => {
        "thumb": thumb.toJson(),
      };
}

class Thumb {
  Thumb({
    this.resize,
    this.w,
    this.h,
  });

  String resize;
  String w;
  String h;

  factory Thumb.fromJson(Map<String, dynamic> json) => Thumb(
        resize: json["resize"],
        w: json["w"],
        h: json["h"],
      );

  Map<String, dynamic> toJson() => {
        "resize": resize,
        "w": w,
        "h": h,
      };
}
