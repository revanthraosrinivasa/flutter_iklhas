import 'dart:convert';

Autocomplete autocompleteFromJson(String str) =>
    Autocomplete.fromJson(json.decode(str));

String autocompleteToJson(Autocomplete data) => json.encode(data.toJson());

class Autocomplete {
  Autocomplete({
    this.predictions,
    this.status,
  });

  List<Prediction> predictions;
  String status;

  factory Autocomplete.fromJson(Map<String, dynamic> json) => Autocomplete(
        predictions: List<Prediction>.from(
            json["predictions"].map((x) => Prediction.fromJson(x))),
        status: json["status"],
      );

  Map<String, dynamic> toJson() => {
        "predictions": List<dynamic>.from(predictions.map((x) => x.toJson())),
        "status": status,
      };
}

class Prediction {
  Prediction({
    this.description,
    this.placeId,
    this.reference,
    this.terms,
  });

  String description;
  String placeId;
  String reference;
  List<Term> terms;

  factory Prediction.fromJson(Map<String, dynamic> json) => Prediction(
        description: json["description"],
        placeId: json["place_id"],
        reference: json["reference"],
        terms: List<Term>.from(json["terms"].map((x) => Term.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "description": description,
        "place_id": placeId,
        "reference": reference,
        "terms": List<dynamic>.from(terms.map((x) => x.toJson())),
      };
}

class Term {
  Term({
    this.offset,
    this.value,
  });

  int offset;
  String value;

  factory Term.fromJson(Map<String, dynamic> json) => Term(
        offset: json["offset"],
        value: json["value"],
      );

  Map<String, dynamic> toJson() => {
        "offset": offset,
        "value": value,
      };
}
