// To parse this JSON data, do
//
//     final merchantDetail = merchantDetailFromJson(jsonString);

import 'dart:convert';

MerchantDetail merchantDetailFromJson(String str) =>
    MerchantDetail.fromJson(json.decode(str));

String merchantDetailToJson(MerchantDetail data) => json.encode(data.toJson());

class MerchantDetail {
  MerchantDetail({
    this.errorCode,
    this.msg,
    this.developerMsg,
    this.data,
  });

  String errorCode;
  String msg;
  String developerMsg;
  MerchantDetailData data;

  factory MerchantDetail.fromJson(Map<String, dynamic> json) => MerchantDetail(
        errorCode: json["errorCode"],
        msg: json["msg"],
        developerMsg: json["developerMsg"],
        data: MerchantDetailData.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "errorCode": errorCode,
        "msg": msg,
        "developerMsg": developerMsg,
        "data": data.toJson(),
      };
}

class MerchantDetailData {
  MerchantDetailData(
      {this.id,
      this.name,
      this.email,
      this.phoneCode,
      this.phone,
      this.code,
      this.longitude,
      this.latitude,
      this.location,
      this.faceBookUrl,
      this.instagramUrl,
      this.describe,
      this.service,
      this.openHour,
      this.website,
      this.merchantPhotos,
      this.previewPhoto,
      this.fansCount,
      this.followed,
      this.type,
      this.isSub});
  int type;
  String id;
  String name;
  String email;
  String phoneCode;
  String phone;
  String code;
  double longitude;
  double latitude;
  String location;
  dynamic faceBookUrl;
  dynamic instagramUrl;
  dynamic describe;
  List<DetailService> service;
  List<OpenHour> openHour;
  dynamic website;
  MerchantPhotos merchantPhotos;
  Photo previewPhoto;
  int fansCount;
  bool followed;
  bool isSub;

  factory MerchantDetailData.fromJson(Map<String, dynamic> json) =>
      MerchantDetailData(
          id: json["id"],
          name: json["name"],
          email: json["email"],
          phoneCode: json["phoneCode"],
          phone: json["phone"],
          code: json["code"],
          longitude: json["longitude"].toDouble(),
          latitude: json["latitude"].toDouble(),
          location: json["location"],
          faceBookUrl: json["faceBookUrl"],
          instagramUrl: json["instagramUrl"],
          describe: json["describe"],
          service: json["service"] == null
              ? []
              : List<DetailService>.from(
                  json["service"].map((x) => DetailService.fromJson(x))),
          openHour: List<OpenHour>.from(
              json["openHour"].map((x) => OpenHour.fromJson(x))),
          website: json["website"],
          merchantPhotos: MerchantPhotos.fromJson(json["merchantPhotos"]),
          previewPhoto: Photo.fromJson(json["previewPhoto"]),
          fansCount: json["fansCount"],
          followed: json["followed"],
          isSub: json["isSub"],
          type: json["type"]);

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "email": email,
        "phoneCode": phoneCode,
        "phone": phone,
        "code": code,
        "longitude": longitude,
        "latitude": latitude,
        "location": location,
        "faceBookUrl": faceBookUrl,
        "instagramUrl": instagramUrl,
        "describe": describe,
        "service": List<dynamic>.from(service.map((x) => x.toJson())),
        "openHour": List<dynamic>.from(openHour.map((x) => x.toJson())),
        "website": website,
        "merchantPhotos": merchantPhotos.toJson(),
        "previewPhoto": previewPhoto.toJson(),
        "fansCount": fansCount,
        "followed": followed,
        "type": type,
        "isSub": isSub
      };
}

class MerchantPhotos {
  MerchantPhotos({
    this.photos,
  });

  List<Photo> photos;

  factory MerchantPhotos.fromJson(Map<String, dynamic> json) => MerchantPhotos(
        photos: json["photos"] == null
            ? []
            : List<Photo>.from(json["photos"].map((x) => Photo.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "photos": List<dynamic>.from(photos.map((x) => x.toJson())),
      };
}

class Photo {
  Photo({
    this.id,
    this.url,
    this.sizes,
  });

  String id;
  String url;
  Sizes sizes;

  factory Photo.fromJson(Map<String, dynamic> json) => Photo(
        id: json["id"],
        url: json["url"],
        sizes: Sizes.fromJson(json["sizes"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "url": url,
        "sizes": sizes.toJson(),
      };
}

class Sizes {
  Sizes({
    this.thumb,
  });

  Thumb thumb;

  factory Sizes.fromJson(Map<String, dynamic> json) => Sizes(
        thumb: Thumb.fromJson(json["thumb"]),
      );

  Map<String, dynamic> toJson() => {
        "thumb": thumb.toJson(),
      };
}

class Thumb {
  Thumb({
    this.resize,
    this.w,
    this.h,
  });

  String resize;
  String w;
  String h;

  factory Thumb.fromJson(Map<String, dynamic> json) => Thumb(
        resize: json["resize"],
        w: json["w"],
        h: json["h"],
      );

  Map<String, dynamic> toJson() => {
        "resize": resize,
        "w": w,
        "h": h,
      };
}

class OpenHour {
  OpenHour({
    this.state,
    this.time,
  });

  bool state;
  List<dynamic> time;

  factory OpenHour.fromJson(Map<String, dynamic> json) => OpenHour(
        state: json["state"],
        time: json["time"] ?? [],
      );

  Map<String, dynamic> toJson() => {
        "state": state,
        "time": time,
      };
}

class DetailService {
  DetailService(
      {this.name, this.en, this.my, this.state, this.contact, this.phone});

  String name;
  String contact;
  String phone;
  String en;
  String my;
  bool state;

  factory DetailService.fromJson(Map<String, dynamic> json) => DetailService(
      name: json["name"],
      contact: json["contact"],
      phone: json["phone"],
      en: json["en"],
      my: json["my"],
      state: json["state"]);

  Map<String, dynamic> toJson() => {
        "name": name,
        "contact": contact,
        "phone": phone,
        "en": en,
        "my": my,
        "state": state
      };
}
