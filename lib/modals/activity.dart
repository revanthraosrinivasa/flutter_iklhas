// To parse this JSON data, do
//
//     final activities = activitiesFromJson(jsonString);

import 'dart:convert';

import 'package:muslim/modals/recommends.dart';

Activities activitiesFromJson(String str) =>
    Activities.fromJson(json.decode(str));

String activitiesToJson(Activities data) => json.encode(data.toJson());

class Activities {
  Activities({
    this.errorCode,
    this.msg,
    this.developerMsg,
    this.data,
  });

  String errorCode;
  String msg;
  String developerMsg;
  List<ActivitiesDatum> data;

  factory Activities.fromJson(Map<String, dynamic> json) => Activities(
        errorCode: json["errorCode"],
        msg: json["msg"],
        developerMsg: json["developerMsg"],
        data: List<ActivitiesDatum>.from(
            json["data"].map((x) => ActivitiesDatum.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "errorCode": errorCode,
        "msg": msg,
        "developerMsg": developerMsg,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class ActivitiesDatum {
  ActivitiesDatum({
    this.id,
    this.merchantId,
    this.merchantName,
    this.activityName,
    this.hasTarget,
    this.targetAmount,
    this.unit,
    this.introduction,
    this.link,
    this.onDate,
    this.urls,
    this.preViewPhoto,
    this.photos,
    this.onId,
  });

  String id;
  String merchantId;
  String merchantName;
  String activityName;
  bool hasTarget;
  int targetAmount;
  dynamic unit;
  String introduction;
  String link;
  DateTime onDate;
  List<Url> urls;
  Photo preViewPhoto;
  List<Photo> photos;
  String onId;

  factory ActivitiesDatum.fromJson(
          Map<String, dynamic> json) =>
      ActivitiesDatum(
          id: json["id"],
          merchantId: json["merchantId"],
          merchantName: json["merchantName"],
          activityName: json["activityName"],
          hasTarget: json["hasTarget"],
          targetAmount: json["targetAmount"],
          unit: json["unit"],
          introduction: json["introduction"],
          link: json["link"],
          onDate: DateTime.parse(json["onDate"]),
          urls: List<Url>.from(json["urls"].map((x) => Url.fromJson(x))),
          preViewPhoto: Photo.fromJson(json["preViewPhoto"]),
          photos:
              List<Photo>.from(json["photos"].map((x) => Photo.fromJson(x))),
          onId: json["onId"]);

  Map<String, dynamic> toJson() => {
        "id": id,
        "merchantId": merchantId,
        "merchantName": merchantName,
        "activityName": activityName,
        "hasTarget": hasTarget,
        "targetAmount": targetAmount,
        "unit": unit,
        "introduction": introduction,
        "link": link,
        "onDate": onDate.toIso8601String(),
        "urls": List<dynamic>.from(urls.map((x) => x.toJson())),
        "preViewPhoto": preViewPhoto.toJson(),
        "photos": List<dynamic>.from(photos.map((x) => x.toJson())),
        "onId": onId
      };
}

class Photo {
  Photo({
    this.id,
    this.url,
    this.active,
    this.sizes,
  });

  String id;
  String url;
  bool active;
  Sizes sizes;

  factory Photo.fromJson(Map<String, dynamic> json) => Photo(
        id: json["id"],
        url: json["url"],
        active: json["active"] == null ? null : json["active"],
        sizes: Sizes.fromJson(json["sizes"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "url": url,
        "active": active == null ? null : active,
        "sizes": sizes.toJson(),
      };
}

class Sizes {
  Sizes({
    this.thumb,
  });

  Thumb thumb;

  factory Sizes.fromJson(Map<String, dynamic> json) => Sizes(
        thumb: Thumb.fromJson(json["thumb"]),
      );

  Map<String, dynamic> toJson() => {
        "thumb": thumb.toJson(),
      };
}

class Thumb {
  Thumb({
    this.resize,
    this.w,
    this.h,
  });

  String resize;
  String w;
  String h;

  factory Thumb.fromJson(Map<String, dynamic> json) => Thumb(
        resize: json["resize"],
        w: json["w"],
        h: json["h"],
      );

  Map<String, dynamic> toJson() => {
        "resize": resize,
        "w": w,
        "h": h,
      };
}

// class Url {
//   Url({
//     this.url,
//     this.indices,
//     this.unwound,
//     this.displayUrl,
//     this.expandedUrl,
//   });

//   String url;
//   List<int> indices;
//   Unwound unwound;
//   String displayUrl;
//   String expandedUrl;

//   factory Url.fromJson(Map<String, dynamic> json) => Url(
//         url: json["url"],
//         indices: List<int>.from(json["indices"].map((x) => x)),
//         unwound: Unwound.fromJson(json["unwound"]),
//         displayUrl: json["display_url"],
//         expandedUrl: json["expanded_url"],
//       );

//   Map<String, dynamic> toJson() => {
//         "url": url,
//         "indices": List<dynamic>.from(indices.map((x) => x)),
//         "unwound": unwound.toJson(),
//         "display_url": displayUrl,
//         "expanded_url": expandedUrl,
//       };
// }

class Unwound {
  Unwound({
    this.url,
    this.title,
    this.status,
    this.description,
  });

  String url;
  String title;
  int status;
  String description;

  factory Unwound.fromJson(Map<String, dynamic> json) => Unwound(
        url: json["url"],
        title: json["title"],
        status: json["status"],
        description: json["description"],
      );

  Map<String, dynamic> toJson() => {
        "url": url,
        "title": title,
        "status": status,
        "description": description,
      };
}
