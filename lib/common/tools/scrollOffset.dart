import 'package:flutter/material.dart';

Offset getPosition(GlobalKey itemkey) {
  RenderBox currentBox = itemkey.currentContext.findRenderObject();
  Offset currentPosition = currentBox.localToGlobal(Offset.zero);
  return currentPosition;
}

Size getSize(GlobalKey itemkey) {
  RenderBox renderBox = itemkey.currentContext.findRenderObject();
  final sizeRed = renderBox.size;
  return sizeRed;
}
