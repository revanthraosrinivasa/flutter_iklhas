import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_picker/Picker.dart';
import 'package:muslim/common/tools/hexColor.dart';

void showPickerWithContext(BuildContext context, PickerAdapter adapter,
    String title, PickerConfirmCallback onConfirm) {
  new Picker(
    // height: 280.0,
    itemExtent: 44.0,
    adapter: adapter,
    title: Text(title),
    selectedTextStyle: TextStyle(color: HexColor("67C1BF"), fontSize: 14.0),
    onConfirm: onConfirm,
  ).showModal(context);
}
