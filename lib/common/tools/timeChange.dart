import 'package:easy_localization/easy_localization.dart';
import 'package:muslim/common/helper/logger.dart';
import 'package:timezone/data/latest.dart' as tz;
import 'package:timezone/timezone.dart' as tz;

class RelativeDateFormat {
  static final num ONE_MINUTE = 60000;
  static final num ONE_HOUR = 3600000;
  static final num ONE_DAY = 86400000;
  static final num ONE_WEEK = 604800000;

  static final String ONE_SECOND_AGO = tr("s");
  static final String ONE_MINUTE_AGO = tr("m");
  static final String ONE_HOUR_AGO = tr("h");
  static final String YEASTERDARY = tr("yesterday");
  static final String ONE_DAY_AGO = tr("days ago");
  static final String ONE_MONTH_AGO = tr("months ago");
  static final String ONE_YEAR_AGO = tr("years ago");

//时间转换
  static String format(DateTime date) {
    num delta =
        DateTime.now().millisecondsSinceEpoch - date.millisecondsSinceEpoch;

    if (delta < 1 * ONE_MINUTE) {
      num seconds = toSeconds(delta);
      return (seconds <= 0 ? 1 : seconds).toInt().toString() + ONE_SECOND_AGO;
    }
    if (delta < 45 * ONE_MINUTE) {
      num minutes = toMinutes(delta);
      return (minutes <= 0 ? 1 : minutes).toInt().toString() + ONE_MINUTE_AGO;
    }
    if (delta < 24 * ONE_HOUR) {
      num hours = toHours(delta);
      return (hours <= 0 ? 1 : hours).toInt().toString() + ONE_HOUR_AGO;
    }
    // if (delta < 48 * ONE_HOUR) {
    //   num hours = toHours(delta);
    //   num minites = toMinutes(delta);

    //   return YEASTERDARY + " ${hours - 24}: ${minites % 60}";
    // }

    // num years = toYears(delta);

    return DateFormat("MMM dd, yyyy").format(date);

    // if (delta < 30 * ONE_DAY) {
    //   num days = toDays(delta);
    //   return (days <= 0 ? 1 : days).toInt().toString() + ONE_DAY_AGO;
    // }
    // if (delta < 12 * 4 * ONE_WEEK) {
    //   num months = toMonths(delta);
    //   return (months <= 0 ? 1 : months).toInt().toString() + ONE_MONTH_AGO;
    // } else {
    //   num years = toYears(delta);
    //   return (years <= 0 ? 1 : years).toInt().toString() + ONE_YEAR_AGO;
    // }
  }

  static num toSeconds(num date) {
    return date ~/ 1000;
  }

  static num toMinutes(num date) {
    return toSeconds(date) ~/ 60;
  }

  static num toHours(num date) {
    return toMinutes(date) ~/ 60;
  }

  static num toDays(num date) {
    return toHours(date) ~/ 24;
  }

  static num toMonths(num date) {
    return toDays(date) ~/ 30;
  }

  static num toYears(num date) {
    return toMonths(date) ~/ 365;
  }
}

String timeChangeTMS(int secs) {
  var hour = secs ~/ 3600;
  var minites = secs % 3600 ~/ 60;
  var seconds = secs % 3600 % 60;

  NumberFormat nf = NumberFormat();
  nf.minimumIntegerDigits = 2;

  return "${nf.format(hour)}:${nf.format(minites)}:${nf.format(seconds)}";
}

tz.TZDateTime nextInstanceOfTenAM(int hours, int minites, String date) {
  print("------------- tz.local ---------- ${tz.local}");
  tz.TZDateTime dateTime =
      tz.TZDateTime.from(DateFormat("dd-MMM-yyyy").parse(date), tz.local);
  final tz.TZDateTime now = tz.TZDateTime.now(tz.local);
  tz.TZDateTime scheduledDate = tz.TZDateTime(
      tz.local, dateTime.year, dateTime.month, dateTime.day, hours, minites);
  if (scheduledDate.isBefore(now)) {
    return null;
  }
  // logger('nextInstanceOfTenAM $scheduledDate -------- $date');
  return scheduledDate;
}

int timeListInYear(DateTime startTime) {
  int year = startTime.year;
  int month = startTime.month;
  int day = startTime.day;
  int daies = 0;
  for (int i = 1; i < month; i++) {
    daies += everyMonthDaies(year, i);
  }
  return daies + day;
}

int everyMonthDaies(int year, int month) {
  int end = 0;
  if (month == 1) {
    end = 31;
  } else if (month == 2) {
    if ((year % 4 == 0) && (year % 100 != 0) || (year % 400 == 0)) {
      end = 29;
    } else {
      end = 28;
    }
  } else if (month == 3) {
    end = 31;
  } else if (month == 4) {
    end = 30;
  } else if (month == 5) {
    end = 31;
  } else if (month == 6) {
    end = 30;
  } else if (month == 7) {
    end = 31;
  } else if (month == 8) {
    end = 31;
  } else if (month == 9) {
    end = 30;
  } else if (month == 10) {
    end = 31;
  } else if (month == 11) {
    end = 30;
  } else if (month == 12) {
    end = 31;
  }
  return end;
}
