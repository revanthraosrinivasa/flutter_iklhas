import 'dart:async';
import 'dart:ui';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:muslim/common/core/storage.dart';
import 'package:muslim/common/provider/app.dart';
import 'package:muslim/common/tools/snackBar.dart';
import 'package:muslim/global.dart';
import "dart:math" as math;
import 'package:photo_view/photo_view.dart';
import 'package:photo_view/photo_view_gallery.dart';

import 'package:muslim/common/tools/hexColor.dart';
import 'package:provider/provider.dart';
import 'package:easy_localization/easy_localization.dart';

void showWaitingDialog(BuildContext context, String text) {
  showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) {
        return Dialog(
          child: Container(
            height: 140.0,
            width: 100.0,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                CircularProgressIndicator(),
                (text == null || text.length == 0)
                    ? Container()
                    : Container(
                        padding: EdgeInsets.only(top: 20, left: 16, right: 16),
                        child: Text(
                          text,
                          textAlign: TextAlign.center,
                        ),
                      )
              ],
            ),
          ),
        );
      });
}

void showEnsureDialog(
    BuildContext context, String title, String content, VoidCallback callBack,
    {String ensureTitle = "Confirm",
    String cancelTitle = "Cancel",
    int type = 1}) {
  List<Widget> actions = [];
  if (cancelTitle != null && cancelTitle.length != 0) {
    actions.add(CupertinoButton(
      child: Text(
        cancelTitle,
        style: TextStyle(color: HexColor("#4C4C50")),
      ),
      onPressed: () => Navigator.of(context).pop(), // 关闭对话框
    ));
  }
  if (ensureTitle != null && ensureTitle.length != 0) {
    actions.add(CupertinoButton(
      child: Text(ensureTitle, style: TextStyle(color: HexColor("67C1BF"))),
      onPressed: () async {
        if (type != 1) {
          callBack();
        } else {
          Navigator.of(context).pop();
          callBack();
        }
      },
    ));
  }
  if (Global.isIOS) {
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (context) {
          return CupertinoAlertDialog(
            title:
                title == null || title.length == 0 ? Container() : Text(title),
            content: Text(content),
            actions: actions,
          );
        });
  } else {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return AlertDialog(
            title:
                title == null || title.length == 0 ? Container() : Text(title),
            content: Text(content),
            actions: actions,
          );
        });
  }
}

void showImagesShadow(BuildContext context, List<String> urls,
    {int currentPage = 0}) {
  showDialog(
      context: context,
      barrierColor: Colors.black,
      builder: (dialogContext) {
        return MyPhotoView(
          urls: urls,
          start: currentPage,
        );
      });
}

class MyPhotoView extends StatefulWidget {
  final List<String> urls;
  final int start;
  final bool canSave;
  MyPhotoView({@required this.urls, this.start = 0, this.canSave = false});
  @override
  _MyPhotoViewState createState() => _MyPhotoViewState();
}

class _MyPhotoViewState extends State<MyPhotoView> {
  PageController pageController;
  int index;

  @override
  void initState() {
    index = widget.start;
    pageController = PageController(initialPage: index);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    AppSettingModel settingModel = Provider.of<AppSettingModel>(context);
    bool isNormal = true;
    if (settingModel.userChooseGraphicsSettngs ==
        UserChooseGraphicsSettings.auto) {
      isNormal =
          settingModel.networkState == ConnectivityResult.mobile ? true : false;
    } else {
      isNormal = settingModel.userChooseGraphicsSettngs ==
          UserChooseGraphicsSettings.low;
    }

    return Scaffold(
      backgroundColor: Colors.black,
      body: InkWell(
        onTap: () {
          Navigator.of(context).pop();
        },
        child: Column(
          children: [
            Container(
              height: 44,
              alignment: Alignment.center,
              child: Text(
                "${index + 1}/${widget.urls.length}",
                style: TextStyle(
                    color: HexColor("FFFFFF"),
                    fontSize: 16,
                    fontWeight: FontWeight.normal),
              ),
            ),
            Expanded(
              child: PhotoViewGallery.builder(
                scrollPhysics: const BouncingScrollPhysics(),
                builder: (context, index) => PhotoViewGalleryPageOptions(
                  imageProvider: CachedNetworkImageProvider(
                    widget.urls[index] +
                        (isNormal ? OSS_IMAGE_SCALE_NORMAL : ""),
                  ),
                  initialScale: PhotoViewComputedScale.contained,
                  minScale:
                      PhotoViewComputedScale.contained * (0.5 + index / 10),
                  maxScale: PhotoViewComputedScale.covered * 4.1,
                ),
                itemCount: widget.urls.length,
                loadingBuilder: (context, e) => Stack(
                  children: [
                    ConstrainedBox(
                      constraints: const BoxConstraints.expand(),
                      child: CachedNetworkImage(
                        imageUrl: widget.urls[index] + OSS_IMAGE_SCALE,
                      ),
                    ),
                    Center(
                      child: ClipRect(
                        child: BackdropFilter(
                            filter: ImageFilter.blur(sigmaX: 5.0, sigmaY: 5.0)),
                      ),
                    ),
                    Container(
                      child: Center(
                          child: Container(
                        child: Center(
                          child: SizedBox(
                            width: 50.0,
                            height: 50.0,
                            child: CircularProgressIndicator(
                              strokeWidth: 2,
                              valueColor:
                                  AlwaysStoppedAnimation(HexColor("67C1BF")),
                            ),
                          ),
                        ),
                      )),
                    ),
                  ],
                ),
                backgroundDecoration: BoxDecoration(
                  color: Colors.black,
                ),
                pageController: pageController,
                onPageChanged: (int i) {
                  setState(() {
                    index = i;
                  });
                },
                scrollDirection: Axis.horizontal,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

bool checkNetworkState(BuildContext context) {
  AppSettingModel appSettingModel =
      Provider.of<AppSettingModel>(context, listen: false);
  if (appSettingModel.networkState == ConnectivityResult.none) {
    showSnackBar(context, tr("no_network_tip"));
    return false;
  }
  return true;
}
