import 'package:flutter/material.dart';
import 'package:muslim/common/tools/hexColor.dart';
import 'package:top_snackbar_flutter/top_snack_bar.dart';
// import 'package:top_snackbar_flutter/custom_snack_bar.dart';

void showSnackBar(BuildContext context, String text, {type = 0}) {
  Widget customSnackBar = Container(
    padding: EdgeInsets.symmetric(horizontal: 16, vertical: 14),
    decoration: BoxDecoration(
        color: HexColor("E6E6E7"),
        borderRadius: BorderRadius.all(Radius.circular(20.0))),
    child: Center(
      child: Text(
        text,
        style: TextStyle(
            fontSize: 14.0,
            fontWeight: FontWeight.w400,
            color: HexColor("636363"),
            decoration: TextDecoration.none,
            fontFamily: null),
      ),
    ),
  );
  // switch (type) {
  //   // 成功
  //   case 0:
  //     customSnackBar = muslimCustomSnackBarWithTitle(
  //       message: text,
  //       icon: null,
  //       backgroundColor: HexColor("E6E6E7"),
  //       textStyle: TextStyle(color: HexColor("636363")),
  //     );
  //     break;
  //   case 1:
  //     // 失败
  //     customSnackBar = CustomSnackBar.error(
  //       message: text,
  //       icon: null,
  //       backgroundColor: HexColor("E6E6E7"),
  //       textStyle: TextStyle(color: HexColor("636363")),
  //     );
  //     break;
  //   case 2:
  //   // 默认
  //   default:
  //     customSnackBar = CustomSnackBar.info(
  //       message: text,
  //       icon: null,
  //       backgroundColor: HexColor("E6E6E7"),
  //     );
  //     break;
  // }
  
  showTopSnackBar(
    context,
    customSnackBar,
    displayDuration: Duration(seconds: 1),
    showOutAnimationDuration: Duration(milliseconds: 700),
    hideOutAnimationDuration: Duration(milliseconds: 300),
  );
}
