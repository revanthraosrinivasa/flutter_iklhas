String filterPhoneNumber(String value) {
  return value
      .split("")
      .where((element) =>
          ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"].contains(element))
      .join();
}
