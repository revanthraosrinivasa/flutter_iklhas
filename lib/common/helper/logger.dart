import 'package:logger/logger.dart';

var loggerOutput = Logger(printer: PrettyPrinter(lineLength: 500));
void logger(dynamic value) {
  loggerOutput.d(value);
}
