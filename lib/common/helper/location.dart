import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:location_permissions/location_permissions.dart';
import 'package:muslim/common/api/google_map.dart';
import 'package:muslim/common/helper/logger.dart';
import 'package:muslim/common/tools/dialog.dart';
import 'package:easy_localization/easy_localization.dart';

class LocationHelper {
  Future<Map<String, dynamic>> fetchLocation(BuildContext context) async {
    bool serviceEnabled;
    LocationPermission permission;
    try {
      serviceEnabled = await Geolocator.isLocationServiceEnabled();
    } catch (error) {
      print("isLocationServiceEnabled : $error");
    }
    if (!serviceEnabled) {
      showEnsureDialog(
          context, tr("location_tip"), tr("location_content"), () async {},
          cancelTitle: "OK", ensureTitle: "");
      return Future.error(null);
    }
    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.deniedForever ||
        permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission != LocationPermission.whileInUse &&
          permission != LocationPermission.always) {
        showEnsureDialog(context, tr("location_tip"), tr("location_content"),
            () async {
          await LocationPermissions().openAppSettings();
        }, ensureTitle: tr("open"));
        return Future.error(null);
      }
    }
    try {
      Position position = await Future.any([
        Geolocator.getCurrentPosition(),
        Future.delayed(Duration(milliseconds: 10000),
            GoogleApiService().fetchLocalLocation)
      ]);
      logger("---------- position $position");
      if (position == null) {
        return Future.error(null);
      }
      return Future.value({
        "msg": {"latitude": position.latitude, "longitude": position.longitude}
      });
    } catch (error) {
      logger("LocationHelper fetch position error ------------_$error");
      return Future.error(null);
    }
  }
}
