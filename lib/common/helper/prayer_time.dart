import 'dart:isolate';
import 'dart:async';
import 'dart:math';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:muslim/common/core/storage.dart';
import 'package:muslim/common/helper/helper.dart';
import 'package:muslim/common/helper/logger.dart';
import 'package:muslim/common/tools/hexColor.dart';
import 'package:muslim/common/tools/timeChange.dart';
import 'package:muslim/global.dart';
import 'package:muslim/modals/times.dart';
import 'package:timezone/data/latest.dart' as tz;
import 'package:timezone/timezone.dart' as tz;

class PrayerTimeViewModel {
  MuslimPrayer currentTimeStr(PrayerTimeResult value) {
    if (value == null) {
      return null;
    }
    int now = DateTime.now().millisecondsSinceEpoch;
    if (parseDateTime(value.isha).millisecondsSinceEpoch - now < 0) {
      return MuslimPrayer(6, parseDateTime(value.isha), tr("Isha"));
    }

    if (parseDateTime(value.maghrib).millisecondsSinceEpoch - now < 0) {
      return MuslimPrayer(5, parseDateTime(value.maghrib), tr("Maghrib"));
    }
    if (parseDateTime(value.asr).millisecondsSinceEpoch - now < 0) {
      return MuslimPrayer(4, parseDateTime(value.asr), tr("Asr"));
    }
    if (parseDateTime(value.dhuhr).millisecondsSinceEpoch - now < 0) {
      return MuslimPrayer(3, parseDateTime(value.dhuhr), tr("Dhuhr"));
    }
    if (parseDateTime(value.syuruk).millisecondsSinceEpoch - now < 0) {
      return MuslimPrayer(2, parseDateTime(value.syuruk), tr("Syuruk"));
    }
    if (parseDateTime(value.fajr).millisecondsSinceEpoch - now < 0) {
      return MuslimPrayer(1, parseDateTime(value.fajr), tr("Fajr"));
    }
    return MuslimPrayer(null, null, null);
  }

  MuslimPrayer nextTimeStr(PrayerTimeResult value) {
    if (value == null) {
      return null;
    }
    int now = DateTime.now().millisecondsSinceEpoch;
    if (now - parseDateTime(value.fajr).millisecondsSinceEpoch < 0) {
      return MuslimPrayer(1, parseDateTime(value.fajr), tr("Fajr"));
    }
    if (now - parseDateTime(value.syuruk).millisecondsSinceEpoch < 0) {
      return MuslimPrayer(2, parseDateTime(value.syuruk), tr("Syuruk"));
    }
    if (now - parseDateTime(value.dhuhr).millisecondsSinceEpoch < 0) {
      return MuslimPrayer(3, parseDateTime(value.dhuhr), tr("Dhuhr"));
    }
    if (now - parseDateTime(value.asr).millisecondsSinceEpoch < 0) {
      return MuslimPrayer(4, parseDateTime(value.asr), tr("Asr"));
    }
    if (now - parseDateTime(value.maghrib).millisecondsSinceEpoch < 0) {
      return MuslimPrayer(5, parseDateTime(value.maghrib), tr("Maghrib"));
    }
    if (now - parseDateTime(value.isha).millisecondsSinceEpoch < 0) {
      return MuslimPrayer(6, parseDateTime(value.isha), tr("Isha"));
    }
    return MuslimPrayer(null, null, null);
  }

  DateTime timeForName(String name, PrayerTimeResult value) {
    if (value == null) {
      return null;
    }
    if (name == tr("Fajr")) {
      return parseDateTime(value.fajr);
    }
    if (name == tr("Syuruk")) {
      return parseDateTime(value.syuruk);
    }
    if (name == tr("Dhuhr")) {
      return parseDateTime(value.dhuhr);
    }
    if (name == tr("Asr")) {
      return parseDateTime(value.asr);
    }
    if (name == tr("Maghrib")) {
      return parseDateTime(value.maghrib);
    }
    if (name == tr("Isha")) {
      return parseDateTime(value.isha);
    }
    return null;
  }

  DateTime parseDateTime(String value) {
    DateTime now = DateTime.now();
    DateTime format = DateFormat("HH:mm:ss").parse(value);
    return DateTime(now.year, now.month, now.day, format.hour, format.minute,
        format.second);
  }

  sendLocaloNitification(List<PrayerTimeResult> prayerTimes) async {
    // NotificationDetails notificationDetails;
    // if (Global.isIOS) {
    //   notificationDetails =
    //       NotificationDetails(iOS: IOSNotificationDetails(presentSound: true));
    // } else {
    //   notificationDetails = NotificationDetails(
    //       android: AndroidNotificationDetails(
    //           "channel id", "your channel name", "your channel description",
    //           icon: "app_icon_3", color: HexColor("67C1BF")));
    // }
    // await Global.flutterLocalNotificationsPlugin.zonedSchedule(2, "hello test",
    //     "Time to test", nextInstanceOfTenAM(18, 36), notificationDetails,
    //     androidAllowWhileIdle: true,
    //     uiLocalNotificationDateInterpretation:
    //         UILocalNotificationDateInterpretation.absoluteTime);
    // await Global.flutterLocalNotificationsPlugin
    //     .show(2, "hello test", "Time to test", notificationDetails);
    // await Global.flutterLocalNotificationsPlugin.zonedSchedule(2, "hello test",
    //     "Time to test", nextInstanceOfTenAM(12, 03), notificationDetails,
    //     uiLocalNotificationDateInterpretation:
    //         UILocalNotificationDateInterpretation.absoluteTime,
    //     androidAllowWhileIdle: true);
    // await Global.flutterLocalNotificationsPlugin.cancelAll();
    // NotificationDetails notificationDetails;
    // if (Global.isIOS) {
    //   notificationDetails =
    //       NotificationDetails(iOS: IOSNotificationDetails(presentSound: true));
    // } else {
    //   notificationDetails = NotificationDetails(
    //       android: AndroidNotificationDetails(
    //           "channel id", "your channel name", "your channel description",
    //           icon: "app_icon_3", color: HexColor("67C1BF")));
    // }
    // tz.TZDateTime nowDateTime = tz.TZDateTime.now(tz.local);
    // logger("tz.TZDateTime -------- local ${tz.local}");
    // List(6).asMap().forEach((key, value) async {
    //   nowDateTime = nowDateTime.add(Duration(minutes: 1));
    //   await Global.flutterLocalNotificationsPlugin.zonedSchedule(
    //       key,
    //       "$key",
    //       nowDateTime.toIso8601String(),
    //       // "Time to $title",
    //       nowDateTime,
    //       notificationDetails,
    //       uiLocalNotificationDateInterpretation:
    //           UILocalNotificationDateInterpretation.absoluteTime,
    //       androidAllowWhileIdle: true);
    // });

    if (prayerTimes == null || prayerTimes.length == 0) return;
    Global.flutterLocalNotificationsPlugin.cancelAll().then((value) {
      List<dynamic> times = StorageHelper().getJSON(ALARM_CLOCK_TIME);
      if (times == null) {
        times = [0, 0, 0, 0, 0, 0];
      } else {
        times = times.cast<int>();
      }
      List<dynamic> switchs = StorageHelper().getJSON(ALARM_CLOCK);
      if (switchs == null) {
        switchs = [true, false, true, true, true, true];
      } else {
        switchs = switchs.cast<bool>();
      }
      NotificationDetails notificationDetails;
      if (Global.isIOS) {
        notificationDetails = NotificationDetails(
            iOS: IOSNotificationDetails(presentSound: true));
      } else {
        notificationDetails = NotificationDetails(
            android: AndroidNotificationDetails(
                "channel id", "your channel name", "your channel description",
                icon: "app_icon_3",
                importance: Importance.max,
                color: HexColor("67C1BF")));
      }
      bool isEnglish = StorageHelper().getString(LOCALE) == null ||
              StorageHelper().getString(LOCALE) == "en"
          ? true
          : false;
      String title = '';
      String message = isEnglish ? 'Time to ' : 'Masa untuk ';
      prayerTimes
          .sublist(0, prayerTimes.length >= 2 ? 2 : prayerTimes.length)
          .asMap()
          .forEach((eIndex, e) {
        List(6).asMap().forEach((index, value) async {
          DateTime dateTime;
          switch (index) {
            case 0:
              dateTime = DateFormat("HH:mm").parse(e.fajr);
              title = isEnglish ? 'Fajr' : 'Subuh';
              break;
            case 1:
              dateTime = DateFormat("HH:mm").parse(e.syuruk);
              title = isEnglish ? 'Syuruk' : 'Syuruk';
              break;
            case 2:
              dateTime = DateFormat("HH:mm").parse(e.dhuhr);
              title = isEnglish ? 'Dhuhr' : 'Zohor';
              break;
            case 3:
              dateTime = DateFormat("HH:mm").parse(e.asr);
              title = isEnglish ? 'Asr' : 'Asar';
              break;
            case 4:
              dateTime = DateFormat("HH:mm").parse(e.maghrib);
              title = isEnglish ? 'Maghrib' : 'Maghrib';
              break;
            case 5:
              dateTime = DateFormat("HH:mm").parse(e.isha);
              title = isEnglish ? 'Isha' : 'Isyak';
              break;
            default:
          }
          dateTime = dateTime.subtract(Duration(minutes: times[index]));
          bool isClock = switchs[index];

          tz.TZDateTime time =
              nextInstanceOfTenAM(dateTime.hour, dateTime.minute, e.date);
          if (isClock && time != null) {
            await Global.flutterLocalNotificationsPlugin.zonedSchedule(
              eIndex * 10 + index,
              title,
              message + title,
              time,
              notificationDetails,
              uiLocalNotificationDateInterpretation:
                  UILocalNotificationDateInterpretation.absoluteTime,
              androidAllowWhileIdle: true,
            );
          }
        });
      });
    }).catchError((error) {
      logger("sendLocaloNitification --------- $error");
      sendLocaloNitification(prayerTimes);
    });
  }
}

// isolateFunc(Map<String, dynamic> params) async {
//   String message = params["message"];
//   bool isEnglish = params["isEnglish"];
//   List<PrayerTimeResult> prayerTimes = params["prayerTimes"];
//   NotificationDetails notificationDetails = params["notificationDetails"];
//   List<int> times = params["times"];
//   List<bool> switchs = params["switchs"];

//   String title = '';
//   prayerTimes.asMap().forEach((eIndex, e) {
//     List(6).asMap().forEach((index, value) async {
//       DateTime dateTime;
//       switch (index) {
//         case 0:
//           dateTime = DateFormat("HH:mm").parse(e.fajr);
//           title = isEnglish ? 'Fajr' : 'Subuh';
//           break;
//         case 1:
//           dateTime = DateFormat("HH:mm").parse(e.syuruk);
//           title = isEnglish ? 'Syuruk' : 'Syuruk';
//           break;
//         case 2:
//           dateTime = DateFormat("HH:mm").parse(e.dhuhr);
//           title = isEnglish ? 'Dhuhr' : 'Zohor';
//           break;
//         case 3:
//           dateTime = DateFormat("HH:mm").parse(e.asr);
//           title = isEnglish ? 'Asr' : 'Asar';
//           break;
//         case 4:
//           dateTime = DateFormat("HH:mm").parse(e.maghrib);
//           title = isEnglish ? 'Maghrib' : 'Maghrib';
//           break;
//         case 5:
//           dateTime = DateFormat("HH:mm").parse(e.isha);
//           title = isEnglish ? 'Isha' : 'Isyak';
//           break;
//         default:
//       }
//       dateTime = dateTime.subtract(Duration(minutes: times[index]));
//       bool isClock = switchs[index];
//       tz.TZDateTime time =
//           nextInstanceOfTenAM(dateTime.hour, dateTime.minute, e.date);
//       if (isClock && time != null) {
//         await Global.flutterLocalNotificationsPlugin.zonedSchedule(
//           eIndex,
//           title,
//           message + title,
//           time,
//           notificationDetails,
//           uiLocalNotificationDateInterpretation:
//               UILocalNotificationDateInterpretation.absoluteTime,
//           androidAllowWhileIdle: true,
//         );
//       }
//     });
//   });

// await Global.flutterLocalNotificationsPlugin.zonedSchedule(
//     index, title, message + title, time, notificationDetails,
//     uiLocalNotificationDateInterpretation:
//         UILocalNotificationDateInterpretation.absoluteTime,
//     androidAllowWhileIdle: true);
// }

class MuslimPrayer {
  final int index;
  final DateTime dateTime;
  final String name;
  MuslimPrayer(this.index, this.dateTime, this.name);
}
