import 'package:flutter/material.dart';
import 'package:muslim/common/provider/app.dart';
import 'package:muslim/common/core/storage.dart';
import 'package:muslim/common/helper/helper.dart';
import 'package:muslim/common/tools/dialog.dart';
import 'package:muslim/common/tools/snackBar.dart';
import 'package:muslim/global.dart';
import 'package:provider/provider.dart';
import 'package:easy_localization/easy_localization.dart';

void handleNetworkError(dynamic value, BuildContext context,
    {String errorString = "网络超时,请重试!", VoidCallback callBack}) async {
  print("handleNetworkError : $value    errorString: $errorString");
  if (value != null) {
    if (value.errorCode == RELOGIN) {
      StorageHelper().remove(USER_TOKEN);
      await Global.flutterLocalNotificationsPlugin.cancelAll();
      Provider.of<AppSettingModel>(context, listen: false).homeIsLogin = true;
      showEnsureDialog(context, "", value.msg ?? tr("logout_error_tip"), () {
        Navigator.of(context)
            .pushNamedAndRemoveUntil("introduce", (route) => false);
      }, cancelTitle: "", ensureTitle: tr("confirmText"), type: 0);
      // callBack();
    } else if (value.errorCode == NEED_TIP) {
      showSnackBar(context, value.msg ?? tr("network_error"), type: 1);
    } else {
      showSnackBar(context, value.developerMsg ?? tr("network_error"), type: 1);
    }
  } else {
    showSnackBar(context, value.developerMsg ?? tr("network_error"), type: 1);
  }
}
