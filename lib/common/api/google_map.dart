import 'package:dio/dio.dart';
import 'package:geolocator/geolocator.dart';
import 'package:muslim/modals/autocomplate.dart';
import 'package:muslim/modals/details.dart';
import 'package:muslim/modals/directions.dart';
import 'package:muslim/modals/location.dart';
import 'dart:async';

import 'package:muslim/modals/timezone.dart';

class GoogleApiService {
  String googleApiKey = "AIzaSyB01m-vyPMXuB2fyl3xIWCnKCXiSeb6A98";
  String googleUrl = "https://maps.googleapis.com";
  String googleDirectionsUrl = "/maps/api/directions/json";
  String googleGeocodeUrl = "/maps/api/geocode/json";
  String googlePlacesUrl = "/maps/api/place/autocomplete/json";
  String googleDistanceMatrixUrl = "/maps/api/distancematrix/json";
  String googleAutocompleteUrl = "/maps/api/place/autocomplete/json";
  String googleDetailsUrl = "/maps/api/place/details/json";
  String googleTimeZoneUrl = "/maps/api/timezone/json";
  BaseOptions _options = BaseOptions(
    receiveTimeout: 10000,
    connectTimeout: 10000,
  );

  /// 地理反编码(知道经纬度 反推出 所在的城市)
  Future<List<Result>> fetchLocalCity(dynamic params, {int type = 0}) async {
    params["key"] = googleApiKey;
    params["language"] = "en";
    if (type == 0) {
      params["result_type"] = "administrative_area_level_1|country";
    }
    try {
      var result = await Dio(_options)
          .get(googleUrl + googleGeocodeUrl, queryParameters: params);
      Locations location = Locations.fromJson(result.data);
      return Future.value(location.results);
    } catch (error) {
      return Future.error(error);
    }
  }

  Future<Position> fetchLocalLocation() async {
    return Dio(_options)
        .post(
            "https://www.googleapis.com/geolocation/v1/geolocate?key=AIzaSyB01m-vyPMXuB2fyl3xIWCnKCXiSeb6A98")
        .then((value) {
      if (value != null && value.statusCode == 200) {
        return Position(
            latitude: value.data["location"]["lat"],
            longitude: value.data["location"]["lng"]);
      } else {
        return null;
      }
    }).catchError((error) {
      print("fetchLocalLocation: $error");
      return null;
    });
  }

  Future<List<Prediction>> fetchAutocompletePlaces(dynamic params) async {
    params["key"] = googleApiKey;
    params["language"] = "en";
    try {
      var result = await Dio(_options)
          .get(googleUrl + googleAutocompleteUrl, queryParameters: params);
      Autocomplete autos = Autocomplete.fromJson(result.data);
      return autos.predictions;
    } catch (error) {
      return Future.error(error);
    }
  }

  Future<Result> fetchPlaceDetails(dynamic params) async {
    params["key"] = googleApiKey;
    params["language"] = "en";
    print("fetchPlaceDetails params $params");
    try {
      var result = await Dio(_options)
          .get(googleUrl + googleDetailsUrl, queryParameters: params);

      Details details = Details.fromJson(result.data);
      return details.result;
    } catch (error) {
      return Future.error(error);
    }
  }

  Future<Directions> fetchDirections(dynamic params) async {
    params["key"] = googleApiKey;
    params["language"] = "en";
    try {
      var result = await Dio()
          .get(googleUrl + googleDirectionsUrl, queryParameters: params);
      Directions direcitons = Directions.fromJson(result.data);
      return Future.value(direcitons);
    } catch (error) {
      return Future.error(error);
    }
  }

  // https://maps.googleapis.com/maps/api/timezone/json?location=35.125801,%20-117.9859038&key=AIzaSyB01m-vyPMXuB2fyl3xIWCnKCXiSeb6A98&timestamp=1331161200
  Future<GoogleTimeZoneData> fetchLocationTimeZone(dynamic params) async {
    params["key"] = googleApiKey;
    params["language"] = "en";
    params["timestamp"] = "1331161200";
    try {
      var result = await Dio()
          .get(googleUrl + googleTimeZoneUrl, queryParameters: params);
      GoogleTimeZoneData timezone = GoogleTimeZoneData.fromJson(result.data);
      return timezone;
    } on DioError catch (error) {
      return Future.error(error);
    }
  }
}
