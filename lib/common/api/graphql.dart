import 'package:dio/dio.dart';
import 'package:muslim/modals/artical.dart';

class GraphQlResult {
  final int code;
  final String message;
  final dynamic data;

  GraphQlResult(this.code, this.message, this.data);
}

String queryAllBlogs(int skip, int limit, String local) {
  return "query{all_blog_posts(locale: \"ms-my\", order_by: updated_at_DESC, skip: $skip, limit: $limit){items {title url system {tags updated_at} artworkConnection {edges {node {url}}}}}}";
}

Future<Artical> fetchAllBlogs(int skip,
    {int limit = 20, String local: "ms-my"}) async {
  BaseOptions options = BaseOptions(
      receiveTimeout: 30000,
      headers: {"access_token": "cs2bf31e3dba0867ecc05c631e"});
  try {
    var value = await Dio(options).get(
        "https://graphql.contentstack.com/stacks/blt2a130c768c36b9df?environment=stg",
        queryParameters: {"query": queryAllBlogs(skip * 10, limit, local)});
    return Artical.fromJson(value.data);
  } on DioError catch (error) {
    return null;
  }
}
