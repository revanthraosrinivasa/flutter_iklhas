import 'dart:async';
import 'package:dio/dio.dart';
import 'package:muslim/common/core/storage.dart';
import 'package:muslim/common/helper/helper.dart';
import 'package:muslim/common/helper/logger.dart';
import 'package:muslim/modals/activity.dart';
import 'package:muslim/modals/follow.dart';
import 'package:muslim/modals/login.dart';
import 'package:muslim/modals/matimes.dart';
import 'package:muslim/modals/merchant.dart';
import 'package:muslim/modals/nearlyMerchant.dart';
import 'package:muslim/modals/normalResult.dart';
import 'package:muslim/modals/prayerzone.dart';
import 'package:muslim/modals/recommends.dart';
import 'package:muslim/modals/refreshToken.dart';
import 'package:muslim/modals/result.dart';
import 'package:muslim/modals/times.dart';
import 'package:muslim/modals/user.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:muslim/modals/version_result.dart';
import 'package:sentry_flutter/sentry_flutter.dart';

enum Environment { debug, test, production }

class NormalApi {
  static NormalApi _instance;
  factory NormalApi() => instance();
  NormalApi._();
  static NormalApi instance() {
    if (_instance == null) {
      _instance = NormalApi._();
    }
    return _instance;
  }

  Environment environment = StorageHelper().getInt(ENVIRONMENT) == null
      ? Environment.production
      : Environment.values[StorageHelper().getInt(ENVIRONMENT)];

  BaseOptions _options = BaseOptions(
    receiveTimeout: 10000,
    connectTimeout: 10000,
  );
  headerAddToken() {
    _options.headers = {
      "Authorization": "Bearer " +
          Token.fromJson(StorageHelper().getJSON(USER_TOKEN)).accessToken
    };
  }

  Dio dio = Dio()..interceptors.add(SentryInterceptor());

  // 本地测试
  String BASE_URL() {
    switch (environment) {
      case Environment.debug:
        return "http://gateway-dev.chatecdn.com/app/d/v1/api";
        break;
      case Environment.test:
        return "http://gateway.chatecdn.com/app/d/v1/api";
        break;
      case Environment.production:
        return "https://gateway.ikhlas.com/app/d/v1/api";
        break;
      default:
    }
  }

  String BASE_AUTHON_URL() {
    switch (environment) {
      case Environment.debug:
        return "http://gateway-dev.chatecdn.com/app/v1/api";
        break;
      case Environment.test:
        return "http://gateway.chatecdn.com/app/v1/api";
        break;
      case Environment.production:
        return "https://gateway.ikhlas.com/app/v1/api";
        break;
      default:
    }
  }

  String UPDATEURL() {
    switch (environment) {
      case Environment.debug:
        return "http://fc-dev.chatecdn.com/version";
        break;
      case Environment.test:
        return "http://fc.chatecdn.com/version";
        break;
      case Environment.production:
        return "https://fc.cannaaa.com/version";
        break;
      default:
    }
  }

  final String SEND_PIN_CODE = "/account/sendPinCode";
  final String LOGIN = "/account/phoneLogin";
  final String LOGIN_PWD = "/account/userLogin";
  final String REFRESHTOKEN = "/account/refreshToken/";
  final String PINCODE_VERIFY = "/account/pwdPhonePinVerify";
  final String SET_PASSWORD = "/account/userUpdatePwd";

  final String RESET_PASSWORD = "/user/setPwd";

  final String CHANGEPROFILE = "/user";
  final String CHANGE_USER_PHONE = "/user/reBind";
  final String LOGOUT = "/user/logout";

  String merchant(String id) => "/merchant/$id";

  final String RECOMMEND = "/merchant/feeds/recommend";
  String merchantFeeds(String id) => "/merchant/$id/feeds";

  final String ACTIVITIES = "/merchant/activities";
  String storyActivity(String id) => "/merchant/$id/activities";

  final String PRAYERTIMER = "/o/prayer-schedule";
  final String FOLLOW_MERCHANTS = "/social/follow/merchants";
  final String FOLLOW_MERCHANTS_FEEDS = "/social/follow/merchants/feeds";
  final String MALAYSIAURL = "/prayer/prayer-times";
  String follow(String id) => "/social/follow/merchant/$id";
  String nearbyMerchantsWithLatLng(double lat, double lon) =>
      "/merchants/nearby/$lon-$lat";
  String nearbyMerchantsList(double lat, double lon) =>
      "/merchants/nearbyRecommend/$lon-$lat";
  final String FEEDBACK = "/o/feedback";
  final String PrayerZones = "/prayer/prayer-zones";
  final String NoticeToken = "/social/follow/noticeToken";
  String notificationSwitchOn(String merchant_id) =>
      "/social/follow/merchant/$merchant_id/switchOn";
  String notificationSwitchOff(String merchant_id) =>
      "/social/follow/merchant/$merchant_id/switchOff";

  Future<NormalResult> fetchPinCode(dynamic params) async {
    dio.options = _options;
    return dio.post(BASE_URL() + SEND_PIN_CODE, data: params).then((value) {
      NormalResult result = NormalResult.fromJson(value.data);
      logger("fetchPinCode result: ${result.toJson()}");
      return result;
    }).catchError((error) {
      logger("fetchPinCode error: $error");
    });
  }

  /// 设置登录密码时的检测验证码
  Future<NormalResult> checkPinCode(dynamic params, {int type = 0}) async {
    if (type == 0) {
      headerAddToken();
    }
    dio.options = _options;
    try {
      Response response =
          await dio.post(BASE_URL() + PINCODE_VERIFY, data: params);
      logger("-------checkPinCode $response");
      return NormalResult.fromJson(response.data);
    } catch (error) {
      logger("checkPinCode Error: ${error.response.data}");
      var result = await _handleErrorHandle(error);
      return NormalResult(errorCode: result.errorCode, msg: result.msg);
    }
  }

  /// 设置登录密码
  Future<NormalResult> setLoginPassword(dynamic params, {int type = 0}) async {
    if (type == 0) {
      headerAddToken();
    }
    dio.options = _options;
    dio.options.headers.addAll({"X-TemporaryToken": params["tempToken"]});
    try {
      Response response =
          await dio.post(BASE_URL() + SET_PASSWORD, data: params);
      logger("setLoginPassword Error: $response");

      NormalResult result = NormalResult.fromJson(response.data);
      return result;
    } catch (error) {
      logger("setLoginPassword Error: ${error.resonse.data}");

      var result = await _handleErrorHandle(error);
      return NormalResult(errorCode: result.errorCode, msg: result.msg);
    }
  }

  /// 忘记密码
  Future<NormalResult> resetPassword(dynamic params) async {
    headerAddToken();
    dio.options = _options;
    try {
      Response response =
          await dio.post(BASE_AUTHON_URL() + RESET_PASSWORD, data: params);
      logger("resetPassword response: ${response.data}");

      NormalResult result = NormalResult.fromJson(response.data);
      return result;
    } catch (error) {
      logger("resetPassword Error: ${error.response.data}");

      var result = await _handleErrorHandle(error);
      return NormalResult(errorCode: result.errorCode, msg: result.msg);
    }
  }

  /// 验证码登录
  Future<LoginResult> fetchLogin(dynamic params, {int type = 0}) async {
    dio.options = _options;

    if (params["phonePrefix"] + params["phoneNumber"] == "60123456789" &&
        params["password"] == "ikhlas123456") {
      environment = Environment.test;
      StorageHelper().setInt(ENVIRONMENT, environment.index);
    } else {
      environment = StorageHelper().getInt(ENVIRONMENT) == null
          ? Environment.production
          : Environment.values[StorageHelper().getInt(ENVIRONMENT)];
      StorageHelper().setInt(ENVIRONMENT, environment.index);
    }
    try {
      var value = await dio.post(
          type == 0 ? BASE_URL() + LOGIN : BASE_URL() + LOGIN_PWD,
          data: params);
      logger("login success ---------$value");
      LoginResult result = LoginResult.fromJson(value.data);
      return result;
    } on DioError catch (error) {
      logger("fetchLogin Error: $error");
      if (error.response == null) {
        return LoginResult(errorCode: NEED_TIP, msg: tr("network_error"));
      } else {
        return LoginResult(
            errorCode: error.response.data["errorCode"],
            msg: type == 0
                ? tr("verfication_error")
                : tr("login_password_error"));
      }
    }
  }

  Future<LoginResult> changeUserInfo(dynamic params) async {
    headerAddToken();
    dio.options = _options;
    try {
      var value =
          await dio.patch(BASE_AUTHON_URL() + CHANGEPROFILE, data: params);
      LoginResult result = LoginResult.fromJson(value.data);
      return result;
    } on DioError catch (error) {
      var result = await _handleErrorHandle(error);
      return LoginResult(errorCode: result.errorCode, msg: result.msg);
    }
  }

  Future<UserResult> fetchUserInfo() async {
    headerAddToken();
    dio.options = _options;
    try {
      var value = await dio.get(BASE_AUTHON_URL() + CHANGEPROFILE);
      logger("fetchUserInfo success ----- $value");
      UserResult result = UserResult.fromJson(value.data);
      return result;
    } on DioError catch (error) {
      var result = await _handleErrorHandle(error);
      return UserResult(errorCode: result.errorCode, msg: result.msg);
    }
  }

  Future<Recommends> fetchRecommends(dynamic params) async {
    headerAddToken();
    dio.options = _options;
    try {
      var response =
          await dio.post(BASE_AUTHON_URL() + RECOMMEND, data: params);
      logger("fetchRecommends --------$response");
      Recommends recommends = Recommends.fromJson(response.data);
      return recommends;
    } on DioError catch (error) {
      logger("fetchRecommends error--------$error");
      var result = await _handleErrorHandle(error);
      return Recommends(errorCode: result.errorCode, msg: result.msg);
    }
  }

  Future<Recommends> fetchFollowMerchantsFeed(dynamic params) async {
    headerAddToken();
    dio.options = _options;
    try {
      var value = await dio.get(BASE_AUTHON_URL() + FOLLOW_MERCHANTS_FEEDS,
          queryParameters: params);
      logger("fetchFollowMerchantsFeed value: $value");
      Recommends recommends = Recommends.fromJson(value.data);
      return recommends;
    } on DioError catch (error) {
      var result = await _handleErrorHandle(error);
      return Recommends(errorCode: result.errorCode, msg: result.msg);
    }
  }

  Future<Activities> fetchActivities(dynamic params) async {
    headerAddToken();
    dio.options = _options;
    try {
      var value = await dio.post(BASE_AUTHON_URL() + ACTIVITIES, data: params);
      logger("fetchActivities : $value");
      Activities activitiesResult = Activities.fromJson(value.data);
      return activitiesResult;
    } on DioError catch (error) {
      var result = await _handleErrorHandle(error);
      return Activities(errorCode: result.errorCode, msg: result.msg);
    }
  }

  Future<String> fetchMonthTime(dynamic params) async {
    headerAddToken();
    dio.options = _options;
    try {
      var value = await dio.post(BASE_AUTHON_URL() + PRAYERTIMER, data: params);
      return value.data as String;
    } on DioError catch (error) {
      var result = await _handleErrorHandle(error);
      return result != null ? result.errorCode : "";
    }
  }

  Future<FollowMerchants> fetchFollowMerchants(dynamic params) async {
    headerAddToken();
    dio.options = _options;
    try {
      var value = await dio.get(BASE_AUTHON_URL() + FOLLOW_MERCHANTS,
          queryParameters: params);
      logger("fetchFollowMerchants : $value");
      FollowMerchants follows = FollowMerchants.fromJson(value.data);
      return follows;
    } on DioError catch (error) {
      var result = await _handleErrorHandle(error);
      return FollowMerchants(errorCode: result.errorCode, msg: result.msg);
    }
  }

  Future<CommonResult> sendFeedBack(dynamic params) async {
    headerAddToken();
    dio.options = _options;
    try {
      var value = await dio.post(BASE_AUTHON_URL() + FEEDBACK, data: params);
      CommonResult result = CommonResult.fromJson(value.data);
      return result;
    } on DioError catch (error) {
      var result = await _handleErrorHandle(error);
      return CommonResult(errorCode: result.errorCode, msg: result.msg);
    }
  }

  Future<MerchantDetail> fetchMerchantDetail(String id) async {
    headerAddToken();
    dio.options = _options;
    try {
      var value = await dio.get(BASE_AUTHON_URL() + merchant(id));
      MerchantDetail result = MerchantDetail.fromJson(value.data);
      return result;
    } on DioError catch (error) {
      var result = await _handleErrorHandle(error);
      return MerchantDetail(errorCode: result.errorCode, msg: result.msg);
    }
  }

  /// 关注
  Future<CommonResult> followMerchant(String merchantId) async {
    headerAddToken();
    dio.options = _options;
    try {
      var value = await dio.post(BASE_AUTHON_URL() + follow(merchantId));
      CommonResult result = CommonResult.fromJson(value.data);
      return result;
    } on DioError catch (error) {
      var result = await _handleErrorHandle(error);
      return CommonResult(errorCode: result.errorCode, msg: result.msg);
    }
  }

  /// 取消关注
  Future<CommonResult> unfollowMerchant(String merchantId) async {
    headerAddToken();
    dio.options = _options;
    try {
      var value = await dio.delete(BASE_AUTHON_URL() + follow(merchantId));
      CommonResult result = CommonResult.fromJson(value.data);
      return result;
    } on DioError catch (error) {
      var result = await _handleErrorHandle(error);
      return CommonResult(errorCode: result.errorCode);
    }
  }

  Future<CommonResult> refreshToken() async {
    Token token = Token.fromJson(StorageHelper().getJSON(USER_TOKEN));
    try {
      var value = await Dio(_options)
          .get(BASE_URL() + REFRESHTOKEN + token.refreshToken);
      RefreshToken result = RefreshToken.fromJson(value.data);
      if (result.errorCode == SUCCESS && result.data != null) {
        StorageHelper().setJSON(USER_TOKEN, result.data);
        return CommonResult(developerMsg: "", msg: "", errorCode: SUCCESS);
      } else {
        return CommonResult(developerMsg: "", msg: "", errorCode: "");
      }
    } on DioError {
      return CommonResult(developerMsg: "", msg: "", errorCode: "");
    }
  }

  /// 商户的活动
  Future<Activities> fetchMerchantActivities(
      dynamic params, String merchantId) async {
    _options.headers = {
      "Authorization": "Bearer " +
          Token.fromJson(StorageHelper().getJSON(USER_TOKEN)).accessToken
    };
    try {
      var value = await Dio(_options)
          .post(BASE_AUTHON_URL() + storyActivity(merchantId), data: params);
      logger("fetchMerchantActivities value: $value");
      Activities activitiesResult = Activities.fromJson(value.data);
      return activitiesResult;
    } on DioError catch (error) {
      var result = await _handleErrorHandle(error);
      return Activities(errorCode: result.errorCode, msg: result.msg);
    }
  }

  /// 获取指定商户的动态
  Future<Recommends> fetchMerchantFeedWithId(
      dynamic params, String merchantId) async {
    _options.headers = {
      "Authorization": "Bearer " +
          Token.fromJson(StorageHelper().getJSON(USER_TOKEN)).accessToken
    };
    try {
      var value = await Dio(_options).get(
          BASE_AUTHON_URL() + merchantFeeds(merchantId),
          queryParameters: params);
      logger("fetchMerchantFeedWithId value: $value");
      Recommends recommends = Recommends.fromJson(value.data);
      return recommends;
    } on DioError catch (error) {
      var result = await _handleErrorHandle(error);
      return Recommends(errorCode: result.errorCode, msg: result.msg);
    }
  }

  Future<NearlyMerchants> fetchNearbyMerchantsListWithLatLng(
      double lat, double lng, dynamic params) async {
    print("fetchNearbyMerchantsListWithLatLng: $params");
    _options.headers = {
      "Authorization": "Bearer " +
          Token.fromJson(StorageHelper().getJSON(USER_TOKEN)).accessToken
    };
    _options.connectTimeout = 10000;
    try {
      var value = await Dio(_options).get(
          BASE_AUTHON_URL() + nearbyMerchantsWithLatLng(lat, lng),
          queryParameters: params);
      logger("fetchNearbyMerchantsListWithLatLng value: $value");
      NearlyMerchants recommends = NearlyMerchants.fromJson(value.data);
      return recommends;
    } on DioError catch (error) {
      var result = await _handleErrorHandle(error);
      return NearlyMerchants(errorCode: result.errorCode, msg: result.msg);
    }
  }

  Future<NearlyMerchants> fetchNearbyMerchantsWithLatLng(
      double lat, double lng, dynamic params) async {
    _options.headers = {
      "Authorization": "Bearer " +
          Token.fromJson(StorageHelper().getJSON(USER_TOKEN)).accessToken
    };
    try {
      var value = await Dio(_options).get(
          BASE_AUTHON_URL() + nearbyMerchantsList(lat, lng),
          queryParameters: params);
      print("fetch fetchNearbyMerchantsWithLatLng: $value");
      NearlyMerchants recommends = NearlyMerchants.fromJson(value.data);
      return recommends;
    } on DioError catch (error) {
      var result = await _handleErrorHandle(error);
      return NearlyMerchants(errorCode: result.errorCode, msg: result.msg);
    }
  }

  Future<LoginResult> changeUserPhone(dynamic params) async {
    _options.headers = {
      "Authorization": "Bearer " +
          Token.fromJson(StorageHelper().getJSON(USER_TOKEN)).accessToken
    };
    try {
      var value = await Dio(_options)
          .post(BASE_AUTHON_URL() + CHANGE_USER_PHONE, data: params);
      LoginResult result = LoginResult.fromJson(value.data);
      return result;
    } on DioError catch (error) {
      var result = await _handleErrorHandle(error);
      return LoginResult(errorCode: result.errorCode, msg: result.msg);
    }
  }

  Future<CommonResult> logout() async {
    _options.headers = {
      "Authorization": "Bearer " +
          Token.fromJson(StorageHelper().getJSON(USER_TOKEN)).accessToken
    };
    try {
      var value = await Dio(_options).delete(BASE_AUTHON_URL() + LOGOUT);
      CommonResult result = CommonResult.fromJson(value.data);
      return result;
    } on DioError catch (error) {
      var result = await _handleErrorHandle(error);
      return CommonResult(errorCode: result.errorCode, msg: result.msg);
    }
  }

  Future<VersionResult> fetchNewVersion(dynamic params) async {
    String url = "/v1/api/version";
    try {
      var value = await dio.post(UPDATEURL() + url, data: params);
      VersionResult result = VersionResult.fromJson(value.data);
      return result;
    } on DioError catch (error) {
      var result = await _handleErrorHandle(error);
      return VersionResult(errorCode: result.errorCode, msg: result.msg);
    }
  }

  Future<MalaysiaPrayerZone> fetchPrayerZones() async {
    _options.headers = {
      "Authorization": "Bearer " +
          Token.fromJson(StorageHelper().getJSON(USER_TOKEN)).accessToken
    };
    try {
      var value = await Dio(_options).get(BASE_AUTHON_URL() + PrayerZones);
      print("fetchPrayerZones success: $value");
      MalaysiaPrayerZone result = MalaysiaPrayerZone.fromJson(value.data);
      return result;
    } catch (error) {
      logger("fetchPrayerZones error : $error");
      var result = await _handleErrorHandle(error);
      return MalaysiaPrayerZone(errorCode: result.errorCode, msg: result.msg);
    }
  }

  Future<PrayerTimesResult> fetchYearPrayerTimes(dynamic data) async {
    _options.headers = {
      "Authorization": "Bearer " +
          Token.fromJson(StorageHelper().getJSON(USER_TOKEN)).accessToken
    };
    print("fetchYearPrayerTimes params ---------------- :: $data");
    try {
      var value = await Dio(_options)
          .get(BASE_AUTHON_URL() + MALAYSIAURL, queryParameters: data);
      print("fetchYearPrayerTimes success:: ${value.data}");
      PrayerTimesResult result = PrayerTimesResult.fromJson(value.data);
      return result;
    } on DioError catch (error) {
      print("fetchYearPrayerTimes error:: ${error.response}");
      var result = await _handleErrorHandle(error);
      return PrayerTimesResult(errorCode: result.errorCode, msg: result.msg);
    }
  }

  Future<MalaysiaPrayerTime> fetchMalaysiaPrayerTimes(
      double lat, double lng) async {
    String url = "https://mpt.i906.my/api/prayer/$lat,$lng";
    try {
      var value = await Dio(_options).get(url);
      logger("fetchMalaysiaPrayerTimes $value");
      if (value.data != null && value.data["data"]["code"] != null) {
        MalaysiaPrayerTime times = MalaysiaPrayerTime.fromJson(value.data);
        return times;
      }
      return null;
    } on DioError catch (error) {
      print("fetchMalaysiaPrayerTimes error: $error");
      return null;
    }
  }

  /// error handle
  Future<CommonResult> _handleErrorHandle(dynamic error) async {
    print("_handleErrorHandle: $error");
    if (error.response != null) {
      if (error.response.statusCode == 400) {
        Map<String, dynamic> data = error.response.data;
        if (data["errorCode"] == PHONE_EXITES) {
          return CommonResult(
              errorCode: NEED_TIP, msg: "The phone number has been registered");
        }
        if (data["errorCode"] == OldCodeError) {
          return CommonResult(
              errorCode: OldCodeError, msg: tr("old_password_error"));
        }
        if (data["errorCode"] == AuthCodeError) {
          return CommonResult(
              errorCode: AuthCodeError, msg: tr("verfication_error"));
        }
        return CommonResult(errorCode: NEED_TIP);
      } else if (error.response.statusCode == 401) {
        try {
          var tokenResult = await refreshToken();
          if (tokenResult != null && tokenResult.errorCode == SUCCESS) {
            return CommonResult(errorCode: NEED_TIP);
          }
          return CommonResult(errorCode: RELOGIN);
        } on DioError {
          return CommonResult(errorCode: RELOGIN);
        }
      } else if (error.response.statusCode == 502) {
        return CommonResult(errorCode: RELOGIN, msg: tr("user_missing"));
      }
      return CommonResult(
          errorCode: error.response.statusCode != null
              ? "${error.response.statusCode}"
              : error.response.code != null
                  ? "${error.response.code}"
                  : NEED_TIP);
    }
    logger("fetchNearbyMerchantsWithLatLng Error: $error");
    // return CommonResult(errorCode: NEED_TIP);
    return null;
  }

  /// 关联token
  Future<NormalResult> relateToPushToken(dynamic params) async {
    _options.headers = {
      "Authorization": "Bearer " +
          Token.fromJson(StorageHelper().getJSON(USER_TOKEN)).accessToken
    };
    try {
      var value = await (dio..options = _options)
          .post(BASE_AUTHON_URL() + NoticeToken, data: params);
      NormalResult result = NormalResult.fromJson(value.data);
      return result;
    } on DioError catch (error) {
      var result = await _handleErrorHandle(error);
      return NormalResult(errorCode: result.errorCode, msg: result.msg);
    }
  }

  /// 打开通知开关
  Future<NormalResult> switchOnPush(String merchanId) async {
    _options.headers = {
      "Authorization": "Bearer " +
          Token.fromJson(StorageHelper().getJSON(USER_TOKEN)).accessToken
    };
    try {
      var value = await (dio..options = _options)
          .post(BASE_AUTHON_URL() + notificationSwitchOn(merchanId));
      NormalResult result = NormalResult.fromJson(value.data);
      return result;
    } on DioError catch (error) {
      var result = await _handleErrorHandle(error);
      return NormalResult(errorCode: result.errorCode, msg: result.msg);
    }
  }

  /// 关闭通知开关
  Future<NormalResult> switchOffPush(String merchanId) async {
    _options.headers = {
      "Authorization": "Bearer " +
          Token.fromJson(StorageHelper().getJSON(USER_TOKEN)).accessToken
    };
    try {
      var value = await (dio..options = _options)
          .post(BASE_AUTHON_URL() + notificationSwitchOff(merchanId));
      NormalResult result = NormalResult.fromJson(value.data);
      return result;
    } on DioError catch (error) {
      var result = await _handleErrorHandle(error);
      return NormalResult(errorCode: result.errorCode, msg: result.msg);
    }
  }
}

class SentryInterceptor extends InterceptorsWrapper {
  @override
  Future onResponse(Response response) {
    Sentry.addBreadcrumb(
      Breadcrumb(
        type: 'http',
        category: 'http',
        data: {
          'url': response.request.uri.toString(),
          'method': response.request.method,
          'status_code': response.statusCode,
        },
      ),
    );
    return super.onResponse(response);
  }

  @override
  Future onError(DioError err) {
    Sentry.addBreadcrumb(
      Breadcrumb(
        type: 'http',
        category: 'http',
        data: {
          'url': err.request.uri.toString(),
          'method': err.request.method,
          'status_code': err.response?.statusCode ?? "NA",
        },
        message: err.toString(),
      ),
    );
    return super.onError(err);
  }
}
