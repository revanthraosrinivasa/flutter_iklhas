import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:adhan/adhan.dart';
import 'package:geolocator/geolocator.dart';
import 'package:muslim/common/core/storage.dart';
import 'package:muslim/common/helper/helper.dart';
import 'package:muslim/global.dart';
import 'package:muslim/modals/times.dart';

class PrayerModel extends ChangeNotifier {
  Coordinates _location;

  String locationCity = StorageHelper().getJSON(PRAYER_TIMES_SETTING_CITY);

  CalculationMethod method =
      StorageHelper().getInt(PRAYER_TIMES_SETTING_METHOD) != null
          ? CalculationMethod
              .values[StorageHelper().getInt(PRAYER_TIMES_SETTING_METHOD)]
          : CalculationMethod.muslim_world_league;

  Madhab madhab = StorageHelper().getInt(PRAYER_TIMES_SETTING_MADHAB) != null
      ? Madhab.values[StorageHelper().getInt(PRAYER_TIMES_SETTING_MADHAB)]
      : Madhab.shafi;

  HighLatitudeRule highLatitudeRule = StorageHelper()
              .getInt(PRAYER_TIMES_SETTING_HIGHLATITUDERULE) !=
          null
      ? HighLatitudeRule
          .values[StorageHelper().getInt(PRAYER_TIMES_SETTING_HIGHLATITUDERULE)]
      : HighLatitudeRule.twilight_angle;

  PrayerAdjustments adjustments = StorageHelper()
              .getJSON(PRAYER_TIMES_SETTING_ADJUSTMENTS) !=
          null
      ? PrayerAdjustments(
          fajr:
              StorageHelper().getJSON(PRAYER_TIMES_SETTING_ADJUSTMENTS)["fajr"],
          asr: StorageHelper().getJSON(PRAYER_TIMES_SETTING_ADJUSTMENTS)["asr"],
          dhuhr: StorageHelper()
              .getJSON(PRAYER_TIMES_SETTING_ADJUSTMENTS)["dhuhr"],
          isha:
              StorageHelper().getJSON(PRAYER_TIMES_SETTING_ADJUSTMENTS)["isha"],
          maghrib: StorageHelper()
              .getJSON(PRAYER_TIMES_SETTING_ADJUSTMENTS)["maghrib"],
          sunrise: StorageHelper()
              .getJSON(PRAYER_TIMES_SETTING_ADJUSTMENTS)["sunrise"],
        )
      : PrayerAdjustments(
          fajr: 0, sunrise: 0, dhuhr: 3, asr: 3, maghrib: 0, isha: 2);

  PrayerAdjustments methodAdjustments =
      StorageHelper().getJSON(PRAYER_TIMES_SETTING_METHODADJUSTMENTS);

  DateComponents dateComponents = DateComponents.from(DateTime.now());

  Prayer currentPrayer;

  int _utcOffset = 8;

  int get utcOffset {
    return _utcOffset;
  }

  set utcOffset(int offset) {
    _utcOffset = offset;
    notifyListeners();
  }

  int paryerConventionsIndex =
      StorageHelper().getInt(PRAYER_TIMES_SETTING_INDEX) ?? 8;

  void changeLocationCity(String city) {
    locationCity = city;
    StorageHelper().setJSON(PRAYER_TIMES_SETTING_CITY, city);
    notifyListeners();
  }

  void changeCurrentPrayer(Prayer current) {
    currentPrayer = current;
    notifyListeners();
  }

  void changeMadhab(Madhab value) {
    madhab = value;
    StorageHelper().setInt(PRAYER_TIMES_SETTING_MADHAB, value.index);
    Global.viewModel.sendLocaloNitification(prayerTimesList);
    notifyListeners();
  }

  void changeToLocations(double latitude, double longitude) {
    _location = Coordinates(latitude, longitude);
    StorageHelper()
        .setJSON(POSITION, Position(latitude: latitude, longitude: longitude));
    Global.viewModel.sendLocaloNitification(prayerTimesList);
    notifyListeners();
  }

  void changeMethod(CalculationMethod value) {
    method = value;
    StorageHelper().setInt(
        PRAYER_TIMES_SETTING_METHOD, CalculationMethod.values.indexOf(value));
    Global.viewModel.sendLocaloNitification(prayerTimesList);
    notifyListeners();
  }

  void changeHighLatitudeRule(HighLatitudeRule value) {
    highLatitudeRule = value;
    Global.viewModel.sendLocaloNitification(prayerTimesList);
    StorageHelper().setInt(PRAYER_TIMES_SETTING_HIGHLATITUDERULE,
        HighLatitudeRule.values.indexOf(value));
    notifyListeners();
  }

  void changeAdjustments(PrayerAdjustments value) {
    adjustments = value;
    Global.viewModel.sendLocaloNitification(prayerTimesList);
    StorageHelper().setJSON(PRAYER_TIMES_SETTING_ADJUSTMENTS, {
      "Fajr": value.fajr,
      "Asr": value.asr,
      "Dhuhr": value.dhuhr,
      "Isha": value.isha,
      "Maghrib": value.maghrib,
      "Sunrise": value.sunrise,
    });
    notifyListeners();
  }

  void changeDateComponents(DateTime value) {
    dateComponents = DateComponents.from(value);
    notifyListeners();
  }

  void changeFajrAndIsha(int index) {
    paryerConventionsIndex = index;
    Global.viewModel.sendLocaloNitification(prayerTimesList);
    StorageHelper().setInt(PRAYER_TIMES_SETTING_INDEX, index);
    notifyListeners();
  }

  PrayerTimes get prayerTimes {
    if (_location == null) {
      return null;
    }
    return PrayerTimes.utcOffset(
        this._location, dateComponents, params, Duration(hours: utcOffset));
  }

  List<Map<String, dynamic>> _addYearContent() {
    List<Map<String, dynamic>> content = [];
    for (var i = 0; i < 365; i++) {
      DateTime time = DateTime.now().add(Duration(days: i));
      PrayerTimes nowTimes = PrayerTimes.utcOffset(
          prayerTimes.coordinates,
          DateComponents.from(time),
          prayerTimes.calculationParameters,
          Duration(hours: utcOffset));
      if (nowTimes != null) {
        var times = {
          "date": DateFormat("dd-MMM-yyyy").format(time),
          // "week": DateFormat("EE").format(time)
        };
        times["fajr"] = DateFormat("HH:mm")
            .format(localChangeUTCtoLocal(nowTimes.timeForPrayer(Prayer.fajr)));
        times["sunrise"] = DateFormat("HH:mm").format(
            localChangeUTCtoLocal(nowTimes.timeForPrayer(Prayer.sunrise)));
        times["dhuhr"] = DateFormat("HH:mm").format(
            localChangeUTCtoLocal(nowTimes.timeForPrayer(Prayer.dhuhr)));
        times["asr"] = DateFormat("HH:mm")
            .format(localChangeUTCtoLocal(nowTimes.timeForPrayer(Prayer.asr)));
        times["maghrib"] = DateFormat("HH:mm").format(
            localChangeUTCtoLocal(nowTimes.timeForPrayer(Prayer.maghrib)));
        times["isha"] = DateFormat("HH:mm")
            .format(localChangeUTCtoLocal(nowTimes.timeForPrayer(Prayer.isha)));
        content.add(times);
      }
    }
    return content;
  }

  List<PrayerTimeResult> get prayerTimesList {
    if (prayerTimes == null) {
      return null;
    }
    // fajr, sunrise, dhuhr, asr, maghrib, isha
    return _addYearContent()
        .map((e) => PrayerTimeResult(
            date: e["date"],
            fajr: e["fajr"],
            syuruk: e["sunrise"],
            dhuhr: e["dhuhr"],
            asr: e["asr"],
            maghrib: e["maghrib"],
            isha: e["isha"]))
        .toList();

    // return PrayerTimeResult(
    //     fajr: DateFormat("HH:mm:ss")
    //         .format(prayerTimes.timeForPrayer(Prayer.values[1])),
    //     syuruk: DateFormat("HH:mm:ss")
    //         .format(prayerTimes.timeForPrayer(Prayer.values[2])),
    //     dhuhr: DateFormat("HH:mm:ss")
    //         .format(prayerTimes.timeForPrayer(Prayer.values[3])),
    //     asr: DateFormat("HH:mm:ss")
    //         .format(prayerTimes.timeForPrayer(Prayer.values[4])),
    //     maghrib: DateFormat("HH:mm:ss")
    //         .format(prayerTimes.timeForPrayer(Prayer.values[5])),
    //     isha: DateFormat("HH:mm:ss")
    //         .format(prayerTimes.timeForPrayer(Prayer.values[6])));
  }

  PrayerTimeResult prayerTimesListWithPrayerTimes(PrayerTimes valueTimes) {
    if (valueTimes == null) {
      return null;
    }
    return PrayerTimeResult(
        fajr: DateFormat("HH:mm:ss")
            .format(valueTimes.timeForPrayer(Prayer.values[1])),
        syuruk: DateFormat("HH:mm:ss")
            .format(valueTimes.timeForPrayer(Prayer.values[2])),
        dhuhr: DateFormat("HH:mm:ss")
            .format(valueTimes.timeForPrayer(Prayer.values[3])),
        asr: DateFormat("HH:mm:ss")
            .format(valueTimes.timeForPrayer(Prayer.values[4])),
        maghrib: DateFormat("HH:mm:ss")
            .format(valueTimes.timeForPrayer(Prayer.values[5])),
        isha: DateFormat("HH:mm:ss")
            .format(valueTimes.timeForPrayer(Prayer.values[6])));
  }

  PrayerTimeResult changeToPrayerTimeResult(PrayerTimes valueTimes) {
    if (prayerTimes == null) {
      return null;
    }
    return PrayerTimeResult(
        fajr: DateFormat("HH:mm:ss").format(
            localChangeUTCtoLocal(valueTimes.timeForPrayer(Prayer.values[1]))),
        syuruk: DateFormat("HH:mm:ss").format(
            localChangeUTCtoLocal(valueTimes.timeForPrayer(Prayer.values[2]))),
        dhuhr: DateFormat("HH:mm:ss").format(
            localChangeUTCtoLocal(valueTimes.timeForPrayer(Prayer.values[3]))),
        asr: DateFormat("HH:mm:ss").format(
            localChangeUTCtoLocal(valueTimes.timeForPrayer(Prayer.values[4]))),
        maghrib: DateFormat("HH:mm:ss").format(
            localChangeUTCtoLocal(valueTimes.timeForPrayer(Prayer.values[5]))),
        isha: DateFormat("HH:mm:ss").format(
            localChangeUTCtoLocal(valueTimes.timeForPrayer(Prayer.values[6]))));
  }

  CalculationParameters get params {
    Map<String, dynamic> paryerConvention =
        PRAYER_CONVENTIONS[paryerConventionsIndex];
    var parames = CalculationParameters(
        fajrAngle: paryerConvention["value"]["Fajr"],
        ishaAngle: paryerConvention["value"]["Isha"] ?? 0.0,
        ishaInterval: paryerConvention["value"]["ishaInterval"] ?? 0);
    if (adjustments != null) {
      parames.adjustments = adjustments;
    }
    if (methodAdjustments != null) {
      parames.methodAdjustments = methodAdjustments;
    }
    if (madhab != null) {
      parames.madhab = madhab;
    }
    if (highLatitudeRule != null) {
      parames.highLatitudeRule = highLatitudeRule;
    }
    return parames;
  }
}

String timeForPrayerToString(Prayer prayer) {
  switch (prayer) {
    case Prayer.fajr:
      return tr("Fajr");
    case Prayer.sunrise:
      return tr("Sunrise");
    case Prayer.dhuhr:
      return tr("Dhuhr");
    case Prayer.asr:
      return tr("Asr");
    case Prayer.maghrib:
      return tr("Maghrib");
    case Prayer.isha:
      return tr("Isha");
    case Prayer.none:
    default:
      return "";
  }
}

Prayer localCurrentPrayerTime(PrayerTimes prayerTimes) {
  var _currentPrayer;
  Prayer.values.skipWhile((value) => value == Prayer.none).forEach((element) {
    var timeString = prayerTimes.timeForPrayer(element).toString();
    timeString = timeString.substring(0, timeString.length - 4);
    if (DateFormat("yyyy-MM-dd HH:mm:ss")
                .parse(timeString)
                .millisecondsSinceEpoch -
            DateTime.now().millisecondsSinceEpoch -
            1800000 <=
        0) {
      _currentPrayer = element;
    }
  });
  return _currentPrayer ?? Prayer.none;
}

Prayer localNextPrayerTime(PrayerTimes prayerTimes) {
  var _nextPrayer;
  Prayer.values.skipWhile((value) => value == Prayer.none).forEach((element) {
    // if (prayerTimes.timeForPrayer(element).toLocal().millisecondsSinceEpoch -
    //         DateTime.now().millisecondsSinceEpoch <=
    //     0) {
    //   _nextPrayer = element;
    // }
    var timeString = prayerTimes.timeForPrayer(element).toString();
    timeString = timeString.substring(0, timeString.length - 4);
    if (DateFormat("yyyy-MM-dd HH:mm:ss")
                .parse(timeString)
                .millisecondsSinceEpoch -
            DateTime.now().millisecondsSinceEpoch <=
        0) {
      _nextPrayer = element;
    }
  });
  if (_nextPrayer == null) {
    return Prayer.fajr;
  }
  if (_nextPrayer == Prayer.values.last) {
    return Prayer.none;
  }
  return Prayer.values[Prayer.values.indexOf(_nextPrayer) + 1];
}

DateTime localChangeUTCtoLocal(DateTime time) {
  var timeString = time.toString();
  timeString = timeString.substring(0, timeString.length - 4);
  return DateFormat("yyyy-MM-dd HH:mm:ss").parse(timeString);
}
