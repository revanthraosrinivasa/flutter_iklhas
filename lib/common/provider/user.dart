import 'package:flutter/cupertino.dart';
import 'package:muslim/common/core/storage.dart';
import 'package:muslim/common/helper/helper.dart';
import 'package:muslim/modals/login.dart';

class UserModel extends ChangeNotifier {
  UserModel();
  Token token = StorageHelper().getJSON(USER_TOKEN) == null
      ? null
      : Token.fromJson(StorageHelper().getJSON(USER_TOKEN));
  User user;
  int _avatarIndex;
  String _fullName;
  String _birthday;
  String _gender;
  bool _hasPwd = false;
  // bool _isAutoSetting = true;
  String pushToken;

  bool get hasPwd {
    return _hasPwd;
  }

  set hasPwd(bool value) {
    _hasPwd = value;
    notifyListeners();
  }

  void changeToken(Token value) {
    token = value;
    notifyListeners();
  }

  void changeUser(User value) {
    user = value;
    avatarIndex = value.avatarId != null &&
            value.avatarId.length != 0 &&
            int.parse(value.avatarId) != null
        ? int.parse(value.avatarId) - 1
        : 0;
    notifyListeners();
  }

  List<bool> _alarmClocks;
  List<bool> get alarmClock {
    if (_alarmClocks == null) {
      List<dynamic> userClocks = StorageHelper().getJSON(ALARM_CLOCK);
      List<bool> clocks = userClocks != null
          ? userClocks.cast<bool>()
          : [true, false, true, true, true, true];
      _alarmClocks = clocks;
    }
    return _alarmClocks;
  }

  set alarmClock(List<bool> value) {
    _alarmClocks = value;
    StorageHelper().setJSON(ALARM_CLOCK, value);
    notifyListeners();
  }

  void changeAlarmClock(int index, bool value) {
    alarmClock[index] = value;
    alarmClock = _alarmClocks;
    // notifyListeners();
  }

  List<int> _alarmAdvanceOfTime;

  List<int> get alarmAdvanceOfTime {
    if (_alarmAdvanceOfTime == null) {
      List<dynamic> times = StorageHelper().getJSON(ALARM_CLOCK_TIME);
      _alarmAdvanceOfTime =
          times != null ? times.cast<int>() : [0, 0, 0, 0, 0, 0];
    }
    return _alarmAdvanceOfTime;
  }

  set alarmAdvanceOfTime(List<int> value) {
    _alarmAdvanceOfTime = value;
    StorageHelper().setJSON(ALARM_CLOCK_TIME, value);
    notifyListeners();
  }

  void changeAlarmAdvanceOfTime(int index, int value) {
    alarmAdvanceOfTime[index] = value;
    alarmAdvanceOfTime = _alarmAdvanceOfTime;
    // notifyListeners();
  }

  bool _isChoiceCity = StorageHelper().getBool(USER_CHOICE_CITY);

  bool get isChoiceCity {
    return _isChoiceCity;
  }

  set isChoiceCity(value) {
    _isChoiceCity = value;
    notifyListeners();
  }

  int get avatarIndex {
    if (user == null || user.avatarId == null) {
      return 0;
    }
    return int.parse(user.avatarId) != null ? int.parse(user.avatarId) - 1 : 0;
  }

  set avatarIndex(int value) {
    _avatarIndex = value;
    notifyListeners();
  }

  get fullName {
    if (_fullName != null) {
      return _fullName;
    }
    if (user != null) {
      _fullName = user.fullName ?? "";
    } else {
      _fullName = "";
    }
    return _fullName;
  }

  set fullName(String value) {
    _fullName = value;
    notifyListeners();
  }

  get birthday {
    if (_birthday != null) {
      return _birthday;
    }
    if (user != null) {
      _birthday = user.birthday ?? "";
    } else {
      _birthday = "";
    }
    return _birthday;
  }

  set birthday(String value) {
    _birthday = value;
    notifyListeners();
  }

  get gender {
    if (_gender != null) {
      return _gender;
    }
    if (user != null) {
      _gender = user.gender ?? "";
    } else {
      _gender = "";
    }
    return _gender;
  }

  set gender(String value) {
    _gender = value;
    notifyListeners();
  }

  factory UserModel.fromJson(Map<String, dynamic> json) => UserModel();

  Map<String, dynamic> toJson() => {};
}
