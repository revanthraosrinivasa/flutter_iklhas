import 'package:connectivity/connectivity.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:muslim/common/core/storage.dart';
import 'package:muslim/common/helper/helper.dart';
import 'package:muslim/common/tools/timeChange.dart';
import 'dart:convert';
import 'package:muslim/modals/times.dart';

enum UserChooseGraphicsSettings { auto, high, low }

class AppSettingModel extends ChangeNotifier {
  bool _homeIsLogin;
  AppPrayerTimeSetting _appSetting;

  ConnectivityResult _netWorkState;
  UserChooseGraphicsSettings _userChoose = UserChooseGraphicsSettings.auto;

  UserChooseGraphicsSettings get userChooseGraphicsSettngs {
    return _userChoose;
  }

  set userChooseGraphicsSettngs(UserChooseGraphicsSettings value) {
    _userChoose = value;
    notifyListeners();
  }

  ConnectivityResult get networkState {
    return _netWorkState ?? ConnectivityResult.mobile;
  }

  set networkState(ConnectivityResult value) {
    _netWorkState = value;
    notifyListeners();
  }

  String get region {
    var value = StorageHelper().getString(REGION);
    return value ?? "";
  }

  set region(String value) {
    StorageHelper().setString(REGION, value);
    notifyListeners();
  }

  String get malaysiaPlaceCode {
    var value = StorageHelper().getString(MALAYSIACODE);
    return value ?? "";
  }

  set malaysiaPlaceCode(String code) {
    StorageHelper().setString(MALAYSIACODE, code);
    notifyListeners();
  }

  List<PrayerTimeResult> _malaysiaPrayerTimes;

  List<PrayerTimeResult> get malaysiaPrayerTimes {
    return _malaysiaPrayerTimes;
  }

  List<PrayerTimeResult> fetchDateListWithParams(
      DateTime startTime, int offset) {
    if (malaysiaPrayerTimes == null || malaysiaPrayerTimes.length == 0) {
      return null;
    }
    int startIndex = timeListInYear(startTime) - 1;
    int endIndex = startIndex + offset;
    if (endIndex > malaysiaPrayerTimes.length) {
      endIndex = malaysiaPrayerTimes.length;
    }

    List<PrayerTimeResult> subs =
        malaysiaPrayerTimes.sublist(startIndex, endIndex);
    return subs;
  }

  set malaysiaPrayerTimes(List<PrayerTimeResult> value) {
    _malaysiaPrayerTimes = value;
    notifyListeners();
  }

  bool get isAutoSetting {
    if (appSetting == null) {
      return false;
    }
    return appSetting.autoSetting;
  }

  bool get homeIsLogin {
    bool homeIsLogin = true;
    var tokenInfo = StorageHelper().getJSON(USER_TOKEN);
    if (tokenInfo != null) {
      // Token token = Token.fromJson(tokenInfo);
      dynamic lastLoginTime = StorageHelper().getString(LAST_LOGIN_TIME);
      if (lastLoginTime != null) {
        DateTime lastTime =
            DateFormat("yyyy-MM-dd HH:mm:ss").parse(lastLoginTime as String);
        DateTime nowTime = DateTime.now();
        if (((nowTime.millisecondsSinceEpoch -
                    lastTime.millisecondsSinceEpoch) ~/
                1000 ~/
                60 ~/
                60 ~/
                24) <
            30) {
          homeIsLogin = false;
        }
      }
    }
    _homeIsLogin = homeIsLogin;
    return _homeIsLogin;
  }

  set homeIsLogin(bool value) {
    _homeIsLogin = value;
    notifyListeners();
  }

  AppPrayerTimeSetting get appSetting {
    String userId = StorageHelper().getString(USERID);
    if (userId == null) {
      return null;
    }
    if (_appSetting == null) {
      List<dynamic> values = StorageHelper().getJSON(APPSETTINGS);
      if (values == null) {
        return null;
      }
      List<AppPrayerTimeSetting> results = values
          .map((e) => AppPrayerTimeSetting.fromJson(e))
          .where((element) => element.userId == userId)
          .toList();
      if (results.length == 0) {
        return null;
      }
      return results.first;
    }
    return _appSetting;
  }

  set appSetting(AppPrayerTimeSetting setting) {
    List<dynamic> values = StorageHelper().getJSON(APPSETTINGS);
    if (values == null) {
      StorageHelper().setJSON(APPSETTINGS, [setting]);
    } else {
      List<AppPrayerTimeSetting> results =
          values.map((e) => AppPrayerTimeSetting.fromJson(e)).toList();
      var index =
          results.indexWhere((element) => element.userId == setting.userId);
      if (index != -1) {
        results.removeAt(index);
      }
      results.add(setting);
      StorageHelper().setJSON(APPSETTINGS, results);
    }
    _appSetting = setting;
    notifyListeners();
  }
}

AppPrayerTimeSetting activitiesFromJson(String str) =>
    AppPrayerTimeSetting.fromJson(json.decode(str));

class AppPrayerTimeSetting {
  String userId;
  String locationCity;
  String locationCountry;
  bool autoSetting;
  bool userAutoSetting;
  int conventions;
  List<dynamic> corrections;
  int calculation;
  int highLatitude;
  AppPrayerTimeSetting(
      {this.userId,
      this.locationCity,
      this.locationCountry,
      this.autoSetting,
      this.userAutoSetting,
      this.conventions,
      this.corrections,
      this.calculation,
      this.highLatitude});

  factory AppPrayerTimeSetting.fromJson(Map<String, dynamic> data) =>
      AppPrayerTimeSetting(
          userId: data["userId"],
          locationCity: data["locationCity"],
          locationCountry: data["locationCountry"],
          autoSetting: data["autoSetting"],
          conventions: data["conventions"],
          corrections: data["corrections"],
          calculation: data["calculation"],
          highLatitude: data["highLatitude"],
          userAutoSetting: data["userAutoSetting"]);

  Map<String, dynamic> toJson() => {
        "userId": userId,
        "locationCity": locationCity,
        "locationCountry": locationCountry,
        "autoSetting": autoSetting,
        "conventions": conventions,
        "corrections": corrections,
        "calculation": calculation,
        "highLatitude": highLatitude,
        "userAutoSetting": userAutoSetting
      };
}
