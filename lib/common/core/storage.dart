import 'dart:ffi';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

/// 用户 - 配置信息
const String STORAGE_USER_PROFILE_KEY = 'user_profile';

const String USER_AVATOR_URL =
    "https://musilim-test.oss-cn-hongkong.aliyuncs.com/avatar/";

const String USER_TOKEN = "token";

const String ENVIRONMENT = "environment";

const String IsDebug = "isDebug";

const String REGION = "region";

const String MALAYSIACODE = "malaysia place code";

// const String MYPRAYERTIMES = "my prayer times";

const String LAST_LOGIN_TIME = "last login time";

/// 设备是否第一次打开
const String STORAGE_DEVICE_ALREADY_OPEN_KEY = 'device_already_open';

/// 位置
const String POSITION = "position";

/// 时区
const String TIMEZONE = "time zone";

/// 时区id
const String TIMEZONEID = "time zone id";

/// 用户的本地城市
const String LOCAL_CITY = "local_city";

/// 用户是否自己选择城市(true： 不用谷歌获取，本地获取就可以)
const String USER_CHOICE_CITY = "user_choice_city";

/// 用户本地祈祷时间 是否打开通知设置
const String ALARM_CLOCK = "alarm clock setting";

/// 用户本地祈祷时间 提前多少分钟
const String ALARM_CLOCK_TIME = "alarm clock time";

/// PrayerTimes HighLatitude Adjustment
///
const String PRAYER_TIMES_SETTING = "prayer times setting";

/// 当前显示的城市
const String PRAYER_TIMES_SETTING_CITY = PRAYER_TIMES_SETTING + " city";

/// 当前的坐标
const String PRAYER_TIMES_SETTING_COORDINATES =
    PRAYER_TIMES_SETTING + " Coordinates";

/// 当前的方法
const String PRAYER_TIMES_SETTING_METHOD = PRAYER_TIMES_SETTING + " method";

/// madhab
const String PRAYER_TIMES_SETTING_MADHAB = PRAYER_TIMES_SETTING + " madhab";

/// highLatitudeRule
const String PRAYER_TIMES_SETTING_HIGHLATITUDERULE =
    PRAYER_TIMES_SETTING + " highLatitudeRule";

/// adjustments
const String PRAYER_TIMES_SETTING_ADJUSTMENTS =
    PRAYER_TIMES_SETTING + " adjustments";

///methodAdjustments
const String PRAYER_TIMES_SETTING_METHODADJUSTMENTS =
    PRAYER_TIMES_SETTING + " methodAdjustments";

/// paryerConventionsIndex
const String PRAYER_TIMES_SETTING_INDEX =
    PRAYER_TIMES_SETTING + " paryerConventionsIndex";

/// user ID
const String USERID = "user id";

/// app settings
const String APPSETTINGS = "app settings";

const String AUTOSETTINGCUNTRY = "Kuala Lumpur";

/// 列表的请求数量
const int LIMIT = 20;

const List<String> HIGHLATITUDEADJUSTMENT = [
  "Middle of the night",
  "1/7 of the night",
  "Angle-based method"
];

const List<String> MADHAB = ["standard(Shafi,Malki,Hanbali)", "hanafi"];

const List<Map<String, dynamic>> PRAYER_CONVENTIONS = [
  {
    "name": "Algerian Minister of Religious Affairs and Wakfs",
    "value": {"Fajr": 18.0, "Isha": 17.0}
  },
  {
    "name": "Diyanet işleri Başkanlığı",
    "value": {"Fajr": 18.0, "Isha": 17.0}
  },
  {
    "name": "Egyptian General Authority",
    "value": {"Fajr": 19.5, "Isha": 17.5}
  },
  {
    "name": "Egyption General Authority (Bis)",
    "value": {"Fajr": 20.0, "Isha": 18.0}
  },
  {
    "name": "Fixed Isha Angle Interval",
    "value": {"Fajr": 19.5, "Isha": 0.0, "ishaInterval": 90}
  },
  {
    "name": "France - Angle 15°",
    "value": {"Fajr": 15.0, "Isha": 15.0}
  },
  {
    "name": "France - Angle 18°",
    "value": {"Fajr": 18.0, "Isha": 18.0}
  },
  {
    "name": "Islamic university, Karachi",
    "value": {"Fajr": 18.0, "Isha": 18.0}
  },
  {
    "name": "JAKIM(Jabatan Kemajuan Islam Malaysia)",
    "value": {"Fajr": 20.0, "Isha": 18.0}
  },
  {
    "name": "MUIS (Majlis Ugama Islam Singapura)",
    "value": {"Fajr": 20.0, "Isha": 18.0}
  },
  {
    "name": "Muslim World League (MWL)",
    "value": {"Fajr": 18.0, "Isha": 17.0}
  },
  {
    "name": "Musulmans de France (ex-UOIF) - Angle 12°",
    "value": {"Fajr": 12.0, "Isha": 12.0}
  },
  {
    "name": "North America (ISNA)",
    "value": {"Fajr": 15.0, "Isha": 15.0}
  },
  {
    "name": "SIHAT/KEMENAG (Kementerian Agama RI)",
    "value": {"Fajr": 20.0, "Isha": 18.0}
  },
  {
    "name": "Shia Ithna Ashari(Jafari)",
    "value": {"Fajr": 16.0, "Isha": 14.0}
  },
  {
    "name": "Tunisian Ministry of Religious Affairs",
    "value": {"Fajr": 18.0, "Isha": 18.0}
  },
  {
    "name": "UAE General Authority of Islamic Affairs And Endowments",
    "value": {"Fajr": 19.5, "Isha": 0.0, "ishaInterval": 90}
  },
  {
    "name": "Umm AI-Qura, Makkah",
    "value": {"Fajr": 18.5, "Isha": 0.0, "ishaInterval": 90}
  },
];

/// 用户头像List
final List User_Avatars = [
  // Image.asset(
  //   "assets/images/profile_avatar_1.png",
  //   fit: BoxFit.cover,
  //   width: 60.0,
  //   height: 60.0,
  // ),
  Image.asset(
    "assets/images/profile_avatar_1.png",
    fit: BoxFit.cover,
    width: 60.0,
    height: 60.0,
  ),
  Image.asset(
    "assets/images/profile_avatar_2.png",
    fit: BoxFit.cover,
    width: 60.0,
    height: 60.0,
  ),
  Image.asset(
    "assets/images/profile_avatar_3.png",
    fit: BoxFit.cover,
    width: 60.0,
    height: 60.0,
  ),
];

/// 选择的语言
String LOCALE = "locale";

/// OSS  图片解析 缩略图
String OSS_IMAGE_SCALE = "?x-oss-process=image/resize,h_600,w_600";

String OSS_IMAGE_SCALE_1 = "?x-oss-process=image/resize,h_500,w_500";

/// OSS 图片解析 普通模式
String OSS_IMAGE_SCALE_NORMAL = "?x-oss-process=image/resize,h_1200,w_1200";

//==========================================
/// 请求的响应码
final String SUCCESS = "000000";

/// 验证码错误
final String AuthCodeError = "100005";

/// 旧密码错误
final String OldCodeError = "100007";

/// 需要提示重新操作
final String NEED_TIP = "000010";

final String PHONE_EXITES = "100002";

/// 需要重新登录
final String RELOGIN = "000020";

/// 需要重新认证
final String REAUTHON = "401";

/// 支持注册的国家
List<Map<String, String>> countries = [
  {"country": "MY", "prefix": "60", "index": "0"},
  {"country": "CN", "prefix": "86", "index": "1"},
];

/// 回教日历
List<String> hijriList = [
  tr("hijri_1"),
  tr("hijri_2"),
  tr("hijri_3"),
  tr("hijri_4"),
  tr("hijri_5"),
  tr("hijri_6"),
  tr("hijri_7"),
  tr("hijri_8"),
  tr("hijri_9"),
  tr("hijri_10"),
  tr("hijri_11"),
  tr("hijri_12")
];

/// 密码正则判断
const String PasswordReg = r"^(?=.*?[a-zA-Z])(?=.*?[0-9]).{8,16}$";
