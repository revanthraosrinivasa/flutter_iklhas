import 'package:flutter/material.dart';
import 'package:muslim/pages/community/detailActivities.dart';
import 'package:muslim/pages/community/detailFeed.dart';
import 'package:muslim/pages/community/galleryPage.dart';
import 'package:muslim/pages/community/nearlyMerchant.dart';
import 'package:muslim/pages/home/qurbanAndAqiqah.dart';
import 'package:muslim/pages/home/sadaqah.dart';
import 'package:muslim/pages/home/articles.dart';
import 'package:muslim/pages/community/details.dart';
import 'package:muslim/pages/community/directionMap.dart';
import 'package:muslim/pages/login/forget.dart';
import 'package:muslim/pages/login/introduce.dart';
import 'package:muslim/pages/login/privacy.dart';
import 'package:muslim/pages/login/pswLogin.dart';
import 'package:muslim/pages/login/teams.dart';
import 'package:muslim/pages/mine/feedback.dart';
import 'package:muslim/pages/login/login.dart';
import 'package:muslim/pages/main.dart';
import 'package:muslim/pages/mine/about.dart';
import 'package:muslim/pages/mine/follows.dart';
import 'package:muslim/pages/mine/general.dart';
import 'package:muslim/pages/mine/graphics.dart';
import 'package:muslim/pages/mine/profile/fullname.dart';
import 'package:muslim/pages/mine/profile/main.dart';
import 'package:muslim/pages/mine/profile/photo.dart';
import 'package:muslim/pages/mine/security/captcha.dart';
import 'package:muslim/pages/mine/security/email.dart';
import 'package:muslim/pages/mine/security/main.dart';
import 'package:muslim/pages/mine/security/phone.dart';
import 'package:muslim/pages/mine/security/resetPwd.dart';
import 'package:muslim/pages/mine/security/setpass.dart';
import 'package:muslim/pages/mine/settting.dart';
import 'package:muslim/pages/prayerTime/alarm.dart';
import 'package:muslim/pages/prayerTime/asrCalculation.dart';
import 'package:muslim/pages/prayerTime/conventions.dart';
import 'package:muslim/pages/prayerTime/highLatitudeRule.dart';
import 'package:muslim/pages/prayerTime/locations.dart';
import 'package:muslim/pages/prayerTime/main.dart';
import 'package:muslim/pages/prayerTime/manual.dart';
import 'package:muslim/pages/prayerTime/region.dart';
import 'package:muslim/pages/prayerTime/setting.dart';
import 'package:muslim/pages/login/profile.dart';
import 'package:muslim/pages/prayerTime/timetable.dart';
import 'package:muslim/pages/qibla/qiblaDirection.dart';
import 'package:muslim/pages/widgets/muslim_webview.dart';

Map<String, WidgetBuilder> routers = {
  "followed": (context) => FollowsPage(),
  "settings": (context) => SettingPage(),
  "store_detail": (context) =>
      StoreDetailPage(storeId: ModalRoute.of(context).settings.arguments),
  "login": (context) => LoginPage(),
  "main": (context) => MainPage(),
  "user_profile": (context) => UserProfilePage(),
  "qibla_direction": (context) => QiblaDirectionPage(),
  "article": (context) => ArticlesPage(),
  "feedback": (context) => FeedBackPage(),
  "prayer_time": (context) => PrayerTimesPage(),
  "prayer_time_setting": (context) => PrayerTimeSettingPage(),
  "search_location": (context) => SearchLocationPage(),
  "high_latitude_page": (context) => HighLatitudeRulePage(),
  "asr_calculation": (context) => AsrCalculationPage(),
  "manual_correction": (context) => ManualCorrectionsPage(),
  "conventions": (context) => ConventionPage(),
  "alarmClockPage": (context) {
    dynamic params = ModalRoute.of(context).settings.arguments;
    return AlarmClockPage(
        prayerIndex: params["index"], prayerTimes: params["prayerTimes"]);
  },
  "my_profile": (context) => MyProfile(),
  "profile_photo": (context) => ProfilePhoto(),
  "profile_fullname": (context) =>
      ProfileFullName(fullName: ModalRoute.of(context).settings.arguments),
  "profile_account_security": (context) => AccountSecurity(),
  "change_phone_number": (context) =>
      ChangePhoneNumber(phoneNumber: ModalRoute.of(context).settings.arguments),
  "change_phone_number_next": (context) => ChangePhoneNumberNext(
        newPhoneNumber: ModalRoute.of(context).settings.arguments,
      ),
  "change_email": (context) => ChangeEmailPage(
        email: ModalRoute.of(context).settings.arguments,
      ),
  "about": (context) => AboutPage(),
  "direction_map": (context) {
    Map<String, dynamic> objects = ModalRoute.of(context).settings.arguments;
    return DirectionMap(
      endLocationLat: objects["latitude"],
      endLocationLon: objects["longitude"],
      endLocationTitle: objects["location"],
    );
  },
  "teams_policy": (context) => TeamsPolicy(),
  "privacy_policy": (context) => PolicyPolicy(),
  "sadaqah": (context) => SadaqahPage(),
  "prayer_timer": (context) => MonthTimeTablePage(),
  "detail_feeds": (context) {
    Map<String, String> arguments = ModalRoute.of(context).settings.arguments;
    return DetailFeedsPage(
        merchantId: arguments["id"], previewUrl: arguments["previewUrl"]);
  },
  "detail_activities": (context) {
    Map<String, String> arguments = ModalRoute.of(context).settings.arguments;
    return DetailActivitiesPage(
        merchantId: arguments["id"], previewUrl: arguments["previewUrl"]);
  },
  "nearly_mosque": (context) =>
      NearlyMerchantsPage(position: ModalRoute.of(context).settings.arguments),
  "gallery": (context) =>
      GalleryPage(photos: ModalRoute.of(context).settings.arguments),
  "aqiqah_qurban": (context) => QurbanAqiqahPage(),
  "introduce": (context) => IntroducePage(),
  "web_view": (context) {
    Map<String, String> arguments = ModalRoute.of(context).settings.arguments;
    return MuslimWebView(url: arguments["url"], title: arguments["title"]);
  },
  "region": (context) => RegionPage(),
  "general": (context) => GeneralPage(),
  "graphics_setting": (context) => GraphicsSettingPage(),
  "send_code": (context) => CheckCaptchaPage(),
  "set_password": (context) {
    Map<String, dynamic> arguments = ModalRoute.of(context).settings.arguments;

    return SetPasswordPage(
        tempToken: arguments["tempToken"],
        phoneNumber: arguments["phoneNumber"],
        phonePrefix: arguments["phonePrefix"],
        type: arguments["type"]);
  },
  "forget_password": (context) => ForgetPasswordPage(),
  "reset_password": (context) => ResetPasswordPage()
};
