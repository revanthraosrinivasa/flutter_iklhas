import 'dart:io';

import 'package:device_info/device_info.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:geolocator/geolocator.dart';
import 'package:muslim/common/core/storage.dart';
import 'package:muslim/common/helper/helper.dart';
import 'package:muslim/common/helper/location.dart';
import 'package:package_info/package_info.dart';
import 'package:muslim/common/provider/user.dart';
import 'package:muslim/common/helper/prayer_time.dart';

import 'package:muslim/common/provider/prayer.dart';
import 'package:muslim/common/provider/app.dart';

import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:timezone/timezone.dart' as tz;
import 'package:timezone/data/latest.dart' as tz;
import 'package:flutter_native_timezone/flutter_native_timezone.dart';
import 'package:muslim/common/api/normal_api.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:matomo/matomo.dart';

class Global {
  /// 是否 release
  static bool get isRelease => bool.fromEnvironment("dart.vm.product");

  /// 是否是 iOS
  static bool isIOS = Platform.isIOS;

  /// 是否是真机
  static bool isPhysicalDevice;

  /// android 设备信息
  static AndroidDeviceInfo androidDeviceInfo;

  /// ios 设备信息
  static IosDeviceInfo iosDeviceInfo;

  /// 包信息
  static PackageInfo packageInfo;

  /// 是否第一次打开
  static bool isFirstOpen = false;

  /// 是否登录
  static bool isLoginIn = true;

  /// 用户信息
  static UserModel userModol;

  /// 时间计算类
  static PrayerTimeViewModel viewModel;

  /// 祈祷时间设置
  static PrayerModel prayerModal;

  static LocationHelper locationHelper;

  static NormalApi normalApi;

  static AppSettingModel appSettingModal;

  static FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      FlutterLocalNotificationsPlugin();

  static Future init() async {
    // 运行初始
    WidgetsFlutterBinding.ensureInitialized();

    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp] //仅支持竖屏
        );

    try {
      await MatomoTracker()
          .initialize(siteId: 4, url: 'https://matomo.cannaaa.com/piwik.php');
    } catch (error) {
      print("MatomoTracker initialize error :$error");
    }

    // 读取设备信息
    DeviceInfoPlugin deviceInfoPlugin = DeviceInfoPlugin();
    if (Global.isIOS) {
      Global.iosDeviceInfo = await deviceInfoPlugin.iosInfo;
      Global.isPhysicalDevice = iosDeviceInfo.isPhysicalDevice;
    } else {
      Global.androidDeviceInfo = await deviceInfoPlugin.androidInfo;
      Global.isPhysicalDevice = androidDeviceInfo.isPhysicalDevice;
    }

    // 包信息
    Global.packageInfo = await PackageInfo.fromPlatform();
    print("Global.packageInfo version: ${Global.packageInfo.version}");

    tz.initializeTimeZones();

    // 工具包初始化
    await StorageHelper.init();
    if (StorageHelper().getInt(ENVIRONMENT) == null ||
        Environment.values[StorageHelper().getInt(ENVIRONMENT)] ==
            Environment.production) {
      MatomoTracker().setOptOut(false);
    } else {
      MatomoTracker().setOptOut(true);
    }
    Global.normalApi = NormalApi();
    Global.userModol = UserModel();
    Global.viewModel = PrayerTimeViewModel();
    Global.locationHelper = LocationHelper();

    Global.prayerModal = PrayerModel();
    if (StorageHelper().getInt(TIMEZONE) != null) {
      Global.prayerModal.utcOffset = StorageHelper().getInt(TIMEZONE);
      tz.setLocalLocation(
          tz.getLocation(StorageHelper().getString(TIMEZONEID)));
    } else {
      final String currentTimeZone =
          await FlutterNativeTimezone.getLocalTimezone();
      tz.setLocalLocation(tz.getLocation(currentTimeZone));
      StorageHelper().setString(TIMEZONEID, currentTimeZone);
      StorageHelper().setInt(TIMEZONE,
          tz.getLocation(currentTimeZone).currentTimeZone.offset ~/ 3600000);
      Global.prayerModal.utcOffset =
          tz.getLocation(currentTimeZone).currentTimeZone.offset ~/ 3600000;
    }

    // await EasyLocalization.ensureInitialized();
    Global.appSettingModal = AppSettingModel();
    if (StorageHelper().getJSON(POSITION) != null) {
      Position position = Position.fromMap(StorageHelper().getJSON(POSITION));
      Global.prayerModal
          .changeToLocations(position.latitude, position.longitude);
    }

    isFirstOpen = !StorageHelper().getBool(STORAGE_DEVICE_ALREADY_OPEN_KEY);
    if (isFirstOpen) {
      StorageHelper().setBool(STORAGE_DEVICE_ALREADY_OPEN_KEY, true);
    }

    var _profileJSON = StorageHelper().getJSON(STORAGE_USER_PROFILE_KEY);
    if (_profileJSON != null) {}
  }

  static Future<bool> saveProfile() {}
}
