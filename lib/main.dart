import 'dart:async';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:muslim/common/core/storage.dart';
import 'package:muslim/common/helper/helper.dart';
import 'package:muslim/common/helper/logger.dart';
import 'package:muslim/common/provider/app.dart';
import 'package:muslim/common/tools/hexColor.dart';
import 'package:muslim/global.dart';
import 'package:muslim/pages/login/introduce.dart';
import 'package:muslim/pages/main.dart';
import 'package:sentry_flutter/sentry_flutter.dart';
import 'common/routes/main.dart';
import 'package:provider/provider.dart';

import 'package:connectivity/connectivity.dart';

void collectLog(String line) {
  print("collectLog: $line");
}

void reportErrorAndLog(FlutterErrorDetails details) async {
  // 上报错误
}

const Mqtt_Server = "post-cn-09k216qk00b.mqtt.aliyuncs.com";

const MqttToken =
    "LzMT+XLFl5s/YWJ/MlDz4t/Lq5HC1iGU1P28HAMaxYxn8aQbALNtml7QZKl9L9kP0P6f47PdSHSVpXeynY2NfujZwDUSzsd4X4qaD3n2TrAwHZ9zCRxdSowFu67o4llPYbz7MJWZDYC3DlW7gLEr3xWwlYRpHY628ff5P3HjyuvzD12lKBwTozhKOSZxhr49wDINiFxPeUXSjzwZgeNkBElzeSozsSruORtRFYh2ORCom1EMXXtsr1xpe+Cjq8A76JKy/mpf9DRYYXuUa9v3drDxPZsiv1geYuYY1M5I9hofCwssr44Br/dN2H38N6d/DP63yhtkqX9kRfF+Ee1AAg==";

const accessKey = "LTAI5tRT9hr44osTyCvxqC3D";
const instanceId = "post-cn-09k216qk00b";

FlutterErrorDetails makeDetails(Object obj, StackTrace stack) {
  return FlutterErrorDetails(exception: obj, stack: stack);
}

const MethodChannel platform =
    MethodChannel('dexterx.dev/flutter_local_notifications_example');

Future<void> main() async {
  FlutterError.onError = (FlutterErrorDetails details) {
    reportErrorAndLog(details);
  };
  runZonedGuarded(() async {
    await SentryFlutter.init(
      (options) {
        options.dsn =
            'http://25327a1229ac42c9a52fa55a42054a41@sentry.chatecdn.com/2';
      },
      appRunner: () => Global.init().then((value) => runApp(MultiProvider(
              providers: [
                ChangeNotifierProvider(create: (context) => Global.userModol),
                ChangeNotifierProvider(create: (context) => Global.prayerModal),
                ChangeNotifierProvider(
                    create: (context) => Global.appSettingModal),
              ],
              child: EasyLocalization(
                  child: MyApp(),
                  supportedLocales: [Locale('en'), Locale('my')],
                  startLocale: StorageHelper().getString(LOCALE) == null
                      ? Locale('en')
                      : Locale(StorageHelper().getString(LOCALE)),
                  path: 'assets/translations')))),
    );

    SystemUiOverlayStyle systemUiOverlayStyle =
        SystemUiOverlayStyle(statusBarColor: Colors.transparent);
    SystemChrome.setSystemUIOverlayStyle(systemUiOverlayStyle);
  }, (Object obj, StackTrace stack) {
    Sentry.captureException(obj, stackTrace: stack);
  });
}

class MyApp extends StatefulWidget {
  // This widget is the root of your application.
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        visualDensity: VisualDensity.adaptivePlatformDensity,
        primaryColor: Colors.white,
        accentColor: HexColor('67C1BF'),
        cursorColor: HexColor('67C1BF'),
        fontFamily: 'DM Sans',
        brightness: Brightness.light,
        pageTransitionsTheme: PageTransitionsTheme(
          builders: {
            TargetPlatform.android: CupertinoPageTransitionsBuilder(),
            TargetPlatform.iOS: CupertinoPageTransitionsBuilder(),
          },
        ),
      ),
      localizationsDelegates: context.localizationDelegates,
      supportedLocales: context.supportedLocales,
      locale: context.locale,
      home: MainLuanchPage(),
      routes: routers,
    );
  }
}

class MainLuanchPage extends StatefulWidget {
  @override
  _MainLuanchPageState createState() => _MainLuanchPageState();
}

class _MainLuanchPageState extends State<MainLuanchPage> {
  StreamSubscription<ConnectivityResult> subscription;
  Connectivity _connectivity = Connectivity();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      AppSettingModel appSettingModel =
          Provider.of<AppSettingModel>(context, listen: false);
      _connectivity.checkConnectivity().then((value) {
        logger("----checkConnectivity $value");
        appSettingModel.networkState = value;
      }).catchError((error) {
        logger("-------check network error $error");
      });
      subscription = _connectivity.onConnectivityChanged
          .listen((ConnectivityResult result) {
        print("Connectivity result :$result");
        appSettingModel.networkState = result;
      });
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    subscription.cancel();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    AppSettingModel appModel =
        Provider.of<AppSettingModel>(context, listen: false);
    return Scaffold(
      body: appModel.homeIsLogin ? IntroducePage() : MainPage(),
    );
  }
}
