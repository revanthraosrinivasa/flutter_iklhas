import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:muslim/common/api/normal_api.dart';
import 'package:muslim/common/core/storage.dart';
import 'package:muslim/common/helper/helper.dart';
import 'package:muslim/common/helper/logger.dart';
import 'package:muslim/common/provider/user.dart';
import 'package:muslim/common/tools/dialog.dart';
import 'package:muslim/common/tools/filterNumber.dart';
import 'package:muslim/common/tools/hexColor.dart';
import 'package:muslim/common/tools/reForce.dart';
import 'package:muslim/common/tools/snackBar.dart';
import 'package:muslim/global.dart';
import 'package:muslim/modals/login.dart';
import 'package:muslim/pages/widgets/muslim_title.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:provider/provider.dart';
import 'package:sentry_flutter/sentry_flutter.dart' as SentryUser;

class PassWordLogin extends StatefulWidget {
  PassWordLogin({Key key, this.callback}) : super(key: key);

  final VoidCallback callback;

  @override
  _PasswordLoginState createState() => _PasswordLoginState();
}

class _PasswordLoginState extends State<PassWordLogin> {
  List<DropdownMenuItem<int>> _items = countries
      .map((value) => DropdownMenuItem(
            value: int.parse(value["index"]),
            child: Text(
              "+ ${value["prefix"]} ",
              style: TextStyle(color: HexColor('212124'), fontSize: 16.0),
            ),
          ))
      .toList();
  String _phoneNumber;
  String _password;
  int _countryCode = 0;
  String _phoneError;
  String testIso = "MY";
  String testPrifix = "60";
  bool _isFetch = false;

  TextEditingController _mobileNumberTC = TextEditingController();
  TextEditingController _mobilePassTC = TextEditingController();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    bool phoneErrorStatus = false;
    if (_phoneError != null && _phoneError.length != 0) {
      phoneErrorStatus = true;
    }

    bool isSubmitBtn = false;
    if (_phoneNumber != null &&
        _phoneNumber.length != 0 &&
        _password != null &&
        _password.length != 0) {
      isSubmitBtn = true;
    }

    if (StorageHelper().getInt(ENVIRONMENT) == null ||
        Environment.values[StorageHelper().getInt(ENVIRONMENT)] ==
            Environment.production) {
      _items = countries
          .where((element) => element["country"] != "CN")
          .map((value) => DropdownMenuItem(
                value: int.parse(value["index"]),
                child: Text(
                  "+ ${value["prefix"]} ",
                  style: TextStyle(color: HexColor('212124'), fontSize: 16.0),
                ),
              ))
          .toList();
    }
    return SingleChildScrollView(
        padding: EdgeInsets.only(left: 16.0, right: 16.0, top: 30.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              alignment: Alignment.center,
              margin: EdgeInsets.only(bottom: 20.0),
              child: Text("pass_login_sub",
                      style: TextStyle(
                          color: HexColor("212124"),
                          fontSize: 18.0,
                          fontWeight: FontWeight.w600))
                  .tr(),
            ),
            Container(
              alignment: Alignment.center,
              padding: EdgeInsets.all(12.0),
              margin: EdgeInsets.only(bottom: 12.0),
              decoration: BoxDecoration(
                  color: HexColor("F2F2F2"),
                  borderRadius: BorderRadius.circular(4.0),
                  border: Border.all(color: HexColor("EAEAEA"))),
              child: Text("pass_login_sub_1",
                  style: TextStyle(
                    color: HexColor("212124"),
                  )).tr(),
            ),
            Text(
              'login_mobile',
              style: TextStyle(
                  fontWeight: FontWeight.bold, color: HexColor('212124')),
            ).tr(),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 10.0),
              child: Row(
                children: [
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 10.0),
                    margin: EdgeInsets.only(right: 10.0),
                    decoration: BoxDecoration(
                        color: HexColor('F2F2F2'),
                        borderRadius: BorderRadius.all(Radius.circular(4.0))),
                    child: DropdownButton(
                      iconSize: 0,
                      value: _countryCode,
                      items: _items,
                      onChanged: _prifixPhoneChange,
                      underline: Container(),
                      itemHeight: 60.0,
                    ),
                  ),
                  Expanded(
                    child: TextField(
                      controller: _mobileNumberTC,
                      decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderSide: BorderSide(
                                  color: phoneErrorStatus
                                      ? Colors.red
                                      : Colors.transparent)),
                          focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                  color: phoneErrorStatus
                                      ? Colors.red
                                      : Colors.transparent)),
                          enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                  color: phoneErrorStatus
                                      ? Colors.red
                                      : Colors.transparent)),
                          hintText: tr('login_mobile_placehold'),
                          hintStyle: TextStyle(color: HexColor('BCBCBC')),
                          fillColor: HexColor('F2F2F2'),
                          filled: true),
                      keyboardType: TextInputType.number,
                      onChanged: _phoneNumberChange,
                    ),
                  )
                ],
              ),
            ),
            Builder(builder: (context) {
              return _phoneError != null && _phoneError.length != 0
                  ? Text(
                      _phoneError,
                      style: TextStyle(color: Colors.red),
                    )
                  : Container();
            }),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 8.0),
              child: Text("password",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: HexColor('212124')))
                  .tr(),
            ),
            Row(
              children: [
                Expanded(
                  child: TextField(
                    controller: _mobilePassTC,
                    obscureText: true,
                    decoration: InputDecoration(
                        border: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.transparent)),
                        focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.transparent)),
                        enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.transparent)),
                        hintText: tr('password_hint'),
                        hintStyle: TextStyle(color: HexColor('BCBCBC')),
                        fillColor: HexColor('F2F2F2'),
                        filled: true),
                    onChanged: _passNumberChange,
                  ),
                ),
              ],
            ),
            Container(
              height: 60.0,
              child: FlatButton(
                child: Text(tr("forget_password")),
                onPressed: () {
                  Navigator.of(context).pushNamed("forget_password");
                },
                textColor: HexColor("67C1BF"),
                padding: EdgeInsets.zero,
              ),
            ),
            Container(
              height: 50.0,
              width: MediaQuery.of(context).size.width,
              margin: EdgeInsets.only(top: 16),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(4.0))),
              child: FlatButton(
                onPressed: isSubmitBtn ? _submitForm : null,
                child: Text(tr("login_submit"),
                    style: TextStyle(fontSize: 16.0, color: Colors.white)),
                color: HexColor('67C1BF'),
                disabledColor: HexColor('67C1BF').withOpacity(0.5),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 20),
              height: 60.0,
              alignment: Alignment.center,
              child: FlatButton(
                  onPressed: () {
                    widget.callback();
                  },
                  child: Text(
                    tr("login_choice"),
                    style: TextStyle(
                        color: HexColor("67C1BF"),
                        fontSize: 16.0,
                        fontWeight: FontWeight.bold),
                  )),
            ),
            Container(
              alignment: Alignment.center,
              margin: EdgeInsets.only(top: 120.0),
              child: Text(
                tr('login_subtitle1'),
                style:
                    TextStyle(color: Colors.black, fontWeight: FontWeight.w400),
              ),
            ),
            Container(
              height: 20,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  ButtonTheme(
                    padding: EdgeInsets.zero,
                    child: FlatButton(
                      onPressed: _onTermsClick,
                      child: Text(tr('login_user_agreenment'),
                          style: TextStyle(color: HexColor('67C1BF'))),
                      color: Colors.transparent,
                      padding: EdgeInsets.zero,
                    ),
                  ),
                  Text(" " + tr('login_and'),
                      style: TextStyle(color: Colors.black)),
                  ButtonTheme(
                    padding: EdgeInsets.zero,
                    height: 20.0,
                    child: FlatButton(
                      onPressed: _onPrivacyClick,
                      child: Text(tr('login_policy'),
                          style: TextStyle(color: HexColor('67C1BF'))),
                      padding: EdgeInsets.zero,
                      color: Colors.transparent,
                    ),
                  )
                ],
              ),
            ),
          ],
        ));
  }

  void _prifixPhoneChange(int value) {
    setState(() {
      _phoneError = "";
      _countryCode = value;
      testIso = countries[value]["country"];
      testPrifix = countries[value]["prefix"];
    });
    print("_prifixPhoneChange: $value");
  }

  void _phoneNumberChange(String value) {
    var phone = filterPhoneNumber(value);
    _mobileNumberTC.value = TextEditingValue(
        text: phone, selection: TextSelection.collapsed(offset: phone.length));
    setState(() {
      _phoneError = "";
      _phoneNumber = phone;
    });
  }

  void _passNumberChange(String value) {
    // var filterPassword = RegExp(PasswordReg).hasMatch(value);
    // logger("-----_passNumberChange-------$filterPassword");
    setState(() {
      _password = value;
    });
  }

  void _submitForm() async {
    removeAllForce(context);
    if (!checkNetworkState(context)) return;
    if (_isFetch) {
      return;
    }
    setState(() {
      _isFetch = true;
    });
    Map<String, dynamic> params = {
      "phoneNumber": _phoneNumber,
      "phonePrefix": testPrifix,
      "password": _password
    };
    showWaitingDialog(context, "");
    Global.normalApi.fetchLogin(params, type: 1).then((value) {
      setState(() {
        _isFetch = false;
      });
      Navigator.of(context).pop();
      if (value != null && value.errorCode == SUCCESS) {
        UserModel userModol = Provider.of<UserModel>(context, listen: false);
        userModol.changeToken(value.data.token);
        userModol.changeUser(value.data.user);
        SentryUser.Sentry.configureScope(
          (scope) => scope.user = SentryUser.User(
              id: value.data.user.id, username: value.data.user.fullName),
        );
        userModol.hasPwd = value.data.user.hasPwd;
        StorageHelper().setJSON(USER_TOKEN, value.data.token);
        StorageHelper().setString(USERID, value.data.user.id);
        if (value.data.firstLogin) {
          Navigator.of(context).pushNamed('user_profile');
        } else {
          Navigator.of(context)
              .pushNamedAndRemoveUntil("main", (route) => false);
        }
      } else {
        if (value != null && value.errorCode == OldCodeError) {
          if (mounted)
            showSnackBar(context, tr("login_password_error"), type: 1);
          return;
        }
        if (mounted)
          showSnackBar(
              context,
              value != null || value.msg != null
                  ? value.msg
                  : tr("network_error"),
              type: 1);
      }
    }).catchError((error) {
      setState(() {
        _isFetch = false;
      });
      Navigator.of(context).pop();
      if (mounted) showSnackBar(context, tr("network_error"), type: 1);
    });
  }

  void _onTermsClick() async {
    Navigator.of(context).pushNamed("teams_policy");
  }

  void _onPrivacyClick() async {
    Navigator.of(context).pushNamed("privacy_policy");
  }
}
