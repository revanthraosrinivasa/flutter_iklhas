import 'dart:async';

import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter_picker/flutter_picker.dart';
import 'package:muslim/common/api/normal_api.dart';
import 'package:muslim/common/core/storage.dart';
import 'package:muslim/common/helper/helper.dart';
import 'package:muslim/common/tools/hexColor.dart';
import 'package:muslim/global.dart';
import 'package:matomo/matomo.dart';

class IntroducePage extends StatefulWidget {
  @override
  _IntroduceState createState() => _IntroduceState();
}

class _IntroduceState extends State<IntroducePage> {
  int _fingleCount = 0;
  Timer _fireTimer;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  fireTimer() {
    if (_fireTimer != null && _fingleCount > 10) {
      _fireTimer.cancel();
      setState(() {
        _fireTimer = null;
      });
      showEnviromentPicker();
      return;
    }
    int count = 5;
    _fireTimer = Timer.periodic(Duration(milliseconds: 5000), (timer) {
      if (count < 0) {
        timer.cancel();
        timer = null;
        setState(() {
          _fingleCount = 0;
          _fireTimer = null;
        });
        return;
      }
      count--;
    });
  }

  showEnviromentPicker() {
    List<String> _list = ["debug", "test", "production"];
    new Picker(
        adapter: PickerDataAdapter(
          pickerdata: _list,
        ),
        cancelText: tr("cancelText"),
        confirmText: tr("confirmText"),
        // height: 280.0,
        itemExtent: 40.0,
        selecteds: [Global.normalApi.environment.index],
        onConfirm: (picker, values) {
          Global.normalApi.environment = Environment.values[values.first];
          StorageHelper().setInt(ENVIRONMENT, values.first);
          if (Environment.values[values.first] == Environment.production) {
            MatomoTracker().setOptOut(false);
          } else {
            MatomoTracker().setOptOut(true);
          }
        }).showModal(context);
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    if (_fireTimer != null) {
      _fireTimer.cancel();
      _fireTimer = null;
    }
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Material(
      child: Column(
        children: [
          Expanded(
            child: InkWell(
              onTap: () {
                setState(() {
                  _fingleCount += 1;
                });
                fireTimer();
              },
              child: Container(
                padding: EdgeInsets.only(bottom: 40.0),
                child: Image.asset(
                  "assets/images/introduce_logo.png",
                  width: 200,
                ),
                alignment: Alignment.bottomCenter,
              ),
            ),
            flex: 2,
          ),
          Expanded(
            child: Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    "welcome_title",
                    style: TextStyle(
                        color: HexColor("212124"),
                        fontSize: 20,
                        height: 1.3,
                        fontWeight: FontWeight.w500),
                  ).tr(),
                  Text(
                    "welcome_subtitle",
                    style: TextStyle(
                        color: HexColor("212124"),
                        fontSize: 16,
                        height: 1.25,
                        fontWeight: FontWeight.w400),
                  ).tr(),
                  Container(
                    padding:
                        EdgeInsets.symmetric(horizontal: 40.0, vertical: 20),
                    child: Text(
                      "welcome_content",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: HexColor("4C4C50"),
                          fontSize: 16,
                          height: 1.375,
                          fontWeight: FontWeight.w400),
                    ).tr(),
                  ),
                  Expanded(
                      child: Container(
                    padding:
                        EdgeInsets.only(bottom: 50.0, left: 16.0, right: 16.0),
                    child: Row(
                      children: [
                        Expanded(
                            child: ButtonTheme(
                          height: 44.0,
                          child: FlatButton(
                            onPressed: () {
                              Navigator.of(context).pushNamed("login");
                            },
                            child: Text("welcome_start",
                                    style: TextStyle(
                                        fontSize: 16.0, color: Colors.white))
                                .tr(),
                            color: HexColor('67C1BF'),
                            disabledColor: HexColor('67C1BF').withOpacity(0.5),
                          ),
                        ))
                      ],
                    ),
                    alignment: Alignment.bottomCenter,
                  ))
                ],
              ),
            ),
            flex: 3,
          )
        ],
      ),
    );
  }
}
