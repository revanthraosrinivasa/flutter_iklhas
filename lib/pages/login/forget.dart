import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:libphonenumber/libphonenumber.dart';
import 'package:muslim/common/api/error_network_handle.dart';
import 'package:muslim/common/core/storage.dart';
import 'package:muslim/common/helper/helper.dart';
import 'package:muslim/common/helper/logger.dart';
import 'package:muslim/common/provider/user.dart';
import 'package:muslim/common/tools/dialog.dart';
import 'package:muslim/common/tools/filterNumber.dart';
import 'package:muslim/common/tools/hexColor.dart';
import 'package:muslim/common/tools/reForce.dart';
import 'package:muslim/common/tools/snackBar.dart';
import 'package:muslim/global.dart';
import 'package:muslim/pages/widgets/muslim_title.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:provider/provider.dart';

class ForgetPasswordPage extends StatefulWidget {
  // _ForgetPasswordState createState() => _ForgetPasswordState();
  @override
  _ForgetPasswordState createState() => _ForgetPasswordState();
}

class _ForgetPasswordState extends State<ForgetPasswordPage> {
  List<DropdownMenuItem<int>> _items = countries
      .map((value) => DropdownMenuItem(
            value: int.parse(value["index"]),
            child: Text(
              "+ ${value["prefix"]} ",
              style: TextStyle(color: HexColor('212124'), fontSize: 16.0),
            ),
          ))
      .toList();

  String _phoneNumber;
  String _code;
  int _countryCode = 0;
  String _phoneError;
  String _codeError;
  String testIso = "MY";
  String testPrifix = "60";
  int _timerCount = 60;
  bool _isFetch = false;

  TextEditingController _mobileNumberTC = TextEditingController();

  TextEditingController _mobileCodeTC = TextEditingController();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    bool phoneErrorStatus = false;
    if (_phoneError != null && _phoneError.length != 0) {
      phoneErrorStatus = true;
    }

    bool isSubmitBtn = false;
    if (_phoneNumber != null &&
        _phoneNumber.length != 0 &&
        _code != null &&
        _code.length == 6) {
      isSubmitBtn = true;
    }
    // TODO: implement build
    return KeyboardDismissOnTap(
      child: Scaffold(
        appBar: AppBar(
            centerTitle: true,
            title: MuslimTitle(
              title: tr("forget_pwd_title"),
            )),
        body: SingleChildScrollView(
          padding: EdgeInsets.only(left: 16.0, right: 16.0, top: 40.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'login_mobile',
                style: TextStyle(
                    fontWeight: FontWeight.bold, color: HexColor('212124')),
              ).tr(),
              Padding(
                padding: EdgeInsets.symmetric(vertical: 10.0),
                child: Row(
                  children: [
                    Container(
                      padding: EdgeInsets.symmetric(horizontal: 10.0),
                      margin: EdgeInsets.only(right: 10.0),
                      decoration: BoxDecoration(
                          color: HexColor('F2F2F2'),
                          borderRadius: BorderRadius.all(Radius.circular(4.0))),
                      child: DropdownButton(
                        iconSize: 0,
                        value: _countryCode,
                        items: _items,
                        onChanged: _prifixPhoneChange,
                        underline: Container(),
                        itemHeight: 60.0,
                      ),
                    ),
                    Expanded(
                      child: TextField(
                        controller: _mobileNumberTC,
                        decoration: InputDecoration(
                            border: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: phoneErrorStatus
                                        ? Colors.red
                                        : Colors.transparent)),
                            focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: phoneErrorStatus
                                        ? Colors.red
                                        : Colors.transparent)),
                            enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: phoneErrorStatus
                                        ? Colors.red
                                        : Colors.transparent)),
                            hintText: tr('login_mobile_placehold'),
                            hintStyle: TextStyle(color: HexColor('BCBCBC')),
                            fillColor: HexColor('F2F2F2'),
                            filled: true),
                        keyboardType: TextInputType.number,
                        onChanged: _phoneNumberChange,
                      ),
                    )
                  ],
                ),
              ),
              Builder(builder: (context) {
                return _phoneError != null && _phoneError.length != 0
                    ? Text(
                        _phoneError,
                        style: TextStyle(color: Colors.red),
                      )
                    : Container();
              }),
              Padding(
                padding: EdgeInsets.symmetric(vertical: 8.0),
                child: Text("login_code",
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: HexColor('212124')))
                    .tr(),
              ),
              Row(
                children: [
                  Expanded(
                    child: TextField(
                      controller: _mobileCodeTC,
                      decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.transparent)),
                          focusedBorder: OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.transparent)),
                          enabledBorder: OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.transparent)),
                          hintText: tr('login_code_placehold'),
                          hintStyle: TextStyle(color: HexColor('BCBCBC')),
                          fillColor: HexColor('F2F2F2'),
                          filled: true),
                      keyboardType: TextInputType.number,
                      onChanged: _codeNumberChange,
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 10.0),
                    child: ButtonTheme(
                      height: 60.0,
                      minWidth: 100.0,
                      buttonColor: HexColor('67C1BF'),
                      child: RaisedButton(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(4.0)),
                        onPressed:
                            _timerCount != 60 ? null : _fetchVertificationCode,
                        child: Text(
                          _timerCount != 60
                              ? "$_timerCount"
                              : tr("login_request"),
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 16.0),
                        ),
                        textColor: Colors.white,
                        elevation: 0,
                      ),
                    ),
                  )
                ],
              ),
              Container(
                height: 50.0,
                width: MediaQuery.of(context).size.width,
                margin: EdgeInsets.only(top: 60),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(4.0))),
                child: FlatButton(
                  onPressed: isSubmitBtn ? _submitForm : null,
                  child: Text(tr("login_submit"),
                      style: TextStyle(fontSize: 16.0, color: Colors.white)),
                  color: HexColor('67C1BF'),
                  disabledColor: HexColor('67C1BF').withOpacity(0.5),
                ),
              )
            ],
            // )
          ),
        ),
      ),
    );
  }

  void _phoneNumberChange(String value) {
    var phone = filterPhoneNumber(value);
    _mobileNumberTC.value = TextEditingValue(
        text: phone, selection: TextSelection.collapsed(offset: phone.length));
    setState(() {
      _phoneError = "";
      _phoneNumber = phone;
    });
  }

  void _codeNumberChange(String value) {
    String code = filterPhoneNumber(value);
    if (code.length > 6) {
      code = code.substring(0, 6);
    }
    setState(() {
      _code = code;
      _mobileCodeTC.value = TextEditingValue(
          text: code, selection: TextSelection.collapsed(offset: code.length));
    });
  }

  void _prifixPhoneChange(int value) {
    setState(() {
      _phoneError = "";
      _countryCode = value;
      testIso = countries[value]["country"];
      testPrifix = countries[value]["prefix"];
    });
    print("_prifixPhoneChange: $value");
  }

  // 发送验证码
  Future<void> _fetchVertificationCode() async {
    removeAllForce(context);
    if (_phoneNumber == null ||
        _phoneNumber.length == 0 ||
        _phoneNumber.substring(0, 1) != "1") {
      setState(() {
        _phoneError = tr("phone_error_tip");
      });
      return;
    }
    if (!checkNetworkState(context)) return;
    try {
      bool isValid = await PhoneNumberUtil.isValidPhoneNumber(
          phoneNumber: _phoneNumber, isoCode: testIso);
      if (!_isFetch) {
        if (isValid) {
          setState(() {
            _isFetch = true;
          });
          //发送请求
          showWaitingDialog(context, "");
          Global.normalApi.fetchPinCode({
            "phoneNumber": _phoneNumber,
            "phonePrefix": testPrifix
          }).then((value) {
            if (value != null && value.errorCode == SUCCESS) {
              if (mounted) showSnackBar(context, tr("message_has_send"));
              _startTimer();
            } else {
              if (mounted)
                showSnackBar(context, tr("send_code_error"), type: 1);
            }
            setState(() {
              _isFetch = false;
            });
          }).whenComplete(() {
            Navigator.of(context).pop();
          });
        } else {
          // 提示错误
          setState(() {
            _phoneError = tr("phone_error_tip");
          });
        }
      }
    } catch (error) {
      setState(() {
        _phoneError = tr("phone_error_tip");
      });
      print("_fetchVertificationCode Error: $error");
    }
  }

  void _submitForm() async {
    removeAllForce(context);
    if (!checkNetworkState(context) || _isFetch) return;
    setState(() {
      _isFetch = true;
    });
    showWaitingDialog(context, "");
    var data = {
      "phoneNumber": _phoneNumber,
      "phonePrefix": testPrifix,
      "pinCode": _code
    };

    Global.normalApi.checkPinCode(data, type: 1).then((value) {
      Navigator.of(context).pop();
      if (value != null && value.errorCode == SUCCESS) {
        Navigator.of(context).pushNamed("set_password", arguments: {
          "tempToken": value.data["tempToken"],
          "phoneNumber": _phoneNumber,
          "phonePrefix": testPrifix,
          "type": 1
        });
      } else {
        if (value != null && value.errorCode == AuthCodeError) {
          if (mounted) showSnackBar(context, tr("verfication_error"), type: 1);
          return;
        }
        if (mounted) handleNetworkError(value, context);
      }
    }).catchError((error) {
      logger("forget password submit error $error");

      Navigator.of(context).pop();
      if (mounted) showSnackBar(context, tr("send_code_error"), type: 1);
    }).whenComplete(() => setState(() {
          _isFetch = false;
        }));
  }

  void _startTimer() {
    if (_timerCount == 60) {
      Timer.periodic(Duration(seconds: 1), (timer) {
        if (_timerCount - 1 < 0) {
          timer.cancel();
          timer = null;
          if (mounted) {
            setState(() {
              _timerCount = 60;
            });
          }
        } else {
          if (mounted) {
            setState(() {
              _timerCount = _timerCount - 1;
            });
          }
        }
      });
    }
  }
}
