import 'dart:convert';
import 'dart:io';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:muslim/common/tools/hexColor.dart';
import 'package:muslim/pages/widgets/backButton.dart';
import 'package:muslim/pages/widgets/muslim_title.dart';
import 'package:webview_flutter/webview_flutter.dart';

class TeamsPolicy extends StatefulWidget {
  @override
  _TeamsPolicyState createState() => _TeamsPolicyState();
}

class _TeamsPolicyState extends State<TeamsPolicy> {
  WebViewController _webViewController;

  bool loading = true;
  static TextStyle _normalStyle = TextStyle(
      fontSize: 13.5, color: HexColor("212124"), fontWeight: FontWeight.normal);
  static TextStyle _lineStyle = TextStyle(
      fontSize: 13.5,
      color: HexColor("212124"),
      fontWeight: FontWeight.normal,
      decoration: TextDecoration.underline);
  static TextStyle _linkStyle = TextStyle(
      fontSize: 13.5,
      color: Colors.blue,
      fontWeight: FontWeight.normal,
      decoration: TextDecoration.underline);
  static TextStyle _subTitleStyle = TextStyle(
      fontSize: 16,
      color: HexColor("212124"),
      fontWeight: FontWeight.bold,
      decoration: TextDecoration.underline);

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if (Platform.isAndroid) {
      WebView.platform = SurfaceAndroidWebView();
    }
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: MuslimTitle(title: tr("TERMS OF USE")),
        leading: MuslimBackButton(),
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.fromLTRB(6, 20, 16, 20),
        physics: ClampingScrollPhysics(),
        child: Column(
          children: [
            Text(
              "TERMS OF USE",
              style: TextStyle(
                  fontSize: 19,
                  color: HexColor("212124"),
                  fontWeight: FontWeight.w500),
            ),
            Padding(padding: EdgeInsets.only(bottom: 6)),
            Text(
              "June 2020 version",
              style: TextStyle(
                fontSize: 10,
                color: HexColor("212124"),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 10, left: 15.5),
              child: RichText(
                  text: TextSpan(
                      text:
                          "This page sets out the terms and conditions for using the"
                          " website: www.ikhlas.com (“Terms of Use”).\n\n",
                      style: _normalStyle,
                      children: <TextSpan>[
                    TextSpan(
                        text: 'PLEASE READ THESE TERMS OF USE CAREFULLY BEFORE '
                            'USING THIS WEBSITE. YOU SHOULD IMMEDIATE STOP AND '
                            'REFRAIN FROM USING THIS WEBSITE IF YOU DO NOT AGREE TO '
                            'THIS TERMS OF USE.\n\n',
                        style: _lineStyle),
                    TextSpan(
                        text:
                            'We amend these terms from time to time. We will note'
                            ' the date that amendments were last made at the top of '
                            'these terms, and any amendments will take effect upon '
                            'posting. Every time you wish to use our site, please '
                            'check these terms to ensure you understand the terms '
                            'that apply at that time.\n\n'
                            'www.ikhlas.com (“This website”) is managed and operated '
                            'by Travel360 Sdn Bhd (“us”, “we” or “our”). We manage '
                            'and operate this website from Malaysia with the server '
                            'also located in Malaysia. Though this website may be '
                            'accessible from anywhere in the world, the availability '
                            'of the functions, products, and services discussed, '
                            'mentioned, supplied, and provided through or on this '
                            'website may vary by country or region.\n\n'
                            'Should you choose to visit this website from countries '
                            'or regions outside of Malaysia, you do so on your own '
                            'volition and shall use our website only as permitted '
                            'by local laws.\n\n'
                            'Ikhlas Com Travel Sdn Bhd is a company incorporated '
                            'under the laws of Malaysia with its principal place of '
                            'business at West Wing, Level 4, Stesen Sentral Kuala '
                            'Lumpur, 50470, Kuala Lumpur.\n\n'
                            'For any content infringement issues, you can send email '
                            'to ',
                        style: _normalStyle),
                    TextSpan(
                        text: 'airasia_privacycompliance@airasia.com',
                        style: _linkStyle,
                        recognizer: TapGestureRecognizer()
                          ..onTap = () async {
                            const url = 'airasia_privacycompliance@airasia.com';
                            if (await canLaunch('mailto:' + url)) {
                              await launch('mailto:' + url);
                            }
                          }),
                  ])),
            ),
            _subTitle('BY USING AND CONTINUE TO USE THIS WEBSITE'),
            _textWithDot('You are deemed to have read, understood and '
                'accepted this Terms of Use. Please stop and refrain '
                'from using this website if you do not agree to this '
                'Terms of Use.'),
            _textWithDot(
                'You warrant that you have reached the age of majority '
                'and have capacity to enter into a legally binding '
                'contract in the territory in which you reside and '
                'that you will only use this website to make legitimate '
                'purchases.'),
            _textWithDot(
                'It is your responsibility to ensure that any products, '
                'services or information available through the website '
                'meet your specific requirements.'),
            _textWithDot(
                'You will not attempt to use the website with crawlers,'
                ' robots, data mining, extraction tools or any other '
                'functionality.'),
            _textWithDot(
                'You will be fully responsible for all matters arising '
                'from your use of this website.'),
            _textWithDot('You will not use this website in any manner which '
                'breaches any applicable laws and regulations.'),
            _textWithDot(
                'You will not interfere or attempt to interfere with the '
                'operation or functionality of the website and not to '
                'obtain or attempt to obtain unauthorised access, via '
                'whatever means, to our systems.'),
            _textWithDot('You agree that we have the right to send you '
                'administrative and promotional emails. We may also send '
                'you information regarding your account activity and '
                'purchases, as well as updates about the website, '
                'products and/or services as well as any other '
                'promotional offers. You are able to opt-out of '
                'receiving our promotional e-mails or to unsubscribe '
                'such information at any time by clicking the '
                'unsubscribe’ link at the bottom of any of such e-mail '
                'correspondence.'),
            _textWithDot('You acknowledge and accept this Terms of Use shall be'
                ' read together with the Privacy Statement, the Terms '
                'and Conditions for Qurban and any other specific terms '
                'and conditions for the relevant product made available '
                'at the point of your purchase, which are also available'
                ' here.'),
            _textWithDot('You acknowledge that the products and/or services '
                'available on the website may be provided by our merchant'
                ' partners and you accept that these merchants will have'
                ' their own applicable terms and conditions in relation'
                ' to the supply of products and/or services, in which '
                'you are further subjected to and shall be abide by '
                'those additional terms and conditions.'),
            _subTitle('RESTRICTION AND PROHIBITION OF USE OF THIS WEBSITE'),
            _textWithDot('Restrictions: Without limitation, you undertake '
                'not to use or permit anyone else to use the website:'),
            _textWithDot2(
                'for a purpose other than which we have designed them or '
                'intended them to be used;'),
            _textWithDot2('for any fraudulent purpose;'),
            _textWithDot2(
                'other than in conformance with accepted Internet practices '
                'and practices of any connected networks; or'),
            _textWithDot(
                'Prohibited uses: The following use of the website are expressly '
                'prohibited and you undertake not to do (or to permit anyone '
                'else to do) any of the following:'),
            _textWithDot2(
                'furnishing false data including false names, addresses and contact details and fraudulent use of credit/debit card numbers;'),
            _textWithDot2(
                'attempting to circumvent our security or network including accessing data not intended for you, logging into a server or account you are not expressly authorised to access, or probing the security of other networks (such as running a port scan);'),
            _textWithDot2(
                'accessing the website in such a way as to, or commit any act that would impose an unreasonable or disproportionately large load on our infrastructure;'),
            _textWithDot2(
                'executing any form of network monitoring which will intercept data not intended for you;'),
            _textWithDot2(
                'entering into fraudulent interactions or transactions with us (which shall include entering into interactions or transactions purportedly on behalf of a third party where you have no authority to bind that third party or you are pretending to be a third party);'),
            _textWithDot2(
                'using the website (or any relevant functionality of either of them) in breach of this Terms of Use;'),
            _textWithDot2(
                'unauthorised use, or forging, of mail header information;'),
            _textWithDot2('scraping of the website;'),
            _textWithDot2(
                'engage in any unlawful activity in connection with the use of the website; or'),
            _textWithDot2(
                'engage in any conduct which, in our exclusive reasonable opinion, restricts or inhibits any other customer from properly using or enjoying the website'),
            _subTitle('FORMATION OF CONTRACT'),
            _text(
                'Purchase, acceptance and completion of the contract between you and us will only be established upon us issuing a confirmation of transaction via email of the purchase to you. For the avoidance of doubt, we shall be entitled to refuse or cancel any purchase without giving any reasons for the same to you prior to the issue of the confirmation of transaction. No concluded contract may be modified or cancelled by you. All purchase, acceptance and completion of the contract is formalised between you and Ikhlas Com Travel Sdn Bhd.'),
            _subTitle('CHANGES TO OUR SITE'),
            _textWithDot(
                'We reserve the right at our absolute discretion and without liability to change, modify, alter, adapt, add or remove any of the terms and conditions contained herein and/or change, suspect or discontinue any aspect of this website.'),
            _textWithDot(
                'We are not required to give you any advanced notice prior to incorporation of any of the above changes and/or modifications into this website.'),
            _subTitle('BIG LOYALTY PROGRAM'),
            _textWithDot(
                'The purchase of products and/or services on this website entitles you to earn BIG Points. To be eligible to receive BIG Points, please follow the steps below:'),
            _textWithDot2(
                'You must be a BIG member by subscribing for membership under the loyalty and rewards program operated by BIGLIFE Sdn Bhd (‘BIG’) under the name (“BIG Loyalty Program”)'),
            _textWithDot2(
                'You would have a unique registration number known as the “BIG Member ID” issued by BIG at the time of purchase; and'),
            _textWithDot2(
                'You have logged in to your BIG account at deals.airasia.com prior to purchasing the products and/or services on this website.'),
            _textWithDot(
                'The usage of BIG Points will be subject to the BIGLIFE Membership Terms and Conditions that are available on BIG’s website (www.airasiabig.com).'),
            _textWithDot(
                'Kindly keep yourself updated with the terms and condition of BIG as it may be updated and revised from time to time.'),
            _subTitle('OTHER SITES AND LINKS PROVIDED'),
            _text(
                'Our website provides links to other sites and vice versa merely for your convenience and information. We shall neither be responsible for the content and availability of such other sites that may be operated and controlled by third parties and by our various branch offices worldwide, nor for the information, products or services contained on or accessible through those websites. Your access and use of such websites remain solely at your own risk. We will not be liable for any direct, indirect, consequential losses and/or damages of whatsoever kind arising out of your access to such sites.'),
            _subTitle('NO WARRANTIES'),
            _textWithDot(
                'We provide no warranty, whether expressly or implied, of any kind including but not limited to any implied warranties or implied terms of satisfactory quality, fitness for a particular purpose or non- infringement. All such implied terms and warranties are hereby excluded.'),
            _textWithDot(
                'We do not warrant that the functions contained in the materials will be uninterrupted or error-free, that defects will be corrected or that this website or this server that makes it available is free of any virus or other harmful elements. We do not warrant or make any representations regarding the correctness, accuracy, reliability or otherwise of the materials in this website or the results of their use.'),
            _textWithDot(
                'The provision of our website is reliant on the internet and devices. You fully understand and agree that we will not be liable for any losses suffered by you as a result of our site not being available due to events, circumstances or causes beyond our reasonable control, including but not limited to internet, system or device instability, computer viruses and hacker attacks.'),
            _subTitle('INDEMNITY'),
            _text(
                'You shall defend, indemnify and hold us and our affiliates harmless from any and all claims, liabilities, costs and expenses, including reasonable attorneys’ fees, arising in any way from your use of the website and/or services or the placement or transmission of any message, information, software or other materials through the website by you or related to any violation of these Terms of Use by you or authorised users of your account.'),
            _subTitle('LIABILITY'),
            _textWithDot(
                'Your use of this website is entire at your own risk. The information contained in this website is for informational purposes only and is provided to you on an “as-is” basis. We do not guarantee the accuracy, timeliness, reliability, authenticity of completeness of any of the information contained on this website.'),
            _textWithDot(
                'Any of our publications may include technical inaccuracies or typographical errors. Changes may be made to these publications from time to time and incorporated in new editions of these publications. At any time without notice, these publications are subject to improvements and changes in service by us.'),
            _textWithDot(
                'We will not be liable under any circumstances, including negligence, for any direct, indirect or consequential loss arising from your use of the information and material contained in our website or from your access to any of the linked sites. We are also not liable nor responsible for any material provided by third parties with their own respective copyright and shall not under any circumstances, be liable for any loss, damages or injury arising from these materials. Our total liability (if any) for all damages, losses, costs and expenses under whatever cause of action, whether in contract, tort or otherwise will not exceed any amount paid by you for accessing our website.'),
            _textWithDot(
                'The exclusions and limitations described herein shall apply to the extent permitted by law, without prejudice to our rights to seek legal redress in accordance with the applicable laws.'),
            _subTitle('COMMUNICATIONS'),
            _text(
                'Any communication from you sent to our customer support shall be treated as non-confidential. Your comments, suggestions, questions, feedback and the like regarding the content and/or our services in general, including any ideas, inventions, concepts, techniques or know-how disclosed therein may be used by us for any purpose, including the developing, manufacturing and/or marketing of goods and/or services, and in such circumstances you are not entitled to any reward or compensation of whatever nature from us. For this purpose, we reserve the right whenever necessary to disclose your personal information without our own offices, authorised agents, government/security agencies or the providers of similar services, in whatever country they may be located.'
                '\n\nTo contact us, feel free to do so here'),
            _subTitle('DATA PROTECTION'),
            _text(
                'We will only use your personal information as set out in our Privacy Statement which can be accessed using the link above. The Privacy Statement also explains the information you are required to supply when using this website.'),
            _subTitle('TRADEMARK AND COPYRIGHT'),
            _textWithDot(
                'The contents of this website are intended for your personal non-commercial use only. Graphics and images on this website are protected by copyright and may not be reproduced or appropriated in any manner without our written permission. Modification of any of the materials or use of the materials for any other purpose will be a violation of our copyright and other intellectual property rights and the copyright and intellectual property rights of the respective owners.'),
            _textWithDot(
                'All intellectual property rights (including all copyright, patents, trademarks, service marks, trade names, designs) whether registered or unregistered) in the website, information content on the website, any database operated by us and all the website design, text and graphics, software, photos, video, music, sound, and their selection and arrangement, and all software compilations, underlying source code and software (including applets and scripts) shall remain the sole and exclusive of our property (or that of our licensors). You shall not, and shall not attempt to, obtain any title to any such intellectual property rights. All rights are reserved.'),
            _textWithDot(
                'None of the materials listed above may be reproduced or redistributed or copied, distributed, republished, downloaded, displayed, posted or transmitted in any form or by any means, sold, rented or sub-licensed, used to create derivative works, or in any way exploited without our prior express written permission. You may, however, retrieve and display the content of the website on a computer screen, store such content in electronic form on disk (but not on any server or other storage device connected to a network) or print one copy of such content for your own personal, non-commercial use, provided you keep intact all and any copyright and proprietary notices. You may not otherwise reproduce, modify, copy or distribute or use for commercial purposes any of the materials or content on the website without our permission.'),
            _textWithDot(
                'All rights (including goodwill and, where relevant, trade marks) in the trade name of AirAsia are owned by us (or our licensors). Other product and company names mentioned on the website are the trademarks or registered trademarks of their respective owners.'),
            _subTitle('BREACH OF TERMS OF USE'),
            _textWithDot(
                'Failure to comply with this Terms of Use may result in us taking all or any of the following actions:'),
            _textWithDot2(
                'Immediate, temporary or permanent withdrawal of your right to use our site;'),
            _textWithDot2('Issue of a warning to you;'),
            _textWithDot2(
                'Legal proceedings against you for reimbursement of all costs on an indemnity basis (including, but not limited to, reasonable administrative and legal costs) resulting from the breach;'),
            _textWithDot2('Further legal action against you;'),
            _textWithDot2(
                'Disclosure of such information to law enforcement authorities as we reasonably feel is necessary or as required by law.'),
            _textWithDot(
                'If we (in our sole discretion) believe that you are in breach, or will be in breach, of any of this Terms of Use, we reserve our right to deny you access to this website without giving you a reason and/or without further reference to you.'),
            _subTitle('APPLICABLE LAW AND JURISDICTION'),
            _text(
                'This Terms of Use and this website’s content shall be governed and construed in accordance with Malaysian laws, and the courts of Malaysia shall have exclusive jurisdiction to adjudicate any dispute which may arise in relation thereto.'),
            _subTitle('LANGUAGE'),
            _text(
                'For ease of understanding, this Terms of Use may appear in other languages. However, this Terms of Use was originally written in English. Therefore, to the extent any translated version of this Terms of Use conflicts with the English version, the English version shall prevail and be given full effect.\n'),
          ],
        ),
      ),
    );
  }

  Widget _subTitle(String text) {
    return Padding(
      padding: EdgeInsets.only(top: 10, bottom: 10, left: 15.5),
      child: Row(
        children: [
          Expanded(
              child: Text(
            text,
            style: _subTitleStyle,
          ))
        ],
      ),
    );
  }

  Widget _text(String text) {
    return Padding(
        padding: EdgeInsets.only(top: 1, left: 15.5),
        child: Text(
          text,
          style: _normalStyle,
        ));
  }

  Widget _textWithDot(String text) {
    return Padding(
      padding: EdgeInsets.only(top: 1),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            height: 3.5,
            width: 3.5,
            margin: EdgeInsets.fromLTRB(4, 6.5, 8, 0),
            decoration: BoxDecoration(
                color: HexColor("212124"),
                borderRadius: BorderRadius.circular(40)),
          ),
          Expanded(
              child: Text(
            text,
            style: _normalStyle,
          ))
        ],
      ),
    );
  }

  Widget _textWithDot2(String text) {
    return Padding(
      padding: EdgeInsets.only(top: 1, left: 20),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            height: 3.5,
            width: 3.5,
            margin: EdgeInsets.fromLTRB(4, 6.5, 8, 0),
            decoration: BoxDecoration(
                color: HexColor("212124"),
                borderRadius: BorderRadius.circular(40)),
          ),
          Expanded(
              child: Text(
            text,
            style: _normalStyle,
          ))
        ],
      ),
    );
  }
}
