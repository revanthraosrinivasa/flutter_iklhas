import 'dart:ui';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:matomo/matomo.dart';
import 'package:muslim/common/api/error_network_handle.dart';
import 'package:muslim/common/core/storage.dart';
import 'package:muslim/common/provider/app.dart';
import 'package:muslim/common/tools/dialog.dart';
import 'package:muslim/common/tools/hexColor.dart';
import 'package:muslim/common/tools/snackBar.dart';
import 'package:muslim/global.dart';
import 'package:muslim/pages/widgets/muslim_title.dart';
import 'package:muslim/pages/widgets/textfiled.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:flutter_picker/Picker.dart';
import 'package:email_validator/email_validator.dart';
import 'package:provider/provider.dart';

class UserProfilePage extends TraceableStatefulWidget {
  @override
  _UserProfilePageState createState() => _UserProfilePageState();
}

class _UserProfilePageState extends State<UserProfilePage> {
  TextEditingController _nameController;
  TextEditingController _emailController;
  TextEditingController _birthdayController;
  TextEditingController _genderController;

  String _fullName = "";
  String _email = "";
  String _birthday = "";
  String _gender = "";
  String _emailError = "";

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _nameController = TextEditingController();
    _emailController = TextEditingController();
    _birthdayController = TextEditingController();
    _genderController = TextEditingController();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    MatomoTracker.trackScreen(context, "Login Profile");
    bool submitBtnEnable = false;
    if (_email.length != 0 ||
        _fullName.length != 0 ||
        _birthday.length != 0 ||
        _gender.length != 0) {
      submitBtnEnable = true;
    }

    return KeyboardDismissOnTap(
      child: Scaffold(
//        BOY removed this
//          resizeToAvoidBottomPadding: false,
          appBar: AppBar(
            centerTitle: true,
            title: MuslimTitle(title: tr("profile_title")),
            leading: Container(),
            actions: [
              RaisedButton(
                  onPressed: () {
                    MatomoTracker.trackEvent("skip", "skip");
                    Navigator.of(context)
                        .pushNamedAndRemoveUntil("main", (route) => false);
                  },
                  child: Text('profile_skip').tr(),
                  textColor: HexColor('67C1BF'),
                  color: Colors.transparent,
                  elevation: 0.0)
            ],
          ),
          body: SingleChildScrollView(
            reverse: true,
            child: Padding(
              padding: EdgeInsets.fromLTRB(
                  16.0, 40.0, 16.0, MediaQuery.of(context).viewInsets.bottom),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    margin: EdgeInsets.fromLTRB(0, 16.0, 0, 8.0),
                    child: Text(
                      "profile_fullname",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: HexColor("212124"),
                          fontSize: 14.0),
                    ).tr(),
                  ),
                  DKMTextFiled(
                    placehold: "profile_fullname_placehold",
                    controller: _nameController,
                    onChange: (value) {
                      setState(() {
                        _fullName = value;
                      });
                    },
                  ),
                  Container(
                    margin: EdgeInsets.fromLTRB(0, 16.0, 0, 8.0),
                    child: Text(
                      "profile_email",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: HexColor("212124"),
                          fontSize: 14.0),
                    ).tr(),
                  ),
                  DKMTextFiled(
                    placehold: "profile_eamil_placehold",
                    controller: _emailController,
                    onChange: (value) {
                      setState(() {
                        _emailError = "";
                        _email = value;
                      });
                    },
                  ),
                  Builder(builder: (context) {
                    return _email.length == 0
                        ? Container()
                        : Text(
                            _emailError,
                            style: TextStyle(fontSize: 12.0, color: Colors.red),
                          );
                  }),
                  Container(
                    margin: EdgeInsets.fromLTRB(0, 16.0, 0, 8.0),
                    child: Text(
                      "profile_birthday",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: HexColor("212124"),
                          fontSize: 14.0),
                    ).tr(),
                  ),
                  InkWell(
                    onTap: () {
                      _openSelectBirthday(context);
                    },
                    child: TextField(
                      enabled: false,
                      decoration: InputDecoration(
                        hintText: tr("profile_birthday_placehold"),
                        border: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.transparent)),
                        focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.transparent)),
                        enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.transparent)),
                        disabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.transparent)),
                        hintStyle: TextStyle(color: HexColor('BCBCBC')),
                        fillColor: HexColor('F2F2F2'),
                        filled: true,
                      ),
                      controller: _birthdayController,
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.fromLTRB(0, 16.0, 0, 8.0),
                    child: Text(
                      "profile_gender",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: HexColor("212124"),
                          fontSize: 14.0),
                    ).tr(),
                  ),
                  InkWell(
                    onTap: () {
                      _openSelectGender(context);
                    },
                    child: TextField(
                      enabled: false,
                      decoration: InputDecoration(
                        hintText: tr("profile_gender_placehold"),
                        border: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.transparent)),
                        focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.transparent)),
                        enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.transparent)),
                        disabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.transparent)),
                        hintStyle: TextStyle(color: HexColor('BCBCBC')),
                        fillColor: HexColor('F2F2F2'),
                        filled: true,
                      ),
                      controller: _genderController,
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: 44.0,
                    margin: EdgeInsets.only(top: 40.0),
                    child: RaisedButton(
                      onPressed:
                          submitBtnEnable ? () => {_submit(context)} : null,
                      child: Text("login_submit",
                              style: TextStyle(
                                  fontSize: 16.0,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white))
                          .tr(),
                      color: HexColor("67C1BF"),
                      textColor: Colors.white,
                      disabledColor: HexColor("67C1BF").withOpacity(0.5),
                      elevation: 0.0,
                    ),
                  )
                ],
              ),
            ),
          )),
    );
  }

  void _openSelectBirthday(BuildContext context) {
    print("_openSelectBirthday");

    var picker = new Picker(
      height: 250.0,
      itemExtent: 44.0,
      adapter: DateTimePickerAdapter(value: DateTime(2020, 10, 10)),
      title: Text(tr("profile_birthday")),
      hideHeader: !Global.isIOS,
      confirmText: tr("confirmText"),
      cancelText: tr("cancelText"),
      cancelTextStyle: TextStyle(color: HexColor("67C1BF"), fontSize: 14.0),
      confirmTextStyle: TextStyle(color: HexColor("67C1BF"), fontSize: 14.0),
      selectedTextStyle: TextStyle(color: HexColor("67C1BF")),
      onConfirm: (Picker picker, List value) {
        print("picker: ${(picker.adapter as DateTimePickerAdapter).value}");
        _birthdayController.text = DateFormat("yyyy-MM-dd")
            .format((picker.adapter as DateTimePickerAdapter).value);
        setState(() {
          _birthday = DateFormat("yyyy-MM-dd")
              .format((picker.adapter as DateTimePickerAdapter).value);
        });
      },
    );
    if (Global.isIOS) {
      picker.showModal(context);
    } else {
      picker.showDialog(context);
    }
  }

  void _openSelectGender(BuildContext context) {
    print("_openSelectGender");
    List<String> genders = [tr("Female"), tr("Male")];
    new Picker(
      // height: 280.0,
      itemExtent: 44.0,
      adapter: PickerDataAdapter(pickerdata: genders),
      selectedTextStyle: TextStyle(color: HexColor("67C1BF")),
      onConfirm: (picker, selecteds) {
        print("Gender change ${genders[selecteds[0]]}");
        _genderController.text = genders[selecteds[0]];
        var genderSelect = selecteds[0] == 0 ? "female" : "male";
        setState(() {
          _gender = genderSelect;
        });
      },
    ).showModal(context);
  }

  void _submit(BuildContext context) async {
    MatomoTracker.trackEvent("profile submit", "profile submit");
    // Navigator.of(context).pushNamed("main");
    if (_email.length != 0) {
      if (!EmailValidator.validate(_email)) {
        setState(() {
          _emailError = tr("email_error_text");
        });
        return;
      }
    }
    var params = {};

    if (_fullName.length != 0) {
      params["fullName"] = _fullName;
    }
    if (_email.length != 0) {
      params["email"] = _email;
    }
    if (_birthday.length != 0) {
      params["birthday"] = _formatBirthdayTime(_birthday);
    }
    if (_gender.length != 0) {
      params["gender"] = _gender.toLowerCase();
    }

    if (params.isNotEmpty) {
      if (!checkNetworkState(context)) return;
      showWaitingDialog(context, null);
      Global.normalApi.changeUserInfo(params).then((value) {
        Navigator.of(context).pop();
        if (value.errorCode == SUCCESS) {
          Navigator.of(context)
              .pushNamedAndRemoveUntil("main", (route) => false);
        } else {
          if (mounted)
            handleNetworkError(value, context, errorString: tr("submit_error"));
        }
      }).catchError((error) {
        print("changeUserInfo: $error");
      });
    }
  }

  String _formatBirthdayTime(String dayStr) {
    if (dayStr.length == 0) {
      return dayStr;
    }
    DateTime time = DateFormat("yyyy-MM-dd").parse(dayStr);
    return DateFormat("yyyy-MM-dd'T'HH:mm:ss").format(time) + "Z";
  }
}
