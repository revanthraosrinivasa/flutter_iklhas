import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:matomo/matomo.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:muslim/pages/login/codeLogin.dart';
import 'package:muslim/pages/login/pswLogin.dart';
import 'package:muslim/pages/widgets/backButton.dart';
import 'package:muslim/pages/widgets/muslim_title.dart';

class LoginPage extends TraceableStatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  PageController _pageController = PageController(initialPage: 0);
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return KeyboardDismissOnTap(
      child: Scaffold(
          appBar: AppBar(
            centerTitle: true,
            title: MuslimTitle(title: tr("login_title_start")),
            leading: MuslimBackButton(),
          ),
          body: PageView(
            physics: NeverScrollableScrollPhysics(),
            controller: _pageController,
            children: [
              VeritifityCodeLogin(callback: jumpToPasswordLoginPage),
              PassWordLogin(callback: jumpToVertifityLoginPage)
            ],
          )),
    );
  }

  jumpToVertifityLoginPage() {
    _pageController.jumpToPage(0);
  }

  jumpToPasswordLoginPage() {
    _pageController.jumpToPage(1);
  }
}
