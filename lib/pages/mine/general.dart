import 'package:flutter/material.dart';
import 'package:muslim/common/core/storage.dart';
import 'package:muslim/common/helper/helper.dart';
import 'package:muslim/common/provider/app.dart';
import 'package:muslim/common/tools/dialog.dart';
import 'package:muslim/common/tools/hexColor.dart';
import 'package:muslim/global.dart';
import 'package:muslim/pages/widgets/horizontalLine.dart';
import 'package:muslim/pages/widgets/muslim_tile.dart';
import 'package:muslim/pages/widgets/muslim_title.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:provider/provider.dart';
import 'package:path_provider/path_provider.dart';
import 'dart:io';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';

class GeneralPage extends StatefulWidget {
  @override
  _GeneralPageState createState() => _GeneralPageState();
}

class _GeneralPageState extends State<GeneralPage> {
  String catchSize = "";
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    fetchCacheLength();
  }

  fetchCacheLength() async {
    double value = await loadApplicationCache();
    setState(() {
      catchSize = formatSize(value);
    });
  }

  @override
  Widget build(BuildContext context) {
    AppSettingModel appSettingModel = Provider.of<AppSettingModel>(context);

    String graphicsSetting = tr("image_auto");
    switch (appSettingModel.userChooseGraphicsSettngs) {
      case UserChooseGraphicsSettings.auto:
        graphicsSetting = tr("image_auto");
        break;
      case UserChooseGraphicsSettings.low:
        graphicsSetting = tr("image_normal");
        break;
      case UserChooseGraphicsSettings.high:
        graphicsSetting = tr("image_high");
        break;
    }

    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: MuslimTitle(title: tr("Setting")),
      ),
      body: Padding(
        padding: EdgeInsets.only(top: 20.0),
        child: Column(
          children: [
            Container(
                margin: EdgeInsets.only(top: 8.0),
                decoration: BoxDecoration(
                    color: Colors.white,
                    border: Border(top: BorderSide(color: HexColor("EAEAEA")))),
                height: 50.0,
                child: MuslimTile(
                  callback: () {
                    changeLanguagePicker(context);
                  },
                  title: tr("Language"),
                  subTitle: context.locale.languageCode == "en"
                      ? "English"
                      : "Malaysia",
                  trailing: Icon(
                    Icons.navigate_next,
                    color: HexColor("75767A"),
                  ),
                )),
            Container(
                margin: EdgeInsets.only(top: 8.0),
                decoration: BoxDecoration(
                    color: Colors.white,
                    border: Border(top: BorderSide(color: HexColor("EAEAEA")))),
                height: 50.0,
                child: MuslimTile(
                  callback: () {
                    Navigator.of(context).pushNamed("graphics_setting");
                  },
                  title: tr("Graphics Settings"),
                  subTitle: graphicsSetting,
                  trailing: Icon(
                    Icons.navigate_next,
                    color: HexColor("75767A"),
                  ),
                )),
            Container(
                margin: EdgeInsets.only(top: 8.0),
                decoration: BoxDecoration(
                    color: Colors.white,
                    border: Border(top: BorderSide(color: HexColor("EAEAEA")))),
                height: 50.0,
                child: MuslimTile(
                  callback: () {
                    showEnsureDialog(
                        context, tr("Tip"), tr("clean_cache_content"),
                        () async {
                      showWaitingDialog(context, "");
                      clearApplicationCache().then((_) {}).whenComplete(() {
                        Navigator.of(context).pop();
                        fetchCacheLength();
                      });
                    }, ensureTitle: tr("clean"), cancelTitle: tr("cancelText"));
                  },
                  title: tr("Clean"),
                  subTitle: catchSize,
                  trailing: Icon(
                    Icons.navigate_next,
                    color: HexColor("75767A"),
                  ),
                ))
          ],
        ),
      ),
    );
  }

  /// 删除缓存
  Future<void> clearApplicationCache() async {
    // Directory directory = await getApplicationDocumentsDirectory();
    //删除缓存目录
    DefaultCacheManager manage = DefaultCacheManager();
    manage.emptyCache();
    if (!Global.isIOS) {
      await deleteDirectory();
    }
  }

  /// 递归方式删除目录
  Future<void> deleteDirectory() async {
    return Future.wait([_deleteCacheDir(), _deleteAppDir()]);
  }

  Future<void> _deleteCacheDir() async {
    final cacheDir = await getTemporaryDirectory();

    if (cacheDir.existsSync()) {
      cacheDir.deleteSync(recursive: true);
    }
  }

  Future<void> _deleteAppDir() async {
    final appDir = await getApplicationSupportDirectory();

    if (appDir.existsSync()) {
      appDir.deleteSync(recursive: true);
    }
  }

  /// 获取缓存
  Future<double> loadApplicationCache() async {
    /// 获取文件夹
    Directory directory = await getApplicationSupportDirectory();
    Directory cacheDir = await getTemporaryDirectory();

    /// 获取缓存大小
    double value = await getTotalSizeOfFilesInDir(directory);
    double value1 = await getTotalSizeOfFilesInDir(cacheDir);

    return value + value1;
  }

  /// 循环计算文件的大小（递归）
  Future<double> getTotalSizeOfFilesInDir(final FileSystemEntity file) async {
    if (file is File) {
      int length = await file.length();
      return double.parse(length.toString());
    }
    if (file is Directory) {
      double total = 0;
      try {
        final List<FileSystemEntity> children = file.listSync();

        if (children != null)
          for (final FileSystemEntity child in children)
            total += await getTotalSizeOfFilesInDir(child);
      } catch (error) {}

      return total;
    }
    return 0;
  }

  /// 缓存大小格式转换
  String formatSize(double value) {
    if (null == value) {
      return '0';
    }
    List<String> unitArr = List()..add('B')..add('K')..add('M')..add('G');
    int index = 0;
    while (value > 1024) {
      index++;
      value = value / 1024;
    }
    String size = value.toStringAsFixed(2);
    return size + unitArr[index];
  }

  changeLanguagePicker(BuildContext context) {
    showModalBottomSheet(
        context: context,
        builder: (context) {
          return BottomSheet(onClosing: () {
            Navigator.of(context).pop();
          }, builder: (context) {
            return Container(
              padding: EdgeInsets.all(16.0),
              color: Colors.white,
              height: 200.0,
              child: Column(
                children: [
                  Container(
                    height: 50,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text("Language").tr(),
                        IconButton(
                            icon: Icon(Icons.clear),
                            onPressed: () {
                              Navigator.of(context).pop();
                            }),
                      ],
                    ),
                  ),
                  HorizontalLine(),
                  Container(
                    height: 50.0,
                    child: InkWell(
                      onTap: () {
                        context.setLocale(Locale("en"));
                        StorageHelper().setString(LOCALE, "en");
                        Navigator.of(context).pop();
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text("English"),
                          context.locale.languageCode == "en"
                              ? Icon(Icons.check_circle_outline,
                                  color: HexColor("67C1BF"))
                              : Container(),
                        ],
                      ),
                    ),
                  ),
                  HorizontalLine(),
                  Container(
                    height: 50.0,
                    child: InkWell(
                      onTap: () {
                        context.setLocale(Locale('my'));
                        StorageHelper().setString(LOCALE, "my");
                        print(
                            "context.locale.languageCode: ${context.locale.languageCode}");
                        Navigator.of(context).pop();
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text("Malay"),
                          context.locale.languageCode == "my"
                              ? Icon(Icons.check_circle_outline,
                                  color: HexColor("67C1BF"))
                              : Container(),
                        ],
                      ),
                    ),
                  ),
                  HorizontalLine(),
                ],
                crossAxisAlignment: CrossAxisAlignment.start,
              ),
            );
          });
        });
  }
}
