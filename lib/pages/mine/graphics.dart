import 'package:flutter/material.dart';
import 'package:muslim/common/provider/app.dart';
import 'package:muslim/common/tools/hexColor.dart';
import 'package:muslim/pages/widgets/horizontalLine.dart';
import 'package:muslim/pages/widgets/muslim_title.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:provider/provider.dart';

class GraphicsSettingPage extends StatefulWidget {
  @override
  _GraphicsSettingPageState createState() => _GraphicsSettingPageState();
}

class _GraphicsSettingPageState extends State<GraphicsSettingPage> {
  List _tableData = [
    {"title": tr("image_auto"), "content": tr("image_auto_sub")},
    {"title": tr("image_high"), "content": tr("image_high_sub")},
    {"title": tr("image_low"), "content": tr("image_low_sub")},
  ];

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    AppSettingModel appSettingModel = Provider.of<AppSettingModel>(context);

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: MuslimTitle(
          title: tr('setting'),
        ),
      ),
      body: ListView.separated(
        padding: EdgeInsets.only(top: 20.0),
        itemBuilder: (context, index) {
          var item = _tableData[index];
          return ListTile(
            tileColor: Colors.white,
            title: Text(item["title"]),
            subtitle: Text(item["content"]),
            trailing: appSettingModel.userChooseGraphicsSettngs.index == index
                ? Icon(Icons.check_circle_outline, color: HexColor("67C1BF"))
                : null,
            onTap: () {
              appSettingModel.userChooseGraphicsSettngs =
                  UserChooseGraphicsSettings.values[index];
              Navigator.of(context).pop();
            },
          );
        },
        separatorBuilder: (context, index) {
          return HorizontalLine();
        },
        itemCount: _tableData.length,
      ),
    );
  }
}
