import 'package:cached_network_image/cached_network_image.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter_easyrefresh/easy_refresh.dart';
import 'package:muslim/common/api/error_network_handle.dart';
import 'package:muslim/common/core/storage.dart';
import 'package:muslim/common/provider/app.dart';
import 'package:muslim/common/tools/dialog.dart';
import 'package:muslim/common/tools/hexColor.dart';
import 'package:muslim/common/tools/snackBar.dart';
import 'package:muslim/common/provider/user.dart';
import 'package:muslim/global.dart';
import 'package:muslim/modals/login.dart';
import 'package:muslim/pages/widgets/horizontalLine.dart';
import 'package:muslim/pages/widgets/muslim_tile.dart';
import 'package:muslim/pages/widgets/muslim_title.dart';
import 'package:provider/provider.dart';
import 'package:sentry_flutter/sentry_flutter.dart' as SentryUser;

class MinePage extends StatefulWidget {
  @override
  _MinePageState createState() => _MinePageState();
}

class _MinePageState extends State<MinePage>
    with AutomaticKeepAliveClientMixin {
  @override
  bool get wantKeepAlive => true;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      _fetchUserInfo();
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    UserModel model = Provider.of<UserModel>(context);
    User user = model.user;

    return Scaffold(
        backgroundColor: HexColor("F9F9F9"),
        appBar: AppBar(
          centerTitle: true,
          title: MuslimTitle(title: tr("Me")),
        ),
        body: EasyRefresh(
          child: Column(
            children: [
              Container(
                  margin: EdgeInsets.symmetric(vertical: 8.0),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      border: Border(
                          bottom: BorderSide(
                            color: HexColor("EAEAEA"),
                          ),
                          top: BorderSide(color: HexColor("EAEAEA")))),
                  height: 100.0,
                  child: InkWell(
                    onTap: () {},
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          padding: EdgeInsets.all(16.0),
                          child: User_Avatars[model.avatarIndex],
                        ),
                        Text(user != null && user.nickName != null
                            ? user.nickName
                            : ""),
                      ],
                    ),
                  )),
              HorizontalLine(),
              MuslimTile(
                leading: Icon(Icons.people, color: HexColor("75767A")),
                title: tr('follow'),
                trailing: Icon(
                  Icons.navigate_next,
                  color: HexColor("75767A"),
                ),
                callback: () {
                  Navigator.of(context).pushNamed("followed");
                },
              ),
              MuslimTile(
                leading: Icon(Icons.settings, color: HexColor("75767A")),
                title: tr('prayer_settings'),
                trailing: Icon(
                  Icons.navigate_next,
                  color: HexColor("75767A"),
                ),
                callback: () {
                  Navigator.of(context).pushNamed("prayer_time_setting");
                },
              ),
              Container(
                height: 8.0,
              ),
              HorizontalLine(),
              MuslimTile(
                leading: Icon(Icons.settings, color: HexColor("75767A")),
                title: tr('settings'),
                trailing: Icon(
                  Icons.navigate_next,
                  color: HexColor("75767A"),
                ),
                callback: () {
                  Navigator.of(context).pushNamed("settings");
                },
              ),
            ],
            // )
          ),
        ));
  }

  Future<void> _fetchUserInfo() async {
    if (!checkNetworkState(context)) return;
    Global.normalApi.fetchUserInfo().then((value) {
      print("_fetchUserInfo: $value");
      if (value != null && value.errorCode == SUCCESS) {
        UserModel userModol = Provider.of<UserModel>(context, listen: false);
        userModol.changeUser(value.data);
        userModol.hasPwd = value.data.hasPwd;
        SentryUser.Sentry.configureScope(
          (scope) => scope.user =
              SentryUser.User(id: value.data.id, username: value.data.fullName),
        );
      } else {
        if (mounted)
          handleNetworkError(value, context, errorString: tr("network_error"));
      }
    }).catchError((error) {
      print("_fetchUserInfo error: $error");
    });
  }
}
