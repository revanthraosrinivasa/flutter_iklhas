import 'dart:async';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:muslim/common/api/error_network_handle.dart';
import 'package:muslim/common/core/storage.dart';
import 'package:muslim/common/provider/app.dart';
import 'package:muslim/common/tools/dialog.dart';
import 'package:muslim/common/tools/hexColor.dart';
import 'package:muslim/common/tools/snackBar.dart';
import 'package:muslim/global.dart';
import 'package:muslim/pages/widgets/backButton.dart';
import 'package:muslim/pages/widgets/muslim_title.dart';
import 'package:provider/provider.dart';
import 'package:top_snackbar_flutter/top_snack_bar.dart';

class FeedBackPage extends StatefulWidget {
  @override
  _FeedBackPageState createState() => _FeedBackPageState();
}

class _FeedBackPageState extends State<FeedBackPage> {
  String _feedback;
  FocusNode _feedFocus = FocusNode();
  TextEditingController _controller = TextEditingController();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _feedback = "";
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return KeyboardDismissOnTap(
      child: Scaffold(
          backgroundColor: HexColor("F9F9F9"),
          appBar: AppBar(
            centerTitle: true,
            title: MuslimTitle(title: tr("feedback")),
            leading: MuslimBackButton(),
          ),
          body: SingleChildScrollView(
            padding: EdgeInsets.all(16.0),
            child: Column(
              children: [
                Card(
                  color: Colors.white,
                  child: TextField(
                      controller: _controller,
                      onChanged: (value) {
                        setState(() {
                          _feedback = value;
                        });
                      },
                      focusNode: _feedFocus,
                      maxLines: 8,
                      decoration: InputDecoration(
                          hintText: tr("enter_text_placehold"),
                          border: InputBorder.none,
                          contentPadding: EdgeInsets.all(16.0))),
                ),
                Row(
                  children: [
                    Expanded(
                        child: Container(
                      height: 44.0,
                      margin: EdgeInsets.only(top: 16.0),
                      child: RaisedButton(
                        color: HexColor("67C1BF"),
                        disabledColor: HexColor("67C1BF").withOpacity(0.5),
                        onPressed: (_feedback == null || _feedback.length == 0)
                            ? null
                            : _sendFeedBack,
                        child: Text(
                          "login_submit",
                          style: TextStyle(color: Colors.white),
                        ).tr(),
                      ),
                    ))
                  ],
                )
              ],
            ),
          )),
    );
  }

  void _sendFeedBack() {
    _feedFocus.unfocus();
    if (!checkNetworkState(context)) return;
    showWaitingDialog(context, "");
    Global.normalApi.sendFeedBack({"content": _feedback}).then((value) {
      print("feed back success");
      Navigator.of(context).pop();
      if (value != null && value.errorCode == SUCCESS) {
        setState(() {
          _feedback = "";
          _controller.text = "";
        });
        Navigator.of(context).pop();
      } else {
        if (mounted) handleNetworkError(value, context, errorString: tr("submit_error"));
      }
    }).catchError((error) {
      print("feed back error");
      Navigator.of(context).pop();
      if (mounted) showSnackBar(context, tr("network_error"), type: 1);
    });
  }
}
