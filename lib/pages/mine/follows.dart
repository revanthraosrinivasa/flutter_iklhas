import 'package:cached_network_image/cached_network_image.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter_easyrefresh/easy_refresh.dart';
import 'package:muslim/common/api/error_network_handle.dart';
import 'package:muslim/common/core/storage.dart';
import 'package:muslim/common/provider/app.dart';
import 'package:muslim/common/tools/dialog.dart';
import 'package:muslim/common/tools/hexColor.dart';
import 'package:muslim/common/tools/snackBar.dart';
import 'package:muslim/global.dart';
import 'package:muslim/modals/follow.dart';
import 'package:muslim/pages/widgets/backButton.dart';
import 'package:muslim/pages/widgets/base_page.dart';
import 'package:muslim/pages/widgets/base_widget.dart';
import 'package:muslim/pages/widgets/empty.dart';
import 'package:muslim/pages/widgets/horizontalLine.dart';
import 'package:muslim/pages/widgets/muslim_title.dart';
import 'package:provider/provider.dart';

class FollowsPage extends StatefulWidget {
  @override
  _FollowsPageState createState() => _FollowsPageState();
}

class _FollowsPageState extends State<FollowsPage> {
  EasyRefreshController _controller = EasyRefreshController();
  List<FollowMerchantsDatum> _tableData;
  String _cursor = "";
  bool _isNetworkError = false;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _fetchTableData();
  }

  List<Widget> _initSlivers() {
    return [
      SliverPadding(
        padding: EdgeInsets.only(top: 8.0),
        sliver: SliverList(
            delegate: SliverChildBuilderDelegate((context, index) {
          if (index == 0) {
            return Container(
              decoration: BoxDecoration(
                  border: Border(
                      top: BorderSide(color: HexColor("EAEAEA")),
                      bottom: BorderSide(color: HexColor("EAEAEA")))),
              child: listTileWithIndex(index),
            );
          }
          if (index == _tableData.length - 1) {
            return Container(
              decoration: BoxDecoration(
                  border:
                      Border(bottom: BorderSide(color: HexColor("EAEAEA")))),
              child: listTileWithIndex(index),
            );
          }
          return Container(
            decoration: BoxDecoration(
              border: Border(bottom: BorderSide(color: HexColor("EAEAEA"))),
            ),
            child: listTileWithIndex(index),
          );
        }, childCount: _tableData != null ? _tableData.length : 0)),
      )
    ];
  }

  Widget _initEmptyWidget() {
    return _isNetworkError && _tableData == null
        ? EmptyPage(
            noDatasTip: tr("no_network_tip"),
          )
        : _tableData != null && _tableData.length == 0
            ? EmptyPage(
                noDatasTip: "No following",
              )
            : null;
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MuslimBasePage(
      appBar: AppBar(
        centerTitle: true,
        title: MuslimTitle(title: tr("follow_title")),
        leading: MuslimBackButton(),
      ),
      header: baseHeader(),
      footer: baseFooter(),
      controller: _controller,
      onLoad: _onLoad,
      onRefresh: _onRefresh,
      emptyWidget: _initEmptyWidget(),
      firstRefresh: true,
      firstRefreshWidget: true,
      slivers: _initSlivers(),
    );
  }

  Widget listTileWithIndex(int index) {
    FollowMerchantsDatum merchants = _tableData[index];
    return ListTile(
      title: Text(
        merchants.name,
        style: TextStyle(
            color: HexColor("212124"),
            fontSize: 16.0,
            fontWeight: FontWeight.w500),
      ),
      subtitle: merchants.state == 0
          ? null
          : Text(merchants.state == 1 ? "Suspended" : "Deleted",
              style: TextStyle(color: HexColor("FB7268"))),
      leading: merchants.previewPhoto == null ||
              merchants.previewPhoto.url == null ||
              merchants.previewPhoto.url.length == 0
          ? Icon(Icons.person)
          : CircleAvatar(
              radius: 20.0,
              backgroundImage: CachedNetworkImageProvider(
                  merchants.previewPhoto.url + OSS_IMAGE_SCALE),
            ),
      tileColor: Colors.white,
      trailing: merchants.state == 0
          ? OutlineButton(
              onPressed: merchants.type == 0
                  ? null
                  : () {
                      _unfollowMerchant(merchants.id, context, merchants.name);
                    },
              child: Text("unfollow").tr(),
              textColor: HexColor("67C1BF"),
              borderSide: BorderSide(color: HexColor("67C1BF")),
            )
          : null,
      onTap: () {
        if (merchants.state == 0) {
          Navigator.of(context)
              .pushNamed("store_detail", arguments: merchants.id);
        } else {
          showEnsureDialog(
              context,
              "",
              merchants.state == 1
                  ? tr("Merchants have been frozen")
                  : tr("Merchants abnormal"), () {
            if (merchants.state == -1) {
              if (!checkNetworkState(context)) return;
              Global.normalApi.unfollowMerchant(merchants.id).then((value) {
                print("_unfollowMerchant success");
                if (value != null && value.errorCode == SUCCESS) {
                  _controller.callRefresh();
                } else {
                  if (mounted) handleNetworkError(value, context);
                }
                // Navigator.of(context).pop(true);
              });
            }
          });
        }
      },
    );
  }

  Future<void> _fetchTableData({int type = 0}) async {
    setState(() {
      _isNetworkError = false;
    });
    var params = {"cursor": _cursor, "limit": LIMIT};
    Global.normalApi.fetchFollowMerchants(params).then((value) {
      if (value != null && value.errorCode == SUCCESS) {
        if (type == 0) {
          setState(() {
            _tableData = value.data;
          });
          _controller.finishRefresh(success: true);
          if (_controller.finishLoadCallBack != null)
            _controller.finishLoadCallBack(noMore: value.data.length < LIMIT);
        } else {
          var data = _tableData;
          data.addAll(value.data);
          setState(() {
            _tableData = data;
          });
          _controller.finishLoad(success: true, noMore: value.data.length < 10);
        }
      } else {
        if (type == 0) {
          _controller.finishRefresh(success: false);
        } else {
          _controller.finishLoad(success: false);
        }
        if (mounted) handleNetworkError(value, context);
      }
    }).catchError((error) {
      print("fetchFollowMerchants: error $error");
      if (type == 0) {
        _controller.finishRefresh(success: false);
      } else {
        _controller.finishLoad(success: false);
      }
      setState(() {
        _isNetworkError = true;
      });
      if (_tableData != null && mounted) {
        showSnackBar(context, tr("network_error"));
      }
    });
  }

  Future<void> _onRefresh() async {
    _controller.resetLoadState();
    setState(() {
      _cursor = "";
    });
    _fetchTableData();
  }

  Future<void> _onLoad() async {
    setState(() {
      _cursor = _tableData.last.id;
    });
    _fetchTableData(type: 1);
  }

  _unfollowMerchant(
      String merchantId, BuildContext context, String merchantName) {
    if (!checkNetworkState(context)) return;
    showEnsureDialog(
        context, tr("unfollow_title"), tr("unfollow_tip", args: [merchantName]),
        () async {
      showWaitingDialog(context, null);
      Global.normalApi.unfollowMerchant(merchantId).then((value) {
        print("_unfollowMerchant success");
        if (value != null && value.errorCode == SUCCESS) {
          _onRefresh();
        } else {
          if (mounted) handleNetworkError(value, context);
        }
        // Navigator.of(context).pop(true);
      }).catchError((error) {
        if (mounted) showSnackBar(context, tr("network_error"));
      }).whenComplete(() {
        Navigator.of(context).pop();
      });
    }, ensureTitle: tr("confirmText"));
  }
}
