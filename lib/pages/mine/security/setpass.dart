import 'package:flutter/material.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:muslim/common/api/error_network_handle.dart';
import 'package:muslim/common/core/storage.dart';
import 'package:muslim/common/provider/user.dart';
import 'package:muslim/common/tools/dialog.dart';
import 'package:muslim/common/tools/hexColor.dart';
import 'package:muslim/common/tools/reForce.dart';
import 'package:muslim/common/tools/snackBar.dart';
import 'package:muslim/global.dart';
import 'package:muslim/pages/widgets/muslim_title.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:provider/provider.dart';

class SetPasswordPage extends StatefulWidget {
  SetPasswordPage(
      {Key key,
      this.tempToken,
      this.phoneNumber,
      this.phonePrefix,
      this.type = 0})
      : super(key: key);
  final String tempToken;
  final String phoneNumber;
  final String phonePrefix;
  final int type;

  _SetPasswordPageState createState() => _SetPasswordPageState();
}

class _SetPasswordPageState extends State<SetPasswordPage> {
  TextEditingController _newPasTC = TextEditingController();
  TextEditingController _confirmPasTC = TextEditingController();
  String _newPwdStr = "";
  String _confrimPwdStr = "";
  bool _isFetch = false;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var isSubmitBtn = false;
    if (_newPwdStr.length != 0 &&
        RegExp(PasswordReg).hasMatch(_newPwdStr) &&
        _newPwdStr == _confrimPwdStr) {
      isSubmitBtn = true;
    }
    // TODO: implement build
    return KeyboardDismissOnTap(
      child: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: MuslimTitle(title: tr("set_pwd_title")),
        ),
        body: ListView(
          children: [
            Container(
              margin: EdgeInsets.only(
                  top: 30.0, bottom: 20.0, left: 16.0, right: 16.0),
              child: Text("set_pwd_sub",
                      style: TextStyle(
                          fontSize: 16.0, fontWeight: FontWeight.w500))
                  .tr(),
            ),
            Container(
              decoration: BoxDecoration(
                border: Border(
                    top: BorderSide(color: HexColor("EAEAEA")),
                    bottom: BorderSide(color: HexColor("EAEAEA"))),
              ),
              child: TextField(
                controller: _newPasTC,
                obscureText: true,
                decoration: InputDecoration(
                    border: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.transparent)),
                    focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.transparent)),
                    enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.transparent)),
                    hintText: tr('new_pwd'),
                    hintStyle: TextStyle(color: HexColor('BCBCBC')),
                    fillColor: HexColor('FFFFFF'),
                    filled: true),
                onChanged: _newPasswordChange,
              ),
            ),
            Container(
              decoration: BoxDecoration(
                border: Border(bottom: BorderSide(color: HexColor("EAEAEA"))),
              ),
              child: TextField(
                controller: _confirmPasTC,
                obscureText: true,
                decoration: InputDecoration(
                    border: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.transparent)),
                    focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.transparent)),
                    enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.transparent)),
                    hintText: tr('confirm_pwd'),
                    hintStyle: TextStyle(color: HexColor('BCBCBC')),
                    fillColor: HexColor('FFFFFF'),
                    filled: true),
                onChanged: _confirmPasswordChange,
              ),
            ),
            Container(
              padding: EdgeInsets.fromLTRB(16.0, 20.0, 16.0, 0),
              child: Text(tr("pwd_rule")),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 16.0),
              child: Container(
                height: 50.0,
                width: MediaQuery.of(context).size.width,
                margin: EdgeInsets.only(top: 30),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(4.0))),
                child: FlatButton(
                  onPressed: isSubmitBtn ? _submitForm : null,
                  child: Text(tr("login_submit"),
                      style: TextStyle(fontSize: 16.0, color: Colors.white)),
                  color: HexColor('67C1BF'),
                  disabledColor: HexColor('67C1BF').withOpacity(0.5),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  _submitForm() {
    removeAllForce(context);
    if (!checkNetworkState(context) || _isFetch) return;
    setState(() {
      _isFetch = true;
    });

    UserModel model = Provider.of<UserModel>(context, listen: false);
    showWaitingDialog(context, "");
    var data = {
      "phoneNumber": widget.phoneNumber,
      "phonePrefix": widget.phonePrefix,
      "password": _newPwdStr,
      "tempToken": widget.tempToken
    };

    Global.normalApi.setLoginPassword(data, type: widget.type).then((value) {
      setState(() {
        _isFetch = false;
      });
      Navigator.of(context).pop();
      if (value != null && value.errorCode == SUCCESS) {
        if (widget.type == 0) {
          model.hasPwd = true;
          Navigator.of(context).popUntil(
              (route) => route.settings.name == "profile_account_security");
        } else {
          Navigator.of(context)
              .popUntil((route) => route.settings.name == "login");
        }
      } else {
        if (mounted) handleNetworkError(value, context);
      }
    }).catchError((error) {
      Navigator.of(context).pop();
      if (mounted) showSnackBar(context, tr("send_code_error"), type: 1);
    }).whenComplete(() => setState(() {
          _isFetch = false;
        }));
  }

  _newPasswordChange(String value) {
    value = value.replaceAll(" ", "");
    _newPasTC.value = TextEditingValue(
        text: value, selection: TextSelection.collapsed(offset: value.length));
    setState(() {
      _newPwdStr = value;
    });
  }

  _confirmPasswordChange(String value) {
    value = value.replaceAll(" ", "");
    _confirmPasTC.value = TextEditingValue(
        text: value, selection: TextSelection.collapsed(offset: value.length));
    setState(() {
      _confrimPwdStr = value;
    });
  }
}
