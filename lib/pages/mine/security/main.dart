import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:muslim/common/tools/hexColor.dart';
import 'package:muslim/common/provider/user.dart';
import 'package:muslim/global.dart';
import 'package:muslim/pages/widgets/backButton.dart';
import 'package:muslim/pages/widgets/muslim_title.dart';
import 'package:provider/provider.dart';

class AccountSecurity extends StatefulWidget {
  @override
  _AccountSecurityState createState() => _AccountSecurityState();
}

// "email": "Email",
// "ik_id": "IKHLAS ID",
// "phone": "Phone",

class _AccountSecurityState extends State<AccountSecurity> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    UserModel model = Provider.of<UserModel>(context);
    List<Map<String, dynamic>> tableData = [
      {"name": "ik_id", "value": model.user.id, "isTap": false, "click": null},
      {
        "name": "phone",
        "value": model.user.phoneNumber,
        "isTap": true,
        "click": (context, phoneNumber) {
          Navigator.of(context)
              .pushNamed("change_phone_number", arguments: phoneNumber);
        }
      },
      {
        "name": "email",
        "value": model.user.email,
        "isTap": true,
        "click": (context, email) {
          Navigator.of(context).pushNamed("change_email", arguments: email);
        }
      },
      {
        "name": "password",
        "value": model.hasPwd ? tr("has_setting") : tr("no_setting"),
        "isTap": true,
        "click": (context, hasPwd) {
          Navigator.of(context).pushNamed(
              hasPwd == tr("has_setting") ? "reset_password" : "send_code");
        },
        "margin": EdgeInsets.only(top: 8.0)
      }
    ];

    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: MuslimTitle(title: tr("account_security_title")),
        leading: MuslimBackButton(),
      ),
      body: Padding(
        padding: EdgeInsets.only(top: 8.0),
        child: ListView.builder(
          itemBuilder: (context, index) {
            Map<String, dynamic> data = tableData[index];
            return Container(
                height: 50.0,
                margin: data["margin"] ?? EdgeInsets.zero,
                padding: EdgeInsets.symmetric(horizontal: 16.0),
                decoration: BoxDecoration(
                    color: Colors.white,
                    border: Border(
                        bottom: BorderSide(color: HexColor("EAEAEA")),
                        top: BorderSide(
                            color: index == 0
                                ? HexColor("EAEAEA")
                                : Colors.transparent))),
                child: InkWell(
                  onTap: data["isTap"]
                      ? () {
                          data["click"](context, data["value"]);
                        }
                      : null,
                  child: Row(
                    children: [
                      Text(
                        data["name"],
                        style: TextStyle(
                            color: HexColor("212124"), fontSize: 16.0),
                      ).tr(),
                      Expanded(child: Container()),
                      Builder(builder: (context) {
                        return data["isTap"]
                            ? Container(
                                width: 200.0,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: [
                                    Text(
                                      data["value"] ?? "",
                                      style: TextStyle(
                                          color: HexColor("4C4C50"),
                                          fontSize: 14.0),
                                    ),
                                    Icon(
                                      Icons.navigate_next,
                                      color: HexColor("75767A"),
                                    )
                                  ],
                                ),
                              )
                            : Text(
                                data["value"] ?? "",
                                style: TextStyle(
                                    color: HexColor("4C4C50"), fontSize: 14.0),
                              );
                      })
                    ],
                  ),
                ));
          },
          itemCount: tableData.length,
        ),
      ),
    );
  }
}
