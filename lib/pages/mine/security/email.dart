import 'dart:async';

import 'package:connectivity/connectivity.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:muslim/common/api/error_network_handle.dart';
import 'package:muslim/common/core/storage.dart';
import 'package:muslim/common/provider/app.dart';
import 'package:muslim/common/tools/dialog.dart';
import 'package:muslim/common/tools/hexColor.dart';
import 'package:email_validator/email_validator.dart';
import 'package:muslim/common/tools/snackBar.dart';
import 'package:muslim/common/provider/user.dart';
import 'package:muslim/global.dart';
import 'package:muslim/pages/widgets/backButton.dart';
import 'package:muslim/pages/widgets/muslim_title.dart';
import 'package:provider/provider.dart';

class ChangeEmailPage extends StatefulWidget {
  ChangeEmailPage({Key key, @required this.email}) : super(key: key);
  final String email;

  @override
  _ChangeEmailState createState() => _ChangeEmailState();
}

class _ChangeEmailState extends State<ChangeEmailPage> {
  String _email;
  String _emailError;
  TextEditingController _tController;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _email = widget.email;
    _tController = TextEditingController(text: widget.email);
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return KeyboardDismissOnTap(
      child: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: MuslimTitle(title: tr("change_email_title")),
          leading: MuslimBackButton(),
        ),
        body: Padding(
          padding: EdgeInsets.only(top: 8.0),
          child: Column(
            children: [
              Row(
                children: [
                  Expanded(
                      child: Container(
                    height: 50.0,
                    padding: EdgeInsets.symmetric(horizontal: 16.0),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        border: Border.symmetric(
                            horizontal: BorderSide(color: HexColor("EAEAEA")))),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "email",
                          style: TextStyle(
                              color: HexColor("212124"), fontSize: 16.0),
                        ).tr(),
                        Expanded(
                            child: TextField(
                                onChanged: (value) {
                                  setState(() {
                                    _email = value;
                                  });
                                },
                                controller: _tController,
                                textAlign: TextAlign.end,
                                decoration: InputDecoration(
                                    border: InputBorder.none,
                                    hintText: tr("email_placehold"),
                                    suffixIcon: (_email == null || _email == "")
                                        ? null
                                        : IconButton(
                                            highlightColor: Colors.white,
                                            splashColor: Colors.transparent,
                                            icon: Icon(
                                              Icons.highlight_off,
                                              color: HexColor("787880")
                                                  .withOpacity(0.16),
                                            ),
                                            onPressed: () {
                                              _tController.clear();
                                              setState(() {
                                                _email = "";
                                              });
                                            }))))
                      ],
                    ),
                  ))
                ],
              ),
              Builder(builder: (context) {
                if (_emailError == null || _emailError.length == 0) {
                  return Container();
                }

                return Container(
                  child: Text(
                    _emailError,
                    style: TextStyle(color: Colors.red),
                  ),
                );
              }),
              Row(
                children: [
                  Expanded(
                      child: Container(
                    height: 44.0,
                    margin: EdgeInsets.all(16.0),
                    child: RaisedButton(
                      color: HexColor("67C1BF"),
                      disabledColor: HexColor("67C1BF").withOpacity(0.5),
                      onPressed: (_email == null || _email.length == 0)
                          ? null
                          : _changeEmailSubmit,
                      child: Text(
                        "login_submit",
                        style: TextStyle(color: Colors.white),
                      ).tr(),
                    ),
                  ))
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  void removeAllFocus() {
    final currentFocus = FocusScope.of(context);
    if (!currentFocus.hasPrimaryFocus && currentFocus.hasFocus) {
      FocusManager.instance.primaryFocus.unfocus();
    }
  }

  void _changeEmailSubmit() {
    removeAllFocus();
    if (!EmailValidator.validate(_email)) {
      setState(() {
        _emailError = tr("email_error_text");
      });
      return;
    }
    if (!checkNetworkState(context)) return;
    showWaitingDialog(context, null);
    Global.normalApi.changeUserInfo({"email": _email}).then((value) {
      Navigator.of(context).pop();
      if (value.errorCode == SUCCESS) {
        Global.normalApi.fetchUserInfo().then((userValue) {
          if (userValue != null && userValue.errorCode == SUCCESS) {
            UserModel model = Provider.of<UserModel>(context, listen: false);
            model.changeUser(userValue.data);
            model.hasPwd = userValue.data.hasPwd;
            Navigator.of(context).pop();
          } else {
            if (mounted) handleNetworkError(value, context);
          }
        });
        // Navigator.of(context).pushNamedAndRemoveUntil("main", (route) => false);
      } else {
        if (mounted)
          showSnackBar(context, value.developerMsg ?? tr("network_error"),
              type: 1);
      }
    }).catchError((error) {
      print("changeUserInfo: $error");
      Navigator.of(context).pop();
      if (mounted) showSnackBar(context, tr("network_error"));
    });
  }
}
