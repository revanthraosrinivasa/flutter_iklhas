import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:muslim/common/api/error_network_handle.dart';
import 'package:muslim/common/core/storage.dart';
import 'package:muslim/common/provider/user.dart';
import 'package:muslim/common/tools/dialog.dart';
import 'package:muslim/common/tools/hexColor.dart';
import 'package:muslim/common/tools/reForce.dart';
import 'package:muslim/common/tools/snackBar.dart';
import 'package:muslim/global.dart';
import 'package:muslim/pages/widgets/muslim_tile.dart';
import 'package:muslim/pages/widgets/muslim_title.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:provider/provider.dart';

class CheckCaptchaPage extends StatefulWidget {
  @override
  _CheckCaptchaPageState createState() => _CheckCaptchaPageState();
}

class _CheckCaptchaPageState extends State<CheckCaptchaPage> {
  int _timerCount = 60;
  TextEditingController _mobileCodeTC = TextEditingController();
  String _codeNumber = "";
  bool _isFetch = false;
  bool _isFirst = true;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      if (_isFirst) _fetchVertificationCode();
      setState(() {
        _isFirst = false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    UserModel model = Provider.of<UserModel>(context);
    bool isSubmitBtn = false;
    if (_codeNumber != null && _codeNumber.length == 6) {
      isSubmitBtn = true;
    }
    // TODO: implement build
    return KeyboardDismissOnTap(
        child: Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: MuslimTitle(title: tr("send_code_title")),
      ),
      body: ListView(
        children: [
          Container(
            margin: EdgeInsets.only(
                top: 30.0, bottom: 20.0, left: 16.0, right: 16.0),
            child: Text("has_send_code",
                    style:
                        TextStyle(fontSize: 16.0, fontWeight: FontWeight.w500))
                .tr(),
          ),
          Container(
            decoration: BoxDecoration(
                border: Border(top: BorderSide(color: HexColor("EAEAEA")))),
            child: MuslimTile(
              title: tr("login_mobile"),
              subTitle: "+${model.user.phonePrefix} ${model.user.phoneNumber}",
            ),
          ),
          Container(
            decoration: BoxDecoration(
                border: Border(bottom: BorderSide(color: HexColor("EAEAEA")))),
            child: ListTile(
              tileColor: Colors.white,
              title: TextField(
                controller: _mobileCodeTC,
                decoration: InputDecoration(
                    contentPadding: EdgeInsets.zero,
                    border: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.transparent)),
                    focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.transparent)),
                    enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.transparent)),
                    hintText: tr('login_code_placehold'),
                    hintStyle: TextStyle(color: HexColor('BCBCBC')),
                    fillColor: HexColor('FFFFFF'),
                    filled: true),
                keyboardType: TextInputType.number,
                onChanged: _codeNumberChange,
              ),
              trailing: Container(
                margin: EdgeInsets.only(left: 10.0),
                child: ButtonTheme(
                  height: 50.0,
                  minWidth: 100.0,
                  buttonColor: HexColor('67C1BF'),
                  child: RaisedButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(4.0)),
                    onPressed:
                        _timerCount != 60 ? null : _fetchVertificationCode,
                    child: Text(
                      _timerCount != 60 ? "$_timerCount" : tr("login_request"),
                      style: TextStyle(
                          fontWeight: FontWeight.bold, fontSize: 16.0),
                    ),
                    textColor: Colors.white,
                    elevation: 0,
                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 16.0),
            child: Container(
              height: 50.0,
              width: MediaQuery.of(context).size.width,
              margin: EdgeInsets.only(top: 60),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(4.0))),
              child: FlatButton(
                onPressed: isSubmitBtn ? _submitForm : null,
                child: Text(tr("login_submit"),
                    style: TextStyle(fontSize: 16.0, color: Colors.white)),
                color: HexColor('67C1BF'),
                disabledColor: HexColor('67C1BF').withOpacity(0.5),
              ),
            ),
          )
        ],
      ),
    ));
  }

  _submitForm() {
    removeAllForce(context);
    if (!checkNetworkState(context) || _isFetch) return;
    setState(() {
      _isFetch = true;
    });
    UserModel model = Provider.of<UserModel>(context, listen: false);
    showWaitingDialog(context, "");
    var data = {
      "phoneNumber": model.user.phoneNumber,
      "phonePrefix": model.user.phonePrefix,
      "pinCode": _codeNumber
    };
    Global.normalApi.checkPinCode(data).then((value) {
      Navigator.of(context).pop();
      setState(() {
        _isFetch = false;
      });
      if (value != null && value.errorCode == SUCCESS) {
        Navigator.of(context).pushNamed("set_password", arguments: {
          "tempToken": value.data["tempToken"],
          "phoneNumber": model.user.phoneNumber,
          "phonePrefix": model.user.phonePrefix,
          "type": 0
        });
      } else {
        if (value != null && value.errorCode == AuthCodeError) {
          if (mounted) showSnackBar(context, tr("verfication_error"), type: 1);
          return;
        }
        if (mounted) handleNetworkError(value, context);
      }
    }).catchError((error) {
      Navigator.of(context).pop();
      if (mounted) showSnackBar(context, tr("send_code_error"), type: 1);
    });
  }

  _codeNumberChange(String value) {
    setState(() {
      _codeNumber = value;
    });
  }

  _fetchVertificationCode() async {
    removeAllForce(context);
    if (!checkNetworkState(context) || _isFetch) return;
    //发送请求
    setState(() {
      _isFetch = true;
    });
    UserModel model = Provider.of<UserModel>(context, listen: false);
    showWaitingDialog(context, "");
    Global.normalApi.fetchPinCode({
      "phoneNumber": model.user.phoneNumber,
      "phonePrefix": model.user.phonePrefix
    }).then((value) {
      if (value != null && value.errorCode == SUCCESS) {
        if (mounted) showSnackBar(context, tr("message_has_send"));
        _startTimer();
      } else {
        if (mounted) showSnackBar(context, tr("send_code_error"), type: 1);
      }
      setState(() {
        _isFetch = false;
      });
    }).whenComplete(() {
      Navigator.of(context).pop();
    });
  }

  void _startTimer() {
    if (_timerCount == 60) {
      Timer.periodic(Duration(seconds: 1), (timer) {
        if (_timerCount - 1 < 0) {
          timer.cancel();
          timer = null;
          if (mounted) {
            setState(() {
              _timerCount = 60;
            });
          }
        } else {
          if (mounted) {
            setState(() {
              _timerCount = _timerCount - 1;
            });
          }
        }
      });
    }
  }
}
