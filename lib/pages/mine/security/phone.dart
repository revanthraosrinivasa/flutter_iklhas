import 'dart:async';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:libphonenumber/libphonenumber.dart';
import 'package:muslim/common/api/error_network_handle.dart';
import 'package:muslim/common/core/storage.dart';
import 'package:muslim/common/helper/helper.dart';
import 'package:muslim/common/provider/app.dart';
import 'package:muslim/common/tools/dialog.dart';
import 'package:muslim/common/tools/filterNumber.dart';
import 'package:muslim/common/tools/hexColor.dart';
import 'package:muslim/common/tools/snackBar.dart';
import 'package:muslim/common/provider/user.dart';
import 'package:muslim/global.dart';
import 'package:muslim/modals/login.dart';
import 'package:muslim/pages/widgets/backButton.dart';
import 'package:muslim/pages/widgets/horizontalLine.dart';
import 'package:muslim/pages/widgets/muslim_title.dart';
import 'package:provider/provider.dart';

class ChangePhoneNumber extends StatefulWidget {
  ChangePhoneNumber({Key key, @required this.phoneNumber}) : super(key: key);

  final String phoneNumber;

  _ChangePhoneNumberState createState() => _ChangePhoneNumberState();
}

class _ChangePhoneNumberState extends State<ChangePhoneNumber> {
  String _newPhone;
  String testPrifix = "60";
  String testIso = "MY";
  String _errorPhone = "";

  TextEditingController _controller = TextEditingController();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _newPhone = "";
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    UserModel model = Provider.of<UserModel>(context);
    User user = model.user;

    return KeyboardDismissOnTap(
        child: Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: MuslimTitle(title: tr("change_phone_title")),
        leading: MuslimBackButton(),
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.only(top: 8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              padding: EdgeInsets.all(16.0),
              decoration: BoxDecoration(
                  border:
                      Border(bottom: BorderSide(color: HexColor("EAEAEA")))),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("change_phone_subtitle",
                          style: TextStyle(
                              color: HexColor("212124"), fontSize: 16.0))
                      .tr(),
                  Container(height: 20.0),
                  Text(
                    "change_phone_current",
                    style: TextStyle(color: HexColor("212124"), fontSize: 16.0),
                  ).tr(args: [user.phonePrefix, user.phoneNumber]),
                ],
              ),
            ),
            ListTile(
              tileColor: Colors.white,
              title: Text("change_phone_country").tr(),
              trailing: Text("Malaysia"),
            ),
            HorizontalLine(),
            ListTile(
              tileColor: Colors.white,
              title: Text("+60"),
              trailing: Container(
                alignment: Alignment.centerRight,
                width: 200.0,
                child: TextField(
                  keyboardType: TextInputType.phone,
                  controller: _controller,
                  textAlign: TextAlign.end,
                  onChanged: (value) {
                    String number = filterPhoneNumber(value);
                    setState(() {
                      _newPhone = number;
                      _errorPhone = "";
                    });
                    _controller.value = TextEditingValue(
                        text: number,
                        selection:
                            TextSelection.collapsed(offset: number.length));
                  },
                  decoration: InputDecoration(
                      border: InputBorder.none,
                      hintText: tr("new_phone_placehold"),
                      hintStyle:
                          TextStyle(fontSize: 14.0, color: HexColor("BCBCBC"))),
                ),
              ),
            ),
            HorizontalLine(),
            Builder(builder: (context) {
              return _errorPhone != null && _errorPhone.length != 0
                  ? Container(
                      padding: EdgeInsets.symmetric(horizontal: 16.0),
                      height: 40,
                      child: Text(
                        _errorPhone,
                        style: TextStyle(color: Colors.red),
                      ),
                    )
                  : Container();
            }),
            Row(
              children: [
                Expanded(
                    child: Container(
                  height: 44.0,
                  margin: EdgeInsets.all(16.0),
                  child: RaisedButton(
                    color: HexColor("67C1BF"),
                    disabledColor: HexColor("67C1BF").withOpacity(0.5),
                    onPressed: (_newPhone == null || _newPhone.length == 0)
                        ? null
                        : _changePhoneNumber,
                    child: Text(
                      "Next",
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                ))
              ],
            )
          ],
        ),
      ),
    ));
  }

  void _changePhoneNumber() async {
    final currentFocus = FocusScope.of(context);
    if (!currentFocus.hasPrimaryFocus && currentFocus.hasFocus) {
      FocusManager.instance.primaryFocus.unfocus();
    }
    if (_newPhone == null ||
        _newPhone.length == 0 ||
        _newPhone.substring(0, 1) != "1") {
      setState(() {
        _errorPhone = tr("phone_error_tip");
      });
      return;
    }

    try {
      bool isValid = await PhoneNumberUtil.isValidPhoneNumber(
          phoneNumber: _newPhone, isoCode: testIso);
      if (isValid) {
        Navigator.of(context)
            .pushNamed("change_phone_number_next", arguments: _newPhone);
      } else {
        setState(() {
          _errorPhone = tr("phone_error_tip");
        });
      }
    } catch (error) {
      setState(() {
        _errorPhone = tr("phone_error_tip");
      });
    }
  }
}

class ChangePhoneNumberNext extends StatefulWidget {
  ChangePhoneNumberNext({Key key, @required this.newPhoneNumber})
      : super(key: key);

  final String newPhoneNumber;

  @override
  _ChangePhoneNumberNextState createState() => _ChangePhoneNumberNextState();
}

class _ChangePhoneNumberNextState extends State<ChangePhoneNumberNext> {
  String _captcha;
  TextEditingController _controller = TextEditingController();
  int _timerCount = 60;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _captcha = "";
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return KeyboardDismissOnTap(
        child: Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: MuslimTitle(title: tr("change_phone_title")),
        leading: MuslimBackButton(),
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              margin: EdgeInsets.all(16.0),
              child: Text(
                "change_phone_current",
                style: TextStyle(color: HexColor("212124"), fontSize: 16.0),
              ).tr(args: ["+60", widget.newPhoneNumber]),
            ),
            Container(
              height: 50,
              padding: EdgeInsets.symmetric(horizontal: 16.0),
              decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border.symmetric(
                      horizontal: BorderSide(color: HexColor("EAEAEA")))),
              child: Row(
                children: [
                  Expanded(
                      child: TextField(
                    keyboardType: TextInputType.phone,
                    controller: _controller,
                    onChanged: (value) {
                      String number = filterPhoneNumber(value);
                      if (number.length > 6) {
                        number = number.substring(0, 6);
                      }
                      setState(() {
                        _captcha = number;
                        _controller.value = TextEditingValue(
                            text: number,
                            selection:
                                TextSelection.collapsed(offset: number.length));
                      });
                    },
                    decoration: InputDecoration(
                        border: InputBorder.none,
                        hintText: tr("Please enter your captcha"),
                        hintStyle: TextStyle(
                            fontSize: 14.0, color: HexColor("BCBCBC"))),
                  )),
                  RaisedButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(4.0)),
                    onPressed: _timerCount != 60 ? null : _fetchCaptcha,
                    child: Text(
                      _timerCount != 60 ? "$_timerCount" : tr("login_request"),
                      style: TextStyle(
                          fontWeight: FontWeight.bold, fontSize: 16.0),
                    ),
                    textColor: Colors.white,
                    color: HexColor("67C1BF"),
                    elevation: 0,
                  ),
                ],
              ),
            ),
            Row(
              children: [
                Expanded(
                    child: Container(
                  height: 44.0,
                  margin: EdgeInsets.all(16.0),
                  child: RaisedButton(
                    color: HexColor("67C1BF"),
                    disabledColor: HexColor("67C1BF").withOpacity(0.5),
                    onPressed: (_captcha == null || _captcha.length != 6)
                        ? null
                        : _changePhoneNumberSubmit,
                    child: Text(
                      "login_submit",
                      style: TextStyle(color: Colors.white),
                    ).tr(),
                  ),
                ))
              ],
            )
          ],
        ),
      ),
    ));
  }

  void removeAllFocus() {
    final currentFocus = FocusScope.of(context);
    if (!currentFocus.hasPrimaryFocus && currentFocus.hasFocus) {
      FocusManager.instance.primaryFocus.unfocus();
    }
  }

  void _fetchCaptcha() async {
    removeAllFocus();
    if (!checkNetworkState(context)) return;
    showWaitingDialog(context, "");
    Global.normalApi.fetchPinCode({
      "phoneNumber": widget.newPhoneNumber,
      "phonePrefix": "60"
    }).then((value) {
      Navigator.of(context).pop();
      if (value != null && value.errorCode == SUCCESS) {
        if (mounted) showSnackBar(context, tr("message_has_send"));
        //发送请求
        _startTimer();
      } else {
        if (mounted)
          handleNetworkError(value, context,
              errorString: tr("send_code_error"));
      }
    }).catchError((error) {
      Navigator.of(context).pop();
      if (mounted) showSnackBar(context, tr("network_error"));
    });
  }

  void _startTimer() {
    if (_timerCount == 60) {
      Timer.periodic(Duration(seconds: 1), (timer) {
        if (_timerCount - 1 < 0) {
          timer.cancel();
          timer = null;
          setState(() {
            _timerCount = 60;
          });
        } else {
          setState(() {
            _timerCount = _timerCount - 1;
          });
        }
      });
    }
  }

  void _changePhoneNumberSubmit() {
    removeAllFocus();
    if (!checkNetworkState(context)) return;
    var params = {
      "phoneNumber": widget.newPhoneNumber,
      "phonePrefix": "60",
      "pinCode": _captcha
    };
    showWaitingDialog(context, "");
    Global.normalApi.changeUserPhone(params).then((value) {
      Navigator.of(context).pop();
      if (value != null && value.errorCode == SUCCESS) {
        UserModel userModol = Provider.of<UserModel>(context, listen: false);
        userModol.changeToken(value.data.token);
        userModol.changeUser(value.data.user);
        StorageHelper().setJSON(USER_TOKEN, value.data.token);
        Navigator.of(context)
            .popUntil(ModalRoute.withName('profile_account_security'));
      } else {
        if (mounted)
          handleNetworkError(value, context,
              errorString: tr("change_phone_error"));
      }
    }).catchError((error) {
      Navigator.of(context).pop();
      if (mounted) showSnackBar(context, tr("network_error"));
    });
  }
}
