import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter_picker/Picker.dart';
import 'package:muslim/common/api/error_network_handle.dart';
import 'package:muslim/common/core/storage.dart';
import 'package:muslim/common/tools/dialog.dart';
import 'package:muslim/common/tools/hexColor.dart';
import 'package:muslim/common/tools/snackBar.dart';
import 'package:muslim/common/provider/user.dart';
import 'package:muslim/global.dart';
import 'package:muslim/modals/login.dart';
import 'package:muslim/pages/widgets/backButton.dart';
import 'package:muslim/pages/widgets/muslim_title.dart';
import 'package:provider/provider.dart';

class MyProfile extends StatefulWidget {
  MyProfile({Key key}) : super(key: key);

  @override
  _MyProfileState createState() => _MyProfileState();
}

class _MyProfileState extends State<MyProfile> {
  List<String> genders = [tr("Female"), tr("Male")];
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    UserModel model = Provider.of<UserModel>(context);
    User user = model.user;
    List<Map<String, dynamic>> tableData = [
      {
        "name": "profile_photo",
        "height": 92.0,
        "value": User_Avatars[model.avatarIndex],
        "isIcon": true,
        "isTap": true,
        "click": (context) {
          Navigator.of(context).pushNamed("profile_photo");
        }
      },
      // {
      //   "name": "profile_nick_name",
      //   "value": user.nickName,
      //   "isTap": false,
      //   "isIcon": false,
      // },
      {
        "name": "profile_fullname",
        "value": model.fullName,
        "isTap": true,
        "isIcon": false,
        "click": (context) {
          Navigator.of(context)
              .pushNamed("profile_fullname", arguments: user.fullName);
        }
      },
      // {
      //   "name": "profile_ikhlas_id",
      //   "value": user.id,
      //   "isTap": false,
      //   "isIcon": false,
      // },
      {
        "name": "profile_birthday",
        "value": model.birthday == null || model.birthday.length == 0
            ? ""
            : DateFormat("yyyy-MM-dd").format(
                DateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(model.birthday)),
        "isTap": true,
        "isIcon": false,
        "click": (context) {
          // Navigator.of(context).pushNamed("profile_photo");
          Picker picker = new Picker(
            height: 250.0,
            itemExtent: 44.0,
            adapter: DateTimePickerAdapter(
                value: model.birthday == null || model.birthday.length == 0
                    ? DateTime.now()
                    : DateFormat("yyyy-MM-dd'T'HH:mm:ss")
                        .parse(model.birthday)),
            title: Text(tr("select_data")),
            selectedTextStyle: TextStyle(color: HexColor("67C1BF")),
            hideHeader: !Global.isIOS,
            confirmText: tr("confirmText"),
            cancelText: tr("cancelText"),
            cancelTextStyle:
                TextStyle(color: HexColor("67C1BF"), fontSize: 14.0),
            confirmTextStyle:
                TextStyle(color: HexColor("67C1BF"), fontSize: 14.0),
            onConfirm: (Picker picker, List value) {
              print(
                  "picker: ${(picker.adapter as DateTimePickerAdapter).value}");
              if (!checkNetworkState(context)) return;
              DateTime time = (picker.adapter as DateTimePickerAdapter).value;
              var params = {
                "birthday":
                    DateFormat("yyyy-MM-dd'T'HH:mm:ss").format(time) + "Z"
              };
              showWaitingDialog(context, null);
              Global.normalApi.changeUserInfo(params).then((value) {
                Navigator.of(context).pop();
                if (value != null && value.errorCode == SUCCESS) {
                  model.birthday =
                      DateFormat("yyyy-MM-dd'T'HH:mm:ss").format(time) + "Z";
                } else {
                  if (mounted)
                    handleNetworkError(value, context, errorString: "Error!");
                }
              }).catchError((error) {
                print("changeUserInfo :$error");
                Navigator.of(context).pop();
                if (mounted) showSnackBar(context, tr("network_error"));
              });
            },
          );
          if (Global.isIOS) {
            picker.showModal(context);
          } else {
            picker.showDialog(context);
          }
        }
      },
      {
        "name": "profile_gender",
        "value": model.gender != null && model.gender.length != 0
            ? model.gender.toLowerCase() == "female"
                ? tr("Female")
                : tr("Male")
            : "",
        "isTap": true,
        "isIcon": false,
        "click": (context) {
          // Navigator.of(context).pushNamed("profile_photo");
          new Picker(
            itemExtent: 44.0,
            selecteds: [user.gender != null && user.gender == "male" ? 1 : 0],
            adapter: PickerDataAdapter(pickerdata: genders),
            selectedTextStyle: TextStyle(color: HexColor("67C1BF")),
            onConfirm: (picker, selecteds) {
              print("Gender change ${genders[selecteds[0]]}");
              if (!checkNetworkState(context)) return;
              var params = {"gender": selecteds[0] == 0 ? "female" : "male"};
              showWaitingDialog(context, null);
              Global.normalApi.changeUserInfo(params).then((value) {
                Navigator.of(context).pop();
                if (value != null && value.errorCode == SUCCESS) {
                  model.gender = selecteds[0] == 0 ? "female" : "male";
                } else {
                  if (mounted)
                    handleNetworkError(value, context, errorString: "Error!");
                }
              }).catchError((error) {
                print("changeUserInfo :$error");
                Navigator.of(context).pop();
                if (mounted) showSnackBar(context, tr("network_error"));
              });
            },
          ).showModal(context);
        }
      },
    ];

    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        centerTitle: true,
        title: MuslimTitle(title: tr("my_profile_title")),
        leading: MuslimBackButton(),
      ),
      body: Padding(
          padding: EdgeInsets.only(top: 8.0),
          child: ListView(
              children: tableData
                  .map((e) => Container(
                      padding: EdgeInsets.symmetric(horizontal: 16.0),
                      height: e["height"] ?? 50.0,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          border: Border(
                              top: BorderSide(
                                  color: e["height"] != null
                                      ? HexColor("EAEAEA")
                                      : Colors.transparent),
                              bottom: BorderSide(color: HexColor("EAEAEA")))),
                      alignment: Alignment.center,
                      child: InkWell(
                        onTap: e["isTap"]
                            ? () {
                                e["click"](context);
                              }
                            : null,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              e["name"],
                              style: TextStyle(
                                color: HexColor("212124"),
                                fontSize: 16.0,
                              ),
                            ).tr(),
                            Container(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  e["isIcon"]
                                      ? e["value"]
                                      : Text(
                                          e["value"],
                                          style: TextStyle(
                                              color: HexColor("4C4C50"),
                                              fontSize: 14.0),
                                        ),
                                  e["isTap"]
                                      ? Icon(
                                          Icons.navigate_next,
                                          color: HexColor("75767A"),
                                        )
                                      : Container()
                                ],
                              ),
                            )
                          ],
                        ),
                      )))
                  .toList())),
    );
  }
}
