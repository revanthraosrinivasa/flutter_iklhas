import 'dart:async';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:muslim/common/api/error_network_handle.dart';
import 'package:muslim/common/core/storage.dart';
import 'package:muslim/common/provider/app.dart';
import 'package:muslim/common/tools/dialog.dart';
import 'package:muslim/common/tools/hexColor.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:muslim/common/tools/snackBar.dart';
import 'package:muslim/common/provider/user.dart';
import 'package:muslim/global.dart';
import 'package:muslim/modals/login.dart';
import 'package:muslim/pages/widgets/backButton.dart';
import 'package:muslim/pages/widgets/muslim_title.dart';
import 'package:provider/provider.dart';

class ProfileFullName extends StatefulWidget {
  ProfileFullName({Key key, this.fullName}) : super(key: key);
  final String fullName;
  @override
  _ProfileFullNameState createState() => _ProfileFullNameState();
}

class _ProfileFullNameState extends State<ProfileFullName> {
  TextEditingController _tController;

  String _fullName;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _tController = TextEditingController(text: widget.fullName ?? "");
    _fullName = widget.fullName ?? "";
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return KeyboardDismissOnTap(
        child: Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: MuslimTitle(title: tr("full_name_setting")),
        leading: MuslimBackButton(),
      ),
      body: Padding(
          padding: EdgeInsets.only(top: 16.0),
          child: Column(
            children: [
              Container(
                decoration: BoxDecoration(
                    color: Colors.white,
                    border: Border.symmetric(
                        horizontal: BorderSide(color: HexColor("EAEAEA")))),
                child: TextField(
                  controller: _tController,
                  onChanged: (value) {
                    setState(() {
                      _fullName = value;
                    });
                  },
                  decoration: InputDecoration(
                      contentPadding: EdgeInsets.symmetric(horizontal: 12.0),
                      hintText: tr("profile_fullname"),
                      border: InputBorder.none,
                      suffix: (_fullName == null || _fullName == "")
                          ? null
                          : IconButton(
                              highlightColor: Colors.white,
                              splashColor: Colors.transparent,
                              icon: Icon(
                                Icons.highlight_off,
                                color: HexColor("787880").withOpacity(0.16),
                              ),
                              onPressed: () {
                                _tController.clear();
                              })),
                ),
              ),
              Row(
                children: [
                  Expanded(
                      child: Container(
                    margin: EdgeInsets.all(16.0),
                    height: 44.0,
                    child: RaisedButton(
                      onPressed:
                          _fullName.length == 0 ? null : _changeUserFullName,
                      child: Text("login_submit",
                              style: TextStyle(color: Colors.white))
                          .tr(),
                      color: HexColor("67C1BF"),
                      disabledColor: HexColor("67C1BF").withOpacity(0.5),
                    ),
                  ))
                ],
              )
            ],
          )),
    ));
  }

  _changeUserFullName() async {
    if (!checkNetworkState(context)) return;
    showWaitingDialog(context, null);
    UserModel model = Provider.of<UserModel>(context, listen: false);
    var params = {"fullname": _fullName};
    Global.normalApi.changeUserInfo(params).then((value) {
      Navigator.of(context).pop();
      if (value != null && value.errorCode == SUCCESS) {
        Global.normalApi.fetchUserInfo().then((userValue) {
          if (userValue != null && userValue.errorCode == SUCCESS) {
            model.changeUser(userValue.data);
            model.fullName = _fullName;
            model.hasPwd = userValue.data.hasPwd;
            Navigator.of(context).pop();
          } else {
            if (mounted) handleNetworkError(value, context);
          }
        });
      } else {
        if (mounted) handleNetworkError(value, context, errorString: "Error!");
      }
    }).catchError((error) {
      print("changeUserInfo :$error");
      Navigator.of(context).pop();
      if (mounted) showSnackBar(context, tr("network_error"));
    });
  }
}
