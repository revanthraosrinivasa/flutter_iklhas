import 'dart:async';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:muslim/common/api/error_network_handle.dart';
import 'package:muslim/common/core/storage.dart';
import 'package:muslim/common/provider/app.dart';
import 'package:muslim/common/tools/dialog.dart';
import 'package:muslim/common/tools/hexColor.dart';
import 'package:muslim/common/tools/snackBar.dart';
import 'package:muslim/common/provider/user.dart';
import 'package:muslim/global.dart';
import 'package:muslim/modals/login.dart';
import 'package:muslim/pages/widgets/backButton.dart';
import 'package:muslim/pages/widgets/muslim_title.dart';
import 'package:provider/provider.dart';

class ProfilePhoto extends StatefulWidget {
  ProfilePhoto({Key key}) : super(key: key);

  @override
  _ProfilePhotoState createState() => _ProfilePhotoState();
}

class _ProfilePhotoState extends State<ProfilePhoto> {
  int _selectedIndex = -1;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    UserModel model = Provider.of<UserModel>(context);
    // TODO: implement build
    double width = MediaQuery.of(context).size.width;

    double space = (width - 32 - 205) / 2;

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: MuslimTitle(title: tr("profile_photo")),
        leading: MuslimBackButton(),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: EdgeInsets.only(top: 8.0),
            padding: EdgeInsets.symmetric(vertical: 16.0),
            decoration: BoxDecoration(
                color: Colors.white,
                border: Border(
                    top: BorderSide(
                      color: HexColor("EAEAEA"),
                    ),
                    bottom: BorderSide(color: HexColor("EAEAEA")))),
            child: User_Avatars[
                _selectedIndex < 0 ? model.avatarIndex : _selectedIndex],
            alignment: Alignment.center,
          ),
          Container(
            padding: EdgeInsets.all(16.0),
            child: Text(
              "recommended_avatar",
              style: TextStyle(fontSize: 16.0, color: HexColor("212124")),
            ).tr(),
          ),
          Container(
              padding: EdgeInsets.all(16.0),
              decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border(
                      bottom: BorderSide(color: HexColor("EAEAEA")),
                      top: BorderSide(color: HexColor("EAEAEA")))),
              height: 92.0,
              child: GridView.builder(
                padding: EdgeInsets.symmetric(horizontal: 16.0),
                scrollDirection: Axis.horizontal,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 1,
                    mainAxisSpacing: space,
                    childAspectRatio: 1.0),
                itemBuilder: (context, index) {
                  return InkWell(
                    onTap: () {
                      setState(() {
                        _selectedIndex = index;
                      });
                    },
                    child: User_Avatars[index],
                  );
                },
                itemCount: User_Avatars.length,
              )),
          Row(
            children: [
              Expanded(
                  child: Container(
                margin: EdgeInsets.all(16.0),
                height: 44.0,
                child: RaisedButton(
                  onPressed: _selectedIndex < 0
                      ? null
                      : () async {
                          AppSettingModel appSettingModel =
                              Provider.of<AppSettingModel>(context,
                                  listen: false);
                          if (appSettingModel.networkState ==
                              ConnectivityResult.none) {
                            if (mounted)
                              showSnackBar(context, tr("no_network_error"));
                            return;
                          }
                          showWaitingDialog(context, null);
                          var params = {"avatarId": "${_selectedIndex + 1}"};
                          Global.normalApi.changeUserInfo(params).then((value) {
                            Navigator.of(context).pop();
                            if (value != null && value.errorCode == SUCCESS) {
                              Global.normalApi
                                  .fetchUserInfo()
                                  .then((valueInfo) {
                                if (valueInfo != null &&
                                    valueInfo.errorCode == SUCCESS) {
                                  UserModel userModol = Provider.of<UserModel>(
                                      context,
                                      listen: false);
                                  userModol.changeUser(valueInfo.data);
                                  Navigator.of(context).pop();
                                } else {
                                  if (mounted)
                                    showSnackBar(context, tr("network_error"),
                                        type: 1);
                                }
                              }).catchError((error) {
                                print("_fetchUserInfo error: $error");
                              });
                            } else {
                              if (mounted)
                                handleNetworkError(value, context,
                                    errorString: tr("change_avatar_error"));
                            }
                          }).catchError((error) {
                            Navigator.of(context).pop();
                            if (mounted)
                              showSnackBar(context, tr("network_error"));
                          });
                        },
                  child: Text("login_submit",
                          style: TextStyle(color: HexColor("FFFFFF")))
                      .tr(),
                  color: HexColor("67C1BF"),
                  disabledColor: HexColor("67C1BF").withOpacity(0.5),
                ),
              ))
            ],
          )
        ],
      ),
    );
  }
}
