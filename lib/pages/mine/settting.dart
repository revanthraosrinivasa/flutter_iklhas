import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter_easyrefresh/easy_refresh.dart';
import 'package:muslim/common/api/error_network_handle.dart';
import 'package:muslim/common/provider/app.dart';
import 'package:muslim/common/core/storage.dart';
import 'package:muslim/common/helper/helper.dart';
import 'package:muslim/common/tools/dialog.dart';
import 'package:muslim/common/tools/hexColor.dart';
import 'package:muslim/common/provider/user.dart';
import 'package:muslim/global.dart';
import 'package:muslim/pages/widgets/backButton.dart';
import 'package:muslim/pages/widgets/muslim_tile.dart';
import 'package:muslim/pages/widgets/muslim_title.dart';
import 'package:provider/provider.dart';
import 'package:sentry_flutter/sentry_flutter.dart' as SentryUser;

class SettingPage extends StatefulWidget {
  @override
  _SettingPageState createState() => _SettingPageState();
}

class _SettingPageState extends State<SettingPage> {
  final List<Map<String, dynamic>> settingNames = [
    {"name": "my_profile_title", "icon": Icons.person},
    {"name": "account_security_title", "icon": Icons.lock},
  ];
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      backgroundColor: Colors.grey[100],
      appBar: AppBar(
        centerTitle: true,
        title: MuslimTitle(title: tr("settings")),
        leading: MuslimBackButton(),
      ),
      body: EasyRefresh(
        child: Column(
          children: [
            ListView.builder(
              padding: EdgeInsets.only(top: 20.0),
              shrinkWrap: true,
              physics: ScrollPhysics(),
              itemBuilder: (context, index) {
                if (index == 0) {
                  return Container(
                    decoration: BoxDecoration(
                        border:
                            Border(top: BorderSide(color: HexColor("EAEAEA")))),
                    child: _listTileWithIndex(context, index),
                  );
                }
                return _listTileWithIndex(context, index);
              },
              itemCount: settingNames.length,
            ),
            Container(
                margin: EdgeInsets.only(top: 8.0),
                decoration: BoxDecoration(
                    color: Colors.white,
                    border: Border(top: BorderSide(color: HexColor("EAEAEA")))),
                height: 50.0,
                child: MuslimTile(
                  callback: () {
                    Navigator.of(context).pushNamed("general");
                  },
                  title: tr("general_title"),
                  leading:
                      Icon(Icons.settings, size: 24, color: HexColor("75767A")),
                  trailing: Icon(
                    Icons.navigate_next,
                    color: HexColor("75767A"),
                  ),
                )),
            Container(
                margin: EdgeInsets.only(top: 8.0, bottom: 16.0),
                decoration: BoxDecoration(
                    color: Colors.white,
                    border: Border(top: BorderSide(color: HexColor("EAEAEA")))),
                height: 50.0,
                child: MuslimTile(
                  callback: () {
                    Navigator.of(context).pushNamed("about");
                  },
                  title: tr("about_title"),
                  subTitle:
                      tr("about_version", args: [Global.packageInfo.version]),
                  leading: Image.asset(
                    "assets/images/ikhlas_logo.png",
                    width: 24,
                  ),
                  trailing: Icon(
                    Icons.navigate_next,
                    color: HexColor("75767A"),
                  ),
                )),
            Row(
              children: [
                Container(
                  height: 56.0,
                  padding: EdgeInsets.symmetric(horizontal: 16.0),
                  width: MediaQuery.of(context).size.width,
                  child: RaisedButton(
                    color: HexColor("FB7268"),
                    textColor: Colors.white,
                    elevation: 0.0,
                    onPressed: () {
                      showWaitingDialog(context, null);
                      Global.normalApi.logout().then((value) async {
                        Navigator.of(context).pop();
                        if (value != null || value.errorCode == SUCCESS) {
                          StorageHelper().remove(USER_TOKEN);
                          await Global.flutterLocalNotificationsPlugin
                              .cancelAll();
                          SentryUser.Sentry.configureScope((scope) {
                            scope.user = null;
                          });
                          Provider.of<AppSettingModel>(context, listen: false)
                              .homeIsLogin = true;
                          Navigator.of(context).pushNamedAndRemoveUntil(
                              "introduce", (route) => false);
                        } else {
                          if (mounted)
                            handleNetworkError(value, context,
                                errorString: tr("Logout Error"));
                        }
                      });
                    },
                    child: Text(
                      'log_out',
                      textScaleFactor: 1.2,
                    ).tr(),
                  ),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget _listTileWithIndex(BuildContext context, int index) {
    Map<String, dynamic> setting = settingNames[index];
    return MuslimTile(
      leading: Icon(setting["icon"], color: HexColor("75767A")),
      title: tr(setting["name"]),
      trailing: Icon(
        Icons.navigate_next,
        color: HexColor("75767A"),
      ),
      callback: () {
        UserModel userModel = Provider.of<UserModel>(context, listen: false);
        switch (index) {
          case 0:
            if (userModel.user != null) {
              Navigator.of(context).pushNamed("my_profile");
            } else {
              showEnsureDialog(context, "", tr("network_error"), () {
                _fetchUserInfo(context);
              }, cancelTitle: null);
            }
            break;
          case 1:
            if (userModel.user != null) {
              Navigator.of(context).pushNamed("profile_account_security");
            } else {
              showEnsureDialog(context, "", tr("network_error"), () {
                _fetchUserInfo(context);
              }, cancelTitle: null);
            }
            break;
          default:
            break;
        }
      },
    );
  }

  _fetchUserInfo(BuildContext context) async {
    if (!checkNetworkState(context)) return;
    Global.normalApi.fetchUserInfo().then((value) {
      print("_fetchUserInfo: $value");
      if (value != null && value.errorCode == SUCCESS) {
        UserModel userModol = Provider.of<UserModel>(context, listen: false);
        userModol.changeUser(value.data);
        userModol.hasPwd = value.data.hasPwd;
        SentryUser.Sentry.configureScope(
          (scope) => scope.user =
              SentryUser.User(id: value.data.id, username: value.data.fullName),
        );
      }
    }).catchError((error) {
      print("_fetchUserInfo error: $error");
    });
  }
}
