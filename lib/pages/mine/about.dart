import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:muslim/common/core/storage.dart';
import 'package:muslim/common/provider/app.dart';
import 'package:muslim/common/tools/dialog.dart';
import 'package:muslim/common/tools/hexColor.dart';
import 'package:muslim/common/tools/snackBar.dart';
import 'package:muslim/global.dart';
import 'package:muslim/pages/widgets/backButton.dart';
import 'package:muslim/pages/widgets/horizontalLine.dart';
import 'package:muslim/pages/widgets/muslim_tile.dart';
import 'package:muslim/pages/widgets/muslim_title.dart';
import 'package:native_updater/native_updater.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

class AboutPage extends StatefulWidget {
  @override
  _AboutPageState createState() => _AboutPageState();
}

class _AboutPageState extends State<AboutPage> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: MuslimTitle(title: tr("about_title")),
          leading: MuslimBackButton(),
        ),
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Center(
                child: Container(
                  width: 100.0,
                  height: 100.0,
                  margin: EdgeInsets.all(16.0),
                  padding: EdgeInsets.symmetric(horizontal: 10.0),
                  decoration: BoxDecoration(
                      color: HexColor("67C1BF"),
                      borderRadius: BorderRadius.all(Radius.circular(10.0))),
                  child: Image.asset(
                    "assets/images/logo_icon.png",
                    fit: BoxFit.contain,
                    color: Colors.white,
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 16.0),
                child: Text(
                  "about_transition",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: HexColor("212124"), fontSize: 14.0, height: 1.4),
                ).tr(),
              ),
              Container(
                margin: EdgeInsets.fromLTRB(0, 8.0, 8, 20.0),
                child: Text(
                  "about_version",
                  style: TextStyle(color: HexColor("4C4C50"), fontSize: 14.0),
                ).tr(args: [Global.packageInfo.version]),
              ),
              HorizontalLine(),
              ListTile(
                tileColor: Colors.white,
                title: Text(
                  "about_update",
                  style: TextStyle(color: HexColor("212124")),
                ).tr(),
                trailing: Icon(
                  Icons.navigate_next,
                  color: HexColor("75767A"),
                ),
                onTap: () {
                  tapAction(context, 1);
                },
              ),
              HorizontalLine(),
              ListTile(
                tileColor: Colors.white,
                title: Text(
                  "about_feedback",
                  style: TextStyle(color: HexColor("212124")),
                ).tr(),
                trailing: Icon(
                  Icons.navigate_next,
                  color: HexColor("75767A"),
                ),
                onTap: () {
                  tapAction(context, 2);
                },
              ),
              HorizontalLine(),
              Container(
                height: 8.0,
              ),
              HorizontalLine(),
              MuslimTile(
                title: tr("about_facebook"),
                leading: Image.asset(
                  "assets/images/facebook.png",
                  width: 24,
                  height: 24,
                ),
                trailing: Icon(
                  Icons.navigate_next,
                  color: HexColor("75767A"),
                ),
                callback: () {
                  tapAction(context, 3);
                },
              ),
              HorizontalLine(),
              MuslimTile(
                title: tr("about_instagram"),
                leading: Image.asset(
                  "assets/images/instagram.png",
                  width: 24,
                  height: 24,
                ),
                trailing: Icon(
                  Icons.navigate_next,
                  color: HexColor("75767A"),
                ),
                callback: () {
                  tapAction(context, 4);
                },
              ),
              HorizontalLine(),
              MuslimTile(
                title: tr("about_youtube"),
                leading: Image.asset(
                  "assets/images/youtube.png",
                  width: 24,
                  height: 24,
                ),
                trailing: Icon(
                  Icons.navigate_next,
                  color: HexColor("75767A"),
                ),
                callback: () {
                  tapAction(context, 5);
                },
              ),
              HorizontalLine(),
              Container(
                height: 8.0,
              ),
              HorizontalLine(),
              ListTile(
                tileColor: Colors.white,
                title: Text(
                  "about_user_agreement",
                  style: TextStyle(color: HexColor("212124")),
                ).tr(),
                trailing: Icon(
                  Icons.navigate_next,
                  color: HexColor("75767A"),
                ),
                onTap: () {
                  tapAction(context, 6);
                },
              ),
              HorizontalLine(),
              ListTile(
                tileColor: Colors.white,
                title: Text(
                  "about_privacy",
                  style: TextStyle(color: HexColor("212124")),
                ).tr(),
                trailing: Icon(
                  Icons.navigate_next,
                  color: HexColor("75767A"),
                ),
                onTap: () {
                  tapAction(context, 7);
                },
              ),
              HorizontalLine(),
              Container(
                height: 8.0,
              ),
              Container(
                margin: EdgeInsets.all(16.0),
                child: Builder(builder: (context) {
                  var date = new DateTime.now();
                  return Text(
                    "Copyright © ${date.year} IKHLAS. All Rights Reserved.",
                    style: TextStyle(color: HexColor("4C4C50")),
                  );
                }),
              )
            ],
          ),
        ));
  }

  tapAction(BuildContext context, int index) async {
    switch (index) {
      case 0:
        break;
      case 1:
        if (!checkNetworkState(context)) return;
        showWaitingDialog(context, null);
        var params = {};
        params["app_id"] = Global.packageInfo.packageName;
        params["version"] = Global.packageInfo.version;
        Global.normalApi.fetchNewVersion(params).then((value) {
          Navigator.of(context).pop();
          print("fetchNewVersion: $value");
          if (value != null && value.errorCode == SUCCESS) {
            if (value.data.isUpdate) {
              showEnsureDialog(
                  context, tr("Update Available"), value.data.updateDesc,
                  () async {
                if (await canLaunch(value.data.apkUrl)) {
                  await launch(value.data.apkUrl);
                }
              },
                  cancelTitle:
                      value.data.isForceUpdate == true ? "" : tr("cancelText"));
              return;
            }
          }

          showEnsureDialog(
              context, tr("Update Available"), tr("latest version tip"), null,
              ensureTitle: tr("confirmText"),
              cancelTitle:
                  value.data != null && value.data.isForceUpdate == true
                      ? ""
                      : tr("cancelText"));
        }).catchError((error) {
          Navigator.of(context).pop();
          print("fetchNewVersion: error $error");
        });
        break;
      case 2:
        Navigator.of(context).pushNamed("feedback");
        break;
      case 3:
        openUrlScheme("https://www.facebook.com/ikhlasdotcom/", 0, context);
        break;
      case 4:
        openUrlScheme("https://www.instagram.com/ikhlasdotcom/", 1, context);
        break;
      case 5:
        openUrlScheme("https://www.youtube.com/ikhlasdotcom", 2, context);
        break;
      case 6:
        Navigator.of(context).pushNamed("teams_policy");
        break;
      case 7:
        Navigator.of(context).pushNamed("privacy_policy");
        break;
      case 8:
        break;
      default:
    }
  }

  openUrlScheme(String url, int type, BuildContext context) async {
    // String url = "https://instagram.com/ikhlasdotcom?igshid=1tvei7sh9h97w";
    String prefixUrl = "";
    switch (type) {
      case 0:
        prefixUrl = "fb://";
        break;
      case 1:
        prefixUrl = "instagram://";
        break;
      case 2:
        prefixUrl = "youtube://";
        break;
      default:
    }

    if (await canLaunch(prefixUrl)) {
      if (type == 0) {
        if (Global.isIOS) {
          await launch(prefixUrl + "profile/111529330420390",
              forceSafariVC: false);
        } else {
          await launch(prefixUrl + "page/111529330420390",
              forceSafariVC: false);
        }
      } else {
        await launch(url, forceSafariVC: false);
      }
    } else {
      if (await canLaunch(url)) {
        await launch(url, forceSafariVC: true);
      } else {
        if (mounted) showSnackBar(context, tr("url_disable"));
      }
    }
  }
}
