import 'dart:ui';
import 'dart:math' show pi;
import 'dart:async';

import 'package:adhan/adhan.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_compass/flutter_compass.dart';
import 'package:geolocator/geolocator.dart';
import 'package:matomo/matomo.dart';
import 'package:muslim/common/core/storage.dart';
import 'package:muslim/common/helper/helper.dart';
import 'package:muslim/common/provider/prayer.dart';
import 'package:muslim/common/tools/hexColor.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter_qiblah/flutter_qiblah.dart';
import 'package:flutter_suncalc/flutter_suncalc.dart';
import 'package:muslim/common/tools/utils.dart';
import 'package:muslim/global.dart';
import 'package:muslim/pages/qibla/location_error_widget.dart';
import 'package:muslim/pages/qibla/map.dart';
import 'package:muslim/pages/widgets/backButton.dart';

import 'package:muslim/pages/widgets/muslim_title.dart';
import 'package:provider/provider.dart';

class QiblaDirectionPage extends TraceableStatefulWidget {
  _QiblaDirectionState createState() => _QiblaDirectionState();
}

class _QiblaDirectionState extends State<QiblaDirectionPage> {
  final _deviceSupport = FlutterQiblah.androidDeviceSensorSupport();
  final _locationStreamController =
      StreamController<LocationStatus>.broadcast();

  get stream => _locationStreamController.stream;

  @override
  void initState() {
    _checkLocationStatus();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: MuslimTitle(title: tr("qibla_title")),
        leading: MuslimBackButton(),
      ),
      body: StreamBuilder(
        stream: stream,
        builder: (context, AsyncSnapshot<LocationStatus> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting)
            return Center(
              child: SizedBox(
                width: 50.0,
                height: 50.0,
                child: CircularProgressIndicator(
                  strokeWidth: 1.5,
                  valueColor: AlwaysStoppedAnimation(HexColor("67C1BF")),
                ),
              ),
            );
          if (snapshot.data.enabled == true) {
            return QiblahCompassWidget(
                locationPermission: snapshot.data.status,
                checkCallBack: _checkLocationStatus);
          } else {
            return LocationErrorWidget(
              error: tr("Please enable Location service"),
              callback: _checkLocationStatus,
            );
          }
        },
      ),
    );
  }

  Future<void> _checkLocationStatus() async {
    final locationStatus = await FlutterQiblah.checkLocationStatus();
    if (locationStatus.enabled &&
        locationStatus.status == LocationPermission.denied) {
      await FlutterQiblah.requestPermissions();
      final s = await FlutterQiblah.checkLocationStatus();
      _locationStreamController.sink.add(s);
    } else {
      _locationStreamController.sink.add(locationStatus);
    }
  }
}

class QiblahCompassWidget extends StatefulWidget {
  QiblahCompassWidget({Key key, this.locationPermission, this.checkCallBack})
      : super(key: key);
  final LocationPermission locationPermission;
  final VoidCallback checkCallBack;
  // final _locationStreamController =
  //     StreamController<LocationStatus>.broadcast();
  @override
  _QiblahCompassWidgetState createState() => _QiblahCompassWidgetState();
}

class _QiblahCompassWidgetState extends State<QiblahCompassWidget> {
  @override
  void initState() {
    // TODO: implement initState
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      try {
        Map<String, dynamic> positionValue =
            await Global.locationHelper.fetchLocation(context);
        Position position = Position(
            latitude: positionValue["msg"]["latitude"],
            longitude: positionValue["msg"]["longitude"]);
        StorageHelper().setJSON(POSITION, position);
      } catch (error) {}
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    PrayerTimes prayerTimes = Provider.of<PrayerModel>(context).prayerTimes;
    bool sunRise = true;
    if (prayerTimes != null) {
      Prayer currentPrayer = localCurrentPrayerTime(prayerTimes);
      if (currentPrayer.index < 2 || currentPrayer.index >= 5) {
        sunRise = false;
      }
    }

    return StreamBuilder(
        stream: FlutterCompass.events,
        builder: (_, AsyncSnapshot<CompassEvent> snapshot) {
          dynamic positonJson = StorageHelper().getJSON(POSITION);
          Position position = Position.fromMap(positonJson);
          if (snapshot.connectionState == ConnectionState.waiting ||
              position == null)
            return Center(
              child: SizedBox(
                width: 50.0,
                height: 50.0,
                child: CircularProgressIndicator(
                  strokeWidth: 1.5,
                  valueColor: AlwaysStoppedAnimation(HexColor("67C1BF")),
                ),
              ),
            );
          String _angleToQible = "0";
          String _qiblaAngle = "0";
          QiblahDirection qiblahDirection;
          double sunriseAzimuth = 0;
          var date = new DateTime.now();
          var locationEnable = true;

          if (position == null) {
            position = Position(latitude: 31.13, longitude: 121.28);
          }
          if (snapshot.data != null && snapshot.data.heading != null) {
            var sunrisePos = SunCalc.getSunPosition(
                date, position.latitude, position.longitude);
            sunriseAzimuth = sunrisePos["azimuth"] * 180 / pi + 180;
            double offSet =
                Utils.getOffsetFromNorth(position.latitude, position.longitude);
            final event = snapshot.data;
            final qiblah = event.heading + (360 - offSet);
            qiblahDirection = QiblahDirection(qiblah, event.heading, offSet);
            if (qiblahDirection == null) {
              locationEnable = false;
              sunRise = false;
            } else {
              _qiblaAngle = (qiblahDirection.offset + 360).toStringAsFixed(0);
              if (qiblahDirection.qiblah < 360) {
                _angleToQible = qiblahDirection.qiblah.toStringAsFixed(0);
              } else if (qiblahDirection.qiblah < 720) {
                _angleToQible =
                    (qiblahDirection.qiblah - 360).toStringAsFixed(0);
              } else {
                _angleToQible =
                    (qiblahDirection.qiblah - 720).toStringAsFixed(0);
              }
            }
          } else {
            locationEnable = false;
            sunRise = false;
          }

          return Column(children: [
            Container(
                height: 60.0,
                color: HexColor("67C1BF"),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Row(
                      children: [
                        Expanded(
                          child: Column(
                            children: [
                              Text(
                                tr("Your Devices Angle To Qibla"),
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Colors.white70,
                                    fontWeight: FontWeight.w400),
                              ),
                              Text(_angleToQible + "°",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.w500))
                            ],
                          ),
                          flex: 2,
                        ),
                        Expanded(
                          child: Column(
                            children: [
                              Text(
                                tr("Qibla Angle"),
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Colors.white70,
                                    fontWeight: FontWeight.w400),
                              ),
                              Text(_qiblaAngle + "°",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.w500))
                            ],
                          ),
                          flex: 1,
                        )
                      ],
                    ),
                  ],
                )),
            Container(
              height: 200.0,
              color: HexColor("EAEAEA"),
              child: MapSample(),
            ),
            Expanded(
                child: Container(
              margin: EdgeInsets.fromLTRB(20, 0, 20, 0),
              child: Stack(
                alignment: Alignment.center,
                children: <Widget>[
                  Transform.rotate(
                    angle: ((qiblahDirection == null
                            ? 0
                            : qiblahDirection.direction) *
                        (pi / 180) *
                        -1),
                    child: Stack(
                      alignment: Alignment.center,
                      children: [
                        Transform.rotate(
                          angle: (180 * (pi / 180) * -1),
                          alignment: Alignment.center,
                          child: Image.asset("assets/images/rotary_table.png"),
                        ),
                        sunRise
                            ? Positioned(
                                child: Transform.rotate(
                                    angle: sunriseAzimuth * (pi / 180),
                                    child: Container(
                                      width: 300,
                                      height: 300,
                                      child: Align(
                                        alignment: Alignment.topCenter,
                                        child: Image.asset(
                                          "assets/images/sunny.png",
                                          width: 60,
                                          height: 60,
                                        ),
                                      ),
                                    )))
                            : Container(),
                        !locationEnable
                            ? Positioned.fill(
                                child: Container(
                                color: Colors.white.withOpacity(0.7),
                                child: Center(
                                  child: Text(
                                          "No compass available on this device")
                                      .tr(),
                                ),
                              ))
                            : Container()
                      ],
                    ),
                  ),
                ],
              ),
            )),
          ]);
        });

    // return StreamBuilder(
    //   stream: FlutterQiblah.qiblahStream,
    //   builder: (_, AsyncSnapshot<QiblahDirection> snapshot) {
    //     if (snapshot.connectionState == ConnectionState.waiting)
    //       return Center(
    //         child: SizedBox(
    //           width: 50.0,
    //           height: 50.0,
    //           child: CircularProgressIndicator(
    //             strokeWidth: 1.5,
    //             valueColor: AlwaysStoppedAnimation(HexColor("67C1BF")),
    //           ),
    //         ),
    //       );
    //     String _angleToQible = "0";
    //     String _qiblaAngle = "0";
    //     var date = new DateTime.now();
    //     dynamic positonJson = StorageHelper().getJSON(POSITION);
    //     Position position = Position.fromMap(positonJson);
    //     if (position == null) {
    //       position = Position(latitude: 31.13, longitude: 121.28);
    //     }
    //     var sunrisePos =
    //         SunCalc.getSunPosition(date, position.latitude, position.longitude);
    //     // get sunrise azimuth in degrees
    //     var sunriseAzimuth = sunrisePos["azimuth"] * 180 / pi + 180;
    //     print("qiblahDirection: $sunriseAzimuth");
    //     final qiblahDirection = snapshot.data;
    //     var locationEnable = true;
    //     if (qiblahDirection == null) {
    //       locationEnable = false;
    //       sunRise = false;
    //       // return LocationErrorWidget(
    //       //   error: "Location service permission is disable",
    //       //   callback: null,
    //       // );
    //     } else {
    //       print("qiblahDirection ::: offset:: ${qiblahDirection.offset} ");
    //       print("qiblahDirection ::: qiblah:: ${qiblahDirection.qiblah} ");
    //       print(
    //           "qiblahDirection ::: direction:: ${qiblahDirection.direction} ");
    //       _qiblaAngle = (qiblahDirection.offset + 360).toStringAsFixed(0);
    //       if (qiblahDirection.qiblah < 360) {
    //         _angleToQible = qiblahDirection.qiblah.toStringAsFixed(0);
    //       } else if (qiblahDirection.qiblah < 720) {
    //         _angleToQible = (qiblahDirection.qiblah - 360).toStringAsFixed(0);
    //       } else {
    //         _angleToQible = (qiblahDirection.qiblah - 720).toStringAsFixed(0);
    //       }
    //     }

    //     return Column(children: [
    //       Container(
    //           height: 60.0,
    //           color: HexColor("67C1BF"),
    //           child: Column(
    //             mainAxisAlignment: MainAxisAlignment.center,
    //             children: [
    //               Row(
    //                 children: [
    //                   Expanded(
    //                     child: Column(
    //                       children: [
    //                         Text(
    //                           tr("Your Devices Angle To Qibla"),
    //                           textAlign: TextAlign.center,
    //                           style: TextStyle(
    //                               color: Colors.white70,
    //                               fontWeight: FontWeight.w400),
    //                         ),
    //                         Text(_angleToQible + "°",
    //                             textAlign: TextAlign.center,
    //                             style: TextStyle(
    //                                 color: Colors.white,
    //                                 fontWeight: FontWeight.w500))
    //                       ],
    //                     ),
    //                     flex: 2,
    //                   ),
    //                   Expanded(
    //                     child: Column(
    //                       children: [
    //                         Text(
    //                           tr("Qibla Angle"),
    //                           textAlign: TextAlign.center,
    //                           style: TextStyle(
    //                               color: Colors.white70,
    //                               fontWeight: FontWeight.w400),
    //                         ),
    //                         Text(_qiblaAngle + "°",
    //                             textAlign: TextAlign.center,
    //                             style: TextStyle(
    //                                 color: Colors.white,
    //                                 fontWeight: FontWeight.w500))
    //                       ],
    //                     ),
    //                     flex: 1,
    //                   )
    //                 ],
    //               ),
    //             ],
    //           )),
    //       Container(
    //         height: 200.0,
    //         color: HexColor("EAEAEA"),
    //         child: MapSample(),
    //       ),
    //       Expanded(
    //           child: Container(
    //         margin: EdgeInsets.fromLTRB(20, 0, 20, 0),
    //         child: Stack(
    //           alignment: Alignment.center,
    //           children: <Widget>[
    //             Transform.rotate(
    //               angle: ((qiblahDirection == null
    //                       ? 0
    //                       : qiblahDirection.direction) *
    //                   (pi / 180) *
    //                   -1),
    //               child: Stack(
    //                 alignment: Alignment.center,
    //                 children: [
    //                   Transform.rotate(
    //                     angle: (180 * (pi / 180) * -1),
    //                     alignment: Alignment.center,
    //                     child: Image.asset("assets/images/rotary_table.png"),
    //                   ),
    //                   sunRise
    //                       ? Positioned(
    //                           child: Transform.rotate(
    //                               angle: sunriseAzimuth * (pi / 180),
    //                               child: Container(
    //                                 width: 300,
    //                                 height: 300,
    //                                 child: Align(
    //                                   alignment: Alignment.topCenter,
    //                                   child: Image.asset(
    //                                     "assets/images/sunny.png",
    //                                     width: 60,
    //                                     height: 60,
    //                                   ),
    //                                 ),
    //                               )))
    //                       : Container(),
    //                   !locationEnable
    //                       ? Positioned.fill(
    //                           child: Container(
    //                           color: Colors.white.withOpacity(0.7),
    //                           child: Center(
    //                             child:
    //                                 Text("No compass available on this device")
    //                                     .tr(),
    //                           ),
    //                         ))
    //                       : Container()
    //                 ],
    //               ),
    //             ),
    //           ],
    //         ),
    //       )),
    //     ]);
    //   },
    // );
  }
}
