import 'dart:async';
import 'package:flutter/services.dart' show rootBundle;

import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:muslim/common/core/storage.dart';
import 'package:muslim/common/helper/helper.dart';

class MapSample extends StatefulWidget {
  @override
  State<MapSample> createState() => MapSampleState();
}

class MapSampleState extends State<MapSample> {
  Completer<GoogleMapController> _controller = Completer();

  CameraPosition _kGooglePlex;
  // 地图属性
  Set<Polyline> _polylines;
  Set<Marker> _markers;

  List<Polyline> _points;
  bool _firstMove;

  // 终点
  LatLng _endLatLng = LatLng(21.2521, 39.4934);

  @override
  void initState() {
    super.initState();
    dynamic positonJson = StorageHelper().getJSON(POSITION);
    Position position = Position.fromMap(positonJson);
    _firstMove = true;
    if (position == null) {
      _kGooglePlex = CameraPosition(
        target: _endLatLng,
        zoom: 0,
      );
    } else {
      _kGooglePlex = CameraPosition(
        target: LatLng(position.latitude, position.longitude),
        zoom: 0,
      );
      _addLineToDigloa();
    }
  }

  Future<void> _addLineToDigloa() async {
    var iconBytes = await rootBundle.load("assets/images/location.png");
    var audioUint8List = iconBytes.buffer
        .asUint8List(iconBytes.offsetInBytes, iconBytes.lengthInBytes);

    var qiblaBytes = await rootBundle.load("assets/images/qibla_icon.png");
    var qiblaUint8List = qiblaBytes.buffer
        .asUint8List(qiblaBytes.offsetInBytes, qiblaBytes.lengthInBytes);

    Marker startMarker = Marker(
        // icon: icon,
        anchor: Offset(0.5, 0.5),
        icon: BitmapDescriptor.fromBytes(audioUint8List),
        markerId: MarkerId("start"),
        position: LatLng(
            _kGooglePlex.target.latitude, _kGooglePlex.target.longitude));

    Marker endMarker = Marker(
        anchor: Offset(0.5, 0.5),
        icon: BitmapDescriptor.fromBytes(qiblaUint8List),
        markerId: MarkerId("end"),
        position: LatLng(_endLatLng.latitude, _endLatLng.longitude));

    GoogleMapController controller = await _controller.future;

    controller.animateCamera(CameraUpdate.newLatLngBounds(
        boundsFromLatLngList([
          LatLng(_kGooglePlex.target.latitude, _kGooglePlex.target.longitude),
          _endLatLng
        ]),
        30));
    var points = endPointsWithTwoPoint(
        LatLng(_kGooglePlex.target.latitude, _kGooglePlex.target.longitude),
        LatLng(_endLatLng.latitude, _endLatLng.longitude));
    setState(() {
      _markers = Set<Marker>.of([startMarker, endMarker]);
      _points = points;
    });
  }

  List<Polyline> endPointsWithTwoPoint(LatLng startPoint, LatLng endPoint) {
    List<Polyline> points = [];
    var maxLength = 40;

    for (var i = 0; i < maxLength; i += 2) {
      points.add(Polyline(
          polylineId: PolylineId("$i"),
          points: [
            LatLng(
                startPoint.latitude +
                    (endPoint.latitude - startPoint.latitude) / maxLength * i,
                startPoint.longitude +
                    (endPoint.longitude - startPoint.longitude) /
                        maxLength *
                        i),
            LatLng(
                startPoint.latitude +
                    (endPoint.latitude - startPoint.latitude) /
                        maxLength *
                        (i + 1),
                startPoint.longitude +
                    (endPoint.longitude - startPoint.longitude) /
                        maxLength *
                        (i + 1))
          ],
          color: Colors.blue,
          width: 2,
          jointType: JointType.round,
          startCap: Cap.squareCap,
          endCap: Cap.squareCap));
    }
    return points;
  }

  LatLngBounds boundsFromLatLngList(List<LatLng> list) {
    assert(list.isNotEmpty);
    double x0, x1, y0, y1;
    for (LatLng latLng in list) {
      if (x0 == null) {
        x0 = x1 = latLng.latitude;
        y0 = y1 = latLng.longitude;
      } else {
        if (latLng.latitude > x1) x1 = latLng.latitude;
        if (latLng.latitude < x0) x0 = latLng.latitude;
        if (latLng.longitude > y1) y1 = latLng.longitude;
        if (latLng.longitude < y0) y0 = latLng.longitude;
      }
    }
    return LatLngBounds(northeast: LatLng(x1, y1), southwest: LatLng(x0, y0));
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: GoogleMap(
        mapType: MapType.normal,
        initialCameraPosition: _kGooglePlex,
        onMapCreated: (GoogleMapController controller) {
          _controller.complete(controller);
        },
        markers: _markers,
        polylines: _polylines,
        onCameraIdle: _mapMoveEnd,
        // myLocationEnabled: true,
      ),
    );
  }

  _mapMoveEnd() {
    if (_firstMove == true && _points != null) {
      var count = 0;
      Timer.periodic(Duration(milliseconds: 100), (timer) {
        if (count >= _points.length) {
          print("_mapMoveEnd :: end");
          timer.cancel();
          timer = null;
          if (mounted) {
            setState(() {
              _firstMove = false;
            });
          }
        }
        if (mounted) {
          setState(() {
            _polylines = Set.of(_points.getRange(0, count));
          });
        }
        count += 2;
      });
    }
  }
}
