import 'package:flutter/material.dart';
import 'package:flutter_easyrefresh/easy_refresh.dart';
import 'package:flutter_html/style.dart';
import 'package:matomo/matomo.dart';
import 'package:muslim/common/tools/hexColor.dart';
import 'package:muslim/pages/widgets/backButton.dart';
import 'package:muslim/pages/widgets/muslim_title.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:easy_localization/easy_localization.dart';

class QurbanAqiqahPage extends TraceableStatefulWidget {
  @override
  _QurbanAqiqahState createState() => _QurbanAqiqahState();
}

class _QurbanAqiqahState extends State<QurbanAqiqahPage> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: MuslimTitle(title: tr("Qurban & Aqiqah")),
        leading: MuslimBackButton(),
      ),
      body: EasyRefresh(
          child: Padding(
        padding: EdgeInsets.all(16.0),
        child: Column(
          children: [
            InkWell(
              onTap: () async {
                MatomoTracker.trackScreen(context, "Aqiqah");
                Navigator.of(context).pushNamed("web_view", arguments: {
                  "url": "https://ikhlas.com/aqiqah/en/",
                  "title": "Aqiqah"
                });
              },
              child: Card(
                clipBehavior: Clip.antiAlias,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(4.0))),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      height: 128.0,
                      width: MediaQuery.of(context).size.width,
                      child: Image.asset(
                        "assets/images/aqiqah_bg.png",
                        fit: BoxFit.cover,
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 0.0),
                      child: Text(
                        tr("Aqiqah"),
                        style: TextStyle(
                            fontSize: 16.0,
                            fontWeight: FontWeight.w500,
                            color: HexColor("212124")),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.fromLTRB(16.0, 0, 16.0, 16.0),
                      child: Text(
                        tr("Aqiqah_content"),
                        style: TextStyle(
                            fontSize: 12.0,
                            fontWeight: FontWeight.w400,
                            height: 1.5),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Container(
              height: 20.0,
            ),
            InkWell(
              onTap: () async {
                // if (await canLaunch("https://ikhlas.com/qurban/en/")) {
                //   await launch("https://ikhlas.com/qurban/en/");
                // }
                MatomoTracker.trackScreen(context, "Qurban");
                Navigator.of(context).pushNamed("web_view", arguments: {
                  "url": "https://ikhlas.com/qurban/en/",
                  "title": "Qurban"
                });
              },
              child: Card(
                clipBehavior: Clip.antiAlias,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(4.0))),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      height: 128.0,
                      width: MediaQuery.of(context).size.width,
                      child: Image.asset(
                        "assets/images/qurban_bg.png",
                        fit: BoxFit.cover,
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 0),
                      child: Text(
                        tr("Qurban"),
                        style: TextStyle(
                            fontSize: 16.0,
                            fontWeight: FontWeight.w500,
                            color: HexColor("212124")),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.fromLTRB(16.0, 0, 16.0, 16.0),
                      child: Text(tr("Qurban_content"),
                          style: TextStyle(
                              fontSize: 12.0,
                              fontWeight: FontWeight.w400,
                              height: 1.5)),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      )),
    );
  }
}
