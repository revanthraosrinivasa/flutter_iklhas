import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_easyrefresh/easy_refresh.dart';
import 'package:matomo/matomo.dart';
import 'package:muslim/common/api/graphql.dart';
import 'package:muslim/common/core/storage.dart';
import 'package:muslim/common/tools/hexColor.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:muslim/common/tools/snackBar.dart';
import 'package:muslim/modals/artical.dart';
import 'package:muslim/pages/widgets/backButton.dart';
import 'package:muslim/pages/widgets/base_page.dart';
import 'package:muslim/pages/widgets/base_widget.dart';
import 'package:muslim/pages/widgets/empty.dart';
import 'package:muslim/pages/widgets/muslim_title.dart';

class ArticlesPage extends TraceableStatefulWidget {
  @override
  _ArticlesPageState createState() => _ArticlesPageState();
}

class _ArticlesPageState extends State<ArticlesPage> {
  List<Item> tableData;
  int _skip = 0;
  EasyRefreshController refreshController = EasyRefreshController();
  static String ArticalUrl = "https://ikhlas.com/bm/posts";
  bool _isNetWorkError = true;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    // _onRefresh();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MuslimBasePage(
      appBar: AppBar(
        centerTitle: true,
        title: MuslimTitle(title: tr("articles_title")),
        leading: MuslimBackButton(),
      ),
      controller: refreshController,
      firstRefresh: true,
      onLoad: _onLoad,
      onRefresh: _onRefresh,
      header: baseHeader(),
      footer: baseFooter(),
      firstRefreshWidget: true,
      emptyWidget: _isNetWorkError && tableData == null
          ? EmptyPage(
              noDatasTip: tr("no_network_tip"),
            )
          : tableData != null && tableData.length == 0
              ? EmptyPage(
                  noDatasTip: tr("No articles at the moment"),
                )
              : null,
      slivers: [
        SliverPadding(
          padding: EdgeInsets.all(16.0),
          sliver: SliverList(
              delegate: SliverChildBuilderDelegate((context, index) {
            return _listChildTile(index);
          }, childCount: tableData != null ? tableData.length : 0)),
        )
      ],
    );
  }

  Widget _listChildTile(int index) {
    Item item = tableData[index];
    return InkWell(
      onTap: () async {
        Navigator.of(context).pushNamed("web_view",
            arguments: {"url": ArticalUrl + item.url, "title": item.title});
      },
      child: Card(
        color: Colors.white,
        clipBehavior: Clip.antiAlias,
        margin: EdgeInsets.only(bottom: 16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            item.artworkConnection.edges == null ||
                    item.artworkConnection.edges.length == 0
                ? Container()
                : Container(
                    height: 128.0,
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                        color: HexColor("#F2F3F6"),
                        image: DecorationImage(
                          image: NetworkImage(
                              item.artworkConnection.edges[0].node.url),
                          fit: BoxFit.cover,
                        )),
                    alignment: Alignment.topLeft,
                    child: item.system.tags == null ||
                            item.system.tags.length == 0
                        ? Container()
                        : Container(
                            decoration: BoxDecoration(
                                color: HexColor("FBEBE9"),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(40.0))),
                            padding: EdgeInsets.symmetric(
                                horizontal: 8.0, vertical: 4.0),
                            margin: EdgeInsets.fromLTRB(8.0, 8.0, 0, 0),
                            child: Text(
                              item.system.tags.join(" "),
                              style: TextStyle(
                                  fontSize: 12.0,
                                  fontWeight: FontWeight.w500,
                                  color: HexColor("C92D21")),
                            ),
                          ),
                  ),
            Container(
              padding: EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 4.0),
              child: Text(
                item.title,
                style: TextStyle(
                    color: HexColor("212124"),
                    fontSize: 16.0,
                    fontWeight: FontWeight.w500),
              ),
            ),
            Container(
              padding: EdgeInsets.fromLTRB(16.0, 0.0, 16.0, 16.0),
              child: Text(
                DateFormat("d MMM yyyy")
                    .format(item.system.updatedAt.toLocal()),
                style: TextStyle(
                    color: HexColor("212124"),
                    fontSize: 12.0,
                    fontWeight: FontWeight.w400),
              ),
            )
          ],
        ),
      ),
    );
  }

  Future<void> _onLoad() async {
    setState(() {
      _skip = _skip + 1;
    });
    _fetchArticals(type: 0);
  }

  Future<void> _onRefresh() async {
    setState(() {
      _skip = 0;
    });
    refreshController.resetLoadState();
    _fetchArticals();
  }

  Future<void> _fetchArticals({int type = 1}) async {
    fetchAllBlogs(_skip).then((value) {
      if (value != null) {
        setState(() {
          _isNetWorkError = false;
        });
        if (type == 1) {
          setState(() {
            tableData = value.data.allBlogPosts.items;
          });
          refreshController.finishRefresh(success: true);
          if (refreshController.finishLoadCallBack != null)
            refreshController.finishLoadCallBack(
                noMore: value.data.allBlogPosts.items.length < LIMIT);
        } else {
          refreshController.finishLoad(
              success: true, noMore: value.data.allBlogPosts.items.length == 0);
          var data = tableData;
          data.addAll(value.data.allBlogPosts.items);
          setState(() {
            tableData = data;
          });
        }
      } else {
        if (type == 1) {
          refreshController.finishRefresh(success: false);
        } else {
          refreshController.finishLoad(success: false);
        }
        setState(() {
          _isNetWorkError = true;
        });
        if (tableData != null && tableData.length != 0 && mounted) {
          showSnackBar(context, tr("network_error"), type: 1);
        }
      }
    }).catchError((error) {
      print("_fetchArticals: $error");
      if (type == 1) {
        refreshController.finishRefresh(success: false);
      } else {
        refreshController.finishLoad(success: false);
      }
      setState(() {
        _isNetWorkError = true;
      });
    });
  }
}
