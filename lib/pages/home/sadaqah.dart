import 'dart:async';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyrefresh/easy_refresh.dart';
import 'package:matomo/matomo.dart';
import 'package:muslim/common/api/error_network_handle.dart';
import 'package:muslim/common/core/storage.dart';
import 'package:muslim/common/tools/dialog.dart';
import 'package:muslim/common/tools/hexColor.dart';
import 'package:muslim/common/tools/snackBar.dart';
import 'package:muslim/global.dart';
import 'package:muslim/modals/activity.dart';
import 'package:muslim/pages/widgets/backButton.dart';
import 'package:muslim/pages/widgets/base_page.dart';
import 'package:muslim/pages/widgets/base_widget.dart';
import 'package:muslim/pages/widgets/empty.dart';
import 'package:easy_localization/easy_localization.dart';

class SadaqahPage extends StatefulWidget {
  @override
  _SadaqahPageState createState() => _SadaqahPageState();
}

class _SadaqahPageState extends State<SadaqahPage>
    with SingleTickerProviderStateMixin {
  PageController _tabController;
  List<String> tabs = [tr("All"), tr("Ikhlas"), tr("Other")];
  bool _isNetworkError = false;

  EasyRefreshController _allController = EasyRefreshController();
  EasyRefreshController _ikhlasController = EasyRefreshController();
  EasyRefreshController _otherController = EasyRefreshController();

  List<ActivitiesDatum> _allDatas;
  List<ActivitiesDatum> _ikhlasDatas;
  List<ActivitiesDatum> _otherDatas;

  bool _firstFirstLoad = true;
  bool _secondFirstLoad = true;
  bool _thirdFirstLoad = true;

  int _currentPage = 0;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _tabController = PageController(initialPage: _currentPage);
    _allRefresh();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text("sadaqah_title").tr(),
          leading: MuslimBackButton(),
          elevation: 0.0,
        ),
        body: Column(
          children: [
            Container(
              height: 40,
              decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border(
                      top: BorderSide(color: HexColor("F9F9F9")),
                      bottom: BorderSide(color: HexColor("EAEAEA")))),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: tabs
                    .map(
                      (e) => Expanded(
                          child: InkWell(
                            onTap: () {
                              int index = tabs.indexOf(e);
                              _tabController.jumpToPage(index);
                              setState(() {
                                _currentPage = index;
                              });
                              switch (index) {
                                case 0:
                                  if (_firstFirstLoad) {
                                    _allRefresh();
                                  }
                                  break;
                                case 1:
                                  if (_secondFirstLoad) {
                                    _ikhlasRefresh();
                                  }
                                  break;
                                case 2:
                                  if (_thirdFirstLoad) {
                                    _otherRefresh();
                                  }
                                  break;
                                default:
                              }
                            },
                            child: Container(
                                height: 40.0,
                                decoration: BoxDecoration(
                                    border: Border(
                                        bottom: BorderSide(
                                            width: 2.0,
                                            color:
                                                tabs.indexOf(e) == _currentPage
                                                    ? HexColor("67C1BF")
                                                    : HexColor("EAEAEA")))),
                                child: Center(
                                  child: Text(
                                    e,
                                    style: TextStyle(
                                        color: tabs.indexOf(e) == _currentPage
                                            ? HexColor("67C1BF")
                                            : HexColor("212124")),
                                  ),
                                )),
                          ),
                          flex: 1),
                    )
                    .toList(),
              ),
            ),
            Expanded(
                child: PageView(
                    physics: NeverScrollableScrollPhysics(),
                    controller: _tabController,
                    children: [
                  _listViewWithType(),
                  _listViewWithType(type: 1),
                  _listViewWithType(type: 2),
                ]))
          ],
        ));
  }

  Widget _listViewWithType({int type = 0}) {
    var data = [];
    var controller;
    var refresh;
    var load;
    switch (type) {
      case 0:
        if (_firstFirstLoad) {
          return Center(
            child: SizedBox(
              width: 50.0,
              height: 50.0,
              child: CircularProgressIndicator(
                strokeWidth: 1.5,
                valueColor: AlwaysStoppedAnimation(HexColor("67C1BF")),
              ),
            ),
          );
        }
        data = _allDatas;
        controller = _allController;
        refresh = _allRefresh;
        load = _allLoad;
        break;
      case 1:
        if (_secondFirstLoad) {
          return Center(
            child: SizedBox(
              width: 50.0,
              height: 50.0,
              child: CircularProgressIndicator(
                strokeWidth: 1.5,
                valueColor: AlwaysStoppedAnimation(HexColor("67C1BF")),
              ),
            ),
          );
        }
        data = _ikhlasDatas;
        controller = _ikhlasController;
        refresh = _ikhlasRefresh;
        load = _ikhlasLoad;
        break;
      case 2:
        if (_thirdFirstLoad) {
          return Center(
            child: SizedBox(
              width: 50.0,
              height: 50.0,
              child: CircularProgressIndicator(
                strokeWidth: 1.5,
                valueColor: AlwaysStoppedAnimation(HexColor("67C1BF")),
              ),
            ),
          );
        }
        controller = _otherController;
        data = _otherDatas;
        refresh = _otherRefresh;
        load = _otherLoad;
        break;
    }
    return MuslimBasePage(
      appBar: null,
      controller: controller,
      header: baseHeader(),
      footer: baseFooter(),
      onRefresh: refresh,
      onLoad: load,
      firstRefresh: false,
      firstRefreshWidget: false,
      emptyWidget: data == null && _isNetworkError
          ? EmptyPage(
              noDatasTip: tr("no_network_tip"),
            )
          : data != null && data.length == 0
              ? EmptyPage(
                  noDatasTip: tr("no_events_content"),
                )
              : null,
      slivers: [
        SliverPadding(
            padding: EdgeInsets.fromLTRB(16.0, 8.0, 16.0, 0.0),
            sliver: SliverList(
                delegate: SliverChildBuilderDelegate((context, index) {
              return _listTileWithIndex(index, data);
            }, childCount: data != null ? data.length : 0)))
      ],
    );
  }

  Widget _listTileWithIndex(int index, List<ActivitiesDatum> data) {
    ActivitiesDatum item = data[index];
    return Container(
      padding: EdgeInsets.only(top: 8.0),
      child: Card(
        clipBehavior: Clip.antiAlias,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            item.photos.length != 0
                ? InkWell(
                    onTap: () async {
                      MatomoTracker.trackEvent(
                          "sadaqah_event", "sadaqah_event");
                      Navigator.of(context).pushNamed("web_view", arguments: {
                        "url": item.link,
                        "title": item.activityName
                      });
                    },
                    child: CachedNetworkImage(
                      imageUrl: item.photos[0].url + OSS_IMAGE_SCALE,
                      height: 128.0,
                      width: MediaQuery.of(context).size.width - 32,
                      fit: BoxFit.cover,
                      placeholder: (context, str) {
                        return Container(
                          color: HexColor("#F2F3F6"),
                        );
                      },
                      errorWidget: (context, str, value) {
                        if (!checkNetworkState(context))
                          return Container(color: HexColor("#F2F3F6"));
                        return Image.network(
                            item.photos[0].url + OSS_IMAGE_SCALE,
                            fit: BoxFit.cover);
                      },
                    ),
                  )
                : Container(),
            Row(
              children: [
                Expanded(
                    child: InkWell(
                  onTap: () async {
                    MatomoTracker.trackEvent("sadaqah_event", "sadaqah_event");
                    Navigator.of(context).pushNamed("web_view", arguments: {
                      "url": item.link,
                      "title": item.activityName
                    });
                  },
                  child: Container(
                    padding: EdgeInsets.fromLTRB(16.0, 8.0, 16.0, 10.0),
                    decoration: BoxDecoration(
                        border: Border(
                            bottom: BorderSide(color: HexColor("EAEAEA")))),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          item.activityName,
                          style: TextStyle(
                              fontSize: 16.0,
                              fontWeight: FontWeight.w500,
                              color: HexColor("212124")),
                        ),
                        Container(
                          height: 2.0,
                        ),
                        Text(
                          item.introduction,
                          style: TextStyle(
                              fontSize: 12.0,
                              fontWeight: FontWeight.w400,
                              color: HexColor("212124")),
                        )
                      ],
                    ),
                  ),
                ))
              ],
            ),
            Container(
              height: 4.0,
            ),
            ListTile(
              leading: CircleAvatar(
                radius: 20.0,
                backgroundImage:
                    CachedNetworkImageProvider(item.preViewPhoto.url),
              ),
              title: Text(
                item.merchantName,
                style: TextStyle(
                    fontSize: 16.0,
                    fontWeight: FontWeight.w500,
                    color: HexColor("212124")),
              ),
              onTap: () {
                MatomoTracker.trackEvent(
                    "sadaqah_store_detail_event", "sadaqah_store_detail_event");
                Navigator.of(context)
                    .pushNamed("store_detail", arguments: item.merchantId);
              },
            ),
            Container(
              height: 4.0,
            ),
          ],
        ),
      ),
    );
  }

  Future<void> _allRefresh() async {
    setState(() {
      _isNetworkError = false;
    });
    _allController.resetLoadState();
    var params = {
      "limit": "$LIMIT" //string 一次请求的动态个数不传就是10
    };
    Global.normalApi.fetchActivities(params).then((value) {
      if (value != null && value.errorCode == SUCCESS) {
        setState(() {
          _allDatas = value.data;
        });
        _allController.finishRefresh(
          success: true,
        );
        if (_allController.finishLoadCallBack != null)
          _allController.finishLoadCallBack(noMore: value.data.length < LIMIT);
      } else if (value != null) {
        _allController.finishRefresh(
          success: false,
        );
        if (mounted) handleNetworkError(value, context);
      } else {
        _allController.finishRefresh(
          success: false,
        );
        setState(() {
          _isNetworkError = true;
        });
        if (_allDatas != null && _allDatas.length != 0 && mounted) {
          showSnackBar(context, tr("network_error"), type: 1);
        }
      }
    }).catchError((error) {
      print("_allRefresh error: ${error.toString()}");
      setState(() {
        _isNetworkError = true;
      });
      _allController.finishRefresh(
        success: false,
      );
      if (_allDatas != null && _allDatas.length != 0 && mounted) {
        showSnackBar(context, tr("network_error"), type: 1);
      }
    }).whenComplete(() {
      setState(() {
        _firstFirstLoad = false;
      });
    });
  }

  Future<void> _allLoad() async {
    setState(() {
      _isNetworkError = false;
    });
    var params = {
      "onId": _allDatas == null || _allDatas.length == 0
          ? ""
          : _allDatas.last.onId, //string 上滑时传的上一个动态的id，下滑是不传
      "limit": "$LIMIT", //string 一次请求的动态个数不传就是10
    };
    Global.normalApi.fetchActivities(params).then((value) {
      if (value != null && value.errorCode == SUCCESS) {
        var oldData = _allDatas;
        oldData.addAll(value.data);
        setState(() {
          _allDatas = oldData;
        });
        _allController.finishLoad(
            success: true, noMore: value == null || value.data.length == 0);
      } else if (value != null) {
        _allController.finishLoad(success: false);
        if (mounted) handleNetworkError(value, context);
      } else {
        setState(() {
          _isNetworkError = true;
        });
        _allController.finishLoad(success: false);
        if (_allDatas != null && _allDatas.length != 0 && mounted) {
          showSnackBar(context, tr("network_error"), type: 1);
        }
      }
    }).catchError((error) {
      print("_allRefresh error: ${error.toString()}");
      _allController.finishLoad(success: false);
      setState(() {
        _isNetworkError = true;
      });
      if (_allDatas != null && _allDatas.length != 0 && mounted) {
        showSnackBar(context, tr("network_error"), type: 1);
      }
    });
  }

  Future<void> _ikhlasRefresh() async {
    setState(() {
      _isNetworkError = false;
    });
    _ikhlasController.resetLoadState();
    var params = {
      "merchantType": "0",
      "limit": "$LIMIT" //string 一次请求的动态个数不传就是10
    };
    Global.normalApi.fetchActivities(params).then((value) {
      if (value != null && value.errorCode == SUCCESS) {
        Timer(Duration(milliseconds: 500), () {
          setState(() {
            _ikhlasDatas = value.data;
          });
          _ikhlasController.finishRefresh(success: true);
          _ikhlasController.finishLoadCallBack(
              noMore: value.data.length < LIMIT);
        });
      } else if (value != null) {
        _ikhlasController.finishRefresh(success: false);
        if (mounted) handleNetworkError(value, context);
      } else {
        _ikhlasController.finishRefresh(success: false);

        setState(() {
          _isNetworkError = true;
        });
        if (_ikhlasDatas != null && _ikhlasDatas.length != 0 && mounted) {
          showSnackBar(context, tr("network_error"), type: 1);
        }
      }
    }).catchError((error) {
      print("_allRefresh error: ${error.toString()}");
      setState(() {
        _isNetworkError = true;
      });
      if (_ikhlasDatas != null && _ikhlasDatas.length != 0 && mounted) {
        showSnackBar(context, tr("network_error"), type: 1);
      }
      _ikhlasController.finishRefresh(success: false);
    }).whenComplete(() {
      setState(() {
        _secondFirstLoad = false;
      });
    });
  }

  Future<void> _ikhlasLoad() async {
    print("_ikhlasLoad");
    setState(() {
      _isNetworkError = false;
    });
    var params = {
      "merchantType": "0",
      "onId": _ikhlasDatas.length == 0
          ? ""
          : _ikhlasDatas.last.onId, //string 上滑时传的上一个动态的id，下滑是不传
      "limit": "$LIMIT", //string 一次请求的动态个数不传就是10
    };
    Global.normalApi.fetchActivities(params).then((value) {
      if (value != null && value.errorCode == SUCCESS) {
        var oldData = _ikhlasDatas;
        oldData.addAll(value.data);
        setState(() {
          _ikhlasDatas = oldData;
        });
        _ikhlasController.finishLoad(
            success: true, noMore: value == null || value.data.length == 0);
      } else if (value != null) {
        _ikhlasController.finishLoad(success: false);
        if (mounted) handleNetworkError(value, context);
      } else {
        setState(() {
          _isNetworkError = true;
        });
        _ikhlasController.finishLoad(success: false);
        if (_ikhlasDatas != null && _ikhlasDatas.length != 0 && mounted) {
          showSnackBar(context, tr("network_error"), type: 1);
        }
      }
    }).catchError((error) {
      print("_allRefresh error: ${error.toString()}");
      _ikhlasController.finishLoad(success: false);
      setState(() {
        _isNetworkError = true;
      });
      if (_ikhlasDatas != null && _ikhlasDatas.length != 0 && mounted) {
        showSnackBar(context, tr("network_error"), type: 1);
      }
    });
  }

  Future<void> _otherRefresh() async {
    setState(() {
      _isNetworkError = false;
    });
    _otherController.resetLoadState();
    print("_otherRefresh");
    var params = {
      "merchantType": "1",
      "limit": "$LIMIT" //string 一次请求的动态个数不传就是10
    };
    Global.normalApi.fetchActivities(params).then((value) {
      if (value != null && value.errorCode == SUCCESS) {
        Timer(Duration(milliseconds: 500), () {
          setState(() {
            _otherDatas = value.data;
          });
          _otherController.finishRefresh(success: true);
          _otherController.finishLoadCallBack(
              noMore: value.data.length < LIMIT);
        });
      } else if (value != null) {
        _otherController.finishRefresh(success: false);
        if (mounted) handleNetworkError(value, context);
      } else {
        _otherController.finishRefresh(success: false);
        setState(() {
          _isNetworkError = true;
        });
        if (_otherDatas != null && _otherDatas.length != 0 && mounted) {
          showSnackBar(context, tr("network_error"), type: 1);
        }
      }
    }).catchError((error) {
      print("_allRefresh error: ${error.toString()}");
      _otherController.finishRefresh(success: false);
      setState(() {
        _isNetworkError = true;
      });
      if (_otherDatas != null && _otherDatas.length != 0 && mounted) {
        showSnackBar(context, tr("network_error"), type: 1);
      }
    }).whenComplete(() {
      setState(() {
        _thirdFirstLoad = false;
      });
    });
  }

  Future<void> _otherLoad() async {
    print("_otherLoad");
    setState(() {
      _isNetworkError = false;
    });
    var params = {
      "merchantType": "1",
      "onId": _otherDatas.length == 0
          ? ""
          : _otherDatas.last.onId, //string 上滑时传的上一个动态的id，下滑是不传
      "limit": "$LIMIT", //string 一次请求的动态个数不传就是10
    };
    Global.normalApi.fetchActivities(params).then((value) {
      if (value != null && value.errorCode == SUCCESS) {
        var oldData = _otherDatas;
        oldData.addAll(value.data);
        setState(() {
          _otherDatas = oldData;
        });
        _otherController.finishLoad(
            success: true, noMore: value == null || value.data.length == 0);
      } else if (value != null) {
        _otherController.finishLoad(success: false);
        if (mounted) handleNetworkError(value, context);
      } else {
        _otherController.finishLoad(success: false);
        setState(() {
          _isNetworkError = true;
        });
        if (_otherDatas != null && _otherDatas.length != 0 && mounted) {
          showSnackBar(context, tr("network_error"), type: 1);
        }
      }
    }).catchError((error) {
      print("_allRefresh error: ${error.toString()}");
      _otherController.finishLoad(success: false);
      setState(() {
        _isNetworkError = true;
      });
      if (_otherDatas != null && _otherDatas.length != 0 && mounted) {
        showSnackBar(context, tr("network_error"), type: 1);
      }
    });
  }
}
