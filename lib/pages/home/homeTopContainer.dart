import 'dart:async';
import 'dart:ui';

import 'package:adhan/adhan.dart';
import 'package:dio/dio.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:hijri/hijri_calendar.dart';
import 'package:muslim/common/helper/logger.dart';
import 'package:muslim/common/provider/app.dart';
import 'package:muslim/common/core/storage.dart';
import 'package:muslim/common/helper/helper.dart';
import 'package:muslim/common/provider/prayer.dart';
import 'package:muslim/common/helper/prayer_time.dart';
import 'package:muslim/common/tools/hexColor.dart';
import 'package:muslim/common/tools/timeChange.dart';
import 'package:muslim/global.dart';
import 'package:muslim/modals/times.dart';
import 'package:provider/provider.dart';
import 'package:intl/intl.dart';

class HomeTopContainer extends StatefulWidget {
  HomeTopContainer({Key key, this.reposition}) : super(key: key);

  final VoidCallback reposition;

  @override
  _HomeTopContainerState createState() => _HomeTopContainerState();
}

class _HomeTopContainerState extends State<HomeTopContainer> {
  String _calendarString;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    var yearMonthDay =
        DateFormat("dd MMMM yyyy", "en_US").format(DateTime.now());
    var hijriToday = new HijriCalendar.now();
    var hijriDay =
        "${hijriToday.hDay} ${hijriList[hijriToday.hMonth - 1]} ${hijriToday.hYear} H";
    _calendarString = yearMonthDay + " / " + hijriDay;
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    PrayerModel prayerModal = Provider.of<PrayerModel>(context);
    AppSettingModel appSetting = Provider.of<AppSettingModel>(context);
    return Container(
      padding: EdgeInsets.all(16.0),
      decoration: BoxDecoration(
        color: HexColor("67C1BF"),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          PrayerTimeContainerWidget(appSetting: appSetting.appSetting),
          Padding(
            padding: EdgeInsets.only(top: 15.0, left: 2.5),
            child: GestureDetector(
              onTap: () {},
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Icon(
                    Icons.calendar_today,
                    color: Colors.white,
                    size: 20,
                  ),
                  Container(
                    width: 8.0,
                  ),
                  Expanded(
                      child: Text(
                    _calendarString ?? "",
                    overflow: TextOverflow.ellipsis,
                    maxLines: 2,
                    style: TextStyle(
                        color: Colors.white, fontWeight: FontWeight.w400),
                  ))
                ],
              ),
            ),
          ),
          Container(height: 4.0),
          GestureDetector(
            onTap: widget.reposition,
            child: Row(
              children: [
                Icon(Icons.room, color: Colors.white),
                Expanded(
                    child: Container(
                  margin: EdgeInsets.only(left: 8.0),
                  child: Text(
                    prayerModal.locationCity ?? tr("positioning"),
                    overflow: TextOverflow.ellipsis,
                    maxLines: 1,
                    style: TextStyle(
                        color: Colors.white, fontWeight: FontWeight.w400),
                  ),
                ))
              ],
            ),
          )
        ],
      ),
    );
  }
}

class PrayerTimeContainerWidget extends StatefulWidget {
  PrayerTimeContainerWidget({Key key, this.appSetting}) : super(key: key);

  final AppPrayerTimeSetting appSetting;

  @override
  _PrayerTimeContainerWidgetState createState() =>
      _PrayerTimeContainerWidgetState();
}

class _PrayerTimeContainerWidgetState extends State<PrayerTimeContainerWidget> {
  String _nowOrNext;
  String _timeString;
  int _time;
  String _startOrEnd;
  Timer _timer;
  bool _isStartType;

  int _currentIndex = 0;

  PrayerTimeResult prayerTimes;

  List<String> imageNames = [
    "assets/images/imsak.png",
    "assets/images/subuh.png",
    "assets/images/zohor.png",
    "assets/images/asar.png",
    "assets/images/maghrib.png",
    "assets/images/Isha.png"
  ];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _isStartType = false;
    _time = 0;
    _fetchPrayerTimeSource();
  }

  _fetchPrayerTimeSource() async {
    if (widget.appSetting != null &&
        widget.appSetting.autoSetting == true &&
        widget.appSetting.userAutoSetting == true) {
      AppSettingModel appSetting =
          Provider.of<AppSettingModel>(context, listen: false);

      var value = appSetting.fetchDateListWithParams(DateTime.now(), 1);
      if (value != null) {
        setState(() {
          prayerTimes = value.first;
        });
      }
    } else {
      PrayerModel prayerModel =
          Provider.of<PrayerModel>(context, listen: false);
      PrayerTimes nowPrayerTimes = prayerModel.prayerTimes;
      if (nowPrayerTimes == null) {
        setState(() {
          prayerTimes = null;
        });
      } else {
        PrayerTimes newPrayerTimes = PrayerTimes.utcOffset(
            nowPrayerTimes.coordinates,
            DateComponents.from(DateTime.now()),
            nowPrayerTimes.calculationParameters,
            Duration(hours: prayerModel.utcOffset));
        setState(() {
          prayerTimes = prayerModel.changeToPrayerTimeResult(newPrayerTimes);
        });
      }
    }
    if (_timer != null) {
      _timer.cancel();
      _timer = null;
    }
    if (prayerTimes != null) {
      _startTimer();
      _calculatePrayerTimes(prayerTimes);
    }
  }

  @override
  void dispose() {
    // TODO: implement dispose
    print("PrayerTimeContainerWidget: dispose");
    if (_timer != null) {
      _timer.cancel();
      _timer = null;
    }
    super.dispose();
  }

  @override
  void didUpdateWidget(covariant PrayerTimeContainerWidget oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);
    // if (oldWidget.appSetting != widget.appSetting) {
    if (_timer != null) {
      _timer.cancel();
      _timer = null;
    }
    _fetchPrayerTimeSource();
    // }
  }

  // prayerTimes 计算当前的礼拜时间 和距离下一个礼拜时间的差值
  void _calculatePrayerTimes(PrayerTimeResult prayerTimes) async {
    var nowDateTime = DateTime.now();
    PrayerTimeViewModel viewModel = Global.viewModel;
    MuslimPrayer currentPrayer = viewModel.currentTimeStr(prayerTimes);
    MuslimPrayer nextPrayer = viewModel.nextTimeStr(prayerTimes);
    // 判断是否还没有开始第一个礼拜时间
    // 否
    if (currentPrayer != null && currentPrayer.index != null) {
      setState(() {
        _currentIndex = currentPrayer.index - 1;
      });
      var currentPrayerTime = currentPrayer.dateTime;
      // currentPrayerTime = localChangeUTCtoLocal(currentPrayerTime);
      // 判断时候超过当天的所有礼拜时间
      // 否
      if (nextPrayer != null && nextPrayer.index != null) {
        var milli = (nowDateTime.millisecondsSinceEpoch -
                currentPrayerTime.millisecondsSinceEpoch) ~/
            1000;
        var nextPrayerTime = nextPrayer.dateTime;
        // nextPrayerTime = localChangeUTCtoLocal(nextPrayerTime);
        var nowToNext = (nextPrayerTime.millisecondsSinceEpoch -
                nowDateTime.millisecondsSinceEpoch) ~/
            1000;
        if (milli <= 1800) {
          setState(() {
            _nowOrNext = tr("Now") + " ${currentPrayer.name}";
            _timeString = DateFormat.jm().format(currentPrayerTime);
            _isStartType = false;
            _time = nowToNext;
            _startOrEnd = tr("End in") + timeChangeTMS(nowToNext);
          });
        } else {
          setState(() {
            _nowOrNext = tr("Next") + " ${nextPrayer.name}";
            _timeString = DateFormat.jm().format(nextPrayerTime);
            _isStartType = true;
            _time = nowToNext;
            _startOrEnd = tr("Start in") + timeChangeTMS(nowToNext);
            _currentIndex = nextPrayer.index - 1;
          });
        }
      } else {
        if (widget.appSetting != null &&
            widget.appSetting.autoSetting == true &&
            widget.appSetting.userAutoSetting == true) {
          AppSettingModel appSetting =
              Provider.of<AppSettingModel>(context, listen: false);
          List<PrayerTimeResult> subs = appSetting.malaysiaPrayerTimes.sublist(
              timeListInYear(DateTime.now()),
              timeListInYear(DateTime.now()) + 1);
          prayerTimes = subs.first;
        } else {
          PrayerModel prayerModel =
              Provider.of<PrayerModel>(context, listen: false);
          PrayerTimes nowTimes = prayerModel.prayerTimes;
          PrayerTimes newDayTimes = PrayerTimes.utcOffset(
              nowTimes.coordinates,
              DateComponents.from(DateTime.now().add(Duration(days: 1))),
              nowTimes.calculationParameters,
              Duration(hours: prayerModel.utcOffset));

          // prayerModel
          //     .changeDateComponents(DateTime.now().add(Duration(days: 1)));
          prayerTimes = prayerModel.changeToPrayerTimeResult(newDayTimes);
        }
        DateTime nextDayPrayerTime =
            viewModel.timeForName(tr("Fajr"), prayerTimes);
        // nextDayPrayerTime = localChangeUTCtoLocal(nextDayPrayerTime);
        var milli = (nowDateTime.millisecondsSinceEpoch -
                currentPrayerTime.millisecondsSinceEpoch) ~/
            1000;
        var nowToNext = (nextDayPrayerTime.millisecondsSinceEpoch +
                24 * 3600000 -
                nowDateTime.millisecondsSinceEpoch) ~/
            1000;
        if (mounted) {
          if (milli <= 1800) {
            setState(() {
              _nowOrNext = tr("Now") + " ${currentPrayer.name}";
              _timeString = DateFormat.jm().format(currentPrayerTime);
              _isStartType = false;
              _time = nowToNext;
              _startOrEnd = tr("End in") + timeChangeTMS(nowToNext);
              _currentIndex = imageNames.length - 1;
            });
          } else {
            setState(() {
              _nowOrNext =
                  tr("Next") + " ${timeForPrayerToString(Prayer.fajr)}";
              _timeString = DateFormat.jm().format(nextDayPrayerTime);
              _isStartType = true;
              _time = nowToNext;
              _startOrEnd = tr("Start in") + timeChangeTMS(nowToNext);
              _currentIndex = 0;
            });
          }
        }
      }
    } else {
      // 是，说明当前时间还没有到第一个礼拜时间
      var nextFajrTime = DateFormat("HH:mm:ss").parse(prayerTimes.fajr);
      var currentPrayerTime = DateTime(
          nowDateTime.year,
          nowDateTime.month,
          nowDateTime.day,
          nextFajrTime.hour,
          nextFajrTime.minute,
          nextFajrTime.second);
      // currentPrayerTime = localChangeUTCtoLocal(currentPrayerTime);
      var milli = (currentPrayerTime.millisecondsSinceEpoch -
              nowDateTime.millisecondsSinceEpoch) ~/
          1000;
      setState(() {
        _nowOrNext = tr("Next") + " ${timeForPrayerToString(Prayer.fajr)}";
        _timeString = DateFormat.jm().format(currentPrayerTime);
        _isStartType = false;
        _time = milli;
        _startOrEnd = tr("End in") + timeChangeTMS(milli);
        _currentIndex = 0;
      });
    }
  }

  void _startTimer() {
    if (_timer == null) {
      int halfOfhourSeconds = 1800;
      _timer = Timer.periodic(Duration(seconds: 1), (timer) {
        halfOfhourSeconds--;
        if (halfOfhourSeconds == 0) {
          Provider.of<PrayerModel>(context, listen: false)
              .changeDateComponents(DateTime.now());
          _fetchPrayerTimeSource();
        }
        if (_time == 0) {
          timer.cancel();
          _timer.cancel();
          timer = null;
          setState(() {
            _timer = null;
            _time = 0;
          });
          Provider.of<PrayerModel>(context, listen: false)
              .changeDateComponents(DateTime.now());
          _fetchPrayerTimeSource();
          return;
        }

        if (mounted) {
          setState(() {
            _startOrEnd = (_isStartType ? tr("Start in") : tr("End in")) +
                timeChangeTMS(_time);
            _time = _time - 1;
          });
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                _nowOrNext ?? "--",
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 16.0,
                    fontWeight: FontWeight.w400),
              ),
              Text(
                _timeString ?? "--",
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 32.0,
                    fontWeight: FontWeight.w500),
              ),
              Text(
                _startOrEnd ?? "--",
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 14.0,
                    fontWeight: FontWeight.w400),
              ),
            ],
          ),
          Image.asset(
            imageNames[_currentIndex],
            fit: BoxFit.cover,
            scale: 2.0,
          )
        ],
      ),
    );
  }
}
