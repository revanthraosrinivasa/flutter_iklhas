import 'dart:async';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:muslim/common/api/error_network_handle.dart';
import 'package:muslim/common/api/google_map.dart';
import 'package:muslim/common/provider/app.dart';
import 'package:muslim/common/core/storage.dart';
import 'package:muslim/common/helper/helper.dart';
import 'package:muslim/common/helper/logger.dart';
import 'package:muslim/common/provider/prayer.dart';
import 'package:muslim/common/provider/user.dart';
import 'package:muslim/common/tools/dialog.dart';
import 'package:muslim/common/tools/hexColor.dart';
import 'package:muslim/common/tools/scrollOffset.dart';
import 'package:muslim/common/tools/snackBar.dart';
import 'package:muslim/global.dart';
import 'package:muslim/modals/recommends.dart';
import 'package:geolocator/geolocator.dart';
import 'package:muslim/modals/timezone.dart';
import 'package:muslim/pages/home/homeFuncBtn.dart';
import 'package:muslim/pages/home/homeListViewCell.dart';
import 'package:muslim/pages/home/homeTopContainer.dart';
import 'package:flutter_easyrefresh/easy_refresh.dart';
import 'package:muslim/pages/widgets/base_page.dart';
import 'package:muslim/pages/widgets/base_widget.dart';
import 'package:muslim/pages/widgets/empty.dart';
import 'package:provider/provider.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:timezone/timezone.dart' as tz;
import 'package:timezone/data/latest.dart' as tz;
import 'package:firebase_messaging/firebase_messaging.dart';

class HomePage extends StatefulWidget {
  HomePage({Key key, this.clickTip}) : super(key: key);
  final int clickTip;
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage>
    with AutomaticKeepAliveClientMixin {
  @override
  bool get wantKeepAlive => true;
  EasyRefreshController _controller = EasyRefreshController();
  ScrollController _scrollController;
  List<Datum> _tableData;
  // bool _isFetchPosition = false;
  bool _isNetworkError = false;
  bool _isLocationPremissem;
  // bool _isFirstLoad = true;

  GlobalKey _scrollKey = GlobalKey();
  List<GlobalKey> _itemKeys = [];

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      Timer(Duration(milliseconds: 300), () {
        _requestLocationStatus();
        _requestPermissions();
      });
    });
    _scrollController = ScrollController();
    _isLocationPremissem = false;
    _fetchTableData();
    _fetchNewVersion();
  }

  @override
  void didUpdateWidget(covariant HomePage oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);
    if (widget.clickTip != oldWidget.clickTip) {
      _scrollController.animateTo(0,
          duration: Duration(milliseconds: 100), curve: Curves.easeInOut);
      _controller.callRefresh();
    }
  }

  /// 请求地理位置信息
  /// 定位（10s的时间） 过期就返回，并标记没有请求到数据，再次下拉刷新的时候重新请求这个方法
  /// 定位后，获取时区 请求是否是Malaysia的地区 是，数据填充，返回，否 googlemap api请求
  ///

  Future<void> _requestLocationStatus() async {
    Map<String, dynamic> positionValue;
    try {
      positionValue = await Global.locationHelper.fetchLocation(context);
    } catch (error) {
      // positionValue = error;
    }
    if (positionValue == null) {
      setState(() {
        _isLocationPremissem = false;
      });
      return;
    }
    if (positionValue["msg"] != null) {
      setState(() {
        _isLocationPremissem = true;
      });
      if (!StorageHelper().getBool(USER_CHOICE_CITY)) {
        logger("-------------- positionValue msg $positionValue");
        Position positions = Position(
            latitude: positionValue["msg"]["latitude"],
            longitude: positionValue["msg"]["longitude"]);
        StorageHelper().setJSON(POSITION, positions);
        Provider.of<PrayerModel>(context, listen: false)
            .changeToLocations(positions.latitude, positions.longitude);
        try {
          GoogleTimeZoneData timeZoneValue = await GoogleApiService()
              .fetchLocationTimeZone(
                  {"location": "${positions.latitude},${positions.longitude}"});
          if (timeZoneValue.status == "OK") {
            // tz.setLocalLocation(tz.getLocation(timeZoneValue.timeZoneId));
            int utcOffSet = tz
                    .getLocation(timeZoneValue.timeZoneId)
                    .currentTimeZone
                    .offset ~/
                3600000;
            StorageHelper().setInt(TIMEZONE, utcOffSet);
            StorageHelper().setString(TIMEZONEID, timeZoneValue.timeZoneId);
            Provider.of<PrayerModel>(context, listen: false).utcOffset =
                utcOffSet;
          }
        } catch (error) {}
        var params = {
          "userId": StorageHelper().getString(USERID),
          "locationCity": "",
          "locationCountry": "",
          "autoSetting": false,
          "userAutoSetting": true,
          "conventions": 0,
          "corrections": [0, 0, 3, 3, 0, 0],
          "calculation": 0,
          "highLatitude": 2,
        };
        AppSettingModel appSettingModel =
            Provider.of<AppSettingModel>(context, listen: false);
        AppPrayerTimeSetting appSetting = appSettingModel.appSetting;
        if (appSetting != null) {
          params = appSetting.toJson();
        }
        try {
          var malaysiaTimes = await Global.normalApi.fetchMalaysiaPrayerTimes(
              positions.latitude, positions.longitude);
          if (malaysiaTimes != null) {
            // 请求对应malaysia 的对应祈祷时间列表
            var data = {
              "zone": malaysiaTimes.data.attributes.jakimCode.toUpperCase(),
              "period": "year"
            };
            var malaysiaYearTime =
                await Global.normalApi.fetchYearPrayerTimes(data);
            if (malaysiaYearTime.errorCode == SUCCESS) {
              Provider.of<PrayerModel>(context, listen: false)
                  .changeLocationCity(malaysiaTimes.data.place);
              params["autoSetting"] = true;
              params["locationCountry"] = malaysiaTimes.data.place;
              params["locationCity"] = malaysiaTimes.data.place;
              appSettingModel.malaysiaPrayerTimes = malaysiaYearTime.prayerTime;
              appSettingModel.malaysiaPlaceCode =
                  malaysiaTimes.data.attributes.jakimCode;
              appSettingModel.region = malaysiaTimes.data.place;
              appSettingModel.appSetting =
                  AppPrayerTimeSetting.fromJson(params);
              var value =
                  appSettingModel.fetchDateListWithParams(DateTime.now(), 400);
              if (value != null) {
                print("home ---------- ${value.first}");
                Global.viewModel.sendLocaloNitification(value);
              }
              return;
            }
          }
        } catch (error) {
          print("Global.normalApi.fetchMalaysiaPrayerTimes error $error");
        }

        GoogleApiService().fetchLocalCity({
          "latlng": "${positions.latitude},${positions.longitude}"
        }).then((listResult) {
          if (listResult.length != 0) {
            Provider.of<PrayerModel>(context, listen: false)
                .changeLocationCity(listResult[0].formattedAddress);
            if (listResult[0].formattedAddress.contains(AUTOSETTINGCUNTRY)) {
              params["locationCountry"] = AUTOSETTINGCUNTRY;
              params["autoSetting"] = true;
            } else {
              String locationCountry = listResult.length < 2
                  ? listResult[0].formattedAddress
                  : listResult[1].formattedAddress;
              params["locationCountry"] = locationCountry;
              params["autoSetting"] = false;
            }
            params["locationCity"] = listResult[0].formattedAddress;
          }
          appSettingModel.appSetting = AppPrayerTimeSetting.fromJson(params);
        }).catchError((error) {
          print("获取地理位置错误: $error");
          appSettingModel.appSetting = AppPrayerTimeSetting.fromJson(params);
        });
      } else {
        AppSettingModel appSettingModel =
            Provider.of<AppSettingModel>(context, listen: false);
        if (appSettingModel != null &&
            appSettingModel.appSetting != null &&
            appSettingModel.appSetting.autoSetting) {
          var data = {
            "zone": appSettingModel.malaysiaPlaceCode.toUpperCase(),
            "period": "year"
          };
          var malaysiaYearTime =
              await Global.normalApi.fetchYearPrayerTimes(data);
          if (malaysiaYearTime.errorCode == SUCCESS) {
            appSettingModel.malaysiaPrayerTimes = malaysiaYearTime.prayerTime;
            var value =
                appSettingModel.fetchDateListWithParams(DateTime.now(), 400);
            if (value != null) {
              print("home ---------- ${value.first}");
              Global.viewModel.sendLocaloNitification(value);
            }
          }
        }
      }
    }
  }

  Future<void> _requestPermissions() async {
    InitializationSettings initializationSettings;
    if (Global.isIOS) {
      final IOSInitializationSettings initializationSettingsIOS =
          IOSInitializationSettings(
              onDidReceiveLocalNotification: (id, title, body, payload) {
        // 收到通知
      });
      initializationSettings =
          InitializationSettings(iOS: initializationSettingsIOS);
    } else {
      const AndroidInitializationSettings initializationSettingsAndroid =
          AndroidInitializationSettings('app_icon');
      initializationSettings =
          InitializationSettings(android: initializationSettingsAndroid);
    }
    await Global.flutterLocalNotificationsPlugin
        .initialize(initializationSettings);
    FirebaseMessaging _firbaseMessaging = FirebaseMessaging();
    _firbaseMessaging.configure(
        onMessage: (message) async {
          print("_firbaseMessaging onMessage ----$message");
        },
        onBackgroundMessage: onBackgroundMessage,
        onLaunch: (message) async {
          print("_firbaseMessaging onLaunch ----$message");
          if (Provider.of<UserModel>(context).pushToken == null)
            dispatchToMerchantDetail(message);
        },
        onResume: (message) async {
          print("_firbaseMessaging onResume ----$message");
          dispatchToMerchantDetail(message);
        });

    // 获取推送token
    _firbaseMessaging.getToken().then((value) {
      logger("FCM TOKEN SUCCESS--------$value");
      if (value != null) {
        _fetchNoticePushToken(value);
        Provider.of<UserModel>(context).pushToken = value;
      }
    }).catchError((error) {
      logger("FCM TOKEN ERROR--------$error");
    });
  }

  dispatchToMerchantDetail(Map<String, dynamic> message) {
    if (message != null) {
      if (Global.isIOS) {
        if (message["id"] != null)
          Navigator.of(context)
              .pushNamed("store_detail", arguments: message["id"]);
      } else {
        if (message["data"] != null && message["data"]["id"] != null) {
          Navigator.of(context)
              .pushNamed("store_detail", arguments: message["data"]["id"]);
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return MuslimBasePage(
      appBar: AppBar(
        brightness: Brightness.light,
        centerTitle: true,
        title: Container(
          width: 100.0,
          alignment: Alignment.center,
          child: Image.asset(
            'assets/images/logo_icon.png',
            fit: BoxFit.contain,
            width: 64.0,
            height: 20.0,
          ),
        ),
      ),
      key: _scrollKey,
      header: baseHeader(),
      footer: baseFooter(),
      controller: _controller,
      scrollController: _scrollController,
      onLoad: _onLoad,
      onRefresh: _onRefresh,
      firstRefresh: false,
      firstRefreshWidget: false,
      slivers: [
        SliverToBoxAdapter(
            child: HomeTopContainer(reposition: _repositionClick)),
        // SliverToBoxAdapter(child: HomeFunBtnListView()),
        HomeFunBtnListView(),
        Builder(builder: (buildContext) {
          if (_tableData == null && _isNetworkError) {
            return SliverToBoxAdapter(
                child: EmptyPage(noDatasTip: tr("no_network_tip")));
          }
          if (_tableData != null && _tableData.length == 0) {
            return SliverToBoxAdapter(
              child: EmptyPage(noDatasTip: tr("no_updates_content")),
            );
          }
          return SliverList(
            delegate: SliverChildBuilderDelegate((context, index) {
              GlobalKey key = _itemKeys[index];
              return HomeListViewCellWidget(
                  key: key, detail: _tableData[index], tapClick: itemClick);
            }, childCount: _tableData != null ? _tableData.length : 0),
          );
        }),
      ],
    );
  }

  /// 计算所有的偏移量
  void itemClick(GlobalKey itemkey) {
    double offset = _scrollController.offset;
    Offset currentPosition = getPosition(itemkey);
    Offset scrollPosition = getPosition(_scrollKey);
    if (currentPosition.dy < scrollPosition.dy) {
      offset -= scrollPosition.dy - currentPosition.dy;
      _scrollController.animateTo(offset,
          duration: Duration(milliseconds: 100), curve: Curves.easeIn);
    }
  }

  void _repositionClick() {
    Navigator.of(context).pushNamed("search_location");
  }

  Future<void> _fetchTableData({int fetchType = 0}) async {
    var params = {
      "limit": "$LIMIT",
      "merchant_id": fetchType == 0
          ? ""
          : _tableData == null || _tableData.length == 0
              ? ""
              : _tableData.last.id
    };
    Global.normalApi.fetchRecommends(params).then((value) {
      print("fetchRecommends: $value");
      if (value != null && value.errorCode == SUCCESS) {
        setState(() {
          _isNetworkError = false;
        });
        if (fetchType == 0) {
          setState(() {
            _tableData = value.data;
          });
          _controller.finishRefresh(success: true);
          if (_controller.finishLoadCallBack != null)
            _controller.finishLoadCallBack(noMore: value.data.length < LIMIT);
        } else {
          var data = _tableData;
          data.addAll(value.data);
          setState(() {
            _tableData = data;
          });
          _controller.finishLoad(success: true, noMore: value.data.length == 0);
        }

        List<GlobalKey> list = [];
        for (int i = 0; i < _tableData.length; i++) {
          list.add(GlobalKey());
        }
        setState(() {
          _itemKeys = list;
        });
      } else if (value != null) {
        setState(() {
          _isNetworkError = false;
        });
        if (mounted) handleNetworkError(value, context);
        if (fetchType == 0) {
          _controller.finishRefresh(success: false);
        } else {
          _controller.finishLoad(success: false);
        }
      } else {
        if (fetchType == 0) {
          _controller.finishRefresh(success: false);
        } else {
          _controller.finishLoad(success: false);
        }
        setState(() {
          _isNetworkError = true;
        });
        if (mounted)
          showSnackBar(context, value.developerMsg ?? tr("network_error"),
              type: 1);
      }
    }).catchError((error) {
      logger("fetchRecommends error : $error");

      if (fetchType == 0) {
        _controller.finishRefresh(success: false);
      } else {
        _controller.finishLoad(success: false);
      }
      if (_tableData != null && _tableData.length != 0 && mounted) {
        showSnackBar(context, tr("network_error"), type: 1);
      }
      setState(() {
        _isNetworkError = true;
      });
    });
  }

  Future<void> _onLoad() async {
    _fetchTableData(fetchType: 1);
  }

  Future<void> _onRefresh() async {
    if (!_isLocationPremissem) {
      _requestLocationStatus();
    }
    AppSettingModel appSettingModel =
        Provider.of<AppSettingModel>(context, listen: false);
    if (appSettingModel != null &&
        appSettingModel.appSetting != null &&
        appSettingModel.appSetting.autoSetting &&
        appSettingModel.malaysiaPrayerTimes == null &&
        appSettingModel.malaysiaPlaceCode != null &&
        appSettingModel.malaysiaPlaceCode != "") {
      var data = {
        "zone": appSettingModel.malaysiaPlaceCode.toUpperCase(),
        "period": "year"
      };
      var malaysiaYearTime = await Global.normalApi.fetchYearPrayerTimes(data);
      if (malaysiaYearTime.errorCode == SUCCESS) {
        appSettingModel.malaysiaPrayerTimes = malaysiaYearTime.prayerTime;
        var value =
            appSettingModel.fetchDateListWithParams(DateTime.now(), 400);
        if (value != null) {
          print("home ---------- ${value.first}");
          Global.viewModel.sendLocaloNitification(value);
        }
      }
    }
    _controller.resetLoadState();
    _fetchTableData();
  }

  _fetchNewVersion() async {
    var params = {};
    params["app_id"] = Global.packageInfo.packageName;
    params["version"] = Global.packageInfo.version;
    Global.normalApi.fetchNewVersion(params).then((value) {
      print("fetchNewVersion: $value");
      if (value != null && value.errorCode == SUCCESS) {
        if (value.data.isUpdate) {
          showEnsureDialog(
              context, tr("Update Available"), value.data.updateDesc, () async {
            if (await canLaunch(value.data.apkUrl)) {
              await launch(value.data.apkUrl);
            }
          },
              cancelTitle: value.data.isForceUpdate == true ? "" : "Cancel",
              ensureTitle: "Confirm");
        }
      }
    }).catchError((error) {
      print("fetchNewVersion: error $error");
    });
  }

  _fetchNoticePushToken(String token) {
    Global.normalApi.relateToPushToken({"token": token}).then((value) {
      logger("_fetchNoticePushToken ------ ${value.toJson()}");
    }).catchError((error) {
      print("_fetchNoticePushToken: error $error");
    });
  }
}

Future<dynamic> onBackgroundMessage(Map<String, dynamic> message) async {
  print("_firbaseMessaging onBackgroundMessage -------- $message");
}
