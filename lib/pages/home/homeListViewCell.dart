//BOY removed this
//import 'dart:wasm';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:muslim/common/tools/hexColor.dart';
import 'package:muslim/common/tools/timeChange.dart';
import 'package:muslim/modals/recommends.dart';
import 'package:muslim/pages/widgets/images.dart';
import 'package:muslim/pages/widgets/textPainer.dart';

class HomeListViewCellWidget extends StatefulWidget {
  HomeListViewCellWidget({Key key, this.detail, this.previewUrl, this.tapClick})
      : super(key: key);
  final Datum detail;
  final void Function(GlobalKey) tapClick;
  String previewUrl;
  @override
  _HomeListViewCellState createState() => _HomeListViewCellState();
}

class _HomeListViewCellState extends State<HomeListViewCellWidget> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 16.0),
      decoration: BoxDecoration(
          color: Colors.white,
          border: Border(
            top: BorderSide(color: HexColor("#EAEAEA"), width: 1),
            bottom: BorderSide(color: HexColor("#F9F9F9"), width: 8),
          )),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          InkWell(
            onTap: () {
              if (widget.previewUrl == null) {
                Navigator.of(context).pushNamed("store_detail",
                    arguments: widget.detail.merchantId);
              }
            },
            child: Container(
              padding: EdgeInsets.only(top: 16.0, bottom: 8.0),
              child: Row(
                children: [
                  CircleAvatar(
                    radius: 20.0,
                    backgroundImage: CachedNetworkImageProvider(
                        widget.previewUrl ?? widget.detail.preViewPhoto.url),
                  ),
                  Container(
                    width: 8.0,
                  ),
                  Expanded(
                      child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        widget.detail.merchantName ?? "",
                        style: TextStyle(
                            color: HexColor('212124'),
                            fontWeight: FontWeight.bold,
                            fontSize: 16.0),
                      ),
                      Container(
                        height: 2.0,
                      ),
                      Text(
                        RelativeDateFormat.format(widget.detail.postTime),
                        style: TextStyle(
                            color: HexColor("4C4C50"), fontSize: 12.0),
                      )
                    ],
                  ))
                ],
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(bottom: 0.0),
            child: DKMRichText(
              text: widget.detail.content,
              style: TextStyle(fontSize: 14.0, color: HexColor('212124')),
              urls: widget.detail.urls,
              callback: callBack,
            ),
          ),
          DKMImagesWidget(
              imageUrls: widget.detail.photos.map((e) {
            return e.url;
          }).toList()),
          Container(height: 20.0)
        ],
      ),
    );
  }

  callBack() {
    widget.tapClick(widget.key);
  }
}
