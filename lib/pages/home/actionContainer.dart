import 'package:flutter/cupertino.dart';

class ActionContainer extends StatefulWidget {
  ActionContainer(
      {Key key,
      @required this.children,
      this.offSet,
      this.height,
      this.startOffSet})
      : super(key: key);
  Widget children;
  double offSet;
  double height;
  double startOffSet;

  @override
  _ActionContainerState createState() => _ActionContainerState();
}

class _ActionContainerState extends State<ActionContainer> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    var opacity = ((widget.offSet - widget.startOffSet + 10) / 10) ?? 0;
    if (opacity < 0) {
      opacity = 0;
    } else if (opacity > 1) {
      opacity = 1;
    }
    return Opacity(opacity: 1 - opacity, child: widget.children);
  }
}
