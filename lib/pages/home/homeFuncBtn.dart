import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:matomo/matomo.dart';
import 'package:muslim/common/api/graphql.dart';
import 'package:muslim/common/core/storage.dart';
import 'package:muslim/common/helper/helper.dart';
import 'package:muslim/common/tools/hexColor.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:muslim/common/tools/timeChange.dart';
import 'package:muslim/global.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

class HomeFuncButton extends StatefulWidget {
  HomeFuncButton({Key key, this.icon, this.title, this.index})
      : super(key: key);
  final Image icon;
  final String title;
  final int index;
  @override
  _HomeFunButtonState createState() => _HomeFunButtonState();
}

class _HomeFunButtonState extends State<HomeFuncButton> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return GestureDetector(
        onTap: _clickTap,
        child: Column(
          // mainAxisAlignment: MainAxisAlignment.center,
          children: [
            CircleAvatar(
              child: widget.icon,
              backgroundColor: HexColor("67C1BF").withOpacity(0.1),
            ),
            Container(
              margin: EdgeInsets.only(top: 8.0),
              child: Text(
                widget.title,
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: 12.0,
                    fontWeight: FontWeight.w400,
                    color: HexColor("212124")),
              ),
            )
          ],
        ));
  }

  void _clickTap() async {
    bool isEn = StorageHelper().getString(LOCALE) == null ||
        StorageHelper().getString(LOCALE) == "en";
    switch (widget.index) {
      case 0:
        MatomoTracker.trackScreen(context, "prayer_time");
        Navigator.of(context).pushNamed("prayer_time");

        // testNotification();
        break;
      case 1:
        MatomoTracker.trackScreen(context, "qibla_direction");
        Navigator.of(context).pushNamed("qibla_direction");
        break;
      case 2:
        MatomoTracker.trackScreen(context, "sadaqah");
        Navigator.of(context).pushNamed("sadaqah");
        break;
      case 3:
        MatomoTracker.trackScreen(context, "shop");
        // Navigator.of(context).pushNamed("aqiqah_qurban");
        Navigator.of(context).pushNamed("web_view", arguments: {
          "url": isEn ? "https://shop.ikhlas.com" : "https://shop.ikhlas.com",
          "title": tr("shop")
        });
        break;
      case 4:
        MatomoTracker.trackScreen(context, "Fidyah");
        Navigator.of(context).pushNamed("web_view", arguments: {
          "url": isEn
              ? "https://ikhlas.com/fidyah/en/gb/"
              : "https://ikhlas.com/fidyah/ms/my/",
          "title": tr("Fidyah")
        });
        break;
      case 5:
        MatomoTracker.trackScreen(context, "Zakat");
        //Zakat
        Navigator.of(context).pushNamed("web_view", arguments: {
          "url": isEn
              ? "https://ikhlas.com/zakat/en/gb/"
              : "https://ikhlas.com/zakat/ms/my/",
          "title": tr("Zakat")
        });
        break;
      case 6:
        MatomoTracker.trackScreen(context, "Aqiqah");
        Navigator.of(context).pushNamed("web_view", arguments: {
          "url": isEn
              ? "https://ikhlas.com/aqiqah/en/"
              : "https://ikhlas.com/aqiqah/bm/",
          "title": tr("Aqiqah")
        });
        break;
      case 7:
        MatomoTracker.trackScreen(context, "Qurban");
        Navigator.of(context).pushNamed("web_view", arguments: {
          "url": isEn
              ? "https://ikhlas.com/qurban/en/"
              : "https://ikhlas.com/qurban/bm/",
          "title": tr("Qurban")
        });
        break;
      default:
    }
  }

  testNotification() async {
    NotificationDetails notificationDetails;
    if (Global.isIOS) {
      notificationDetails =
          NotificationDetails(iOS: IOSNotificationDetails(presentSound: true));
    } else {
      notificationDetails = NotificationDetails(
          android: AndroidNotificationDetails(
              "channel id", "your channel name", "your channel description",
              icon: "app_icon_3", color: HexColor("67C1BF")));
    }
    // await Global.flutterLocalNotificationsPlugin.zonedSchedule(2, "hello test",
    //     "Time to test", nextInstanceOfTenAM(18, 36), notificationDetails,
    //     androidAllowWhileIdle: true,
    //     uiLocalNotificationDateInterpretation:
    //         UILocalNotificationDateInterpretation.absoluteTime);
    await Global.flutterLocalNotificationsPlugin
        .show(2, "hello test", "Time to test", notificationDetails);
  }
}

class HomeFunBtnListView extends StatelessWidget {
  HomeFunBtnListView({Key key}) : super(key: key);
  final List<Map<String, dynamic>> names = [
    {
      "title": tr("prayer_times"),
      "icon": Image.asset(
        "assets/images/ic_product_prayer.png",
        width: 45,
        height: 45,
      )
    },
    {
      "title": tr("qibla_title"),
      "icon": Image.asset(
        "assets/images/ic_product_qibla.png",
        width: 45,
        height: 45,
      )
    },
    {
      "title": tr("sadaqah_title"),
      "icon": Image.asset(
        "assets/images/ic_product_sadaqah.png",
        width: 45,
        height: 45,
      )
    },
    {
      "title": tr("shop"),
      "icon": Image.asset(
        "assets/images/ic_product_shop.png",
        width: 45,
        height: 45,
      )
    },
    {
      "title": tr("Fidyah"),
      "icon": Image.asset(
        "assets/images/ic_product_fidyah.png",
        width: 45,
        height: 45,
      )
    },
    {
      "title": tr("Zakat"),
      "icon": Image.asset(
        "assets/images/ic_product_zakat.png",
        width: 45,
        height: 45,
      )
    },
    {
      "title": tr("Aqiqah"),
      "icon": Image.asset(
        "assets/images/ic_product_aqiqah.png",
        width: 45,
        height: 45,
      )
    },
    {
      "title": tr("Qurban"),
      "icon": Image.asset(
        "assets/images/ic_product_qurban.png",
        width: 45,
        height: 45,
      )
    },
  ];

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return SliverPadding(
      padding: EdgeInsets.fromLTRB(16.0, 20.0, 16.0, 0.0),
      sliver: SliverGrid(
          delegate: SliverChildBuilderDelegate((context, index) {
            var e = names[index];
            return HomeFuncButton(
              icon: e["icon"],
              title: e["title"],
              index: names.indexOf(e),
            );
          }, childCount: names.length),
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 4,
          )),
    );
  }
}
