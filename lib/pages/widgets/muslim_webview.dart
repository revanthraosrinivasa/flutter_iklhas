import 'dart:async';
import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:muslim/common/tools/hexColor.dart';
import 'package:muslim/pages/widgets/backButton.dart';
import 'package:muslim/pages/widgets/muslim_title.dart';
import 'package:webview_flutter/webview_flutter.dart';

class MuslimWebView extends StatefulWidget {
  MuslimWebView({Key key, @required this.url, @required this.title})
      : super(key: key);
  final String url;
  final String title;

  @override
  _MuslimWebViewState createState() => _MuslimWebViewState();
}

class _MuslimWebViewState extends State<MuslimWebView> {
  bool _isLoading = true;
  final Completer<WebViewController> _controller =
      Completer<WebViewController>();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if (Platform.isAndroid) WebView.platform = SurfaceAndroidWebView();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      Timer(Duration(seconds: 10), () {
        if (mounted) {
          setState(() {
            _isLoading = false;
          });
        }
      });
    });
  }

  void goback() async {
    _onBackPressed();
  }

  Future<bool> _onBackPressed() async {
    WebViewController controller = await _controller.future;
    bool canNavigate = await controller.canGoBack();
    if (canNavigate) {
      controller.goBack();
      return Future.value(false);
    } else {
      await controller.clearCache();
      Navigator.of(context).pop();
      return Future.value(true);
    }
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: MuslimTitle(title: widget.title ?? ""),
        leading: MuslimBackButton(
          callback: goback,
        ),
      ),
      body: WillPopScope(
        onWillPop: _onBackPressed,
        child: Stack(
          children: [
            Positioned.fill(
                child: WebView(
              initialUrl: widget.url,
              javascriptMode: JavascriptMode.unrestricted,
              onWebViewCreated: (controller) {
                _controller.complete(controller);
              },
              onPageFinished: (str) {
                setState(() {
                  _isLoading = false;
                });
                print("onPageFinished");
              },
              onWebResourceError: (error) {
                print("onWebResourceError ::$error");
              },
            )),
            _isLoading
                ? Positioned.fill(
                    child: Center(
                    child: SizedBox(
                      width: 50.0,
                      height: 50.0,
                      child: CircularProgressIndicator(
                        strokeWidth: 1.5,
                        valueColor: AlwaysStoppedAnimation(HexColor("67C1BF")),
                      ),
                    ),
                  ))
                : Container()
          ],
        ),
      ),
    );
  }
}
