import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:muslim/common/tools/hexColor.dart';
import 'package:easy_localization/easy_localization.dart';

class EmptyPage extends StatelessWidget {
  EmptyPage({Key key, this.noDatasTip}) : super(key: key);

  String noDatasTip;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      margin: EdgeInsets.only(top: 60.0, bottom: 60.0),
      child: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Image.asset("assets/images/illustration.png"),
            Container(
              height: 10,
            ),
            Text(
              noDatasTip ?? tr("no_data"),
              style: TextStyle(fontSize: 16.0, color: HexColor("212124")),
            )
          ],
        ),
      ),
    );
  }
}
