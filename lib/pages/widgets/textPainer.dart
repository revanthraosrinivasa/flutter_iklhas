import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'dart:ui' as td;
import 'package:easy_localization/easy_localization.dart';
import 'package:muslim/common/helper/logger.dart';
import 'package:muslim/common/tools/hexColor.dart';
import 'package:muslim/modals/recommends.dart';
import 'package:url_launcher/url_launcher.dart';

class DKMRichText extends StatefulWidget {
  DKMRichText(
      {Key key, this.text, this.style, @required this.urls, this.callback})
      : super(key: key);

  final String text;
  final TextStyle style;
  final List<Url> urls;
  final VoidCallback callback;

  @override
  _DKMRichTextState createState() => _DKMRichTextState();
}

class _DKMRichTextState extends State<DKMRichText> {
  bool _isUnfold;
  int mainLines = 6;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _isUnfold = false;
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return LayoutBuilder(builder: (context, size) {
      final span = TextSpan(text: widget.text, style: widget.style);
      final textPainer = TextPainter(
          text: span, maxLines: mainLines, textDirection: td.TextDirection.ltr);
      textPainer.layout(maxWidth: size.maxWidth);
      if (textPainer.didExceedMaxLines) {
        return InkWell(
          onTap: () {
            setState(() {
              _isUnfold = !_isUnfold;
            });
            if (!_isUnfold) {
              widget.callback();
            }
          },
          child: Container(
            width: MediaQuery.of(context).size.width,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                _isUnfold
                    ? RichText(text: TextSpan(children: _textSpanLists()))
                    : RichText(
                        maxLines: mainLines,
                        overflow: TextOverflow.ellipsis,
                        text: TextSpan(children: _textSpanLists())),
                GestureDetector(
                  behavior: HitTestBehavior.translucent,
                  onTap: () {
                    setState(() {
                      _isUnfold = !_isUnfold;
                    });
                    if (!_isUnfold) {
                      widget.callback();
                    }
                  },
                  child: Container(
                    padding: EdgeInsets.only(top: 2.0),
                    child: Text(
                      _isUnfold ? tr("pickup") : tr("viewall"),
                      style: TextStyle(
                          fontSize: widget.style != null
                              ? widget.style.fontSize
                              : null,
                          color: HexColor('67C1BF')),
                    ),
                  ),
                )
              ],
            ),
          ),
        );
      } else {
        return RichText(text: TextSpan(children: _textSpanLists()));
      }
    });
  }

  List<TextSpan> _textSpanLists() {
    if (widget.urls == null || widget.urls.length == 0) {
      return [TextSpan(text: widget.text, style: widget.style)];
    }

    List<TextSpan> urlTextSpanList = [
      TextSpan(
          text: widget.text.substring(0, widget.urls[0].indices[0]),
          style: widget.style)
    ];

    for (int i = 0; i < widget.urls.length; i++) {
      Url currentUrl = widget.urls[i];
      if (i != 0) {
        Url lastUrl = widget.urls[i - 1];
        urlTextSpanList.add(TextSpan(
            text: widget.text
                .substring(lastUrl.indices[1], currentUrl.indices[0]),
            style: widget.style));
      }
      urlTextSpanList.add(TextSpan(
          text: widget.text
              .substring(currentUrl.indices[0], currentUrl.indices[1]),
          style: TextStyle(
              fontSize: 12.0,
              color: Colors.blue,
              decoration: TextDecoration.underline),
          recognizer: TapGestureRecognizer()
            ..onTap = () async {
              logger("-------------");
              Navigator.of(context).pushNamed("web_view",
                  arguments: {"url": currentUrl.url, "title": ""});
            }));
    }

    urlTextSpanList.add(TextSpan(
        text: widget.text.substring(widget.urls.last.indices[1]),
        style: widget.style));
    return urlTextSpanList;
  }
}
