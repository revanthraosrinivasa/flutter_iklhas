import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:muslim/common/core/storage.dart';
import 'package:muslim/common/helper/logger.dart';
import 'package:muslim/common/tools/dialog.dart';
import 'package:muslim/common/tools/hexColor.dart';

class DKMImagesWidget extends StatefulWidget {
  DKMImagesWidget({Key key, this.imageUrls}) : super(key: key);

  final List<String> imageUrls;

  @override
  _DKMImagesState createState() => _DKMImagesState();
}

class _DKMImagesState extends State<DKMImagesWidget> {
  @override
  Widget build(BuildContext context) {
    if (widget.imageUrls == null || widget.imageUrls.length == 0) {
      return Container();
    }
    // TODO: implement build
    var crossAxisCount = 3;
    var childAspectRatio = 1.0;
    if (widget.imageUrls.length <= 3) {
      crossAxisCount =
          widget.imageUrls.length == 0 ? 1 : widget.imageUrls.length;
    } else if (widget.imageUrls.length == 4) {
      crossAxisCount = 2;
    }
    return GridView(
      padding: EdgeInsets.only(top: 8.0),
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: crossAxisCount,
          childAspectRatio: childAspectRatio,
          mainAxisSpacing: 6.0,
          crossAxisSpacing: 6.0),
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      children: widget.imageUrls
          .map((imageUrl) => InkWell(
                onTap: () {
                  showImagesShadow(context, widget.imageUrls,
                      currentPage: widget.imageUrls.indexOf(imageUrl));
                },
                child: CachedNetworkImage(
                  imageUrl: imageUrl + OSS_IMAGE_SCALE,
                  imageBuilder: (imageContext, provider) {
                    if (mounted) {
                      return Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(4),
                          image: DecorationImage(
                            image: provider,
                            fit: BoxFit.cover,
                          ),
                        ),
                      );
                    }
                    return Container();
                  },
                  placeholder: (context, url) => Container(
                    child: Image.asset('assets/images/placeholder.png'),
                    color: HexColor("#EAEAEA"),
                  ),
                  errorWidget: (context, str, value) {
                    if (!checkNetworkState(context))
                      return Container(color: HexColor("#F2F3F6"));
                    return Image.network(imageUrl + OSS_IMAGE_SCALE,
                        fit: BoxFit.cover);
                  },
                  fit: BoxFit.cover,
                ),
              ))
          .toList(),
    );
  }
}
