import 'package:flutter/material.dart';
import 'package:muslim/common/tools/hexColor.dart';

class HorizontalLine extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      height: 1.0,
      color: HexColor("EAEAEA"),
    );
  }
}
