import 'package:flutter/cupertino.dart';
import 'package:muslim/common/tools/hexColor.dart';

class MuslimTitle extends StatelessWidget {
  MuslimTitle({Key key, @required this.title}) : super(key: key);
  final String title;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Text(title ?? "",
        style: TextStyle(
            fontSize: 16.0,
            fontWeight: FontWeight.w500,
            color: HexColor("212124")));
  }
}
