import 'package:flutter/material.dart';
import 'package:muslim/common/tools/hexColor.dart';

class MuslimTile extends StatelessWidget {
  MuslimTile({
    Key key,
    this.title,
    this.subTitle,
    this.leading,
    this.trailing,
    this.callback,
  }) : super(key: key);
  final String title;
  final String subTitle;
  final Widget trailing;
  final Widget leading;
  final VoidCallback callback;
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return InkWell(
      onTap: () {
        if (callback != null) {
          callback();
        }
      },
      child: Container(
        height: 50.0,
        padding: EdgeInsets.symmetric(horizontal: 16.0),
        decoration: BoxDecoration(
            color: Colors.white,
            border: Border(bottom: BorderSide(color: HexColor("EAEAEA")))),
        child: Row(
          children: [
            leading ?? Container(),
            Container(
              width: leading != null ? 10 : 0,
            ),
            Text(
              title ?? "",
              style: TextStyle(color: HexColor("212124"), fontSize: 16.0),
            ),
            Expanded(child: Container()),
            Text(subTitle ?? ""),
            trailing ?? Container()
          ],
        ),
      ),
    );
  }
}
