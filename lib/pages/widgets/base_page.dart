import 'dart:async';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyrefresh/easy_refresh.dart';
import 'package:muslim/common/provider/app.dart';
import 'package:muslim/common/tools/hexColor.dart';
import 'package:muslim/common/tools/snackBar.dart';
import 'package:provider/provider.dart';
import 'package:easy_localization/easy_localization.dart';

class MuslimBasePage extends StatefulWidget {
  MuslimBasePage(
      {Key key,
      this.appBar,
      this.slivers,
      this.header,
      this.footer,
      this.controller,
      this.firstRefresh,
      this.onLoad,
      this.onRefresh,
      this.emptyWidget,
      this.firstRefreshWidget,
      this.scrollController})
      : super(key: key);
  AppBar appBar;
  List<Widget> slivers;
  Header header;
  Footer footer;
  EasyRefreshController controller;
  bool firstRefresh = false;
  Future<void> Function() onLoad;
  Future<void> Function() onRefresh;
  Widget emptyWidget;
  bool firstRefreshWidget = false;
  ScrollController scrollController;
  @override
  _MuslimBasePageState createState() => _MuslimBasePageState();
}

class _MuslimBasePageState extends State<MuslimBasePage> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: widget.appBar,
      body: EasyRefresh.custom(
        footer: widget.footer,
        header: widget.header,
        controller: widget.controller,
        firstRefresh: widget.firstRefresh ?? false,
        onLoad: widget.onLoad == null ? null : _onLoad,
        onRefresh: widget.onRefresh == null ? null : _onRefresh,
        enableControlFinishRefresh: true,
        enableControlFinishLoad: false,
        emptyWidget: widget.emptyWidget,
        scrollController: widget.scrollController,
        firstRefreshWidget:
            widget.firstRefreshWidget ? FirstRefreshWidget() : null,
        slivers: widget.slivers,
      ),
    );
  }

  Future<void> _onLoad() async {
    AppSettingModel appSettingModel =
        Provider.of<AppSettingModel>(context, listen: false);
    if (widget.onLoad != null) {
      if (appSettingModel.networkState != ConnectivityResult.none) {
        widget.onLoad();
      } else {
        if (mounted) showSnackBar(context, tr("no_network_tip"));
        Timer(Duration(milliseconds: 300), () {
          widget.controller.finishLoad();
        });
      }
    }
  }

  Future<void> _onRefresh() async {
    AppSettingModel appSettingModel =
        Provider.of<AppSettingModel>(context, listen: false);
    if (widget.onRefresh != null) {
      if (appSettingModel.networkState != ConnectivityResult.none) {
        widget.onRefresh();
      } else {
        if (mounted) showSnackBar(context, tr("no_network_tip"));
        Timer(Duration(milliseconds: 300), () {
          widget.controller.finishRefresh();
        });
      }
    }
  }
}

class FirstRefreshWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      width: double.infinity,
      height: double.infinity,
      child: Center(
        child: SizedBox(
          width: 50.0,
          height: 50.0,
          child: CircularProgressIndicator(
            strokeWidth: 1.5,
            valueColor: AlwaysStoppedAnimation(HexColor("67C1BF")),
          ),
        ),
      ),
    );
  }
}
