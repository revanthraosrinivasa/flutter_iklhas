import 'package:cached_network_image/cached_network_image.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:matomo/matomo.dart';
import 'package:muslim/common/api/error_network_handle.dart';
import 'package:muslim/common/core/storage.dart';
import 'package:muslim/common/provider/app.dart';
import 'package:muslim/common/tools/dialog.dart';
import 'package:muslim/common/tools/hexColor.dart';
import 'package:muslim/common/tools/snackBar.dart';
import 'package:muslim/global.dart';
import 'package:muslim/modals/nearlyMerchant.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:provider/provider.dart';

class NearStoreTipWidget extends StatefulWidget {
  NearStoreTipWidget(
      {Key key, this.stores, this.callBack, this.followedCallBack})
      : super(key: key);
  final List<NearlyMerchantsDatum> stores;
  final VoidCallback callBack;
  final VoidCallback followedCallBack;

  @override
  _NearStoreTipState createState() => _NearStoreTipState();
}

class _NearStoreTipState extends State<NearStoreTipWidget> {
  List<NearlyMerchantsDatum> stores;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    stores = widget.stores;
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    if (stores == null || stores.length == 0) {
      return Container();
    }
    return Container(
        padding: EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 0),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text('nearly_mosque',
                        style: TextStyle(
                            fontSize: 20.0,
                            color: HexColor("333333"),
                            fontWeight: FontWeight.bold))
                    .tr(),
                // IconButton(icon: Icon(Icons.clear), onPressed: () {}),
              ],
            ),
            Builder(builder: (context) {
              if (stores.length >= 1) {
                NearlyMerchantsDatum infos = stores[0];
                return Card(
                  child: ListTile(
                    leading: InkWell(
                      onTap: () {
                        Navigator.of(context)
                            .pushNamed("store_detail", arguments: infos.id);
                      },
                      child: CircleAvatar(
                        radius: 20.0,
                        backgroundImage: CachedNetworkImageProvider(
                            infos.previewPhoto != null &&
                                    infos.previewPhoto.url.length != 0
                                ? infos.previewPhoto.url + OSS_IMAGE_SCALE
                                : ""),
                      ),
                    ),
                    title: Text(infos.name),
                    subtitle:
                        Text((infos.distance / 1000).toStringAsFixed(2) + "km"),
                    trailing: RaisedButton(
                      onPressed: () {
                        MatomoTracker.trackEvent("nearby_recommended_follow",
                            "nearby_recommended_follow");
                        _followMerchant(infos.id);
                      },
                      child: Text(
                        "follow",
                        style: TextStyle(
                            color: HexColor("67C1BF"), fontSize: 16.0),
                      ).tr(),
                      highlightColor: Colors.white,
                      color: Colors.transparent,
                      elevation: 0.0,
                    ),
                    shape: RoundedRectangleBorder(side: BorderSide()),
                  ),
                );
              }
              return Container();
            }),
            Builder(builder: (context) {
              if (stores.length >= 2) {
                NearlyMerchantsDatum infos = stores[1];
                return Card(
                  child: ListTile(
                    leading: InkWell(
                      onTap: () {
                        Navigator.of(context)
                            .pushNamed("store_detail", arguments: infos.id);
                      },
                      child: CircleAvatar(
                        radius: 20.0,
                        backgroundImage: CachedNetworkImageProvider(
                            infos.previewPhoto.url ?? ""),
                      ),
                    ),
                    title: Text(infos.name),
                    subtitle:
                        Text((infos.distance / 1000).toStringAsFixed(2) + "km"),
                    trailing: RaisedButton(
                      onPressed: () {
                        _followMerchant(infos.id);
                      },
                      child: Text(
                        "follow",
                        style: TextStyle(
                            color: HexColor("67C1BF"), fontSize: 16.0),
                      ).tr(),
                      highlightColor: Colors.white,
                      color: Colors.transparent,
                      elevation: 0.0,
                    ),
                    shape: RoundedRectangleBorder(side: BorderSide()),
                  ),
                );
              }
              return Container();
            }),
            FlatButton(
              onPressed: () {
                widget.callBack();
              },
              child: Text("More nearby businesses",
                      style:
                          TextStyle(color: HexColor("67C1BF"), fontSize: 16.0))
                  .tr(),
            ),
            Container(
              height: 20.0,
            )
          ],
        ));
  }

  _followMerchant(String id) async {
    if (!checkNetworkState(context)) return;
    showWaitingDialog(context, null);
    Global.normalApi.followMerchant(id).then((value) {
      print("_followMerchant success");
      Navigator.of(context).pop();

      if (value != null && value.errorCode == SUCCESS) {
        widget.followedCallBack();
      } else {
        if (mounted) handleNetworkError(value, context);
      }
    }).catchError((error) {
      print("_followMerchant :$error");
      Navigator.of(context).pop();
      if (mounted) showSnackBar(context, tr("network_error"));
    });
  }
}
