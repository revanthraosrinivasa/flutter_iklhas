import 'package:flutter/material.dart';
import 'package:muslim/common/tools/hexColor.dart';
import 'package:easy_localization/easy_localization.dart';

class DKMTextFiled extends StatefulWidget {
  DKMTextFiled(
      {Key key, this.placehold, this.error, this.onChange, this.controller})
      : super(key: key);

  final String placehold;
  final String error;
  final Function(String) onChange;
  final TextEditingController controller;

  @override
  DKMTextFiledState createState() => DKMTextFiledState();
}

class DKMTextFiledState extends State<DKMTextFiled> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return TextField(
      controller: widget.controller,
      decoration: InputDecoration(
          border: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.transparent)),
          focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.transparent)),
          enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.transparent)),
          hintText: tr(widget.placehold ?? ""),
          hintStyle: TextStyle(color: HexColor('BCBCBC')),
          fillColor: HexColor('F2F2F2'),
          filled: true,
          errorText: widget.error),
      onChanged: widget.onChange,
    );
  }
}
