import 'dart:ui';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyrefresh/easy_refresh.dart';

const Color baseColor = Color(0xFF67C1BF);
const Color redLogoColor = Color(0xFFF67466);
const Color bgColor = Color(0xFFF9F9F9);
const Color bgColor2 = Color(0xFFF5F5F5);
const Color lineColor = Color(0xFFEAEAEA);
const Color textColor1 = Color(0xFF212124);
const Color textColor2 = Color(0xFF4C4C50);
const Color textColor3 = Color(0xFF75767A);
const Color inputHintColor = Color(0xFFBCBCBC);
const Color redBgColor = Color(0xFFFBEBE9);
const Color redTextColor = Color(0xFFC92D21);
const Color greenBgColor = Color(0xFFE2FFFE);
const Color blueTextColor = Color(0xFF1877F2);
const Color blueBgColor = Color(0xFFCBE2FF);
const Color yellowTextColor = Color(0xFFFFA800);
const Color yellowBgColor = Color(0xFFFFF7E2);

const Color dialogBgColor = Color(0xFF1A1E28);

const Color lightTextColor = Color(0xFFE6E6E8);
const Color lightTextColor2 = Color(0xFFF8F8F8);
const Color lightTextColor3 = Color(0xFFEDEDEF);

const Color grayTextColor = Color(0xFF8D8D92);
const Color grayTextColor2 = Color(0xFF8D8D92);
const Color grayTextColor3 = Color(0xFF8D8D92);
const Color grayTextColor4 = Color(0xFF98989D);

const Color dialogTextColor = Color(0xFF636363);
const Color dialogTextColor2 = Color(0xFF9A9C9D);
const Color dialogButtonDisableColor = Color(0xFF818287);

const Color accentBlueColor = Color(0xFF8696DD);

const Color baseColor2 = Color(0xFF2168a0);

const Color lightBlueColor = Color(0xFF00DEFF);
const Color lightBlueColorT = Color(0x6000DEFF);
const Color lightBlueColor2 = Color(0xFF00DBFF);

const Color errorTextColor = Color(0xFFFD0200);

const Color orangeTextColor = Color(0xFFFF6F00);
const Color orangeTextColor2 = Color(0xFFEF6616);
const Color orangeTextColor3 = Color(0xFFFF5B00);

const Color greenTextColor = Color(0xFF60A441);
const Color baseYellow = Color(0xFFFFCC33);

const Color baseTextColor = Color(0xFF646464);

const Color titleTextColor = Color(0xFF343434);
const Color blackTextColor = Color(0xFF2B2B2B);
const Color blackTextColor2 = Color(0xFF464647);

const Color dialogLineColor = Color(0xFFCDCED2);
const Color baseBackgroundColor = Color(0xFFF7F7F7);
const Color navigationBgColor = Color(0xFFE2F2DF);
const Color navigationLineColor = Color(0xFFEBEBEB);
const Color baseLineColor = Color(0xFFB3B3B3);
const Color baseLineColor2 = Color(0xFFD0D0D0);

const Color baseLineColor3 = Color(0xFFF3F3F3);
const Color baseDialogBgColor = Color(0xFFE6E6E9);
const Color transparentColor = Color(0x00FFFFFF);
const Color whiteColor = Color(0xFFFFFFFF);
const Color redColor = Color(0xFFFF0000);
const Color greenColor = Color(0xFF00FF00);
const Color blueColor = Color(0xFF0000FF);
const Color yellowColor = Color(0xFFFFFF00);

const List<BoxShadow> baseShadow = [
  BoxShadow(
      color: Colors.black26,
      offset: Offset(0.0, 8), //阴影xy轴偏移量
      blurRadius: 5, //阴影模糊程度
      spreadRadius: 2 //阴影扩散程度
      )
];

//const String baseFont = "PingFangSC";
/// TextStyle 管理类
class TextStyleMs {
  TextStyleMs._();

  static TextStyle black1_12 =
      TextStyle(color: textColor1, fontSize: 12, fontWeight: FontWeight.normal);
  static TextStyle black1_14 =
      TextStyle(color: textColor1, fontSize: 14, fontWeight: FontWeight.normal);
  static TextStyle black1M_14 =
      TextStyle(color: textColor1, fontSize: 14, fontWeight: FontWeight.w500);
  static TextStyle black1_16 =
      TextStyle(color: textColor1, fontSize: 16, fontWeight: FontWeight.normal);
  static TextStyle black1M_16 =
      TextStyle(color: textColor1, fontSize: 16, fontWeight: FontWeight.w500);

  static TextStyle black2_14 =
      TextStyle(color: textColor2, fontSize: 14, fontWeight: FontWeight.normal);

  static TextStyle black3M_12 =
      TextStyle(color: textColor3, fontSize: 12, fontWeight: FontWeight.w500);

  static TextStyle baseM_12 =
      TextStyle(color: baseColor, fontSize: 12, fontWeight: FontWeight.w500);
  static TextStyle base_14 =
      TextStyle(color: baseColor, fontSize: 14, fontWeight: FontWeight.normal);
  static TextStyle baseM_14 =
      TextStyle(color: baseColor, fontSize: 14, fontWeight: FontWeight.w500);
  static TextStyle baseB_14 =
      TextStyle(color: baseColor, fontSize: 14, fontWeight: FontWeight.bold);
  static TextStyle base_16 =
      TextStyle(color: baseColor, fontSize: 16, fontWeight: FontWeight.normal);
  static TextStyle baseM_16 =
      TextStyle(color: baseColor, fontSize: 16, fontWeight: FontWeight.w500);

  static TextStyle hint_14 = TextStyle(
      color: inputHintColor, fontSize: 14, fontWeight: FontWeight.normal);
  static TextStyle hint_16 = TextStyle(
      color: inputHintColor, fontSize: 16, fontWeight: FontWeight.normal);

  static TextStyle white_16 =
      TextStyle(color: whiteColor, fontSize: 16, fontWeight: FontWeight.normal);
  static TextStyle whiteM_16 =
      TextStyle(color: whiteColor, fontSize: 16, fontWeight: FontWeight.w500);

  static TextStyle redM_12 =
      TextStyle(color: redTextColor, fontSize: 12, fontWeight: FontWeight.w500);
  static TextStyle redM_16 =
      TextStyle(color: redTextColor, fontSize: 16, fontWeight: FontWeight.w500);

  static TextStyle black_14 = TextStyle(
      color: Colors.black, fontSize: 14, fontWeight: FontWeight.normal);
  static TextStyle blackM_19 =
      TextStyle(color: Colors.black, fontSize: 19, fontWeight: FontWeight.w500);

  static TextStyle dialogText = TextStyle(
      color: dialogTextColor,
      fontSize: 13,
      fontWeight: FontWeight.normal,
      fontFamily: "Roboto",
      decoration: TextDecoration.none);
  static TextStyle dialogButton = TextStyle(
      color: lightTextColor,
      fontSize: 17,
      fontWeight: FontWeight.normal,
      fontFamily: "Roboto",
      decoration: TextDecoration.none);
  static TextStyle dialogText2 = TextStyle(
      color: dialogTextColor2,
      fontSize: 13,
      fontWeight: FontWeight.normal,
      fontFamily: "Roboto",
      decoration: TextDecoration.none);
  static TextStyle dialogButtonDisable = TextStyle(
      color: dialogButtonDisableColor,
      fontSize: 17,
      fontWeight: FontWeight.normal,
      fontFamily: "Roboto",
      decoration: TextDecoration.none);

  static TextStyle whiteText_9 = TextStyle(
      color: whiteColor,
      fontSize: 9,
      fontWeight: FontWeight.normal,
      fontFamily: "Roboto",
      decoration: TextDecoration.none);
  static TextStyle whiteText_17 = TextStyle(
      color: whiteColor,
      fontSize: 17,
      fontWeight: FontWeight.normal,
      fontFamily: "Roboto",
      decoration: TextDecoration.none);
  static TextStyle whiteText_20 = TextStyle(
      color: whiteColor,
      fontSize: 20,
      fontWeight: FontWeight.normal,
      fontFamily: "Roboto",
      decoration: TextDecoration.none);

  static TextStyle lightBlue_10 = TextStyle(
      color: lightBlueColor,
      fontSize: 10,
      fontWeight: FontWeight.normal,
      fontFamily: "Trivial",
      decoration: TextDecoration.none);
  static TextStyle lightBlue_12 = TextStyle(
      color: lightBlueColor,
      fontSize: 12,
      fontWeight: FontWeight.normal,
      fontFamily: "Roboto",
      decoration: TextDecoration.none);
  static TextStyle lightBlue_13 = TextStyle(
      color: lightBlueColor,
      fontSize: 13,
      fontWeight: FontWeight.normal,
      fontFamily: "Roboto",
      decoration: TextDecoration.none);
  // static TextStyle lightBlue_13_2 = TextStyle(color: lightBlueColor, fontSize: 13, fontWeight: FontWeight.normal, decoration: TextDecoration.none);
  static TextStyle lightBlue_14 = TextStyle(
      color: lightBlueColor,
      fontSize: 14,
      fontWeight: FontWeight.normal,
      fontFamily: "Roboto",
      decoration: TextDecoration.none);
  static TextStyle lightBlueT_14 = TextStyle(
      color: lightBlueColorT,
      fontSize: 14,
      fontWeight: FontWeight.normal,
      fontFamily: "Roboto",
      decoration: TextDecoration.none);
  static TextStyle lightBlue_15 = TextStyle(
      color: lightBlueColor,
      fontSize: 15,
      fontWeight: FontWeight.normal,
      fontFamily: "Roboto",
      decoration: TextDecoration.none);
  static TextStyle lightBlueMedium_17 = TextStyle(
      color: lightBlueColor,
      fontSize: 17,
      fontWeight: FontWeight.w500,
      fontFamily: "Roboto",
      decoration: TextDecoration.none);
  static TextStyle lightBlueBold_20 = TextStyle(
      color: lightBlueColor,
      fontSize: 20,
      fontWeight: FontWeight.w700,
      fontFamily: "Trivial",
      decoration: TextDecoration.none);
  static TextStyle lightBlueBold_20R = TextStyle(
      color: lightBlueColor,
      fontSize: 20,
      fontWeight: FontWeight.w700,
      fontFamily: "Roboto",
      decoration: TextDecoration.none);

  static TextStyle lightBlue2_20 = TextStyle(
      color: lightBlueColor2,
      fontSize: 20,
      fontWeight: FontWeight.w500,
      fontFamily: "Trivial",
      decoration: TextDecoration.none);

  static TextStyle light3_20 = TextStyle(
      color: lightTextColor3,
      fontSize: 20,
      fontWeight: FontWeight.normal,
      fontFamily: "Roboto",
      decoration: TextDecoration.none);

  static TextStyle gray2_14 = TextStyle(
      color: grayTextColor2,
      fontSize: 14,
      fontWeight: FontWeight.normal,
      fontFamily: "Roboto",
      decoration: TextDecoration.none);

  static TextStyle hint_11 = TextStyle(
      color: inputHintColor,
      fontSize: 11,
      fontWeight: FontWeight.normal,
      fontFamily: "Roboto",
      decoration: TextDecoration.none);
  static TextStyle hint_12 = TextStyle(
      color: inputHintColor,
      fontSize: 12,
      fontWeight: FontWeight.normal,
      fontFamily: "Roboto",
      decoration: TextDecoration.none);
  static TextStyle hint_15 = TextStyle(
      color: inputHintColor,
      fontSize: 15,
      fontWeight: FontWeight.normal,
      fontFamily: "Roboto",
      decoration: TextDecoration.none);

  static TextStyle error_11 = TextStyle(
      color: errorTextColor,
      fontSize: 11,
      fontWeight: FontWeight.normal,
      fontFamily: "Roboto",
      fontStyle: FontStyle.italic,
      decoration: TextDecoration.none);

  static TextStyle barTitle = TextStyle(
      color: lightTextColor,
      fontSize: 17,
      fontWeight: FontWeight.w500,
      fontFamily: "Roboto",
      decoration: TextDecoration.none);

  static TextStyle accentBlue_12 = TextStyle(
      color: accentBlueColor,
      fontSize: 12,
      fontWeight: FontWeight.normal,
      fontFamily: "Roboto",
      decoration: TextDecoration.none);
  static TextStyle accentBlue_13 = TextStyle(
      color: accentBlueColor,
      fontSize: 13,
      fontWeight: FontWeight.normal,
      fontFamily: "Roboto",
      decoration: TextDecoration.none);
  static TextStyle accentBlue_14 = TextStyle(
      color: accentBlueColor,
      fontSize: 14,
      fontWeight: FontWeight.normal,
      fontFamily: "Roboto",
      decoration: TextDecoration.none);
  static TextStyle accentBlue_15 = TextStyle(
      color: accentBlueColor,
      fontSize: 15,
      fontWeight: FontWeight.normal,
      fontFamily: "Roboto",
      decoration: TextDecoration.none);
  static TextStyle accentBlue_18 = TextStyle(
      color: accentBlueColor,
      fontSize: 18,
      fontWeight: FontWeight.w500,
      fontFamily: "Roboto",
      decoration: TextDecoration.none);

  static TextStyle orangeBold_12 = TextStyle(
      color: orangeTextColor,
      fontSize: 12,
      fontWeight: FontWeight.w700,
      fontFamily: "Trivial",
      decoration: TextDecoration.none);

  static TextStyle orange2_12 = TextStyle(
      color: orangeTextColor2,
      fontSize: 12,
      fontWeight: FontWeight.normal,
      fontFamily: "Roboto",
      decoration: TextDecoration.none);

  static TextStyle orange3Bold_16 = TextStyle(
      color: orangeTextColor3,
      fontSize: 16,
      fontWeight: FontWeight.w700,
      fontFamily: "Roboto",
      decoration: TextDecoration.none);
  static TextStyle orange3Bold_20 = TextStyle(
      color: orangeTextColor3,
      fontSize: 20,
      fontWeight: FontWeight.w700,
      fontFamily: "Roboto",
      decoration: TextDecoration.none);
  static TextStyle orange3Bold_25 = TextStyle(
      color: orangeTextColor3,
      fontSize: 25,
      fontWeight: FontWeight.w700,
      fontFamily: "Roboto",
      decoration: TextDecoration.none);
}

Header baseHeader() {
  return new ClassicalHeader(
      refreshText: tr("pull_to_refresh"),
      refreshReadyText: tr("release_to_refresh"),
      refreshingText: tr("refreshing"),
      refreshedText: tr("refresh_success"),
      refreshFailedText: tr("refresh_failed"),
      noMoreText: tr("no_more"),
      showInfo: true,
      textColor: grayTextColor,
      infoColor: grayTextColor);
}

Footer baseFooter() {
  // return MaterialFooter(enableInfiniteLoad: false);
  return new ClassicalFooter(
      loadText: tr("push_to_load"),
      loadReadyText: tr("release_to_load"),
      loadingText: tr("loading"),
      loadedText: tr("load_success"),
      loadFailedText: tr("load_failed"),
      noMoreText: tr("no_more"),
      showInfo: false,
      overScroll: true,
      enableInfiniteLoad: false,
      textColor: grayTextColor,
      infoColor: grayTextColor);
}

class BackWidget extends StatelessWidget {
  final Function onBack;
  BackWidget({this.onBack});
  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.centerLeft,
      child: GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: onBack,
        child: Container(
            margin: EdgeInsets.only(left: 6),
            padding: EdgeInsets.only(left: 6, right: 10, top: 6, bottom: 6),
            child: Icon(Icons.arrow_back_ios_rounded, size: 18)),
      ),
    );
  }
}

class ForwardArrow extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Icon(
      CupertinoIcons.chevron_forward,
      size: 20,
      color: textColor3,
    );
  }
}

/// 分割线 height=0.5  B4B3B4
class BaseDivider extends StatelessWidget {
  BaseDivider();
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 0.5,
      decoration: BoxDecoration(color: baseLineColor),
    );
  }
}

/// 分割线2 height=8 B4B3B4
class BaseDivider2 extends StatelessWidget {
  BaseDivider2();
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 8,
      decoration: BoxDecoration(color: baseLineColor2),
    );
  }
}

/// 分割线3 height=1 F3F3F3 两边宽16白色
class BaseDivider3 extends StatelessWidget {
  BaseDivider3();
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.maxFinite,
      height: 1,
      child: Row(
        children: <Widget>[
          Container(
            width: 16,
            height: 1,
            decoration: BoxDecoration(color: Colors.white),
          ),
          Expanded(
            child: Container(
              width: double.maxFinite,
              height: 1,
              decoration: BoxDecoration(color: baseLineColor3),
            ),
          ),
          Container(
            width: 16,
            height: 1,
            decoration: BoxDecoration(color: Colors.white),
          )
        ],
      ),
    );
  }
}

/// BottomNavigationBar 分割线
class BaseBottomNavigationLine extends StatelessWidget {
  BaseBottomNavigationLine();
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 0.4,
      decoration: BoxDecoration(color: navigationLineColor),
    );
  }
}

/// 去掉滚动控件滚到头时的水波效果
class NoScrollBehavior extends ScrollBehavior {
  @override
  Widget buildViewportChrome(
      BuildContext context, Widget child, AxisDirection axisDirection) {
    return child;
  }
}

/// 带返回的标题栏
class BaseTitleBar extends StatelessWidget {
  final String title;
  final Function onBack;
  final Color backColor;

  BaseTitleBar({this.title, this.onBack, this.backColor = baseColor});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 44,
      width: double.infinity,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          GestureDetector(
            behavior: HitTestBehavior.translucent,
            onTap: onBack,
            child: Container(
                width: 45, child: Icon(CupertinoIcons.back, color: backColor)),
          ),
          Expanded(
            flex: 1,
            child: Center(
              child: Text(
                title,
                style: TextStyle(color: Color(0xFF333333), fontSize: 17),
              ),
            ),
          ),
          Container(
            width: 45,
          ),
        ],
      ),
    );
  }
}

/// 不跟随系统变化大小的 Text
class FixedSizeText extends StatelessWidget {
  final String data;
  final TextStyle style;
  final int maxLines;
  final TextAlign textAlign;
  final TextOverflow overflow;

  FixedSizeText(this.data,
      {this.style, this.maxLines = 1, this.textAlign, this.overflow});

  @override
  Widget build(BuildContext context) {
    return Text(
      data == null ? "" : data,
      style: style,
      textScaleFactor: 1.0,
      maxLines: maxLines,
      textAlign: textAlign,
      overflow: TextOverflow.ellipsis,
    );
  }
}

/// LoadingDialog
class LoadingDialog extends Dialog {
  final String text;

  LoadingDialog({this.text = "Loading"});

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        return false;
      },
      child: Material(
        //创建透明层
        type: MaterialType.transparency, //透明类型
        child: new GestureDetector(
          onTap: () {
//            Navigator.pop(context);
          },
          child: new Center(
              //保证控件居中效果
              child: new SizedBox(
                  width: 120.0,
                  height: 120.0,
                  child: new Container(
                    decoration: ShapeDecoration(
                      color: bgColor,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(
                          Radius.circular(12.0),
                        ),
                      ),
                    ),
                    child: new Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        new CircularProgressIndicator(
                          strokeWidth: 2,
                          valueColor: AlwaysStoppedAnimation(baseColor),
                        ),
                        new Padding(
                          padding: const EdgeInsets.only(
                            top: 20.0,
                          ),
                          child: new FixedSizeText(
                            text,
                            style: TextStyleMs.black1_16,
                          ),
                        ),
                      ],
                    ),
                  ))),
        ),
      ),
    );
  }
}

class TitleText extends StatelessWidget {
  final String text;
  final Color textColor;
  final Color iconColor;
  final GestureTapCallback onBack;

  TitleText(
      {@required this.text,
      this.textColor = baseTextColor,
      this.iconColor = baseTextColor,
      this.onBack});

  @override
  Widget build(BuildContext context) {
    return new Container(
        height: 44,
        width: double.maxFinite,
        padding: EdgeInsets.only(top: 4),
        child: new Stack(
          alignment: AlignmentDirectional.centerStart,
          children: <Widget>[
            new Center(
                child: new FixedSizeText(
              text,
              style: TextStyle(color: textColor, fontSize: 17),
            )),
            onBack != null
                ? GestureDetector(
                    behavior: HitTestBehavior.opaque,
                    onTap: onBack,
                    child: Container(
                      width: 45,
                      height: 44,
                      alignment: Alignment.center,
                      child: Image(
                        image: AssetImage("images/ic_login_back.png"),
                        color: iconColor,
                      ),
                    ),
                  )
                : new Container()
          ],
        ));
  }
}

/// 虚线分割线
class DottedLine extends StatelessWidget {
  final double height;
  final Color color;
  final double padding; //虚线宽度

  const DottedLine(
      {this.height = 1, this.color = Colors.black, this.padding = 4});

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) {
        final boxWidth = constraints.constrainWidth();
        final dashWidth = padding;
        final dashHeight = height;
        final dashCount = (boxWidth / (2 * dashWidth)).floor();
        return Flex(
          children: List.generate(dashCount, (_) {
            return SizedBox(
              width: dashWidth,
              height: dashHeight,
              child: DecoratedBox(
                decoration: BoxDecoration(color: color),
              ),
            );
          }),
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          direction: Axis.horizontal,
        );
      },
    );
  }
}

class MessageDialog extends Dialog {
  final String message;
  final String buttonText;
  final Function onPress;

  MessageDialog({
    Key key,
    this.message,
    this.buttonText,
    this.onPress,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        return false;
      },
      child: new Material(
        type: MaterialType.transparency,
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            new Container(
              width: 270,
              height: 144,
              decoration: ShapeDecoration(
                color: Color(0xFFFFFFFF),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(
                    Radius.circular(13),
                  ),
                ),
              ),
              child: new Column(
                children: <Widget>[
                  Expanded(
                    flex: 1,
                    child: new Container(
                      margin:
                          const EdgeInsets.only(top: 10, left: 15, right: 15),
                      alignment: Alignment.center,
                      child: new Text(
                        message,
                        style: new TextStyle(
                            fontSize: 13, color: Color(0xFF111111)),
                        textAlign: TextAlign.center,
                        textScaleFactor: 1.0,
                      ),
                    ),
                  ),
                  Container(
                    color: dialogLineColor,
                    height: 0.5,
                  ),
                  GestureDetector(
                    behavior: HitTestBehavior.opaque,
                    onTap: onPress,
                    child: Container(
                      height: 44,
                      alignment: Alignment.center,
                      child: FixedSizeText(
                        buttonText,
                        style:
                            TextStyle(color: Color(0xFF409EFF), fontSize: 17),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

/// 单/双按钮Dialog
class MessageDialog2 extends Dialog {
  final String title;
  final String message;
  final String negativeText;
  final String positiveText;
  final Function onCloseEvent;
  final Function onPositivePressEvent;

  MessageDialog2({
    Key key,
    this.title,
    this.message,
    this.negativeText,
    this.positiveText,
    this.onCloseEvent,
    this.onPositivePressEvent,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        return false;
      },
      child: new Material(
        type: MaterialType.transparency,
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            new Container(
              width: 270,
              height: 144,
              decoration: ShapeDecoration(
                color: Color(0xFFFFFFFF),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(
                    Radius.circular(13),
                  ),
                ),
              ),
              child: new Column(
                children: <Widget>[
                  new Padding(
                    padding:
                        const EdgeInsets.only(top: 20, left: 15, right: 15),
                    child: new FixedSizeText(
                      title,
                      style: new TextStyle(fontSize: 17, color: Colors.black),
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: new Container(
                      margin:
                          const EdgeInsets.only(top: 10, left: 15, right: 15),
                      child: new Text(
                        message,
                        style: new TextStyle(
                            fontSize: 13, color: Color(0xFF111111)),
                        textAlign: TextAlign.center,
                        textScaleFactor: 1.0,
                      ),
                    ),
                  ),
                  Container(
                    color: dialogLineColor,
                    height: 0.5,
                  ),
                  Container(
                    height: 45,
                    child: this._buildButtonGroup(),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildButtonGroup() {
    var widgets = <Widget>[];
    if (onCloseEvent != null) {
      widgets.add(_buildButton(negativeText, onCloseEvent));
    }
    if (onCloseEvent != null && onPositivePressEvent != null) {
      widgets.add(new Container(
          color: dialogLineColor, width: 0.5, height: double.maxFinite));
    }
    if (onPositivePressEvent != null) {
      widgets.add(_buildButton(positiveText, onPositivePressEvent));
    }
    return new Flex(
      direction: Axis.horizontal,
      children: widgets,
    );
  }

  Widget _buildButton(text, onPress) {
    return new Expanded(
      flex: 1,
      child: new FlatButton(
        onPressed: onPress,
        child: new FixedSizeText(
          text,
          style: TextStyle(color: Color(0xFF409EFF), fontSize: 17),
        ),
        splashColor: transparentColor,
        highlightColor: transparentColor,
      ),
    );
  }
}

class SwitchButtonOn extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 20,
      width: 34,
      decoration: BoxDecoration(
        color: baseColor,
        borderRadius: BorderRadius.circular(10),
      ),
      alignment: Alignment.centerRight,
      child: Container(
        height: 20,
        width: 20,
        decoration: BoxDecoration(
          color: whiteColor,
          borderRadius: BorderRadius.circular(10),
          border: Border.all(color: Color(0xFF80B8B8B8), width: 0.5),
        ),
      ),
    );
  }
}

class SwitchButtonOff extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 20,
      width: 34,
      decoration: BoxDecoration(
        color: Color(0xFF80B8B8B8),
        borderRadius: BorderRadius.circular(10),
      ),
      alignment: Alignment.centerLeft,
      child: Container(
        height: 20,
        width: 20,
        decoration: BoxDecoration(
          color: whiteColor,
          borderRadius: BorderRadius.circular(10),
          border: Border.all(color: Color(0xFF80B8B8B8), width: 0.5),
        ),
      ),
    );
  }
}
