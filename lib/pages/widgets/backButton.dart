import 'package:flutter/material.dart';

class MuslimBackButton extends StatelessWidget {
  MuslimBackButton({Key key, this.color, this.callback}) : super(key: key);
  VoidCallback callback;
  Color color;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return IconButton(
        icon: Icon(
          Icons.navigate_before,
          color: color == null ? Colors.black : color,
        ),
        onPressed: () {
          if (callback != null) {
            callback();
          } else {
            Navigator.of(context).pop();
          }
        });
  }
}
