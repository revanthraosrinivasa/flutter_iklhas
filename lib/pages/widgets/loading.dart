import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:muslim/common/tools/hexColor.dart';

class LoadingIndicator extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 150.0),
      child: Center(
        child: SizedBox(
          width: 50.0,
          height: 50.0,
          child: CircularProgressIndicator(
            strokeWidth: 1.5,
            valueColor: AlwaysStoppedAnimation(HexColor("67C1BF")),
          ),
        ),
      ),
    );
  }
}


