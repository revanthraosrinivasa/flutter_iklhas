import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:muslim/common/core/storage.dart';
import 'package:muslim/common/helper/helper.dart';
import 'package:muslim/common/tools/hexColor.dart';
import 'package:muslim/pages/community/main.dart';
import 'package:muslim/pages/home/home.dart';
import 'package:muslim/pages/mine/main.dart';

class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage>
    with SingleTickerProviderStateMixin {
  PageController _tabController;
  int _selectedIndex;
  int _homeClick;
  int _community;
  Color _selectedTabColor = HexColor("FB7268");
  Color _unselectedTabColor = HexColor("75767A");

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _selectedIndex = 0;
    _homeClick = 0;
    _community = 0;
    _tabController = PageController(initialPage: _selectedIndex);
    StorageHelper().setString(LAST_LOGIN_TIME,
        DateFormat("yyyy-MM-dd HH:mm:ss").format(DateTime.now()));
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _selectedIndex,
        selectedItemColor: _selectedTabColor,
        unselectedItemColor: _unselectedTabColor,
        items: [
          BottomNavigationBarItem(
              icon: Image.asset('assets/images/tabbar_home.png',
                  color: _selectedIndex == 0
                      ? _selectedTabColor
                      : _unselectedTabColor,
                  height: 25,
                  width: 25),
              label: tr("Home")),
          BottomNavigationBarItem(
              icon: Image.asset('assets/images/tabbar_community.png',
                  color: _selectedIndex == 1
                      ? _selectedTabColor
                      : _unselectedTabColor,
                  height: 25,
                  width: 25),
              label: tr("Community")),
          BottomNavigationBarItem(
              icon: Image.asset('assets/images/tabbar_account.png',
                  color: _selectedIndex == 2
                      ? _selectedTabColor
                      : _unselectedTabColor,
                  height: 25,
                  width: 25),
              label: tr('Account'))
        ],
        onTap: (index) {
          _tabController.jumpToPage(index);
          if (index == 0 && _selectedIndex == index) {
            setState(() {
              _homeClick = _homeClick + 1;
            });
          } else if (index == 1 && _selectedIndex == index) {
            setState(() {
              _community = _community + 1;
            });
          }
          setState(() {
            _selectedIndex = index;
          });
        },
      ),
      body: PageView(
        physics: NeverScrollableScrollPhysics(),
        children: [
          HomePage(clickTip: _homeClick),
          CommunityMainPage(clickTip: _community),
          MinePage()
        ],
        controller: _tabController,
      ),
    );
  }
}
