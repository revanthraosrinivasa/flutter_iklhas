import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:geolocator/geolocator.dart';
import 'package:muslim/common/api/google_map.dart';
import 'package:muslim/common/helper/logger.dart';
import 'package:muslim/common/provider/app.dart';
import 'package:muslim/common/core/storage.dart';
import 'package:muslim/common/helper/helper.dart';
import 'package:muslim/common/provider/prayer.dart';
import 'package:muslim/common/tools/dialog.dart';
import 'package:muslim/common/tools/hexColor.dart';
import 'package:muslim/common/tools/snackBar.dart';
import 'package:muslim/common/provider/user.dart';
import 'package:muslim/global.dart';
import 'package:muslim/modals/autocomplate.dart';
import 'package:muslim/modals/details.dart';
import 'package:muslim/modals/matimes.dart';
import 'package:muslim/modals/times.dart';
import 'package:muslim/modals/timezone.dart';
import 'package:muslim/pages/widgets/backButton.dart';
import 'package:muslim/pages/widgets/horizontalLine.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:provider/provider.dart';
import 'package:timezone/timezone.dart' as tz;
import 'package:timezone/data/latest.dart' as tz;

class SearchLocationPage extends StatefulWidget {
  @override
  SearchLocationPageState createState() => SearchLocationPageState();
}

class SearchLocationPageState extends State<SearchLocationPage> {
  List<Prediction> _locations;
  String _location;
  bool _isFetchLocation;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _locations = [];
    _location = "";
    _isFetchLocation = false;
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return KeyboardDismissOnTap(
        child: Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Container(
          color: HexColor("F2F2F2"),
          height: 44.0,
          width: 260.0,
          child: TextField(
            onChanged: (value) {
              _fetchLocations(value);
            },
            decoration: InputDecoration(
                border: InputBorder.none,
                prefixIcon: Icon(
                  Icons.search,
                  color: HexColor("67C1BF"),
                ),
                hintText: tr("search_location_placehold"),
                hintStyle: TextStyle(fontSize: 14.0)),
          ),
        ),
        leading: MuslimBackButton(),
        actions: [
          Container(
            width: 60.0,
            child: IconButton(
                icon: Icon(Icons.near_me), onPressed: _fetchLocalLocation),
          )
        ],
      ),
      body: _isFetchLocation
          ? Container(
              margin: EdgeInsets.only(top: 20.0),
              child: CircularProgressIndicator(),
              alignment: Alignment.topCenter,
            )
          : ListView.separated(
              itemBuilder: (context, index) {
                Prediction prediction = _locations[index];
                String titleStr = "";
                String subTitleStr = "";
                if (prediction.terms.length == 1) {
                  titleStr = prediction.terms[0].value;
                } else if (prediction.terms.length == 2) {
                  titleStr = prediction.terms[0].value;
                  subTitleStr = prediction.terms[1].value;
                } else if (prediction.terms.length > 2) {
                  titleStr = prediction.terms
                      .getRange(0, prediction.terms.length - 2)
                      .map((e) => e.value)
                      .join(",");
                  subTitleStr = prediction.terms
                      .getRange(
                          prediction.terms.length - 2, prediction.terms.length)
                      .map((e) => e.value)
                      .join(",");
                }
                return ListTile(
                  tileColor: Colors.white,
                  title: Text(
                    titleStr,
                    style: TextStyle(color: HexColor("212124"), fontSize: 16.0),
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  ),
                  subtitle: Text(subTitleStr,
                      style:
                          TextStyle(color: HexColor("4C4C50"), fontSize: 14.0)),
                  onTap: () {
                    unforceTextField();
                    _fetchPlaceWithInfo(prediction);
                  },
                );
              },
              separatorBuilder: (context, index) {
                return HorizontalLine();
              },
              itemCount: _locations.length),
    ));
  }

  Future<void> _fetchLocations(String location) async {
    setState(() {
      _location = location;
    });
    if (_isFetchLocation == false) {
      Timer(Duration(milliseconds: 1000), () async {
        if (mounted) {
          setState(() {
            _isFetchLocation = true;
          });
          Map<String, dynamic> params = {"input": _location};
          GoogleApiService().fetchAutocompletePlaces(params).then((value) {
            if (mounted) {
              setState(() {
                _isFetchLocation = false;
                _locations = value;
              });
            }
          }).catchError((error) {
            if (mounted) {
              setState(() {
                _isFetchLocation = false;
              });
            }
          });
        }
      });
    }
  }

  /// Google 模糊搜索  place id => 经纬度 => 是否是马来西亚的城市 => 是，祈祷时间列表 / 否 => 请求所在的时区
  Future<void> _fetchPlaceWithInfo(Prediction prediction) async {
    showWaitingDialog(context, "");
    Map<String, dynamic> params = {"place_id": prediction.placeId};
    Result value;
    try {
      value = await GoogleApiService().fetchPlaceDetails(params);
    } catch (placeDetailError) {
      logger("_fetchPlaceWithInfo detail error $placeDetailError");
    }

    if (value == null) {
      if (mounted) {
        goBack(context);
        showSnackBar(context, tr("network_error"));
      }
      return;
    }
    logger("_fetchPlaceWithInfo detail success ${value.toJson()}");

    Position position = Position(
        latitude: value.geometry.location.lat,
        longitude: value.geometry.location.lng);
    AppSettingModel appSetting =
        Provider.of<AppSettingModel>(context, listen: false);
    PrayerModel prayerModel = Provider.of<PrayerModel>(context, listen: false);
    UserModel userModel = Provider.of<UserModel>(context, listen: false);
    var appSettingJson = {
      "userId": StorageHelper().getString(USERID),
      "locationCity": "",
      "locationCountry": "",
      "autoSetting": false,
      "userAutoSetting": true,
      "conventions": 0,
      "corrections": [0, 0, 3, 3, 0, 0],
      "calculation": 0,
      "highLatitude": 2,
    };
    if (appSetting.appSetting != null) {
      appSettingJson = appSetting.appSetting.toJson();
    }
    appSettingJson["locationCountry"] = value.formattedAddress;
    appSettingJson["locationCity"] = value.formattedAddress;
    MalaysiaPrayerTime malaysiaTimes;
    PrayerTimesResult malaysiaYearTime;
    try {
      malaysiaTimes = await _fetchMalaysiaCity(position);
    } catch (error) {}
    if (malaysiaTimes != null) {
      if (malaysiaTimes.data.attributes.jakimCode ==
          appSetting.malaysiaPlaceCode) {
        appSettingJson["autoSetting"] = true;
      } else {
        var data = {
          "zone": malaysiaTimes.data.attributes.jakimCode.toUpperCase(),
          "period": "year"
        };
        try {
          malaysiaYearTime = await Global.normalApi.fetchYearPrayerTimes(data);
          if (malaysiaYearTime.errorCode == SUCCESS) {
            appSettingJson["autoSetting"] = true;
          }
        } catch (error) {}
      }
    } else {
      appSettingJson["autoSetting"] = false;
    }
    try {
      int timeZone = await _fetchLocalTimeZone(position);
      if (timeZone != null) {
        if (malaysiaYearTime != null)
          appSetting.malaysiaPrayerTimes = malaysiaYearTime.prayerTime;
        if (malaysiaTimes != null) {
          appSetting.malaysiaPlaceCode =
              malaysiaTimes.data.attributes.jakimCode;
          appSetting.region = malaysiaTimes.data.place;
        }
        userModel.isChoiceCity = true;
        StorageHelper().setJSON(POSITION, position);
        StorageHelper().setBool(USER_CHOICE_CITY, true);
        StorageHelper()
            .setJSON(PRAYER_TIMES_SETTING_CITY, value.formattedAddress);
        prayerModel.changeToLocations(position.latitude, position.longitude);
        prayerModel.changeLocationCity(value.formattedAddress);
        prayerModel.utcOffset = timeZone;
        StorageHelper().setInt(TIMEZONE, timeZone);
        appSetting.appSetting = AppPrayerTimeSetting.fromJson(appSettingJson);
        if (appSettingJson["autoSetting"]) {
          var value = appSetting.fetchDateListWithParams(DateTime.now(), 400);
          if (value != null) {
            Global.viewModel.sendLocaloNitification(value);
          }
        }
        if (mounted) {
          goBack(context, type: 1);
        }
        return;
      } else {
        if (mounted) {
          goBack(context);
          showSnackBar(context, tr("location_not_exist"));
        }
      }
    } catch (error) {
      if (mounted) {
        goBack(context);
        showSnackBar(context, tr("network_error"));
      }
    }
  }

  void unforceTextField() {
    final currentFocus = FocusScope.of(context);
    if (!currentFocus.hasPrimaryFocus && currentFocus.hasFocus) {
      FocusManager.instance.primaryFocus.unfocus();
    }
  }

  /// 定位 经纬度 => 是否马来西亚城市 => 是，祈祷时间列表/否，地理反编译 => 当地时区
  Future<void> _fetchLocalLocation() async {
    showWaitingDialog(context, "");
    Map<String, dynamic> positionValue;
    try {
      positionValue = await Global.locationHelper.fetchLocation(context);
    } catch (error) {}
    if (positionValue == null || positionValue["msg"] == null) {
      goBack(context);
      if (mounted) {
        showSnackBar(context, tr("location_tip_1"));
      }
      return;
    }
    Position position = Position(
        latitude: positionValue["msg"]["latitude"],
        longitude: positionValue["msg"]["longitude"]);
    PrayerModel prayerModel = Provider.of<PrayerModel>(context, listen: false);
    AppSettingModel appSetting =
        Provider.of<AppSettingModel>(context, listen: false);
    UserModel userModel = Provider.of<UserModel>(context, listen: false);

    var appSettingJson = {
      "userId": StorageHelper().getString(USERID),
      "locationCity": "",
      "locationCountry": "",
      "autoSetting": false,
      "userAutoSetting": true,
      "conventions": 0,
      "corrections": [0, 0, 3, 3, 0, 0],
      "calculation": 0,
      "highLatitude": 2,
    };
    if (appSetting.appSetting != null) {
      appSettingJson = appSetting.appSetting.toJson();
    }

    MalaysiaPrayerTime malaysiaTimes;
    PrayerTimesResult malaysiaYearTime;
    try {
      malaysiaTimes = await _fetchMalaysiaCity(position);
      if (malaysiaTimes != null) {
        if (malaysiaTimes.data.attributes.jakimCode ==
            appSetting.malaysiaPlaceCode) {
          appSettingJson["autoSetting"] = true;
        } else {
          var data = {
            "zone": malaysiaTimes.data.attributes.jakimCode.toUpperCase(),
            "period": "year"
          };
          malaysiaYearTime = await Global.normalApi.fetchYearPrayerTimes(data);
          if (malaysiaYearTime.errorCode == SUCCESS) {
            appSettingJson["autoSetting"] = true;
          }
        }
      }
    } catch (error) {}

    try {
      int timeZone = await _fetchLocalTimeZone(position);
      if (timeZone != null) {
        if (malaysiaTimes != null) {
          appSetting.malaysiaPlaceCode =
              malaysiaTimes.data.attributes.jakimCode;
          appSetting.region = malaysiaTimes.data.place;
          appSettingJson["locationCountry"] = malaysiaTimes.data.place;
          appSettingJson["locationCity"] = malaysiaTimes.data.place;
          StorageHelper()
              .setJSON(PRAYER_TIMES_SETTING_CITY, malaysiaTimes.data.place);
          prayerModel.changeLocationCity(malaysiaTimes.data.place);
        } else {
          List<Result> values = await GoogleApiService().fetchLocalCity(
              {"latlng": "${position.latitude},${position.longitude}"});
          String locationCountry = values.length < 2
              ? values[0].formattedAddress
              : values[1].formattedAddress;
          appSettingJson["locationCountry"] = locationCountry;
          appSettingJson["locationCity"] = locationCountry;
          appSettingJson["autoSetting"] = false;
          StorageHelper()
              .setJSON(PRAYER_TIMES_SETTING_CITY, values[0].formattedAddress);
          prayerModel.changeLocationCity(values[0].formattedAddress);
        }
        if (malaysiaYearTime != null)
          appSetting.malaysiaPrayerTimes = malaysiaYearTime.prayerTime;
        userModel.isChoiceCity = true;
        StorageHelper().setJSON(POSITION, position);
        StorageHelper().setBool(USER_CHOICE_CITY, true);
        prayerModel.utcOffset = timeZone;
        prayerModel.changeToLocations(position.latitude, position.longitude);
        StorageHelper().setInt(TIMEZONE, timeZone);
        if (appSettingJson["autoSetting"]) {
          var value = appSetting.fetchDateListWithParams(DateTime.now(), 400);
          if (value != null) {
            Global.viewModel.sendLocaloNitification(value);
          }
        }
        appSetting.appSetting = AppPrayerTimeSetting.fromJson(appSettingJson);
        if (mounted) {
          goBack(context, type: 1);
        }
        return;
      } else {
        if (mounted) {
          goBack(context);
          showSnackBar(context, tr("location_not_exist"));
        }
      }
    } catch (error) {
      if (mounted) {
        goBack(context);
        showSnackBar(context, tr("network_error"));
      }
    }
  }
}

Future<MalaysiaPrayerTime> _fetchMalaysiaCity(Position position) async {
  try {
    var malaysiaTimes = await Global.normalApi
        .fetchMalaysiaPrayerTimes(position.latitude, position.longitude);
    return malaysiaTimes;
  } catch (error) {
    logger("-----_fetchMalaysiaCity error ------- $error");
    return null;
  }
}

Future<PrayerTimesResult> _fetchMalaysiaPrayerTimes(String zone) async {
  var data = {"zone": zone, "period": "year"};
  try {
    PrayerTimesResult malaysiaYearTime =
        await Global.normalApi.fetchYearPrayerTimes(data);
    return malaysiaYearTime;
  } catch (error) {
    return null;
  }
}

Future<int> _fetchLocalTimeZone(Position position) async {
  try {
    GoogleTimeZoneData timeZoneValue = await GoogleApiService()
        .fetchLocationTimeZone(
            {"location": "${position.latitude},${position.longitude}"});
    if (timeZoneValue.status == "OK") {
      StorageHelper().setString(TIMEZONEID, timeZoneValue.timeZoneId);
      return tz.getLocation(timeZoneValue.timeZoneId).currentTimeZone.offset ~/
          3600000;
    } else {
      return null;
    }
  } catch (error) {
    return null;
  }
}

goBack(BuildContext context, {int type = 0}) {
  if (Navigator.of(context).canPop()) {
    Navigator.of(context)
        .popUntil((route) => route.settings.name == "search_location");
    if (type != 0) {
      Timer(Duration(milliseconds: 300), () {
        Navigator.of(context).pop();
      });
    }
  }
}
