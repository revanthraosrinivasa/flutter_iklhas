import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:muslim/common/core/storage.dart';
import 'package:muslim/common/provider/prayer.dart';
import 'package:muslim/common/tools/hexColor.dart';
import 'package:muslim/pages/widgets/backButton.dart';
import 'package:muslim/pages/widgets/horizontalLine.dart';
import 'package:muslim/pages/widgets/muslim_title.dart';
import 'package:provider/provider.dart';

class ConventionPage extends StatefulWidget {
  @override
  _ConventionState createState() => _ConventionState();
}

class _ConventionState extends State<ConventionPage> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    PrayerModel prayerModal = context.watch<PrayerModel>();

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: MuslimTitle(title: tr("Prayer Times Conventions")),
        leading: MuslimBackButton(),
      ),
      body: Padding(
        padding: EdgeInsets.only(top: 8.0),
        child: ListView.builder(
            itemBuilder: (context, index) {
              return _initListTileWithIndex(context, index, prayerModal);
            },
            itemCount: PRAYER_CONVENTIONS.length),
      ),
    );
  }

  Widget _initListTileWithIndex(
      BuildContext context, int index, PrayerModel prayerModal) {
    Map<String, dynamic> map = PRAYER_CONVENTIONS[index];
    Map<String, num> value = map["value"];
    var subTitleText = "Fajr: ${value['Fajr']}° / Isha'a: ${value['Isha']}°";
    if (index == 5 || index == 17) {
      subTitleText = subTitleText + " min after Maghrib";
    } else if (index == 18) {
      subTitleText = subTitleText + "/120 min after Maghrib";
    }
    Color color = index == prayerModal.paryerConventionsIndex
        ? HexColor("67C1BF")
        : HexColor("212124");
    return Container(
      decoration: BoxDecoration(
          border: Border(
              top: BorderSide(color: HexColor("EAEAEA")),
              bottom: BorderSide(
                  color: index == PRAYER_CONVENTIONS.length - 1
                      ? HexColor("EAEAEA")
                      : Colors.transparent))),
      child: ListTile(
        title: Text(
          map["name"],
          style: TextStyle(color: color),
        ),
        subtitle: Text(
          subTitleText,
          style: TextStyle(color: color),
        ),
        trailing: index == prayerModal.paryerConventionsIndex
            ? Icon(
                Icons.done,
                color: color,
              )
            : null,
        onTap: () {
          prayerModal.changeFajrAndIsha(index);
        },
      ),
    );
  }
}
