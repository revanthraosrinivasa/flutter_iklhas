import 'package:adhan/adhan.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:muslim/common/provider/app.dart';
import 'package:muslim/common/core/storage.dart';
import 'package:muslim/common/helper/helper.dart';
import 'package:muslim/common/provider/prayer.dart';
import 'package:muslim/common/helper/prayer_time.dart';
import 'package:muslim/common/tools/hexColor.dart';
import 'package:muslim/common/provider/user.dart';
import 'package:muslim/global.dart';
import 'package:muslim/modals/times.dart';
import 'package:muslim/pages/widgets/horizontalLine.dart';
import 'package:provider/provider.dart';

class PrayerListWidget extends StatefulWidget {
  PrayerListWidget(
      {Key key, @required this.offset, @required this.isLeft, this.appSetting})
      : super(key: key);
  final int offset;
  final int isLeft;
  final AppPrayerTimeSetting appSetting;
  @override
  _PrayerListWidgetState createState() => _PrayerListWidgetState();
}

class _PrayerListWidgetState extends State<PrayerListWidget> {
  PageController _pageController;
  bool locationError = false;
  PrayerTimeResult _prayerTime;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _pageController = PageController();
    _fetchPrayerTimeSource();
  }

  _fetchPrayerTimeSource() async {
    if (widget.appSetting != null &&
        widget.appSetting.autoSetting == true &&
        widget.appSetting.userAutoSetting == true) {
      AppSettingModel appSetting =
          Provider.of<AppSettingModel>(context, listen: false);
      try {
        var value = appSetting.fetchDateListWithParams(
            DateTime.now().add(Duration(days: widget.offset)), 1);
        if (value != null) {
          setState(() {
            _prayerTime = value.first;
            locationError = false;
          });
        }
        return null;
      } catch (error) {
        return null;
      }
    } else {
      PrayerModel prayerModel =
          Provider.of<PrayerModel>(context, listen: false);
      PrayerTimes prayerTimes = prayerModel.prayerTimes;
      if (prayerTimes == null) {
        Position position = Position.fromMap(StorageHelper().getJSON(POSITION));
        if (position != null) {
          prayerTimes = PrayerTimes.utcOffset(
              Coordinates(position.latitude, position.longitude),
              DateComponents.from(DateTime.fromMillisecondsSinceEpoch(
                      DateTime.now().millisecondsSinceEpoch +
                          widget.offset * 24 * 3600 * 1000)
                  .toUtc()),
              prayerModel.params,
              Duration(hours: prayerModel.utcOffset));
          var value = prayerModel.changeToPrayerTimeResult(prayerTimes);
          setState(() {
            _prayerTime = value;
            locationError = false;
          });
        } else {
          setState(() {
            locationError = true;
          });
        }
      } else {
        prayerTimes = PrayerTimes.utcOffset(
            prayerTimes.coordinates,
            DateComponents.from(DateTime.fromMillisecondsSinceEpoch(
                    DateTime.now().millisecondsSinceEpoch +
                        widget.offset * 24 * 3600 * 1000)
                .toUtc()),
            prayerModel.params,
            Duration(hours: prayerModel.utcOffset));
        var value = prayerModel.changeToPrayerTimeResult(prayerTimes);
        setState(() {
          _prayerTime = value;
          locationError = false;
        });
      }
    }
  }

  @override
  void didUpdateWidget(covariant PrayerListWidget oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);
    print("_PrayerListWidgetState didUpdateWidget");
    if (widget.isLeft > 0) {
      _pageController.jumpToPage(0);
      _pageController.animateToPage(1,
          duration: Duration(milliseconds: 300), curve: Curves.easeIn);
    } else if (widget.isLeft < 0) {
      _pageController.jumpToPage(1);
      _pageController.animateToPage(0,
          duration: Duration(milliseconds: 300), curve: Curves.easeIn);
    }
    _fetchPrayerTimeSource();
  }

  @override
  Widget build(BuildContext context) {
    bool isShowPrayerList = true;
    // TODO: implement build
    PrayerTimeViewModel viewModel = Global.viewModel;
    if (_prayerTime == null) {
      isShowPrayerList = false;
    }

    UserModel userModal = context.watch<UserModel>();
    List<bool> alarmClock = userModal.alarmClock;

    return Container(
        height: 340.0,
        decoration: BoxDecoration(
            border: Border(bottom: BorderSide(color: HexColor("EAEAEA")))),
        child: PageView.builder(
          physics: NeverScrollableScrollPhysics(),
          controller: _pageController,
          itemCount: 2,
          itemBuilder: (context, index) {
            return !isShowPrayerList || locationError
                ? Center(
                    child: SizedBox(
                      width: 50.0,
                      height: 50.0,
                      child: CircularProgressIndicator(
                        strokeWidth: 1.5,
                        valueColor: AlwaysStoppedAnimation(HexColor("67C1BF")),
                      ),
                    ),
                  )
                : ListView.separated(
                    physics: NeverScrollableScrollPhysics(),
                    itemBuilder: (context, index) {
                      // Prayer prayer = Prayer.values[index + 1];
                      bool _isCurrentList = false;
                      if (widget.offset == 0) {
                        DateTime nowDateTime = DateTime.now();
                        MuslimPrayer currentPrayer =
                            viewModel.currentTimeStr(_prayerTime);
                        if (currentPrayer.index == null) {
                          if (index == 0) {
                            _isCurrentList = true;
                          }
                        } else {
                          if (nowDateTime.millisecondsSinceEpoch -
                                  currentPrayer
                                      .dateTime.millisecondsSinceEpoch <
                              1800000) {
                            if (index == currentPrayer.index - 1) {
                              _isCurrentList = true;
                            }
                          } else {
                            if (currentPrayer.index > 5 && index == 5) {
                              _isCurrentList = true;
                            } else {
                              if (index == currentPrayer.index) {
                                _isCurrentList = true;
                              }
                            }
                          }
                        }
                      }
                      var name = "";
                      DateTime dateTime;
                      switch (index) {
                        case 0:
                          name = tr("Fajr");
                          break;
                        case 1:
                          name = tr("Syuruk");
                          break;
                        case 2:
                          name = tr("Dhuhr");
                          break;
                        case 3:
                          name = tr("Asr");
                          break;
                        case 4:
                          name = tr("Maghrib");
                          break;
                        case 5:
                          name = tr("Isha");
                          break;
                        default:
                      }
                      dateTime = viewModel.timeForName(name, _prayerTime);
                      return ListTile(
                        tileColor:
                            _isCurrentList ? HexColor("EAEAEA") : Colors.white,
                        leading: alarmClock[index]
                            ? Icon(
                                Icons.notifications_active,
                                color: HexColor("FB7268"),
                              )
                            : Icon(Icons.notifications_off),
                        title: Container(
                          padding: EdgeInsets.only(left: 0),
                          child: Text(name),
                        ),
                        trailing: Container(
                          width: 100.0,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              Text(dateTime == null
                                  ? "--"
                                  : DateFormat.jm().format(dateTime)),
                              Icon(
                                Icons.navigate_next,
                                color: HexColor("75767A"),
                              )
                            ],
                          ),
                        ),
                        onTap: () {
                          Navigator.of(context).pushNamed("alarmClockPage",
                              arguments: {
                                "index": index,
                                "prayerTimes": _prayerTime
                              });
                        },
                      );
                    },
                    separatorBuilder: (context, index) {
                      return HorizontalLine();
                    },
                    itemCount: 6,
                  );
          },
        ));
  }
}
