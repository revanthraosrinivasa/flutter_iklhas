import 'dart:async';

import 'package:adhan/adhan.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:muslim/common/provider/prayer.dart';
import 'package:muslim/common/tools/hexColor.dart';
import 'package:muslim/pages/widgets/backButton.dart';
import 'package:muslim/pages/widgets/horizontalLine.dart';
import 'package:muslim/pages/widgets/muslim_title.dart';
import 'package:provider/provider.dart';

class AsrCalculationPage extends StatefulWidget {
  @override
  _AsrCalculationState createState() => _AsrCalculationState();
}

class _AsrCalculationState extends State<AsrCalculationPage> {
  List<String> _asrs = [tr("standard"), tr("hanafi")];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    int asrIndex = 0;
    asrIndex = Madhab.values.indexOf(Provider.of<PrayerModel>(context).madhab);
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: MuslimTitle(title: tr("Asr Calculation")),
          leading: MuslimBackButton(),
        ),
        body: Padding(
          padding: EdgeInsets.only(top: 8.0),
          child: ListView.builder(
              itemBuilder: (context, index) {
                return Container(
                  decoration: BoxDecoration(
                      border: Border(
                          top: BorderSide(color: HexColor("EAEAEA")),
                          bottom: BorderSide(
                              color: index == _asrs.length - 1
                                  ? HexColor("EAEAEA")
                                  : Colors.transparent))),
                  child: ListTile(
                    tileColor: Colors.white,
                    title: Text(
                      _asrs[index],
                      style: TextStyle(
                          color: asrIndex == index
                              ? HexColor("67C1BF")
                              : HexColor("212124")),
                    ),
                    trailing: asrIndex == index
                        ? Icon(
                            Icons.done,
                            color: HexColor("67C1BF"),
                          )
                        : null,
                    onTap: () {
                      if (asrIndex == index) {
                        return;
                      }
                      setState(() {
                        asrIndex = index;
                      });
                      Provider.of<PrayerModel>(context, listen: false)
                          .changeMadhab(Madhab.values[index]);
                    },
                  ),
                );
              },
              itemCount: _asrs.length),
        ));
  }
}
