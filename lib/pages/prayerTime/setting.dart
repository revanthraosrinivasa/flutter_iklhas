import 'package:adhan/adhan.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:matomo/matomo.dart';
import 'package:muslim/common/provider/app.dart';
import 'package:muslim/common/core/storage.dart';
import 'package:muslim/common/provider/prayer.dart';
import 'package:muslim/common/tools/dialog.dart';
import 'package:muslim/common/tools/hexColor.dart';
import 'package:muslim/common/provider/user.dart';
import 'package:muslim/pages/widgets/backButton.dart';
import 'package:muslim/pages/widgets/horizontalLine.dart';
import 'package:muslim/pages/widgets/muslim_title.dart';
import 'package:provider/provider.dart';

class PrayerTimeSettingPage extends StatefulWidget {
  @override
  _PrayerTimeSettingState createState() => _PrayerTimeSettingState();
}

class _PrayerTimeSettingState extends State<PrayerTimeSettingPage> {
  List<String> highRules = [
    tr("Middle of the night"),
    tr("1/7 of the night"),
    tr("Angle-based method")
  ];
  List<String> _asrs = [tr("standard"), tr("hanafi")];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // UserModel user = context.watch<UserModel>();
    PrayerModel prayerModal = context.watch<PrayerModel>();
    AppSettingModel prayerTimeSetting = Provider.of<AppSettingModel>(context);

    PrayerTimes prayerTimes = prayerModal.prayerTimes;
    Map<String, String> _names = {
      tr("Location"): "",
      tr("Prayer Time Conventions"): PRAYER_CONVENTIONS[8]["name"],
      tr("Manaual Corrections"): "0,0,3,3,0,2",
      tr("Asr Calculation"): "${_asrs[0]}",
      tr("High Latitude Adjustment"): "${highRules[2]}"
    };
    if (prayerTimes != null) {
      PrayerAdjustments prayerAdjustments =
          prayerTimes.calculationParameters.adjustments;
      Madhab madhab = prayerModal.madhab;
      // Madhab madhab = prayerTimes.calculationParameters.madhab;
      HighLatitudeRule highLatitude =
          prayerTimes.calculationParameters.highLatitudeRule;
      Map<String, dynamic> convention = PRAYER_CONVENTIONS[
          prayerModal != null ? prayerModal.paryerConventionsIndex : 8];
      _names = {
        tr("Location"): prayerModal.locationCity ?? "",
        tr("Prayer Time Conventions"): convention["name"] ?? "",
        tr("Manaual Corrections"):
            "${prayerAdjustments.fajr},${prayerAdjustments.sunrise},${prayerAdjustments.dhuhr},${prayerAdjustments.asr},${prayerAdjustments.maghrib},${prayerAdjustments.isha}",
        tr("Asr Calculation"): "${_asrs[Madhab.values.indexOf(madhab)]}",
        tr("High Latitude Adjustment"):
            "${highRules[HighLatitudeRule.values.indexOf(highLatitude)]}"
      };
    }

    bool automacticSetting = false;
    if (prayerTimeSetting.appSetting != null &&
        prayerTimeSetting.appSetting.autoSetting &&
        prayerTimeSetting.appSetting.userAutoSetting) {
      automacticSetting = true;
      _names = {
        tr("Location"): prayerModal.locationCity ?? "",
        tr("Region"):
            "JAKIM - ${prayerTimeSetting.malaysiaPlaceCode.toUpperCase()} - ${prayerTimeSetting.region}"
      };
    }
    int tableLength = automacticSetting ? 3 : 1 + _names.keys.length;
    // TODO: implement build
    return Scaffold(
      backgroundColor: HexColor("F9F9F9"),
      appBar: AppBar(
        centerTitle: true,
        title: MuslimTitle(title: tr("prayer_timer_setting_title")),
        leading: MuslimBackButton(),
      ),
      body: Container(
        padding: EdgeInsets.only(top: 8.0),
        decoration: BoxDecoration(
            border: Border(bottom: BorderSide(color: HexColor("EAEAEA")))),
        child: ListView.separated(
            itemBuilder: (context, index) {
              if (index == 0) {
                return Container(
                  margin: EdgeInsets.symmetric(vertical: 20),
                  decoration: BoxDecoration(
                      border: Border.symmetric(
                          horizontal: BorderSide(color: HexColor("EAEAEA")))),
                  child: ListTile(
                    tileColor: Colors.white,
                    title: Text(tr("Location"),
                        style: TextStyle(
                            fontSize: 16.0,
                            fontWeight: FontWeight.w400,
                            color: HexColor("212124"))),
                    subtitle: _names[tr("Location")].length == 0
                        ? null
                        : Text(
                            _names[tr("Location")],
                            style: TextStyle(
                                fontSize: 14.0, color: HexColor("4C4C50")),
                          ),
                    trailing: Icon(
                      Icons.navigate_next,
                      color: HexColor("75767A"),
                    ),
                    onTap: () {
                      _pushToNextPage(1);
                    },
                  ),
                );
              }
              if (index == 1) {
                return ListTile(
                  tileColor: Colors.white,
                  title: Text(tr("Automatic Settings"),
                      style: TextStyle(
                          fontSize: 16.0,
                          fontWeight: FontWeight.w400,
                          color: HexColor("212124"))),
                  subtitle: prayerTimeSetting.appSetting == null ||
                          prayerTimeSetting.appSetting.autoSetting == false
                      ? Text("not_applicable").tr()
                      : null,
                  trailing: CupertinoSwitch(
                      activeColor: HexColor("67C1BF"),
                      value: automacticSetting,
                      onChanged: (value) {
                        if (prayerTimeSetting.appSetting == null ||
                            prayerTimeSetting.appSetting.autoSetting == false) {
                          return;
                        }
                        if (value) {
                          MatomoTracker.trackEvent(
                              "autosetting_open", "autosetting_open");
                        } else {
                          MatomoTracker.trackEvent(
                              "autosetting_close", "autosetting_close");
                        }
                        var json = prayerTimeSetting.appSetting.toJson();
                        json["userAutoSetting"] =
                            !prayerTimeSetting.appSetting.userAutoSetting;
                        prayerTimeSetting.appSetting =
                            AppPrayerTimeSetting.fromJson(json);
                      }),
                );
              }
              List<String> keys = _names.keys.toList();
              List<String> values = _names.values.toList();
              if (tableLength == index + 1) {
                return Container(
                  decoration: BoxDecoration(
                      border: Border(
                          bottom: BorderSide(color: HexColor("EAEAEA")))),
                  child: ListTile(
                    tileColor: Colors.white,
                    title: Text(keys[index - 1],
                        style: TextStyle(
                            fontSize: 16.0,
                            fontWeight: FontWeight.w400,
                            color: HexColor("212124"))),
                    subtitle: Text(
                      values[index - 1],
                      style:
                          TextStyle(fontSize: 14.0, color: HexColor("4C4C50")),
                    ),
                    trailing: Icon(
                      Icons.navigate_next,
                      color: HexColor("75767A"),
                    ),
                    onTap: () {
                      _pushToNextPage(index);
                    },
                  ),
                );
              }
              return ListTile(
                tileColor: Colors.white,
                title: Text(keys[index - 1],
                    style: TextStyle(
                        fontSize: 16.0,
                        fontWeight: FontWeight.w400,
                        color: HexColor("212124"))),
                subtitle: Text(
                  values[index - 1],
                  style: TextStyle(fontSize: 14.0, color: HexColor("4C4C50")),
                ),
                trailing: Icon(
                  Icons.navigate_next,
                  color: HexColor("75767A"),
                ),
                onTap: () {
                  _pushToNextPage(index);
                },
              );
            },
            separatorBuilder: (context, index) {
              return HorizontalLine();
            },
            itemCount: tableLength),
      ),
    );
  }

  void _pushToNextPage(int index) {
    switch (index) {
      case 1:
        // 跳转到location
        Navigator.of(context).pushNamed("search_location");
        break;
      case 2:
        AppSettingModel prayerTimeSetting =
            Provider.of<AppSettingModel>(context, listen: false);
        if (prayerTimeSetting.appSetting != null &&
            prayerTimeSetting.appSetting.autoSetting &&
            prayerTimeSetting.appSetting.userAutoSetting) {
          Navigator.of(context).pushNamed("region");
        } else {
          Navigator.of(context).pushNamed("conventions");
        }
        // 跳转到 prayer time
        break;
      case 3:
        // 跳转到 manual corrections
        Navigator.of(context).pushNamed("manual_correction");
        break;
      case 4:
        // 跳转到 asr calculation
        Navigator.of(context).pushNamed("asr_calculation");
        break;
      case 5:
        // 跳转到 high latitude adjustment
        Navigator.of(context).pushNamed("high_latitude_page");
        break;
      default:
        break;
    }
  }
}
