import 'dart:convert';
import 'package:adhan/adhan.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:matomo/matomo.dart';
import 'package:muslim/common/api/error_network_handle.dart';
import 'package:muslim/common/provider/app.dart';
import 'package:muslim/common/core/storage.dart';
import 'package:muslim/common/provider/prayer.dart';
import 'package:muslim/common/tools/dialog.dart';
import 'package:muslim/common/tools/hexColor.dart';
import 'package:muslim/common/tools/snackBar.dart';
import 'package:muslim/global.dart';
import 'package:muslim/modals/result.dart';
import 'package:muslim/pages/widgets/backButton.dart';
import 'package:muslim/pages/widgets/empty.dart';
import 'package:muslim/pages/widgets/horizontalLine.dart';
import 'package:muslim/pages/widgets/muslim_title.dart';
import 'package:provider/provider.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:flutter_picker/Picker.dart';

class MonthTimeTablePage extends TraceableStatefulWidget {
  @override
  _MonthTimeTableState createState() => _MonthTimeTableState();
}

class _MonthTimeTableState extends State<MonthTimeTablePage> {
  DateTime _nowDate = DateTime.now();
  WebViewController _controller;
  bool loading = true;
  bool _isNetworkError = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: MuslimTitle(title: tr("Prayer Timetable")),
        leading: MuslimBackButton(),
      ),
      body: Padding(
        padding: EdgeInsets.only(top: 8.0),
        child: Column(
          children: [
            ListTile(
              tileColor: Colors.white,
              title: Text("start_date_of_timetable").tr(),
              trailing: Container(
                width: 120.0,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Text(DateFormat("yyyy-MM-dd").format(_nowDate)),
                    Icon(
                      Icons.navigate_next,
                      color: HexColor("75767A"),
                    )
                  ],
                ),
              ),
              onTap: () {
                Picker picker = new Picker(
                  height: 250.0,
                  itemExtent: 44.0,
                  adapter: DateTimePickerAdapter(
                      value: _nowDate,
                      yearBegin: DateTime.now().year,
                      yearEnd: DateTime.now().year),
                  title: Text("Date").tr(),
                  hideHeader: !Global.isIOS,
                  selectedTextStyle:
                      TextStyle(color: HexColor("67C1BF"), fontSize: 14.0),
                  confirmText: tr("confirmText"),
                  cancelText: tr("cancelText"),
                  cancelTextStyle:
                      TextStyle(color: HexColor("67C1BF"), fontSize: 14.0),
                  confirmTextStyle:
                      TextStyle(color: HexColor("67C1BF"), fontSize: 14.0),
                  onConfirm: (Picker picker, List value) {
                    print(
                        "picker: ${(picker.adapter as DateTimePickerAdapter).value}");
                    setState(() {
                      _nowDate =
                          (picker.adapter as DateTimePickerAdapter).value;
                    });
                    _fetchTimeTable();
                  },
                );
                if (Global.isIOS) {
                  picker.showModal(context);
                } else {
                  picker.showDialog(context);
                }
              },
            ),
            HorizontalLine(),
            _isNetworkError
                ? EmptyPage(noDatasTip: tr("no_network_tip"))
                : Expanded(
                    child: Stack(
                    children: <Widget>[
                      Builder(builder: (BuildContext context) {
                        return WebView(
                          initialUrl: "",
                          onPageFinished: (String s) {
                            setState(() {
                              loading = false;
                            });
                          },
                          onWebViewCreated: (controller) {
                            _controller = controller;
                            _fetchTimeTable();
                          },
                        );
                      }),
                      Offstage(
                        offstage: !loading,
                        child: Container(
                          width: MediaQuery.of(context).size.width,
                          height: MediaQuery.of(context).size.height,
                          color: Colors.white,
                        ),
                      ),
                    ],
                  )),
          ],
        ),
      ),
    );
  }

  _fetchTimeTable() async {
    if (!checkNetworkState(context)) return;
    setState(() {
      _isNetworkError = false;
    });
    showWaitingDialog(context, null);
    PrayerModel model = Provider.of<PrayerModel>(context, listen: false);
    Map<String, dynamic> params = {
      "title": {
        "title": tr("Prayer Timetable"),
        "desc": model != null &&
                model.locationCity != null &&
                model.locationCity.length != 0
            ? model.locationCity
            : "Kuala Lumpur, Malaysia",
        // "detail": "JAKIM (Jabatan Kemajuan Islam Malaysia)"
        "detail": ""
      },
    };
    AppSettingModel appSettingModel =
        Provider.of<AppSettingModel>(context, listen: false);
    AppPrayerTimeSetting appSetting = appSettingModel.appSetting;
    if (appSetting != null &&
        appSetting.autoSetting &&
        appSetting.userAutoSetting) {
      try {
        var value = appSettingModel.fetchDateListWithParams(_nowDate, 30);
        if (value != null && value.length != 0) {
          params["contents"] = value
              .map((e) => {
                    "date": DateFormat("dd-MMM-yyyy")
                        .format(DateFormat("DD-MMM-yyyy").parse(e.date)),
                    // "week": DateFormat("EE")
                    //     .format(DateFormat("DD-MMM-yyyy").parse(e.date)),
                    "fajr": DateFormat.jm()
                        .format(DateFormat("HH:mm:ss").parse(e.fajr)),
                    "sunrise": DateFormat.jm()
                        .format(DateFormat("HH:mm:ss").parse(e.syuruk)),
                    "dhuhr": DateFormat.jm()
                        .format(DateFormat("HH:mm:ss").parse(e.dhuhr)),
                    "asr": DateFormat.jm()
                        .format(DateFormat("HH:mm:ss").parse(e.asr)),
                    "maghrib": DateFormat.jm()
                        .format(DateFormat("HH:mm:ss").parse(e.maghrib)),
                    "isha": DateFormat.jm()
                        .format(DateFormat("HH:mm:ss").parse(e.isha))
                  })
              .toList();
        } else {
          params["contents"] = null;
        }
      } catch (error) {
        params["contents"] = null;
      }
    } else {
      PrayerTimes prayerTimes = model.prayerTimes;
      if (prayerTimes == null) {
        Navigator.of(context).pop();
        if (mounted) showSnackBar(context, tr("location_content"));
        return;
      }
      print("_fetchTimeTable PrayerModel");
      params["contents"] = _addMonthContent();
    }
    if (params["contents"] == null) {
      Navigator.of(context).pop();
      if (mounted) showSnackBar(context, tr("network_error"));
      return;
    }
    params["content_key"] = [
      tr("Date"),
      tr("Fajr"),
      tr("Sunrise"),
      tr("Dhuhr"),
      tr("Asr"),
      tr("Maghrib"),
      tr("Isha"),
    ];

    Global.normalApi.fetchMonthTime(params).then((value) async {
      print("_fetchTimeTable");
      Navigator.of(context).pop();
      if (value.length != 0 && value != NEED_TIP && value != RELOGIN) {
        final String contentBase64 =
            base64Encode(const Utf8Encoder().convert(value));
        await _controller.loadUrl('data:text/html;base64,$contentBase64');
      } else {
        CommonResult result = CommonResult(errorCode: value);
        if (mounted) handleNetworkError(result, context);
      }
    }).catchError((error) {
      Navigator.of(context).pop();
      print("_fetchTimeTable error $error");
      setState(() {
        _isNetworkError = true;
      });
    });
  }

  dynamic _addMonthContent() {
    var content = [];
    PrayerModel model = Provider.of<PrayerModel>(context, listen: false);
    PrayerTimes prayerTimes = model.prayerTimes;
    for (var i = 0; i < 30; i++) {
      DateTime time = DateTime.fromMillisecondsSinceEpoch(
          _nowDate.millisecondsSinceEpoch + i * 24 * 60 * 60 * 1000);
      PrayerTimes nowTimes = PrayerTimes.utcOffset(
          prayerTimes.coordinates,
          DateComponents.from(time),
          prayerTimes.calculationParameters,
          Duration(hours: model.utcOffset));
      if (nowTimes != null) {
        var times = {
          "date": DateFormat("dd-MMM-yyyy").format(time),
          // "week": DateFormat("EE").format(time)
        };
        times["fajr"] = DateFormat.jm()
            .format(localChangeUTCtoLocal(nowTimes.timeForPrayer(Prayer.fajr)));
        times["sunrise"] = DateFormat.jm().format(
            localChangeUTCtoLocal(nowTimes.timeForPrayer(Prayer.sunrise)));
        times["dhuhr"] = DateFormat.jm().format(
            localChangeUTCtoLocal(nowTimes.timeForPrayer(Prayer.dhuhr)));
        times["asr"] = DateFormat.jm()
            .format(localChangeUTCtoLocal(nowTimes.timeForPrayer(Prayer.asr)));
        times["maghrib"] = DateFormat.jm().format(
            localChangeUTCtoLocal(nowTimes.timeForPrayer(Prayer.maghrib)));
        times["isha"] = DateFormat.jm()
            .format(localChangeUTCtoLocal(nowTimes.timeForPrayer(Prayer.isha)));
        content.add(times);
      }
    }
    return content;
  }
}
