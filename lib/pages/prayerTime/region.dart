import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyrefresh/easy_refresh.dart';
import 'package:muslim/common/api/error_network_handle.dart';
import 'package:muslim/common/provider/app.dart';
import 'package:muslim/common/core/storage.dart';
import 'package:muslim/common/tools/dialog.dart';
import 'package:muslim/common/tools/hexColor.dart';
import 'package:muslim/global.dart';
import 'package:muslim/modals/prayerzone.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:muslim/pages/widgets/empty.dart';
import 'package:muslim/pages/widgets/muslim_tile.dart';
import 'package:muslim/pages/widgets/muslim_title.dart';
import 'package:provider/provider.dart';

class RegionPage extends StatefulWidget {
  @override
  _RegionPageState createState() => _RegionPageState();
}

class _RegionPageState extends State<RegionPage> {
  List<MalaysiaPrayerZoneDatum> _tableData;
  bool _isFetchData = true;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    fetchRegions();
  }

  fetchRegions() async {
    try {
      MalaysiaPrayerZone regionResult =
          await Global.normalApi.fetchPrayerZones();
      setState(() {
        _isFetchData = false;
      });
      if (regionResult.errorCode == SUCCESS) {
        setState(() {
          _tableData = regionResult.data;
        });
      } else {
        if (mounted)
          handleNetworkError(regionResult, context,
              errorString: tr("network_error"));
      }
    } catch (error) {
      setState(() {
        _isFetchData = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    AppSettingModel model =
        Provider.of<AppSettingModel>(context, listen: false);
    print("regoin AppSettingModel ${model.malaysiaPlaceCode.toUpperCase()}");
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: MuslimTitle(
          title: tr("Region"),
        ),
      ),
      body: Builder(builder: (context) {
        if (_isFetchData) {
          return Center(
            child: SizedBox(
              width: 50.0,
              height: 50.0,
              child: CircularProgressIndicator(
                strokeWidth: 1.5,
                valueColor: AlwaysStoppedAnimation(HexColor("67C1BF")),
              ),
            ),
          );
        }
        if (_tableData == null) {
          return EmptyPage(
            noDatasTip: tr("no_network_tip"),
          );
        }

        return EasyRefresh.custom(slivers: [
          SliverList(
              delegate: SliverChildBuilderDelegate((context, index) {
            MalaysiaPrayerZoneDatum zone = _tableData[index];
            return Container(
              decoration: BoxDecoration(
                  border:
                      Border(bottom: BorderSide(color: HexColor("EAEAEA")))),
              child: ListTile(
                tileColor: Colors.white,
                title: Text(
                  "JAKIM - ${zone.zone} - ${zone.description}",
                  maxLines: 20,
                  overflow: TextOverflow.visible,
                ),
                trailing: zone.zone == model.malaysiaPlaceCode.toUpperCase()
                    ? Icon(Icons.done, color: HexColor("67C1BF"))
                    : null,
                onTap: () async {
                  if (zone.zone == model.malaysiaPlaceCode.toUpperCase()) {
                    return;
                  }
                  model.malaysiaPlaceCode = zone.zone;
                  model.region = zone.description;
                  var data = {"zone": zone.zone, "period": "year"};
                  showWaitingDialog(context, "");
                  try {
                    var malaysiaYearTime =
                        await Global.normalApi.fetchYearPrayerTimes(data);
                    Navigator.of(context).pop();
                    if (malaysiaYearTime.errorCode == SUCCESS) {
                      model.malaysiaPrayerTimes = malaysiaYearTime.prayerTime;
                      var params = model.appSetting.toJson();
                      params["locationCity"] = zone.description;
                      model.appSetting = AppPrayerTimeSetting.fromJson(params);
                      var values =
                          model.fetchDateListWithParams(DateTime.now(), 400);
                      if (values != null) {
                        Global.viewModel.sendLocaloNitification(values);
                      }
                      Navigator.of(context).pop();
                    } else {
                      if (mounted)
                        handleNetworkError(malaysiaYearTime, context);
                    }
                  } catch (error) {
                    // Navigator.of(context).pop();
                    Navigator.of(context).pop();
                  }
                },
              ),
            );
          }, childCount: _tableData.length))
        ]);
      }),
    );
  }
}
