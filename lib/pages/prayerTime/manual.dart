import 'package:adhan/adhan.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:muslim/common/provider/prayer.dart';
import 'package:muslim/common/tools/hexColor.dart';
import 'package:muslim/pages/widgets/backButton.dart';
import 'package:muslim/pages/widgets/horizontalLine.dart';
import 'package:muslim/pages/widgets/muslim_title.dart';
import 'package:provider/provider.dart';
import 'package:flutter_picker/Picker.dart';

class ManualCorrectionsPage extends StatefulWidget {
  @override
  _ManualCorrectionsState createState() => _ManualCorrectionsState();
}

class _ManualCorrectionsState extends State<ManualCorrectionsPage> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    PrayerTimes prayerTimes = Provider.of<PrayerModel>(context).prayerTimes;
    PrayerAdjustments prayerAdjustment = prayerTimes == null
        ? PrayerAdjustments(
            fajr: 0, sunrise: 0, dhuhr: 3, asr: 3, maghrib: 0, isha: 2)
        : prayerTimes.calculationParameters.adjustments;
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: MuslimTitle(title: tr("Manual Corrections")),
        leading: MuslimBackButton(),
      ),
      body: Padding(
        padding: EdgeInsets.only(top: 8.0),
        child: ListView.builder(
            physics: NeverScrollableScrollPhysics(),
            itemBuilder: (context, index) {
              var title = timeForPrayerToString(Prayer.values[index + 1]);
              var adjustMinute = 0;
              switch (index) {
                case 0:
                  adjustMinute = prayerAdjustment.fajr;
                  break;
                case 1:
                  adjustMinute = prayerAdjustment.sunrise;
                  break;
                case 2:
                  adjustMinute = prayerAdjustment.dhuhr;
                  break;
                case 3:
                  adjustMinute = prayerAdjustment.asr;
                  break;
                case 4:
                  adjustMinute = prayerAdjustment.maghrib;
                  break;
                case 5:
                  adjustMinute = prayerAdjustment.isha;
                  break;
              }
              return Container(
                decoration: BoxDecoration(
                    border: Border(
                        top: BorderSide(color: HexColor("EAEAEA")),
                        bottom: BorderSide(
                            color: index == 5
                                ? HexColor("EAEAEA")
                                : Colors.transparent))),
                child: ListTile(
                  tileColor: Colors.white,
                  title: Text(title),
                  trailing: Text("$adjustMinute minute"),
                  onTap: () {
                    _pickerPrayerAdjustmentsWithIndex(prayerAdjustment, index);
                  },
                ),
              );
            },
            itemCount: 6),
      ),
    );
  }

  void _pickerPrayerAdjustmentsWithIndex(
      PrayerAdjustments prayerAdjustment, int prayerAdjustmentIndex) {
    List<int> _pickerDatas = List(121)
        .asMap()
        .map((key, value) => MapEntry(key, -60 + key))
        .values
        .toList();
    int adjustMinute = 60;
    switch (prayerAdjustmentIndex) {
      case 0:
        adjustMinute = prayerAdjustment.fajr;
        break;
      case 1:
        adjustMinute = prayerAdjustment.sunrise;
        break;
      case 2:
        adjustMinute = prayerAdjustment.dhuhr;
        break;
      case 3:
        adjustMinute = prayerAdjustment.asr;
        break;
      case 4:
        adjustMinute = prayerAdjustment.maghrib;
        break;
      case 5:
        adjustMinute = prayerAdjustment.isha;
        break;
    }
    new Picker(
      cancelText: tr("cancelText"),
      confirmText: tr("confirmText"),
      // height: 280.0,
      itemExtent: 40.0,
      adapter: PickerDataAdapter(
          pickerdata: _pickerDatas.map((e) => "$e minute").toList()),
      selecteds: [_pickerDatas.indexOf(adjustMinute) ?? 60],
      onConfirm: (picker, selecteds) {
        int pickerAdapter = _pickerDatas[selecteds[0]];
        switch (prayerAdjustmentIndex) {
          case 0:
            prayerAdjustment.fajr = pickerAdapter;
            break;
          case 1:
            prayerAdjustment.sunrise = pickerAdapter;
            break;
          case 2:
            prayerAdjustment.dhuhr = pickerAdapter;
            break;
          case 3:
            prayerAdjustment.asr = pickerAdapter;
            break;
          case 4:
            prayerAdjustment.maghrib = pickerAdapter;
            break;
          case 5:
            prayerAdjustment.isha = pickerAdapter;
            break;
        }
        Provider.of<PrayerModel>(context, listen: false)
            .changeAdjustments(prayerAdjustment);
      },
    ).showModal(context);
  }
}
