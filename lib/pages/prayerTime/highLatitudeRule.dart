import 'package:adhan/adhan.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:muslim/common/provider/prayer.dart';
import 'package:muslim/common/tools/hexColor.dart';
import 'package:muslim/pages/widgets/backButton.dart';
import 'package:muslim/pages/widgets/horizontalLine.dart';
import 'package:muslim/pages/widgets/muslim_title.dart';
import 'package:provider/provider.dart';

class HighLatitudeRulePage extends StatefulWidget {
  @override
  _HighLatitudeRuleState createState() => _HighLatitudeRuleState();
}

class _HighLatitudeRuleState extends State<HighLatitudeRulePage> {
  List<String> _rules;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _rules = [
      tr("Middle of the night"),
      tr("1/7 of the night"),
      tr("Angle-based method")
    ];
  }

  @override
  Widget build(BuildContext context) {
    PrayerModel model = Provider.of<PrayerModel>(context);
    int ruleIndex = 2;
    ruleIndex = HighLatitudeRule.values.indexOf(model.highLatitudeRule);
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: MuslimTitle(title: tr("high_latitude_method_title")),
        leading: MuslimBackButton(),
      ),
      body: Padding(
        padding: EdgeInsets.only(top: 8.0),
        child: ListView.builder(
            itemBuilder: (context, index) {
              return Container(
                decoration: BoxDecoration(
                    border: Border(
                        top: BorderSide(color: HexColor("EAEAEA")),
                        bottom: BorderSide(
                            color: index == _rules.length - 1
                                ? HexColor("EAEAEA")
                                : Colors.transparent))),
                child: ListTile(
                  tileColor: Colors.white,
                  title: Text(
                    _rules[index],
                    style: TextStyle(
                        color:
                            HexColor(index == ruleIndex ? "67C1BF" : "212124")),
                  ),
                  trailing: index == ruleIndex
                      ? Icon(Icons.done, color: HexColor("67C1BF"))
                      : null,
                  onTap: () {
                    if (index == ruleIndex) return;
                    model
                        .changeHighLatitudeRule(HighLatitudeRule.values[index]);
                  },
                ),
              );
            },
            itemCount: _rules.length),
      ),
    );
  }
}
