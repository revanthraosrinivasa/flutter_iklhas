import 'package:adhan/adhan.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:matomo/matomo.dart';
import 'package:muslim/common/provider/app.dart';
import 'package:muslim/common/core/storage.dart';
import 'package:muslim/common/provider/prayer.dart';
import 'package:muslim/common/tools/hexColor.dart';
import 'package:muslim/pages/prayerTime/prayerList.dart';
import 'package:hijri/hijri_calendar.dart';
import 'package:muslim/pages/widgets/backButton.dart';
import 'package:muslim/pages/widgets/muslim_title.dart';
import 'package:provider/provider.dart';
import 'package:intl/intl.dart';

class PrayerTimesPage extends TraceableStatefulWidget {
  @override
  _PrayerTimesPageState createState() => _PrayerTimesPageState();
}

class _PrayerTimesPageState extends State<PrayerTimesPage> {
  String _normalDateString;
  String _hijriDateString;
  int _offset;
  int _isLeft;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _offset = 0;
    _normalDateString = "";
    _hijriDateString = "";
    _isLeft = 0;
    _changeNowDateTime();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    PrayerModel prayerModal = context.watch<PrayerModel>();
    AppSettingModel appSettingModel = context.watch<AppSettingModel>();
    Map<String, dynamic> convention = PRAYER_CONVENTIONS[
        prayerModal != null ? prayerModal.paryerConventionsIndex : 8];
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: MuslimTitle(title: tr("prayer_times_title")),
        leading: MuslimBackButton(),
        actions: [
          IconButton(
              icon: Icon(Icons.settings),
              onPressed: () {
                Navigator.of(context).pushNamed("prayer_time_setting");
              })
        ],
      ),
      body: SingleChildScrollView(
        reverse: true,
        child: Column(
          children: [
            Container(
              color: HexColor("67C1BF"),
              padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
              // width: MediaQuery.of(context).size.width,
              child: InkWell(
                  onTap: () {
                    print("inkwell tap");
                  },
                  highlightColor: Colors.white,
                  hoverColor: Colors.white,
                  focusColor: Colors.white,
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Icon(
                        Icons.room,
                        color: Colors.white,
                      ),
                      Container(
                        width: 10.0,
                      ),
                      Expanded(
                          child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            prayerModal.locationCity ?? tr("positioning"),
                            maxLines: 10,
                            // overflow: TextOverflow.ellipsis,
                            style: TextStyle(color: Colors.white),
                          ),
                          Container(
                            height: 4,
                          ),
                          Text(
                            appSettingModel != null &&
                                    appSettingModel.appSetting != null &&
                                    appSettingModel.appSetting.autoSetting &&
                                    appSettingModel.appSetting.userAutoSetting
                                ? "JAKIM - " +
                                    appSettingModel.malaysiaPlaceCode
                                        .toUpperCase() +
                                    " - " +
                                    appSettingModel.appSetting.locationCity
                                : convention["name"],
                            maxLines: 10,
                            // overflow: TextOverflow.ellipsis,
                            style: TextStyle(color: Colors.white),
                          )
                        ],
                      ))
                    ],
                  )),
            ),
            Container(
              height: 72,
              margin: EdgeInsets.symmetric(vertical: 8.0),
              decoration: BoxDecoration(
                  border: Border(bottom: BorderSide(color: HexColor("EAEAEA"))),
                  color: Colors.white),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    child: IconButton(
                        icon: Icon(Icons.navigate_before),
                        onPressed: () {
                          _changeNowDateTime(offset: -1);
                        }),
                    alignment: Alignment.centerRight,
                  ),
                  InkWell(
                    child: Column(
                      children: [
                        Expanded(
                            flex: 1,
                            child: Container(
                              child: Text(
                                _normalDateString,
                                style: TextStyle(
                                    color: HexColor("212124"),
                                    fontSize: 16.0,
                                    fontWeight: FontWeight.w500),
                              ),
                              alignment: Alignment.bottomCenter,
                            )),
                        Expanded(
                            flex: 1,
                            child: Container(
                              child: Text(_hijriDateString,
                                  style: TextStyle(
                                      color: HexColor("4C4C50"),
                                      fontSize: 12.0,
                                      fontWeight: FontWeight.w400)),
                              alignment: Alignment.topCenter,
                            )),
                      ],
                    ),
                  ),
                  Container(
                    child: IconButton(
                        icon: Icon(
                          Icons.navigate_next,
                          color: HexColor("75767A"),
                        ),
                        onPressed: () {
                          _changeNowDateTime(offset: 1);
                        }),
                    alignment: Alignment.centerLeft,
                  ),
                ],
              ),
            ),
            PrayerListWidget(
                offset: _offset,
                isLeft: _isLeft,
                appSetting: appSettingModel.appSetting),
            Container(
              margin: EdgeInsets.only(top: 24.0),
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.symmetric(horizontal: 16.0),
              height: 44.0,
              child: RaisedButton(
                color: HexColor("67C1BF"),
                onPressed: () {
                  MatomoTracker.trackEvent(
                      "Monthly Prayer Timetable", "Monthly Prayer Timetable");
                  Navigator.of(context).pushNamed("prayer_timer");
                },
                child: Text(tr("Monthly Prayer Timetable")),
                textTheme: ButtonTextTheme.primary,
                textColor: Colors.white,
              ),
            )
          ],
        ),
      ),
    );
  }

  void _changeNowDateTime({int offset = 0}) {
    var dateTime = DateTime.now().millisecondsSinceEpoch +
        (offset + _offset) * 24 * 3600 * 1000;
    DateTime calculateDateTime = DateTime.fromMillisecondsSinceEpoch(dateTime);

    String nowTimeString = DateFormat("EEEE, MMM d").format(calculateDateTime);
    print("_changeNowDateTime $nowTimeString" + "$calculateDateTime");

    HijriCalendar hijri = HijriCalendar.fromDate(calculateDateTime);
    String hijriDateString =
        "${hijri.hDay} ${hijriList[hijri.hMonth - 1]} ${hijri.hYear} H";
    setState(() {
      _normalDateString = nowTimeString;
      _hijriDateString = hijriDateString;
      _offset = _offset + offset;
      _isLeft = offset;
    });
  }
}
