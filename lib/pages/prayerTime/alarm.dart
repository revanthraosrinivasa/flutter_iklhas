import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter_picker/flutter_picker.dart';
import 'package:muslim/common/provider/app.dart';
import 'package:muslim/common/provider/prayer.dart';
import 'package:muslim/common/tools/hexColor.dart';
import 'package:muslim/common/provider/user.dart';
import 'package:muslim/global.dart';
import 'package:muslim/modals/times.dart';
import 'package:muslim/pages/widgets/horizontalLine.dart';
import 'package:muslim/pages/widgets/muslim_title.dart';
import 'package:provider/provider.dart';

class AlarmClockPage extends StatefulWidget {
  AlarmClockPage({Key key, this.prayerIndex, this.prayerTimes})
      : super(key: key);

  final int prayerIndex;
  final PrayerTimeResult prayerTimes;

  @override
  State<StatefulWidget> createState() => _AlarmClockState();
}

class _AlarmClockState extends State<AlarmClockPage> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    UserModel userModal = context.watch<UserModel>();
    List<bool> alarmClocks = userModal.alarmClock;
    List<int> alarmClocksTimes = userModal.alarmAdvanceOfTime;
    List<int> selectes = [0, 5, 10, 15, 20, 25, 30];

    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: MuslimTitle(title: tr("Maghrib Alarm")),
        ),
        body: Padding(
          padding: EdgeInsets.only(top: 8.0),
          child: Column(children: [
            ListTile(
              tileColor: Colors.white,
              title: Text("enable_alarm").tr(),
              trailing: CupertinoSwitch(
                value: alarmClocks[widget.prayerIndex] ?? false,
                onChanged: (value) {
                  userModal.changeAlarmClock(widget.prayerIndex, value);
                  sendNotificationWithChange();
                },
              ),
            ),
            HorizontalLine(),
            InkWell(
              onTap: () {
                new Picker(
                    adapter: PickerDataAdapter(
                      pickerdata:
                          selectes.map((e) => "$e " + tr("Minutes")).toList(),
                    ),
                    cancelText: tr("cancelText"),
                    confirmText: tr("confirmText"),
                    // height: 280.0,
                    itemExtent: 40.0,
                    selecteds: [
                      selectes.indexOf(alarmClocksTimes[widget.prayerIndex])
                    ],
                    onConfirm: (picker, values) {
                      userModal.changeAlarmAdvanceOfTime(
                          widget.prayerIndex, selectes[values[0]]);
                      sendNotificationWithChange();
                    }).showModal(context);
              },
              child: Container(
                height: 50.0,
                color: Colors.white,
                padding: EdgeInsets.symmetric(horizontal: 16.0),
                child: Row(
                  children: [
                    Text(
                      "alarm_time",
                      style: TextStyle(fontSize: 16.0),
                    ).tr(),
                    Expanded(
                        child: Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text("alarm_before_maghrib").tr(args: [
                            alarmClocksTimes[widget.prayerIndex].toString()
                          ]),
                          Icon(
                            Icons.navigate_next,
                            color: HexColor("75767A"),
                          )
                        ],
                      ),
                    ))
                  ],
                ),
              ),
            ),
            HorizontalLine()
          ]),
        ));
  }

  sendNotificationWithChange() {
    AppSettingModel appSetting =
        Provider.of<AppSettingModel>(context, listen: false);
    if (appSetting.appSetting != null &&
        appSetting.appSetting.autoSetting == true &&
        appSetting.appSetting.userAutoSetting == true) {
      var value = appSetting.fetchDateListWithParams(DateTime.now(), 400);
      if (value != null) {
        Global.viewModel.sendLocaloNitification(value);
      }
    } else {
      PrayerModel prayerModel =
          Provider.of<PrayerModel>(context, listen: false);
      Global.viewModel.sendLocaloNitification(prayerModel.prayerTimesList);
    }
  }
}
