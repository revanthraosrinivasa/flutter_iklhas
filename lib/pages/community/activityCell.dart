import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:muslim/common/core/storage.dart';
import 'package:muslim/common/tools/dialog.dart';
import 'package:muslim/common/tools/hexColor.dart';
import 'package:muslim/modals/activity.dart';
import 'package:easy_localization/easy_localization.dart';

class ActivityCell extends StatelessWidget {
  ActivityCell({Key key, this.activityInfo, this.previewUrl}) : super(key: key);

  final ActivitiesDatum activityInfo;
  String previewUrl;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      padding: EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 0),
      decoration: BoxDecoration(
          border: Border(bottom: BorderSide(color: HexColor("EAEAEA"))),
          color: Colors.white),
      child: InkWell(
        onTap: () async {
          Navigator.of(context).pushNamed("web_view", arguments: {
            "url": activityInfo.link,
            "title": activityInfo.activityName
          });
        },
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            activityInfo.photos.length == 0
                ? Container()
                : CachedNetworkImage(
                    imageUrl: activityInfo.photos[0].url + OSS_IMAGE_SCALE,
                    height: 128.0,
                    width: MediaQuery.of(context).size.width - 32,
                    fit: BoxFit.cover,
                    placeholder: (context, str) {
                      return Container(
                        color: HexColor("#F2F3F6"),
                      );
                    },
                    errorWidget: (context, str, value) {
                      if (!checkNetworkState(context))
                        return Container(color: HexColor("#F2F3F6"));
                      return Image.network(
                          activityInfo.photos[0].url + OSS_IMAGE_SCALE,
                          fit: BoxFit.cover);
                    },
                  ),
            Container(
              height: 10,
            ),
            Text(
              activityInfo.activityName,
              style: TextStyle(
                  color: HexColor("212124"),
                  fontSize: 16.0,
                  fontWeight: FontWeight.w500),
            ),
            Container(
              margin: EdgeInsets.only(top: 8.0, bottom: 8.0),
              child: Text(
                activityInfo.introduction,
                style: TextStyle(
                    fontSize: 14.0,
                    fontWeight: FontWeight.w400,
                    color: HexColor("212124")),
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
              ),
            ),
            Container(
              padding: EdgeInsets.only(bottom: 16.0),
              child: FlatButton(
                onPressed: () {},
                child: Text(tr("fundraising")),
                textColor: HexColor("C92D21"),
                color: HexColor("FBEBE9"),
                padding:
                    EdgeInsets.only(top: 4.0, bottom: 4.0, left: 8, right: 8.0),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(40.0))),
              ),
            )
          ],
        ),
      ),
    );
  }
}
