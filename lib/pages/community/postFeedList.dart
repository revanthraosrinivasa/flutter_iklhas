import 'package:flutter/material.dart';
import 'package:muslim/common/tools/hexColor.dart';
import 'package:muslim/modals/recommends.dart';
import 'package:muslim/pages/community/postFeedCell.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:muslim/pages/widgets/empty.dart';

class PostFeedList extends StatelessWidget {
  PostFeedList({Key key, this.infos, this.callBack}) : super(key: key);

  final List<Datum> infos;
  final VoidCallback callBack;

  @override
  Widget build(BuildContext context) {
    List<Widget> _listWidget = [];
    if (infos != null) {
      if (infos.length != 0) {
        //   _listWidget.add(EmptyPage(
        //     noDatasTip: "No Feeds at the moment",
        //   ));
        // } else {
        _listWidget = [
          InkWell(
            onTap: () {
              callBack();
            },
            child: Container(
              padding: EdgeInsets.only(
                  top: 26.0, left: 16.0, right: 16.0, bottom: 8.0),
              decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border(top: BorderSide(color: HexColor("EAEAEA")))),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text("post_feed_title",
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 20.0,
                              color: HexColor("333333")))
                      .tr(),
                  Container(
                    width: 8,
                  ),
                  Expanded(child: Container()),
                  Icon(
                    Icons.navigate_next,
                    color: HexColor("75767A"),
                  ),
                ],
              ),
            ),
          )
        ];

        _listWidget.addAll((infos.length > 2 ? infos.getRange(0, 2) : infos)
            .map((e) => PostFeedCell(info: e) as Widget));

        if (infos.length > 2) {
          _listWidget.addAll([
            ListTile(
              tileColor: Colors.white,
              title: Text(
                tr("see_more_updates"),
                style: TextStyle(
                  color: HexColor("67C1BF"),
                  fontSize: 16.0,
                ),
                textAlign: TextAlign.left,
              ),
              onTap: () {
                callBack();
              },
            ),
          ]);
        }
        _listWidget.add(Container(
          height: 20.0,
          decoration: BoxDecoration(
              border: Border(top: BorderSide(color: HexColor("EAEAEA")))),
        ));
      }
    }

    // TODO: implement build
    return SliverList(
        delegate: SliverChildListDelegate(_listWidget,
            addAutomaticKeepAlives: false, addRepaintBoundaries: false));
  }
}
