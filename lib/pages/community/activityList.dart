import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:muslim/common/tools/hexColor.dart';
import 'package:muslim/modals/activity.dart';
import 'package:muslim/pages/community/activityCell.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:muslim/pages/widgets/empty.dart';

class ActivityList extends StatefulWidget {
  ActivityList({Key key, this.activitiesInfo, this.callBack}) : super(key: key);

  final List<ActivitiesDatum> activitiesInfo;
  final VoidCallback callBack;

  @override
  _ActivityListState createState() => _ActivityListState();
}

class _ActivityListState extends State<ActivityList> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final _tableData = widget.activitiesInfo;
    List<Widget> _listWidget = [];
    if (_tableData != null) {
      if (_tableData.length != 0) {
        //   _listWidget.add(EmptyPage(
        //     noDatasTip: "No updates at the moment",
        //   ));
        // } else {
        bool _hasMore;
        if (_tableData.length > 2) {
          _hasMore = true;
        } else {
          _hasMore = false;
        }
        _listWidget = (_hasMore ? _tableData.getRange(0, 2) : _tableData)
            .map((e) => ActivityCell(activityInfo: e))
            .map((e) => e as Widget)
            .toList();
        if (_hasMore) {
          _listWidget.add(Container(
            padding: EdgeInsets.all(16.0),
            decoration: BoxDecoration(
                border: Border(bottom: BorderSide(color: HexColor("EAEAEA"))),
                color: Colors.white),
            child: GestureDetector(
                onTap: () {
                  widget.callBack();
                },
                child: Text("activity_more",
                        style: TextStyle(
                            color: HexColor("67C1BF"),
                            fontSize: 16.0,
                            fontWeight: FontWeight.bold))
                    .tr()),
          ));
        }
        _listWidget.insert(
            0,
            InkWell(
              onTap: () {
                widget.callBack();
              },
              child: Container(
                color: Colors.white,
                padding: EdgeInsets.fromLTRB(16.0, 26.0, 16.0, 6.0),
                child: Text(
                  'activity_title',
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20.0,
                      color: HexColor("333333")),
                ).tr(),
              ),
            ));
      }
    }
    // TODO: implement build
    return SliverList(delegate: SliverChildListDelegate(_listWidget));
  }
}
