import 'package:flutter/material.dart';
import 'package:matomo/matomo.dart';
import 'package:muslim/common/tools/hexColor.dart';
import 'package:muslim/modals/merchant.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:easy_localization/easy_localization.dart';

class DetailsServicesList extends StatelessWidget {
  DetailsServicesList({Key key, this.services}) : super(key: key);

  final List<DetailService> services;
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return SliverList(
        delegate: SliverChildListDelegate([
      services == null || services.length == 0
          ? Container()
          : Container(
              color: Colors.white,
              padding: EdgeInsets.all(16.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(tr("services"),
                      style: TextStyle(
                          fontSize: 20.0,
                          fontWeight: FontWeight.w500,
                          color: HexColor("333333"))),
                  Wrap(
                    spacing: 10.0,
                    children: services
                        .map((e) => InkWell(
                              onTap: () {
                                if (e.contact != null ||
                                    e.contact.length != 0) {
                                  _showServiceContactInfo(
                                      context, services.indexOf(e));
                                }
                              },
                              child: Chip(
                                  label: Container(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 8.0, vertical: 4.0),
                                child: Text(
                                  e.name,
                                  style: TextStyle(fontSize: 16.0),
                                ).tr(),
                              )),
                            ))
                        .toList(),
                  )
                ],
              )),
    ], addAutomaticKeepAlives: false, addRepaintBoundaries: false));
  }

  _showServiceContactInfo(BuildContext context, int index) {
    DetailService detailService = services[index];
    MatomoTracker.trackEvent("detail-service-$index", "detail-service-$index");
    showModalBottomSheet(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(4.0), topRight: Radius.circular(4.0))),
        clipBehavior: Clip.antiAlias,
        context: context,
        builder: (context) {
          return Container(
            height: 136.0,
            child: Column(
              children: [
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 16.0),
                  decoration: BoxDecoration(
                      border: Border(
                          bottom: BorderSide(color: HexColor("EAEAEA")))),
                  height: 56.0,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                          child: Text(
                        tr(detailService.name),
                        style: TextStyle(
                            color: HexColor("212124"),
                            fontSize: 16.0,
                            fontWeight: FontWeight.w500),
                      )),
                      IconButton(
                          icon: Icon(Icons.clear),
                          onPressed: () {
                            Navigator.of(context).pop();
                          })
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 16.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        "call",
                        style: TextStyle(
                            fontWeight: FontWeight.w500,
                            fontSize: 16.0,
                            color: HexColor("212124")),
                      ).tr(),
                      Container(
                        width: 8.0,
                      ),
                      Expanded(
                        child: Text(detailService.contact,
                            style: TextStyle(
                                color: HexColor("4C4C50"), fontSize: 14.0)),
                      ),
                      FlatButton(
                        onPressed: () async {
                          print("servce phone number : ${detailService.phone}");
                          String phoneNumber = detailService.phone;
                          if (phoneNumber.contains(" ")) {
                            phoneNumber = phoneNumber.replaceAll(" ", "");
                          }
                          if (await canLaunch("tel:$phoneNumber")) {
                            await launch("tel:$phoneNumber");
                          } else {
                            print("cant call phone number $phoneNumber");
                          }
                        },
                        child: Text("call").tr(),
                        textColor: Colors.white,
                        color: HexColor("67C1BF"),
                      )
                    ],
                  ),
                )
              ],
            ),
          );
        });
  }
}
