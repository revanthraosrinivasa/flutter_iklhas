import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:muslim/common/core/storage.dart';
import 'package:muslim/common/tools/dialog.dart';
import 'package:muslim/common/tools/hexColor.dart';
import 'package:muslim/modals/merchant.dart';
import 'package:muslim/pages/widgets/muslim_title.dart';
import 'package:easy_localization/easy_localization.dart';

class GalleryPage extends StatefulWidget {
  GalleryPage({Key key, @required this.photos}) : super(key: key);
  final List<Photo> photos;
  _GalleryPageState createState() => _GalleryPageState();
}

class _GalleryPageState extends State<GalleryPage> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: MuslimTitle(title: tr("gallery")),
        ),
        body: GridView.builder(
            padding: EdgeInsets.all(16.0),
            itemCount: widget.photos.length,
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2, crossAxisSpacing: 8.0, mainAxisSpacing: 8.0),
            itemBuilder: (context, index) {
              Photo photo = widget.photos[index];
              return InkWell(
                onTap: () {
                  showImagesShadow(
                      context, widget.photos.map((e) => e.url).toList(),
                      currentPage: index);
                },
                child: Container(
                  child: CachedNetworkImage(
                    imageUrl: photo.url + OSS_IMAGE_SCALE,
                    fit: BoxFit.cover,
                    placeholder: (context, url) => Container(
                      color: HexColor("F2F3F6"),
                      // child: Center(
                      //   child: Icon(
                      //     Icons.image_search,
                      //     color: HexColor("#9B9B9D"),
                      //   ),
                      // ),
                    ),
                    errorWidget: (context, str, value) {
                      if (!checkNetworkState(context))
                        return Container(color: HexColor("#F2F3F6"));
                      return Image.network(photo.url + OSS_IMAGE_SCALE,
                          fit: BoxFit.cover);
                    },
                  ),
                ),
              );
            }));
  }
}
