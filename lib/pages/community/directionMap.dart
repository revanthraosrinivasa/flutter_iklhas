import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:muslim/common/api/google_map.dart';
import 'package:muslim/common/core/storage.dart';
import 'package:muslim/common/helper/helper.dart';
import 'package:muslim/common/tools/hexColor.dart';
import 'package:muslim/global.dart';
import 'package:muslim/modals/directions.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:muslim/pages/widgets/backButton.dart';
import 'package:muslim/pages/widgets/muslim_title.dart';
import 'package:flutter/services.dart' show rootBundle;

class DirectionMap extends StatefulWidget {
  DirectionMap(
      {Key key,
      this.endLocationLat,
      this.endLocationLon,
      this.endLocationTitle})
      : super(key: key);

  final String endLocationTitle;
  final double endLocationLat;
  final double endLocationLon;

  _DirectionMapState createState() => _DirectionMapState();
}

class _DirectionMapState extends State<DirectionMap> {
  Completer<GoogleMapController> _controller = Completer();
  Geolocator _geolocator = Geolocator();

  LatLng _endLatLng;
  LatLng _currentLatLng;

  Set<Polyline> _polylines = Set();
  Set<Marker> _markers = Set();
  TextEditingController _startController =
      TextEditingController(text: "Now location");
  TextEditingController _endController;
  bool _isFetchPosition = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _endLatLng = LatLng(widget.endLocationLat, widget.endLocationLon);
    if (StorageHelper().getJSON(POSITION) != null) {
      Position position = Position.fromMap(StorageHelper().getJSON(POSITION));
      _currentLatLng = LatLng(position.latitude, position.longitude);
      fetchPolyLines(_currentLatLng);
    } else {
      _isFetchPosition = true;
      _geolocatorLocation();
    }
    _endController = TextEditingController(text: widget.endLocationTitle);
    _markers =
        [Marker(markerId: MarkerId("endMarker"), position: _endLatLng)].toSet();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: MuslimTitle(title: tr("direction")),
        leading: MuslimBackButton(),
      ),
      body: _currentLatLng == null && _isFetchPosition
          ? Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                CircularProgressIndicator(),
                Container(
                  height: 10,
                ),
                Text(tr("positioning")),
              ],
            )
          : Stack(
              children: [
                Positioned.fill(
                    child: GoogleMap(
                        mapType: MapType.normal,
                        initialCameraPosition: CameraPosition(
                            target: _currentLatLng != null
                                ? _currentLatLng
                                : LatLng(widget.endLocationLat,
                                    widget.endLocationLon),
                            zoom: 18.0),
                        onMapCreated: (controller) {
                          _controller.complete(controller);
                        },
                        markers: _markers,
                        myLocationEnabled: true,
                        polylines: _polylines)),
                Positioned(
                    top: 8.0,
                    height: 100,
                    left: 16,
                    right: 16,
                    child: Container(
                      padding: EdgeInsets.symmetric(horizontal: 12.0),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.all(Radius.circular(4.0))),
                      child: Column(
                        children: [
                          Row(
                            children: [
                              Container(
                                width: 60,
                                alignment: Alignment.centerRight,
                                child: Text(tr("start"),
                                    style: TextStyle(
                                        fontSize: 16.0,
                                        fontWeight: FontWeight.w500)),
                              ),
                              Container(
                                width: 20.0,
                              ),
                              Expanded(
                                  child: TextField(
                                style: TextStyle(fontSize: 12.0),
                                enabled: false,
                                controller: _startController,
                                cursorColor: HexColor("67C1BF"),
                              ))
                            ],
                          ),
                          Row(
                            children: [
                              Container(
                                width: 60,
                                alignment: Alignment.centerRight,
                                child: Text(
                                  tr("end"),
                                  style: TextStyle(
                                      fontSize: 16.0,
                                      fontWeight: FontWeight.w500),
                                ),
                              ),
                              Container(
                                width: 20.0,
                              ),
                              Expanded(
                                  child: TextField(
                                      style: TextStyle(fontSize: 12.0),
                                      enabled: false,
                                      controller: _endController,
                                      cursorColor: HexColor("FB7268")))
                            ],
                          )
                        ],
                      ),
                    ))
              ],
            ),
    );
  }

  Future<void> fetchPolyLines(LatLng currentLatLng) async {
    var params = {
      "origin": "${currentLatLng.latitude},${currentLatLng.longitude}",
      "destination": "${widget.endLocationLat},${widget.endLocationLon}"
    };
    try {
      Directions results = await GoogleApiService().fetchDirections(params);
      GoogleMapController controller = await _controller.future;
      controller.animateCamera(CameraUpdate.newLatLngBounds(
          LatLngBounds(
              southwest: LatLng(results.routes[0].bounds.southwest.lat,
                  results.routes[0].bounds.southwest.lng),
              northeast: LatLng(results.routes[0].bounds.northeast.lat,
                  results.routes[0].bounds.northeast.lng)),
          150));

      List<PointLatLng> values = results.routes.map((e) {
        return _getPolyline(e.overviewPolyline.points);
      }).toList()[0];
      Leg value = results.routes[0].legs[0];

      var iconBytes = await rootBundle.load("assets/images/location.png");
      var audioUint8List = iconBytes.buffer
          .asUint8List(iconBytes.offsetInBytes, iconBytes.lengthInBytes);
      Marker startMarker = Marker(
          // icon: icon,
          anchor: Offset(0.5, 0.5),
          icon: BitmapDescriptor.fromBytes(audioUint8List),
          markerId: MarkerId("start"),
          position: LatLng(value.startLocation.lat, value.startLocation.lng));

      // Marker startMarker = Marker(
      //     infoWindow: InfoWindow(title: "Start"),
      //     markerId: MarkerId(value.startAddress),
      //     position: LatLng(value.startLocation.lat, value.startLocation.lng));
      Marker endMarker = Marker(
          infoWindow: InfoWindow(title: "End:" + widget.endLocationTitle),
          markerId: MarkerId(value.endAddress),
          position: LatLng(value.endLocation.lat, value.endLocation.lng));

      setState(() {
        _polylines = Set.of([
          Polyline(
              polylineId: PolylineId("1"),
              points: values.map((e) {
                return LatLng(e.latitude, e.longitude);
              }).toList(),
              color: Colors.blue,
              width: 6,
              startCap: Cap.roundCap,
              endCap: Cap.roundCap)
        ]);
        _markers = Set.of([startMarker, endMarker]);
      });
    } catch (error) {
      print("Error $error");
    }
  }

  _geolocatorLocation() async {
    Map<String, dynamic> positionValue;
    try {
      positionValue = await Global.locationHelper.fetchLocation(context);
    } catch (error) {
      positionValue = error;
    }
    if (positionValue == null) {
      return;
    }
    if (positionValue["msg"] != null) {
      Position position = Position(
          latitude: positionValue["msg"]["latitude"],
          longitude: positionValue["msg"]["longitude"]);
      _fecthLocalLocationInfo(position);
      setState(() {
        _currentLatLng = LatLng(position.latitude, position.longitude);
      });
      GoogleMapController controller = await _controller.future;
      controller.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
          target: LatLng(position.latitude, position.longitude), zoom: 18)));
      fetchPolyLines(LatLng(position.latitude, position.longitude));
    }
  }

  _fecthLocalLocationInfo(Position value) {
    var params = {"latlng": "${value.latitude},${value.longitude}"};
    GoogleApiService().fetchLocalCity(params, type: 1).then((value) {
      if (value != null || value.length != 0) {
        _startController.text = value[0].formattedAddress;
      }
    });
  }

  // 反编译路线信息
  List<PointLatLng> _getPolyline(String polyline) {
    PolylinePoints polylinePoints = PolylinePoints();
    return polylinePoints.decodePolyline(polyline);
  }
}
