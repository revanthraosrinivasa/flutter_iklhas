import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyrefresh/easy_refresh.dart';
import 'package:geolocator/geolocator.dart';
import 'package:location_permissions/location_permissions.dart';
import 'package:matomo/matomo.dart';
import 'package:muslim/common/api/error_network_handle.dart';
import 'package:muslim/common/core/storage.dart';
import 'package:muslim/common/tools/dialog.dart';
import 'package:muslim/common/tools/hexColor.dart';
import 'package:muslim/common/tools/snackBar.dart';
import 'package:muslim/global.dart';
import 'package:muslim/modals/nearlyMerchant.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:muslim/pages/widgets/backButton.dart';
import 'package:muslim/pages/widgets/base_page.dart';
import 'package:muslim/pages/widgets/base_widget.dart';
import 'package:muslim/pages/widgets/empty.dart';
import 'package:muslim/pages/widgets/muslim_title.dart';

class NearlyMerchantsPage extends TraceableStatefulWidget {
  NearlyMerchantsPage({Key key, this.position}) : super(key: key);
  final Position position;
  @override
  _NearlyMerchantsPageState createState() => _NearlyMerchantsPageState();
}

class _NearlyMerchantsPageState extends State<NearlyMerchantsPage> {
  EasyRefreshController _controller = EasyRefreshController();
  Position _position;
  List<NearlyMerchantsDatum> _merchants;
  String cursor = "";
  bool _isNetworkError = true;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _position = widget.position;
    // _fetchNearlyMosque();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MuslimBasePage(
      appBar: AppBar(
        centerTitle: true,
        title: MuslimTitle(title: tr("nearly_mosque")),
        leading: MuslimBackButton(),
      ),
      header: baseHeader(),
      footer: baseFooter(),
      controller: _controller,
      onLoad: _onLoad,
      onRefresh: _onRefresh,
      firstRefresh: true,
      firstRefreshWidget: true,
      emptyWidget: _isNetworkError && _merchants == null
          ? EmptyPage(noDatasTip: tr("no_network_tip"))
          : null,
      slivers: [
        SliverList(
            delegate: SliverChildBuilderDelegate((context, index) {
          return _listChildTile(index);
        }, childCount: _merchants != null ? _merchants.length : 0))
      ],
    );
  }

  Widget _listChildTile(int index) {
    NearlyMerchantsDatum infos = _merchants[index];
    return Card(
      child: ListTile(
        leading: InkWell(
          onTap: () {
            Navigator.of(context)
                .pushNamed("store_detail", arguments: infos.id);
          },
          child: CircleAvatar(
            radius: 20.0,
            backgroundImage: CachedNetworkImageProvider(
                infos.previewPhoto != null && infos.previewPhoto.url.length != 0
                    ? infos.previewPhoto.url + OSS_IMAGE_SCALE
                    : ""),
          ),
        ),
        title: InkWell(
          onTap: () {
            Navigator.of(context)
                .pushNamed("store_detail", arguments: infos.id);
          },
          child: Text(infos.name),
        ),
        subtitle: InkWell(
          onTap: () {
            Navigator.of(context)
                .pushNamed("store_detail", arguments: infos.id);
          },
          child: Text((infos.distance / 1000).toStringAsFixed(2) + "km"),
        ),
        trailing: RaisedButton(
          onPressed: infos.type == 0
              ? null
              : () {
                  if (infos.followed) {
                    _unfollowMerchant(infos.id, infos.name);
                  } else {
                    _followMerchant(infos.id);
                  }
                },
          child: Text(
            infos.followed || infos.type == 0 ? tr("following") : tr("follow"),
            style: TextStyle(
                color: infos.type == 0
                    ? HexColor("67C1BF").withOpacity(0.5)
                    : HexColor("67C1BF"),
                fontSize: 16.0),
          ),
          highlightColor: Colors.white,
          color: Colors.transparent,
          disabledColor: Colors.white,
          elevation: 0.0,
        ),
        shape: RoundedRectangleBorder(side: BorderSide()),
      ),
    );
  }

  _fetchNearlyMosque({int type = 0}) async {
    setState(() {
      _isNetworkError = false;
    });
    if (_position == null) {
      try {
        Map<String, dynamic> positionValue =
            await Global.locationHelper.fetchLocation(context);
        if (positionValue == null) {
          if (type == 0) {
            _controller.finishRefresh(success: false);
          } else {
            _controller.finishLoad(success: false);
          }
          return;
        }
        if (positionValue["msg"] != null) {
          Position position = Position(
              latitude: positionValue["msg"]["latitude"],
              longitude: positionValue["msg"]["longitude"]);
          setState(() {
            _position = position;
          });
        }
      } catch (error) {
        if (type == 0) {
          _controller.finishRefresh(success: false);
        } else {
          _controller.finishLoad(success: false);
        }
      }
    }

    Global.normalApi.fetchNearbyMerchantsListWithLatLng(
        _position.latitude,
        _position.longitude,
        {"limit": "$LIMIT", "cursor": "$cursor"}).then((value) {
      print("fetchNearbyMerchantsListWithLatLng: ${value.data}");
      if (value != null && value.errorCode == SUCCESS) {
        if (type == 0) {
          setState(() {
            _merchants = value.data;
          });
          _controller.finishRefresh(success: true);
          if (_controller.finishLoadCallBack != null)
            _controller.finishLoadCallBack(noMore: value.data.length < LIMIT);
        } else {
          var oldData = _merchants;
          oldData.addAll(value.data);
          setState(() {
            _merchants = oldData;
          });
          _controller.finishLoad(
              success: true, noMore: value.data.length != 20);
        }
      } else {
        if (type == 0) {
          _controller.finishRefresh(success: false);
        } else {
          _controller.finishLoad(success: false);
        }
        if (mounted) handleNetworkError(value, context);
      }
    }).catchError((error) {
      if (type == 0) {
        _controller.finishRefresh(success: false);
      } else {
        _controller.finishLoad(success: false);
      }
      setState(() {
        _isNetworkError = true;
      });
      if (_merchants != null && mounted) {
        showSnackBar(context, tr("network_error"));
      }
    });
  }

  _followMerchant(String id) async {
    showWaitingDialog(context, "");
    Global.normalApi.followMerchant(id).then((value) {
      Navigator.of(context).pop();
      print("_followMerchant success");
      if (value != null && value.errorCode == SUCCESS) {
        _onRefresh();
      } else if (value != null) {
        if (mounted) handleNetworkError(value, context);
      } else {
        if (mounted)
          showSnackBar(context, value.developerMsg ?? tr("network_error"),
              type: 1);
      }
    }).catchError((error) {
      if (mounted) showSnackBar(context, tr("network_error"), type: 1);
      Navigator.of(context).pop();
      print("_followMerchant :$error");
    });
  }

  _unfollowMerchant(String id, String name) async {
    showEnsureDialog(
        context, tr("unfollow_title"), tr("unfollow_tip", args: [name]),
        () async {
      showWaitingDialog(context, "");
      Global.normalApi.unfollowMerchant(id).then((value) {
        Navigator.of(context).pop();
        print("_unfollowMerchant success");
        if (value != null && value.errorCode == SUCCESS) {
          _onRefresh();
        } else if (value != null) {
          if (mounted) handleNetworkError(value, context);
        } else {
          if (mounted)
            showSnackBar(context, value.developerMsg ?? tr("network_error"),
                type: 1);
        }
      }).catchError((error) {
        Navigator.of(context).pop();
        if (mounted) showSnackBar(context, tr("network_error"), type: 1);
        print("_unfollowMerchant :$error");
      });
    }, ensureTitle: tr("confirmText"));
  }

  Future<void> _onLoad() async {
    setState(() {
      cursor = _merchants == null || _merchants.length == 0
          ? ""
          : _merchants.last.row;
    });
    _fetchNearlyMosque(type: 1);
  }

  Future<void> _onRefresh() async {
    setState(() {
      cursor = "";
    });
    _controller.resetLoadState();
    _fetchNearlyMosque();
  }
}
