import 'dart:async';
import 'dart:math' as math;
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyrefresh/easy_refresh.dart';
import 'package:matomo/matomo.dart';
import 'package:muslim/common/api/error_network_handle.dart';
import 'package:muslim/common/core/storage.dart';
import 'package:muslim/common/tools/dialog.dart';
import 'package:muslim/common/tools/hexColor.dart';
import 'package:muslim/common/tools/snackBar.dart';
import 'package:muslim/global.dart';
import 'package:muslim/modals/activity.dart';
import 'package:muslim/modals/merchant.dart';
import 'package:muslim/modals/recommends.dart';
import 'package:muslim/pages/community/PostFeedList.dart';
import 'package:muslim/pages/community/activityList.dart';
import 'package:muslim/pages/community/detailsTop.dart';
import 'package:muslim/pages/community/services.dart';
import 'package:muslim/pages/widgets/backButton.dart';
import 'package:muslim/pages/widgets/base_page.dart';
import 'package:muslim/pages/widgets/base_widget.dart';
import 'package:muslim/pages/widgets/empty.dart';
import 'package:muslim/pages/widgets/muslim_title.dart';
import 'package:easy_localization/easy_localization.dart';

class StoreDetailPage extends TraceableStatefulWidget {
  StoreDetailPage({Key key, this.storeId}) : super(key: key);
  final String storeId;

  @override
  _StoreDetailPageState createState() => _StoreDetailPageState();
}

class _StoreDetailPageState extends State<StoreDetailPage> {
  EasyRefreshController _controller = EasyRefreshController();
  ScrollController _scrollCntroller = ScrollController();
  List<ActivitiesDatum> _tableData;
  List<Datum> _postFeedData;
  MerchantDetailData _detailsData;
  bool _isNetworkError = false;
  bool _showNavibar = false;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      _onRefresh();
    });
    _scrollCntroller.addListener(() {
      if (_scrollCntroller.offset < 130 && _showNavibar == true) {
        setState(() {
          _showNavibar = false;
        });
      } else if (_scrollCntroller.offset > 130 && _showNavibar == false) {
        setState(() {
          _showNavibar = true;
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    if (_detailsData == null) {
      if (!_isNetworkError) {
        return Scaffold(
          appBar: AppBar(
            centerTitle: true,
            title: MuslimTitle(
              title: "Merchant Details",
            ),
          ),
          body: Container(
            color: Colors.white,
            child: Center(
              child: CircularProgressIndicator(),
            ),
          ),
        );
      } else {
        return Scaffold(
          appBar: AppBar(
            centerTitle: true,
            title: MuslimTitle(
              title: "Merchant Details",
            ),
          ),
          body: EmptyPage(
            noDatasTip: tr("no_network_tip"),
          ),
        );
      }
    }
    bool hasTopPhoto = _detailsData.merchantPhotos.photos.length != 0;
    return MuslimBasePage(
      header: baseHeader(),
      footer: baseFooter(),
      controller: _controller,
      scrollController: _scrollCntroller,
      onRefresh: _onRefresh,
      firstRefresh: false,
      firstRefreshWidget: false,
      slivers: [
        SliverAppBar(
            expandedHeight: hasTopPhoto ? 200.0 : 44,
            pinned: true,
            title: MuslimTitle(
                title: !hasTopPhoto
                    ? _detailsData.name
                    : !_showNavibar
                        ? ""
                        : _detailsData.name ?? ""),
            leading: MuslimBackButton(
              color: !hasTopPhoto
                  ? Colors.black
                  : !_showNavibar
                      ? Colors.white
                      : Colors.black,
            ),
            backgroundColor: Colors.white,
            flexibleSpace: hasTopPhoto
                ? Stack(
                    children: [
                      Positioned.fill(
                        child: FlexibleSpaceBar(
                          background: CachedNetworkImage(
                            imageUrl:
                                _detailsData.merchantPhotos.photos[0].url +
                                    OSS_IMAGE_SCALE,
                            fit: BoxFit.cover,
                            errorWidget: (context, str, value) {
                              if (!checkNetworkState(context))
                                return Container(color: HexColor("#F2F3F6"));
                              return Image.network(
                                  _detailsData.merchantPhotos.photos[0].url +
                                      OSS_IMAGE_SCALE,
                                  fit: BoxFit.cover);
                            },
                          ),
                        ),
                      ),
                      Positioned(
                          bottom: 16.0,
                          right: 16.0,
                          child: Container(
                            height: 30.0,
                            child: !_showNavibar
                                ? FlatButton.icon(
                                    onPressed: () {
                                      print(
                                          "gallery photos length :${_detailsData.merchantPhotos.photos.length}");
                                      Navigator.of(context).pushNamed("gallery",
                                          arguments: _detailsData
                                              .merchantPhotos.photos);
                                    },
                                    icon: Icon(Icons.image),
                                    label: Text('gallery').tr(),
                                    color: Colors.black.withOpacity(0.5),
                                    textColor: HexColor("FFFFFF"),
                                  )
                                : Container(),
                          ))
                    ],
                  )
                : Container()),
        StoreDetailTopWidget(
          storeDetails: _detailsData,
          callBack: _fetchMerchantDetail,
        ),
        ActivityList(activitiesInfo: _tableData, callBack: _pushToActivities),
        DetailsServicesList(
          services: _detailsData.service,
        ),
        PostFeedList(infos: _postFeedData, callBack: _pushToFeeds),
      ],
    );
  }

  @override
  void dispose() {
    _scrollCntroller.dispose();
    super.dispose();
  }

  Future<void> _fetchFeedList() async {
    Global.normalApi
        .fetchMerchantFeedWithId({"limit": "3"}, widget.storeId).then((value) {
      if (value != null && value.errorCode == SUCCESS) {
        setState(() {
          _postFeedData = value.data;
        });
      } else if (value != null) {
        if (mounted) handleNetworkError(value, context);
      } else {
        if (mounted) {
          showSnackBar(context, value.developerMsg ?? tr("network_error"),
              type: 1);
        }
      }
    }).catchError((error) {
      print("_fetchFeedList error : $error");
    });
  }

  Future<void> _onRefresh() async {
    if (!checkNetworkState(context)) {
      setState(() {
        _isNetworkError = true;
      });
      return;
    }
    Future.wait(
            [_fetchMerchantDetail(), _fetchFeedList(), _fetchActivitiesList()])
        .then((value) {
      _controller.finishRefresh();
    });
  }

  Future<void> _fetchMerchantDetail() async {
    setState(() {
      _isNetworkError = false;
    });
    print("_fetchMerchantDetail_fetchMerchantDetail");
    Global.normalApi.fetchMerchantDetail(widget.storeId).then((value) {
      print("fetchMerchantDetail ---------- ${value.toJson()}");
      if (value != null && value.errorCode == SUCCESS) {
        setState(() {
          _detailsData = value.data;
        });
      } else if (value != null) {
        if (mounted) handleNetworkError(value, context);
      } else {
        if (mounted) {
          showSnackBar(context, value.developerMsg ?? tr("network_error"),
              type: 1);
        }
      }
    }).catchError((error) {
      setState(() {
        _isNetworkError = true;
      });
    });
  }

  Future<void> _fetchActivitiesList() async {
    var params = {"limit": "$LIMIT"};
    Global.normalApi
        .fetchMerchantActivities(params, widget.storeId)
        .then((value) {
      if (value != null || value.errorCode == SUCCESS) {
        setState(() {
          _tableData = value.data;
        });
      } else if (value != null) {
        if (mounted) handleNetworkError(value, context);
      } else {
        if (mounted)
          showSnackBar(context, value.developerMsg ?? tr("network_error"),
              type: 1);
      }
    }).catchError((error) {
      print("_fetchActivitiesList: error$error");
    });
  }

  void _pushToActivities() {
    Navigator.of(context).pushNamed("detail_activities", arguments: {
      "id": widget.storeId,
      "previewUrl": _detailsData.previewPhoto.url
    });
  }

  void _pushToFeeds() {
    Navigator.of(context).pushNamed("detail_feeds", arguments: {
      "id": widget.storeId,
      "previewUrl": _detailsData.previewPhoto.url
    });
  }
}
