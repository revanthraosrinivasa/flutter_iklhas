import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:muslim/common/api/error_network_handle.dart';
import 'package:muslim/common/core/storage.dart';
import 'package:muslim/common/helper/logger.dart';
import 'package:muslim/common/tools/dialog.dart';
import 'package:muslim/common/tools/hexColor.dart';
import 'package:muslim/common/tools/snackBar.dart';
import 'package:muslim/global.dart';
import 'package:muslim/modals/merchant.dart';
import 'package:muslim/pages/widgets/horizontalLine.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:map_launcher/map_launcher.dart';
import 'package:flutter_svg/flutter_svg.dart';

class StoreDetailTopWidget extends StatefulWidget {
  StoreDetailTopWidget({Key key, this.storeDetails, this.callBack})
      : super(key: key);

  final MerchantDetailData storeDetails;
  final VoidCallback callBack;
  @override
  _StoreDetailTopWidgetState createState() => _StoreDetailTopWidgetState();
}

class _StoreDetailTopWidgetState extends State<StoreDetailTopWidget> {
  MerchantDetailData storeDetails;
  VoidCallback callBack;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    storeDetails = widget.storeDetails;
    callBack = widget.callBack;
    // TODO: implement build
    OpenHour hours = widget.storeDetails.openHour != null &&
            widget.storeDetails.openHour.length != 0
        ? widget.storeDetails.openHour[DateTime.now().weekday - 1]
        : null;
    return SliverList(
      delegate: SliverChildListDelegate(
        [
          Container(
              padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 14.0),
              decoration: BoxDecoration(
                  border:
                      Border(bottom: BorderSide(color: HexColor("EAEAEA")))),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CircleAvatar(
                    radius: 20.0,
                    backgroundImage: CachedNetworkImageProvider(
                        storeDetails.previewPhoto.url + OSS_IMAGE_SCALE),
                  ),
                  Expanded(
                      child: Padding(
                    padding: EdgeInsets.only(left: 8.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(storeDetails.name,
                            maxLines: 2,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                                color: HexColor("212124"),
                                fontWeight: FontWeight.bold,
                                fontSize: 16.0)),
                        Text(
                          "detail_fans",
                          style: TextStyle(
                              fontSize: 12.0, color: HexColor("4C4C50")),
                        ).tr(args: ["${storeDetails.fansCount}"]),
                        Builder(builder: (context) {
                          if (storeDetails.describe == null ||
                              storeDetails.describe.length == 0) {
                            return Container();
                          }
                          return InkWell(
                            onTap: () {
                              _showBottomSheetDetails(context);
                            },
                            child: Text(
                              "detail_more_info",
                              style: TextStyle(
                                  fontSize: 12.0,
                                  color: HexColor("67C1BF"),
                                  fontWeight: FontWeight.w500),
                            ).tr(),
                          );
                        })
                      ],
                    ),
                  )),
                  Container(
                    width: 10.0,
                  ),
                  ButtonTheme(
                    height: 40,
                    child: storeDetails.type == 0
                        ? RaisedButton(
                            elevation: 0.0,
                            color: HexColor("67C1BF").withOpacity(0.4),
                            disabledColor: HexColor("67C1BF").withOpacity(0.4),
                            onPressed: null,
                            child: Text(
                              "following",
                              style: TextStyle(color: Colors.white),
                            ).tr(),
                          )
                        : storeDetails.followed
                            ? RaisedButton(
                                elevation: 0.0,
                                color: HexColor("67C1BF"),
                                onPressed: () {
                                  _unfollowMerchant(context);
                                },
                                // disabledColor:
                                //     HexColor("67C1BF").withOpacity(0.3),
                                child: Text("following").tr(),
                                textColor: Colors.white,
                              )
                            : OutlineButton(
                                color: Colors.white,
                                borderSide:
                                    BorderSide(color: HexColor("67C1BF")),
                                child: Text('follow').tr(),
                                textColor: HexColor("67C1BF"),
                                onPressed: () {
                                  _followMerchant(context);
                                }),
                  ),
                  storeDetails.type == 0
                      ? IconButton(
                          padding: EdgeInsets.fromLTRB(8, 4, 8, 8),
                          iconSize: 40,
                          icon: Image.asset(
                              "assets/images/notification_disable.png"),
                          onPressed: null,
                          color: HexColor("67C1BF").withOpacity(0.4),
                          disabledColor: HexColor("67C1BF").withOpacity(0.3),
                        )
                      : IconButton(
                          padding: EdgeInsets.fromLTRB(8, 4, 8, 8),
                          iconSize: 40,
                          icon: Image.asset(
                            storeDetails.isSub
                                ? "assets/images/notification_on.png"
                                : "assets/images/notification_off.png",
                          ),
                          onPressed: storeDetails.type == 0
                              ? null
                              : () {
                                  if (storeDetails.isSub) {
                                    _switchOffPush(context);
                                  } else {
                                    _switchOnPush(context);
                                  }
                                },
                          disabledColor: HexColor("67C1BF").withOpacity(0.3),
                        ),
                ],
              )),
          Container(
            height: 50.0,
            margin: EdgeInsets.only(bottom: 8),
            decoration: BoxDecoration(
                border: Border(bottom: BorderSide(color: HexColor("EAEAEA")))),
            child: ListTile(
              title: hours == null || hours.time.length == 0
                  ? Text(tr("today") + "     " + tr("detail_hour_rest"))
                  : RichText(
                      text: TextSpan(children: [
                      TextSpan(
                          text: tr("hours") + " ",
                          style: TextStyle(
                              color: HexColor("212124"),
                              fontSize: 16.0,
                              fontWeight: FontWeight.w500)),
                      TextSpan(
                          text: hours.time.join(","),
                          style: TextStyle(
                              color: HexColor("4C4C50"),
                              fontSize: 14.0,
                              fontWeight: FontWeight.w400))
                    ])),
              trailing: Icon(
                Icons.navigate_next,
                color: HexColor("75767A"),
              ),
              onTap: () {
                _showHoursSheet(context);
              },
            ),
          ),
          Padding(
              padding: EdgeInsets.symmetric(horizontal: 12.0, vertical: 8.0),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Icon(Icons.room, color: HexColor("75767A")),
                  Expanded(
                    child: Container(
                        margin: EdgeInsets.only(left: 8.0, top: 4.0),
                        child: Text(
                          storeDetails.location ?? "",
                          style: TextStyle(
                              fontWeight: FontWeight.w400, fontSize: 14.0),
                          maxLines: 100,
                        )),
                  )
                ],
              )),
          Padding(
            padding: EdgeInsets.fromLTRB(12, 0, 12, 8),
            child: Row(
              children: [
                Icon(Icons.email, color: HexColor("67C1BF")),
                Expanded(
                  child: InkWell(
                    onTap: () async {
                      String emailNumber = storeDetails.email;
                      if (await canLaunch("mailto:" + emailNumber)) {
                        await launch("mailto:" + emailNumber);
                      } else {
                        print("dont email the emailNumber $emailNumber");
                      }
                    },
                    child: Container(
                        padding: EdgeInsets.only(left: 8.0, top: 4.0),
                        child: Text(
                          storeDetails.email,
                          style: TextStyle(
                              color: HexColor("67C1BF"),
                              fontWeight: FontWeight.w400,
                              fontSize: 14.0),
                        )),
                  ),
                )
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 12.0),
            child: Row(
              children: [
                Icon(
                  Icons.phone,
                  color: HexColor("67C1BF"),
                  // color: HexColor("75767A"),
                ),
                Expanded(
                    child: InkWell(
                        onTap: () async {
                          String phoneNumber =
                              storeDetails.phoneCode + storeDetails.phone;
                          phoneNumber = phoneNumber.replaceAll(" ", "");
                          if (await canLaunch("tel:" + phoneNumber)) {
                            await launch("tel:" + phoneNumber);
                          } else {
                            print("dont call the phone $phoneNumber");
                          }
                        },
                        child: Container(
                          padding: EdgeInsets.only(left: 8.0, top: 4.0),
                          child: Text(
                            storeDetails.phoneCode + " " + storeDetails.phone,
                            style: TextStyle(
                                color: HexColor("67C1BF"),
                                fontWeight: FontWeight.w400,
                                fontSize: 14.0),
                          ),
                        )))
              ],
            ),
          ),
          Container(
              padding: EdgeInsets.symmetric(horizontal: 16.0),
              margin: EdgeInsets.only(bottom: 16.0, top: 16),
              child: ButtonTheme(
                height: 44.0,
                child: RaisedButton(
                    color: HexColor("67C1BF"),
                    elevation: 0.0,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(4.0))),
                    onPressed: _showAvaliableMaps,
                    child: Text(
                      tr("get_direction"),
                      style: TextStyle(color: Colors.white),
                    )),
              )),
          HorizontalLine(),
          Container(
            child: Row(
              children: [
                storeDetails.faceBookUrl == null ||
                        storeDetails.faceBookUrl.length == 0
                    ? Container()
                    : Expanded(
                        child: FlatButton.icon(
                          onPressed: () async {
                            logger("----pressed fb button!");
//                            logger("----FB link: ${storeDetails.faceBookUrl}");
                            String fbProtocolUrl;
                            if (Global.isIOS) {
                              fbProtocolUrl =
                                  'fb://profile/' + storeDetails.faceBookUrl;
                            } else {
                              fbProtocolUrl =
                                  'fb://page/' + storeDetails.faceBookUrl;
                            }

                            String fallbackUrl =
                                "https://www.facebook.com/${storeDetails.faceBookUrl}";

                            try {
                              bool launched = await launch(fbProtocolUrl,
                                  forceSafariVC: false);

                              if (!launched) {
                                await launch(fallbackUrl, forceSafariVC: false);
                              }
                            } catch (e) {
                              await launch(fallbackUrl, forceSafariVC: false);
                            }
                          },
                          icon: Image.asset(
                            "assets/images/facebook.png",
                            height: 18,
                            width: 18,
                          ),
                          label: Text('Facebook'),
                          textColor: HexColor("4C4C50"),
                        ),
                        flex: 1),
                storeDetails.faceBookUrl == null ||
                        storeDetails.faceBookUrl.length == 0
                    ? Container()
                    : Container(
                        width: 1.0,
                        height: 50.0,
                        color: HexColor("EAEAEA"),
                      ),
                storeDetails.instagramUrl == null ||
                        storeDetails.instagramUrl.length == 0
                    ? Container()
                    : Expanded(
                        child: FlatButton.icon(
                          onPressed: () async {
                            if (await canLaunch("instagram://")) {
                              await launch(storeDetails.instagramUrl,
                                  forceSafariVC: false);
                            } else {
                              if (await canLaunch(storeDetails.instagramUrl)) {
                                await launch(storeDetails.instagramUrl,
                                    forceSafariVC: false);
                              } else {
                                showEnsureDialog(
                                    context, "", tr("url_disable"), () {},
                                    cancelTitle: "");
                              }
                            }
                          },
                          icon: Image.asset(
                            "assets/images/instagram.png",
                            height: 18,
                            width: 18,
                          ),
                          label: Text('Instagram'),
                          textColor: HexColor("4C4C50"),
                        ),
                        flex: 1)
              ],
            ),
          ),
          HorizontalLine(),
        ],
      ),
    );
  }

  void _showHoursSheet(BuildContext context) {
    Map<String, String> hours = {
      "Monday": storeDetails.openHour[0].state
          ? storeDetails.openHour[0].time.join(",")
          : tr("detail_hour_rest"),
      "Tuesday": storeDetails.openHour[1].state
          ? storeDetails.openHour[1].time.join(",")
          : tr("detail_hour_rest"),
      "Wednesday": storeDetails.openHour[2].state
          ? storeDetails.openHour[2].time.join(",")
          : tr("detail_hour_rest"),
      "Thursday": storeDetails.openHour[3].state
          ? storeDetails.openHour[3].time.join(",")
          : tr("detail_hour_rest"),
      "Friday": storeDetails.openHour[4].state
          ? storeDetails.openHour[4].time.join(",")
          : tr("detail_hour_rest"),
      "Saturday": storeDetails.openHour[5].state
          ? storeDetails.openHour[5].time.join(",")
          : tr("detail_hour_rest"),
      "Sunday": storeDetails.openHour[6].state
          ? storeDetails.openHour[6].time.join(",")
          : tr("detail_hour_rest"),
    };
    List<String> keys = hours.keys.toList();
    DateTime nowTime = DateTime.now();

    double width = MediaQuery.of(context).size.width;

    List<Container> _listContainer = keys
        .map((e) => Container(
              height: 50.0,
              width: width,
              padding: EdgeInsets.symmetric(horizontal: 16.0),
              decoration: BoxDecoration(
                  border: Border(bottom: BorderSide(color: HexColor("EAEAEA"))),
                  color: nowTime.weekday - 1 == keys.indexOf(e)
                      ? HexColor("EAEAEA")
                      : Colors.white),
              child: Row(
                children: [
                  Expanded(
                    child: Text(
                      e,
                      style: TextStyle(
                          fontSize: 16.0, fontWeight: FontWeight.w500),
                    ),
                    flex: 1,
                  ),
                  Expanded(
                    child: Text(
                      hours[e].length == 0 ? tr("detail_hour_rest") : hours[e],
                      textAlign: TextAlign.end,
                    ),
                    flex: 2,
                  )
                ],
              ),
            ))
        .toList();

    _listContainer.insert(
        0,
        Container(
          width: width,
          height: 30.0,
          padding: EdgeInsets.symmetric(horizontal: 16.0),
          decoration: BoxDecoration(
              border: Border(bottom: BorderSide(color: HexColor("EAEAEA")))),
          child: Text(
            "detail_hours_title",
            style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.w500),
            textAlign: TextAlign.left,
          ).tr(),
        ));

    showModalBottomSheet(
        clipBehavior: Clip.antiAlias,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(4.0), topRight: Radius.circular(4.0))),
        context: context,
        builder: (context) {
          return BottomSheet(
              backgroundColor: Colors.white,
              onClosing: () {
                Navigator.of(context).pop();
              },
              builder: (context) {
                return SingleChildScrollView(
                  padding: EdgeInsets.symmetric(vertical: 16.0),
                  child: Column(
                    children: _listContainer,
                    crossAxisAlignment: CrossAxisAlignment.start,
                  ),
                );
              });
        });
  }

  _followMerchant(BuildContext context) async {
    if (!checkNetworkState(context)) return;
    showWaitingDialog(context, "");
    Global.normalApi.followMerchant(storeDetails.id).then((value) {
      Navigator.of(context).pop();
      if (value != null && value.errorCode == SUCCESS) {
        callBack();
      } else if (value != null) {
        if (mounted) handleNetworkError(value, context);
      } else {
        if (mounted)
          showSnackBar(context, value.developerMsg ?? tr("network_error"),
              type: 1);
      }
    }).catchError((error) {
      Navigator.of(context).pop();
      if (mounted) showSnackBar(context, tr("network_error"));
    });
  }

  _unfollowMerchant(BuildContext context) async {
    if (!checkNetworkState(context)) return;
    showEnsureDialog(context, tr("unfollow_title"),
        tr("unfollow_tip", args: [storeDetails.name]), () async {
      showWaitingDialog(context, "");
      Global.normalApi.unfollowMerchant(storeDetails.id).then((value) {
        Navigator.of(context).pop();
        if (value != null && value.errorCode == SUCCESS) {
          callBack();
        } else if (value != null) {
          if (mounted) handleNetworkError(value, context);
        } else {
          if (mounted)
            showSnackBar(context, value.developerMsg ?? tr("network_error"),
                type: 1);
        }
      }).catchError((error) {
        Navigator.of(context).pop();
        if (mounted) showSnackBar(context, tr("network_error"));
      });
    }, ensureTitle: tr("Yes"), cancelTitle: tr("cancelText"));
  }

  _showBottomSheetDetails(BuildContext context) {
    showModalBottomSheet(
        clipBehavior: Clip.antiAlias,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(4.0), topRight: Radius.circular(4.0))),
        context: context,
        builder: (context) {
          return Container(
            color: Colors.white,
            height: 320.0,
            child: Column(
              children: [
                Container(
                  height: 56.0,
                  padding: EdgeInsets.symmetric(horizontal: 16.0),
                  decoration: BoxDecoration(
                      border: Border(
                          bottom: BorderSide(color: HexColor("EAEAEA")))),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "About",
                        style: TextStyle(
                            fontSize: 16.0,
                            fontWeight: FontWeight.w500,
                            color: HexColor("212124")),
                      ).tr(),
                      IconButton(
                          icon: Icon(Icons.clear),
                          onPressed: () {
                            Navigator.of(context).pop();
                          })
                    ],
                  ),
                ),
                Expanded(
                    child: SingleChildScrollView(
                  padding: EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 30),
                  child: Text(
                    storeDetails.describe,
                    maxLines: 100,
                    style: TextStyle(
                      color: HexColor("4C4C50"),
                      fontSize: 14.0,
                    ),
                  ),
                ))
              ],
            ),
          );
        });
  }

  _showAvaliableMaps() async {
    try {
      final coords = Coords(
          storeDetails.latitude.toDouble(), storeDetails.longitude.toDouble());
      final String title = storeDetails.location;
      final availableMaps = await MapLauncher.installedMaps;

      showModalBottomSheet(
        context: context,
        builder: (BuildContext context) {
          return SafeArea(
            child: SingleChildScrollView(
              child: Container(
                child: Wrap(
                  children: <Widget>[
                    for (var map in availableMaps)
                      ListTile(
                        onTap: () => map.showMarker(
                          coords: coords,
                          title: title,
                        ),
                        title: Text(map.mapName),
                        leading: SvgPicture.asset(
                          map.icon,
                          height: 30.0,
                          width: 30.0,
                        ),
                      ),
                  ],
                ),
              ),
            ),
          );
        },
      );
    } catch (e) {
      print(e);
    }
  }

  _switchOffPush(BuildContext context) {
    if (!checkNetworkState(context)) return;
    showEnsureDialog(context, tr("unfollow_title"),
        tr("unnotification_tip", args: [storeDetails.name]), () async {
      showWaitingDialog(context, "");
      Global.normalApi.switchOffPush(storeDetails.id).then((value) {
        Navigator.of(context).pop();
        if (value != null && value.errorCode == SUCCESS) {
          callBack();
        } else if (value != null) {
          if (mounted) handleNetworkError(value, context);
        } else {
          if (mounted)
            showSnackBar(context, value.developerMsg ?? tr("network_error"),
                type: 1);
        }
      }).catchError((error) {
        Navigator.of(context).pop();
        if (mounted) showSnackBar(context, tr("network_error"));
      });
    }, ensureTitle: tr("Yes"), cancelTitle: tr("cancelText"));
  }

  _switchOnPush(BuildContext context) {
    if (!checkNetworkState(context)) return;
    if (storeDetails.followed == false) {
      showEnsureDialog(context, tr("unfollow_title"),
          tr("notification_tip", args: [storeDetails.name]), () {
        _followMerchant(context);
      }, ensureTitle: tr("follow"), cancelTitle: tr("cancelText"));
      return;
    }
    showWaitingDialog(context, "");
    Global.normalApi.switchOnPush(storeDetails.id).then((value) {
      Navigator.of(context).pop();
      if (value != null && value.errorCode == SUCCESS) {
        callBack();
      } else if (value != null) {
        if (mounted) handleNetworkError(value, context);
      } else {
        if (mounted)
          showSnackBar(context, value.developerMsg ?? tr("network_error"),
              type: 1);
      }
    }).catchError((error) {
      Navigator.of(context).pop();
      if (mounted) showSnackBar(context, tr("network_error"));
    });
  }
}
