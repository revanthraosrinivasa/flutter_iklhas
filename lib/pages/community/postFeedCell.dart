import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:muslim/common/tools/hexColor.dart';
import 'package:muslim/common/tools/timeChange.dart';
import 'package:muslim/modals/recommends.dart';
import 'package:muslim/pages/widgets/images.dart';
import 'package:muslim/pages/widgets/textPainer.dart';

class PostFeedCell extends StatelessWidget {
  PostFeedCell({Key key, this.info}) : super(key: key);

  final Datum info;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
      decoration: BoxDecoration(
          color: Colors.white,
          border: Border(bottom: BorderSide(color: HexColor("EAEAEA")))),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.symmetric(vertical: 10.0),
            child: Text(RelativeDateFormat.format(info.postTime)),
          ),
          DKMRichText(
            text: info.content,
            style: TextStyle(fontSize: 14.0, color: Colors.grey),
            urls: info.urls
          ),
          DKMImagesWidget(
              imageUrls: info.photos.map((e) {
            return e.url;
          }).toList()),
        ],
      ),
    );
  }
}
