import 'dart:convert' show json;
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyrefresh/easy_refresh.dart';
import 'package:geolocator/geolocator.dart';
import 'package:location_permissions/location_permissions.dart';
import 'package:matomo/matomo.dart';
import 'package:muslim/common/api/error_network_handle.dart';
import 'package:muslim/common/provider/app.dart';
import 'package:muslim/common/core/storage.dart';
import 'package:muslim/common/helper/helper.dart';
import 'package:muslim/common/tools/dialog.dart';
import 'package:muslim/common/tools/hexColor.dart';
import 'package:muslim/common/tools/scrollOffset.dart';
import 'package:muslim/common/tools/snackBar.dart';
import 'package:muslim/global.dart';
import 'package:muslim/modals/nearlyMerchant.dart';
import 'package:muslim/modals/recommends.dart';
import 'package:muslim/pages/home/homeListViewCell.dart';
import 'package:muslim/pages/widgets/base_page.dart';
import 'package:muslim/pages/widgets/base_widget.dart';
import 'package:muslim/pages/widgets/empty.dart';
import 'package:muslim/pages/widgets/muslim_title.dart';
import 'package:muslim/pages/widgets/nearStoreTip.dart';
import 'package:provider/provider.dart';

class CommunityMainPage extends StatefulWidget {
  CommunityMainPage({Key key, this.clickTip}) : super(key: key);
  final int clickTip;
  @override
  _CommunityMainPageState createState() => _CommunityMainPageState();
}

class _CommunityMainPageState extends State<CommunityMainPage>
    with AutomaticKeepAliveClientMixin {
  @override
  bool get wantKeepAlive => true;

  bool _isNetworkError = false;
  bool _isFirstPage = true;
  bool _hasFollow;
  Position _position;
  List<Datum> _tableData;
  List<NearlyMerchantsDatum> _merchants;
  ScrollController _scrollController = ScrollController();
  EasyRefreshController _controller = EasyRefreshController();

  GlobalKey _scrollKey = GlobalKey();

  List<GlobalKey> _itemKeys = [];
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _hasFollow = true;
  }

  @override
  void didUpdateWidget(covariant CommunityMainPage oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);
    if (widget.clickTip != oldWidget.clickTip) {
      _scrollController.animateTo(0,
          duration: Duration(milliseconds: 100), curve: Curves.easeInOut);
      _controller.callRefresh();
    }
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MuslimBasePage(
      appBar: AppBar(
        centerTitle: true,
        title: MuslimTitle(title: tr("community_title")),
        actions: [
          GestureDetector(
            child: Container(
              padding: EdgeInsets.only(right: 16.0),
              alignment: Alignment.center,
              child: Icon(Icons.search),
            ),
            onTap: () {
              MatomoTracker.trackEvent("nearby_merchants", "nearby_merchants");
              _pushToNearlyMerchantsList();
            },
          ),
        ],
      ),
      key: _scrollKey,
      header: baseHeader(),
      footer: baseFooter(),
      onLoad: _onLoad,
      onRefresh: _onRefresh,
      controller: _controller,
      scrollController: _scrollController,
      firstRefresh: true,
      firstRefreshWidget: true,
      slivers: [
        SliverToBoxAdapter(
          child: _hasFollow
              ? Container()
              : NearStoreTipWidget(
                  stores: _merchants ?? [],
                  callBack: _pushToNearlyMerchantsList,
                  followedCallBack: _followedCallBack),
        ),
        Builder(builder: (buildContext) {
          if (_tableData == null && _isNetworkError) {
            return SliverToBoxAdapter(
              child: EmptyPage(noDatasTip: tr("no_network_tip")),
            );
          }
          if (_tableData != null && _tableData.length == 0) {
            return SliverToBoxAdapter(
              child: EmptyPage(noDatasTip: tr("no_posts_content")),
            );
          }
          return SliverList(
            delegate: SliverChildBuilderDelegate((context, index) {
              GlobalKey itemKey = _itemKeys[index];
              return HomeListViewCellWidget(
                  detail: _tableData[index], key: itemKey, tapClick: itemClick);
            }, childCount: _tableData != null ? _tableData.length : 0),
          );
        })
      ],
    );
  }

  itemClick(GlobalKey itemKey) {
    double offset = _scrollController.offset;
    Offset currentPosition = getPosition(itemKey);
    Offset scrollPosition = getPosition(_scrollKey);
    if (currentPosition.dy < scrollPosition.dy) {
      offset -= scrollPosition.dy - currentPosition.dy;
      _scrollController.animateTo(offset,
          duration: Duration(milliseconds: 100), curve: Curves.easeIn);
    }
  }

  Future<void> _fetchNearlyMerchant() async {
    Map<String, dynamic> positionValue;
    try {
      positionValue = await Global.locationHelper.fetchLocation(context);
    } catch (error) {}
    if (positionValue == null) {
      _controller.finishRefresh(success: false);
      setState(() {
        _isFirstPage = false;
      });
      _fetchListData();
      return;
    }
    if (positionValue != null && positionValue["msg"] != null) {
      Position position = Position(
          latitude: positionValue["msg"]["latitude"],
          longitude: positionValue["msg"]["longitude"]);
      setState(() {
        _position = position;
        _isNetworkError = false;
      });
      try {
        var value = await Global.normalApi.fetchNearbyMerchantsWithLatLng(
            position.latitude, position.longitude, {"limit": "2"});
        if (value != null && value.errorCode == SUCCESS) {
          _fetchListData();
          if (value.data.length != 0) {
            setState(() {
              _hasFollow = false;
              _merchants = value.data;
            });
          } else {
            setState(() {
              _hasFollow = true;
            });
          }
        } else {
          if (mounted) handleNetworkError(value, context);
          setState(() {
            _isFirstPage = false;
          });
        }
      } catch (error) {
        setState(() {
          _isFirstPage = false;
          _isNetworkError = true;
        });
        if (_merchants != null && mounted) {
          showSnackBar(context, tr("network_error"), type: 1);
        }
        _fetchListData();
      }
    }
  }

  Future<void> _fetchListData({int fetchType = 0}) async {
    var params = {
      "cursor": fetchType == 0
          ? ""
          : _tableData == null || _tableData.length == 0
              ? ""
              : _tableData.last.id,
      "limit": "$LIMIT"
    };
    Global.normalApi.fetchFollowMerchantsFeed(params).then((value) {
      setState(() {
        _isFirstPage = false;
      });
      print("fetchFollowMerchantsFeed :");
      if (value != null && value.errorCode == SUCCESS) {
        if (fetchType == 0) {
          setState(() {
            _tableData = value.data;
          });
          _controller.finishRefresh(success: true);
          if (_controller.finishLoadCallBack != null)
            _controller.finishLoadCallBack(noMore: value.data.length < LIMIT);
        } else {
          var oldData = _tableData;
          oldData.addAll(value.data);
          setState(() {
            _tableData = oldData;
          });
          _controller.finishLoad(success: true, noMore: value.data.length == 0);
        }
        List<GlobalKey> list = [];
        for (var i = 0; i < _tableData.length; i++) {
          list.add(GlobalKey());
        }
        setState(() {
          _itemKeys = list;
        });
      } else {
        if (fetchType == 0) {
          _controller.finishRefresh(success: false);
        } else {
          _controller.finishLoad(success: false);
        }
        if (mounted) handleNetworkError(value, context);
      }
    }).catchError((error) {
      setState(() {
        _isFirstPage = false;
        _isNetworkError = true;
      });
      if (_merchants != null && mounted) {
        showSnackBar(context, tr("network_error"), type: 1);
      }
      if (fetchType == 0) {
        _controller.finishRefresh(success: false);
      } else {
        _controller.finishLoad(success: false);
      }
    });
  }

  Future<void> _onRefresh() async {
    _controller.resetLoadState();
    _fetchNearlyMerchant();
  }

  Future<void> _onLoad() async {
    _fetchListData(fetchType: 1);
  }

  void _followedCallBack() {
    setState(() {
      _merchants = [];
    });
    _controller.callRefresh();
  }

  void _pushToNearlyMerchantsList() {
    Navigator.of(context).pushNamed("nearly_mosque", arguments: _position);
  }
}
