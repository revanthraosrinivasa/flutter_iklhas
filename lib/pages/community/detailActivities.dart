import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_easyrefresh/easy_refresh.dart';
import 'package:muslim/common/api/error_network_handle.dart';
import 'package:muslim/common/core/storage.dart';
import 'package:muslim/common/provider/app.dart';
import 'package:muslim/common/tools/hexColor.dart';
import 'package:muslim/common/tools/snackBar.dart';
import 'package:muslim/global.dart';
import 'package:muslim/modals/activity.dart';
import 'package:muslim/pages/community/activityCell.dart';
import 'package:muslim/pages/widgets/backButton.dart';
import 'package:muslim/pages/widgets/base_page.dart';
import 'package:muslim/pages/widgets/base_widget.dart';
import 'package:muslim/pages/widgets/empty.dart';
import 'package:muslim/pages/widgets/muslim_title.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:provider/provider.dart';

class DetailActivitiesPage extends StatefulWidget {
  DetailActivitiesPage({Key key, this.merchantId, this.previewUrl})
      : super(key: key);

  final String merchantId;
  final String previewUrl;

  @override
  _DetailActivitiesState createState() => _DetailActivitiesState();
}

class _DetailActivitiesState extends State<DetailActivitiesPage> {
  EasyRefreshController _controller = EasyRefreshController();
  List<ActivitiesDatum> _tableData;
  bool _isNetWorkError = true;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MuslimBasePage(
      appBar: AppBar(
        centerTitle: true,
        title: MuslimTitle(title: tr("merchant_activities_title")),
        leading: MuslimBackButton(),
      ),
      header: baseHeader(),
      footer: baseFooter(),
      controller: _controller,
      emptyWidget: _isNetWorkError && _tableData == null
          ? EmptyPage(noDatasTip: tr("no_network_tip"))
          : _tableData != null && _tableData.length == 0
              ? EmptyPage(noDatasTip: tr("no_feeds_content"))
              : null,
      firstRefresh: true,
      firstRefreshWidget: true,
      onLoad: _onLoad,
      onRefresh: _onRefresh,
      slivers: [
        SliverList(
            delegate: SliverChildBuilderDelegate((context, index) {
          return ActivityCell(
            activityInfo: _tableData[index],
          );
        }, childCount: _tableData != null ? _tableData.length : 0))
      ],
    );
  }

  Future<void> _fetchTableData({int fetchType = 0}) async {
    setState(() {
      _isNetWorkError = false;
    });
    var params = {
      "limit": "$LIMIT",
      "onId": fetchType == 0
          ? ""
          : _tableData == null || _tableData.length == 0
              ? ""
              : _tableData.last.onId,
    };

    Global.normalApi
        .fetchMerchantActivities(params, widget.merchantId)
        .then((value) {
      if (value != null && value.errorCode == SUCCESS) {
        if (fetchType == 0) {
          setState(() {
            _tableData = value.data;
          });
          _controller.finishRefresh(success: true);
          if (_controller.finishLoadCallBack != null)
            _controller.finishLoadCallBack(noMore: value.data.length < LIMIT);
        } else {
          var data = _tableData;
          data.addAll(value.data);
          setState(() {
            _tableData = data;
          });
          _controller.finishLoad(success: true, noMore: value.data.length == 0);
        }
      } else {
        if (fetchType == 0) {
          _controller.finishRefresh(success: false);
        } else {
          _controller.finishLoad(success: false);
        }
        if (mounted) handleNetworkError(value, context);
      }
    }).catchError((error) {
      print("fetchMerchantActivities: $error");
      if (fetchType == 0) {
        _controller.finishRefresh(success: false);
      } else {
        _controller.finishLoad(success: false);
      }
      setState(() {
        _isNetWorkError = true;
      });
      if (_tableData != null && mounted) {
        showSnackBar(context, tr("network_error"));
      }
    });
  }

  Future<void> _onLoad() async {
    await _fetchTableData(fetchType: 1);
  }

  Future<void> _onRefresh() async {
    _controller.resetLoadState();
    await _fetchTableData();
  }
}
