import 'package:flutter/material.dart';
import 'package:flutter_easyrefresh/easy_refresh.dart';
import 'package:muslim/common/api/error_network_handle.dart';
import 'package:muslim/common/core/storage.dart';
import 'package:muslim/common/tools/hexColor.dart';
import 'package:muslim/common/tools/scrollOffset.dart';
import 'package:muslim/common/tools/snackBar.dart';
import 'package:muslim/global.dart';
import 'package:muslim/modals/recommends.dart';
import 'package:muslim/pages/home/homeListViewCell.dart';
import 'package:muslim/pages/widgets/backButton.dart';
import 'package:muslim/pages/widgets/base_page.dart';
import 'package:muslim/pages/widgets/base_widget.dart';
import 'package:muslim/pages/widgets/empty.dart';
import 'package:muslim/pages/widgets/muslim_title.dart';
import 'package:easy_localization/easy_localization.dart';

class DetailFeedsPage extends StatefulWidget {
  DetailFeedsPage({Key key, this.merchantId, this.previewUrl})
      : super(key: key);
  final String merchantId;
  String previewUrl;

  @override
  _DetailFeedsState createState() => _DetailFeedsState();
}

class _DetailFeedsState extends State<DetailFeedsPage> {
  List<Datum> _tableData;
  EasyRefreshController _controller = EasyRefreshController();
  ScrollController _scrollController = ScrollController();
  bool _isNetWorkError = true;
  GlobalKey _scrollKey = GlobalKey();

  List<GlobalKey> _itemKeys = [];

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MuslimBasePage(
      appBar: AppBar(
          centerTitle: true,
          title: MuslimTitle(title: tr("merchant_feeds_title")),
          leading: MuslimBackButton()),
      key: _scrollKey,
      header: baseHeader(),
      footer: baseFooter(),
      firstRefresh: true,
      firstRefreshWidget: true,
      onLoad: _onLoad,
      onRefresh: _onRefresh,
      controller: _controller,
      scrollController: _scrollController,
      emptyWidget: _isNetWorkError && _tableData == null
          ? EmptyPage(noDatasTip: tr("no_network_tip"))
          : _tableData != null && _tableData.length == 0
              ? EmptyPage(noDatasTip: tr("no_activites_content"))
              : null,
      slivers: [
        SliverList(
            delegate: SliverChildBuilderDelegate(
          (context, index) {
            GlobalKey itemKey = _itemKeys[index];
            return HomeListViewCellWidget(
              key: itemKey,
              detail: _tableData[index],
              previewUrl: widget.previewUrl,
              tapClick: itemClick,
            );
          },
          childCount: _tableData == null ? 0 : _tableData.length,
        ))
      ],
    );
  }

  itemClick(GlobalKey itemKey) {
    double offset = _scrollController.offset;
    Offset currentPosition = getPosition(itemKey);
    Offset scrollPosition = getPosition(_scrollKey);
    if (currentPosition.dy < scrollPosition.dy) {
      offset -= scrollPosition.dy - currentPosition.dy;
      _scrollController.animateTo(offset,
          duration: Duration(milliseconds: 100), curve: Curves.easeIn);
    }
  }

  Future<void> _fetchTableData({int fetchType = 0}) async {
    setState(() {
      _isNetWorkError = false;
    });
    var params = {
      "limit": "$LIMIT",
      "cursor": fetchType == 0
          ? ""
          : _tableData == null || _tableData.length == 0
              ? ""
              : _tableData.last.id
    };

    Global.normalApi
        .fetchMerchantFeedWithId(params, widget.merchantId)
        .then((value) {
      print("fetchMerchantFeedWithId:: value $value");
      if (value != null && value.errorCode == SUCCESS) {
        if (fetchType == 0) {
          setState(() {
            _tableData = value.data;
          });
          _controller.finishRefresh(success: true);
          if (_controller.finishLoadCallBack != null)
            _controller.finishLoadCallBack(noMore: value.data.length < LIMIT);
        } else {
          var data = _tableData;
          data.addAll(value.data);
          setState(() {
            _tableData = data;
          });
          _controller.finishLoad(success: true, noMore: value.data.length == 0);
        }
        List<GlobalKey> list = [];
        for (var i = 0; i < _tableData.length; i++) {
          list.add(GlobalKey());
        }
        setState(() {
          _itemKeys = list;
        });
      } else {
        if (fetchType == 0) {
          _controller.finishRefresh(success: false);
        } else {
          _controller.finishLoad(success: false);
        }
        if (mounted) handleNetworkError(value, context);
      }
    }).catchError((error) {
      print("error :::: _fetchTableData");
      if (fetchType == 0) {
        _controller.finishRefresh(success: false);
      } else {
        _controller.finishLoad(success: false);
      }
      setState(() {
        _isNetWorkError = true;
      });
      if (_tableData != null && mounted) {
        showSnackBar(context, tr("network_error"));
      }
    });
  }

  Future<void> _onLoad() async {
    await _fetchTableData(fetchType: 1);
  }

  Future<void> _onRefresh() async {
    _controller.resetLoadState();
    await _fetchTableData();
  }
}
